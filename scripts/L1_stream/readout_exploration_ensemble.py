"""
Script to read out the exploration ensemble and handle/filter out some of the bad stuff
"""
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"


def print_unique_values_parameter_cols(df, parameter_cols):
    """
    Function to print the unique values of the parameter columns
    TODO: add the printing of the grouped probability for this
    """

    for col in parameter_cols:
        sorted_unique_values = sorted(df[col].unique())

        aggregate_dict = {col: "first"}
        aggregate_dict["probability"] = "sum"

        grouped_df = df.groupby(col, as_index=False).aggregate(aggregate_dict)

        print("Unique values for column {}:".format(col))
        print(grouped_df.to_string(index=False))


def plot_unique_values_parameters_cols(df, parameter_cols):
    """
    Function to generate a plot based on the unique values of these columns
    """

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=4, ncols=1)

    for col_i, col in enumerate(parameter_cols):
        ax = fig.add_subplot(gs[col_i, :])

        aggregate_dict = {col: "first"}
        aggregate_dict["probability"] = "sum"

        grouped_df = df.groupby(col, as_index=False).aggregate(aggregate_dict)
        values = grouped_df.to_numpy()

        ax.plot(values[:, 0], values[:, 1], "bo")
        ax.set_title(col)
        ax.set_yscale("log")

    plt.show()


def readout_and_filter_exploration_ensemble(filename):
    """
    Function to read out and filter the exploration ensemble
    """

    df = pd.read_csv(filename, sep="\s+", header=0)

    # Rename columns
    df = df.rename(columns={"exploration_velocity_dispersion": "weight_type"})
    parameter_cols = [
        col for col in df.columns if not col in ["probability", "weight_type"]
    ]

    # Filter probability
    df = df[df.probability > 0]
    print_unique_values_parameter_cols(df, parameter_cols)

    # Now perform extra filters custom designed for the outliers:
    df = df[df.log10q_acc_don > -200]
    df = df[df.log10normalised_thermal_velocity > -200]
    df = df[df.frac_sync_donor > -200]
    print_unique_values_parameter_cols(df, parameter_cols)

    # After the print we can generate some plots
    plot_unique_values_parameters_cols(df, parameter_cols)


if __name__ == "__main__":
    filename = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"),
        "RLOF/TEST_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.001/subproject_ballistic_ensemble_data.csv",
    )
    filename = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"),
        "RLOF/LOW_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.002/subproject_ballistic_ensemble_data.csv",
    )

    readout_and_filter_exploration_ensemble(filename)
