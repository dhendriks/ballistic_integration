"""
Script containing the function to generate the grid of trajectory settings
"""

import copy
import itertools

from ballistic_integration_code.settings import standard_system_dict


def grid_generator_stage_1(settings):
    """
    Generator function to generate the trajectory settings for the ballistic stream integrator
    """

    # Set the parameter ranges
    f_range = settings["f_range"]
    q_range = settings["q_range"]

    # Loop over parameters and yield the settings
    for parameters in itertools.product(f_range, q_range):
        # Unpack
        synchronicity_factor = parameters[0]
        massratio_accretor_donor = parameters[1]

        # Set system dict
        settings["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": synchronicity_factor,
            "mass_accretor": massratio_accretor_donor
            * standard_system_dict["mass_donor"],
        }

        # Set grid point
        settings["grid_point"] = {
            "massratio_accretor_donor": massratio_accretor_donor,
            "synchronicity_factor": synchronicity_factor,
        }

        # Copy the settings
        returned_settings = copy.deepcopy(settings)

        yield returned_settings
