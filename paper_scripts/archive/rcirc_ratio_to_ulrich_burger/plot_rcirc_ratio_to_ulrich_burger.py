"""
Function to plot the rcirc as a ratio to that of the urlich & burger formula
TODO: add fucntion to plot ratio to ulrich & burger
TODO: add function to plot ratio to something that includes something that takes into account the non-synch
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integrator.functions.plot_functions import (
    fkr,
    return_lubow_shu_table_2_data,
    ulrich_kolb,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

custom_mpl_settings.load_mpl_rc()


fontsize_lines = 14


def handle_query(df, query_string):
    """
    Function  to handle query
    """

    if query_string:
        df = df.query(query_string)

    return df


def plot_grid(
    result_file,
    query=None,
    add_ulrich_plot=True,
    add_fkr_plot=True,
    add_lubow_plot=True,
    plot_settings={},
):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    # Read out interpolation textfile
    result_df = pd.read_csv(result_file, sep="\s+", header=0)
    result_df = result_df[result_df.exit_code == 7]
    # print(result_df.)
    result_df = handle_query(result_df, query)

    #
    unique_synchronicity_factors = sorted(result_df["synchronicity_factor"].unique())

    #
    mass_accretor_array = 10 ** np.linspace(-2, 2, 100)
    mass_donor_array = np.ones(mass_accretor_array.shape)

    # Calculte mass ratio
    q_acc_don = mass_accretor_array / mass_donor_array
    result_df["rcirc"] = result_df["rmin"] * 1.7

    #
    fig, axes = plt.subplots(figsize=(16, 16))

    # # Plot the results for different prescriptions in literature
    # if add_ulrich_plot:
    #     rcirc_ulrich = ulrich_kolb(mass_donor_array, mass_accretor_array)
    #     axes.plot(q_acc_don, rcirc_ulrich, "--", label="Ulrich & Kolb 1976 fit")
    # if add_fkr_plot:
    #     rcirc_fkr = fkr(mass_donor_array, mass_accretor_array)
    #     axes.plot(q_acc_don, rcirc_fkr, "--", label="Frank, King & Raine 2002 formula")
    # if add_lubow_plot:
    #     lubow_shu_datapoints = return_lubow_shu_table_2_data()
    #     axes.plot(
    #         lubow_shu_datapoints["q_acc_don"],
    #         lubow_shu_datapoints["r_circ"],
    #         "dr",
    #         zorder=200,
    #         label="Lubow & Shu 1975 data",
    #     )

    # Plot results for each synchronicity factor
    cmap = plt.cm.get_cmap("viridis", len(unique_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    for i, synchronicity_factor in enumerate(unique_synchronicity_factors):
        filtered_df = result_df[result_df.synchronicity_factor == synchronicity_factor]

        current_mass_accretor_values = filtered_df[
            "massratio_accretor_donor"
        ].to_numpy()
        current_mass_donor_values = np.ones(current_mass_accretor_values.shape)

        rcirc_ulrich = ulrich_kolb(
            current_mass_donor_values, current_mass_accretor_values
        )
        ratio_to_ulrich = filtered_df["rcirc"].to_numpy() / rcirc_ulrich

        rightmost_q_val = filtered_df["massratio_accretor_donor"].tolist()[-1]
        rightmost_y_val = ratio_to_ulrich.tolist()[-1]

        axes.plot(
            filtered_df["massratio_accretor_donor"],
            ratio_to_ulrich,
            color=cmaplist[i],
            label="f = {:1.1f}".format(synchronicity_factor),
            alpha=0.8,
        )

        axes.text(
            rightmost_q_val * 1.1,
            rightmost_y_val,
            "f = {:1.1f}".format(synchronicity_factor),
            fontsize=fontsize_lines,
        )

    ######
    # Make up
    axes.legend()
    axes.set_xscale("log")
    axes.set_yscale("log")
    axes.set_ylabel(
        r"Ratio $\it{R}_{\mathrm{circ\ new}}/\it{R}_{\mathrm{circ\ Ulrich\ &\ Kolb}}$ "
    )
    axes.set_xlabel(
        r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{accretor}}/\it{M}_{\mathrm{donor}}$"
    )
    current_xlim = axes.get_xlim()
    axes.set_xlim([current_xlim[0], current_xlim[1] * 2])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_disks/paper_tex/figures/ballistic"
    )
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # plot_dir = "plots/"

    ##
    # Stage 1 results
    result_dir = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_1",
    )
    result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    #
    basename = "comparison_methods_rcirc_stage_1_minimal.pdf"

    #
    plot_grid(
        result_file,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # ##
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2",
    # )

    # #
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "normalized_thermal_velocity_dispersion==-3.5"
    # basename = "comparison_methods_rcirc_stage_2_low_temp.pdf"

    # #
    # plot_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "normalized_thermal_velocity_dispersion==-0.5"
    # basename = "comparison_methods_rcirc_stage_2_high_temp.pdf"

    # #
    # plot_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=False,
    #     add_lubow_plot=False,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # Low temp result
    # query_string = "normalized_thermal_velocity_dispersion==-3.5"
    # basename = "comparison_methods_rcirc_stage_2_low_temp.pdf"

    # #
    # plot_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )
