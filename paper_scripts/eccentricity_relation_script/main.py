"""
Function to create a grid of scalings between vectors and angles between vectors and calculate several properties like rcirc/rmin and |ecc|

The scenario is s.t. the system is a single star system with the star centred on the origin of the reference frame

TODO: change y-ticklabels to pi
"""


import itertools
import os
from functools import partial
from multiprocessing.pool import Pool

import astropy.constants as const
import astropy.units as u
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import cm, colors

from ballistic_integration_code.paper_scripts.eccentricity_relation_script.functions import (
    new_vector,
    rotate_vector,
    scale_vector,
    set_magnitude_vector,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


dimensionless_unit = u.m / u.m


def calculate_quantities(parameters, settings):
    """
    Function to calculate the quantities
    """

    system_dict = settings["system_dict"]
    mu = const.G * system_dict["mass_accretor"] * u.Msun

    #
    result_dict = {}

    # unpack
    scale = parameters[0]
    result_dict["scale"] = scale

    angle = parameters[1]
    result_dict["angle"] = angle

    ####
    # Set initial position vector
    position_vector = new_vector()

    # scale to 1 au
    distance_particle = 1 * u.AU
    position_vector = set_magnitude_vector(position_vector, distance_particle)

    ####
    # Set initial velocity vector
    velocity_vector = new_vector()

    # scale to keplerian circular velocity at distance position vector
    keplerian_circular_velocity = np.sqrt((mu) / (distance_particle)).si
    velocity_vector = set_magnitude_vector(velocity_vector, keplerian_circular_velocity)

    ####
    # record the 'standard' ratio of the magnitude of the vectors
    standard_ratio_magnitude_position_velocity = np.linalg.norm(
        position_vector
    ) / np.linalg.norm(velocity_vector)

    ####
    # Modify the velocity vector

    # calculate rotation of vector
    velocity_vector = rotate_vector(velocity_vector, angle)

    # scale the vectors
    velocity_vector = scale_vector(velocity_vector, scale)

    normalised_velocity = np.linalg.norm(velocity_vector) / keplerian_circular_velocity
    result_dict["normalised_velocity"] = normalised_velocity.value
    # assert normalised_velocity.unit == dimensionless_unit

    # Calculate ratio magnitudes position and velocity vector
    ratio_magnitudes_position_velocity = np.linalg.norm(
        position_vector
    ) / np.linalg.norm(velocity_vector)
    result_dict[
        "ratio_magnitudes_position_velocity"
    ] = ratio_magnitudes_position_velocity

    normalized_ratio_magnitudes_position_velocity = (
        ratio_magnitudes_position_velocity / standard_ratio_magnitude_position_velocity
    )
    result_dict[
        "normalized_ratio_magnitudes_position_velocity"
    ] = normalized_ratio_magnitudes_position_velocity.value

    # assert normalized_ratio_magnitudes_position_velocity.unit == dimensionless_unit

    ############
    # calculate rcirc/rmin
    specific_angmom_rmin_vector = np.cross(position_vector, velocity_vector)
    rcirc = (specific_angmom_rmin_vector**2) / (mu)

    rcirc_vector = new_vector()
    rcirc_vector = set_magnitude_vector(velocity_vector, np.linalg.norm(rcirc))

    ratio_rcirc_rmin = (
        np.linalg.norm(rcirc_vector) / np.linalg.norm(position_vector)
    ).si
    result_dict["ratio_rcirc_rmin"] = ratio_rcirc_rmin.value

    # assert ratio_rcirc_rmin.unit == dimensionless_unit

    ############
    # calculate eccentricity_vector
    eccentricity_vector = (
        (np.cross(velocity_vector, specific_angmom_rmin_vector) / mu)
        - (position_vector / np.linalg.norm(position_vector))
    ).si
    result_dict["eccentricity_vector"] = eccentricity_vector

    eccentricity_magnitude = np.linalg.norm(eccentricity_vector)
    result_dict["eccentricity_magnitude"] = eccentricity_magnitude.value

    # assert eccentricity_magnitude.unit == dimensionless_unit

    return result_dict


def plot_eccentricity_distribution_for_position_and_velocity_vectors(
    settings, plot_settings
):
    """
    Function to plot three panels for the eccentricity magnitude as a function of position and velocity vectors
    """

    # readout config
    scale_range = settings["scale_range"]
    angle_range = settings["angle_range"]
    system_dict = settings["system_dict"]
    mu = const.G * system_dict["mass_accretor"] * u.Msun

    shape = (len(scale_range), len(angle_range))
    num_calcs = len(scale_range) * len(angle_range)

    ####
    # Multiprocess grid of scales and angles
    arg_arr = itertools.product(scale_range, angle_range)
    with Pool(processes=os.cpu_count()) as pool:
        results = pool.map(partial(calculate_quantities, settings=settings), arg_arr)

    # load results into dataframe
    df = pd.DataFrame.from_records(results)

    ##################
    # plotting

    # Set up figure
    fig = plt.figure(figsize=(8, 18))
    fig.subplots_adjust(hspace=0.1)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=3, ncols=6)

    ax_ratio = fig.add_subplot(gs[0, :-1])
    ax_ratio_cb = fig.add_subplot(gs[0, -1])

    ax_ecc = fig.add_subplot(gs[1, :-1])
    ax_ecc_cb = fig.add_subplot(gs[1, -1])

    ax_scatter = fig.add_subplot(gs[2, :-1])

    x_values_reshaped = df["normalised_velocity"].to_numpy().reshape(shape)
    y_values_reshaped = df["angle"].to_numpy().reshape(shape)

    ###################
    # plot the rcirc/rmin values
    cmap_ratio = cm.coolwarm
    norm_ratio = colors.LogNorm()

    ratio_values_reshaped = df["ratio_rcirc_rmin"].to_numpy().reshape(shape)

    # Plot the data
    _ = ax_ratio.pcolormesh(
        x_values_reshaped,
        y_values_reshaped,
        ratio_values_reshaped,
        cmap=cmap_ratio,
        norm=norm_ratio,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    ax_ratio.set_xscale("log")

    ticks = np.arange(0, 2 * np.pi + 0.01, np.pi / 2)
    labels = ["$0$", r"$\pi/2$", r"$\pi$", r"$3\pi/2$", r"$2\pi$"]

    ax_ratio.set_yticks(ticks)
    ax_ratio.set_yticklabels(labels)

    ylabel = (
        r"$\angle \overrightarrow{v}_{\mathrm{min}} \overrightarrow{r}_{\mathrm{min}}$"
    )
    xlabel = r"$|\overrightarrow{v}_{\mathrm{min}}| \qquad \left[|\overrightarrow{v}_{\mathrm{min,\,circ}}|\right]$"

    ax_ratio.set_ylabel(ylabel)
    ax_ecc.set_xlabel(xlabel, labelpad=80)

    ##########
    # plot color bar
    cbar = matplotlib.colorbar.ColorbarBase(
        ax_ratio_cb, cmap=cmap_ratio, norm=norm_ratio
    )
    zlabel = r"$|r_{\mathrm{circ}}| / |r_{\mathrm{min}}|$"
    cbar.ax.set_ylabel(zlabel)

    ###################
    # plot the eccentricity values
    cmap_ecc = cm.coolwarm
    norm_ecc = colors.LogNorm()

    eccentricity_values_reshaped = (
        df["eccentricity_magnitude"].to_numpy().reshape(shape)
    )

    # Plot the data
    _ = ax_ecc.pcolormesh(
        x_values_reshaped,
        y_values_reshaped,
        eccentricity_values_reshaped,
        cmap=cmap_ecc,
        norm=norm_ecc,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    ax_ecc.set_xscale("log")

    ax_ecc.set_yticks(ticks)
    ax_ecc.set_yticklabels(labels)

    ##########
    # plot color bar
    cbar = matplotlib.colorbar.ColorbarBase(ax_ecc_cb, cmap=cmap_ecc, norm=norm_ecc)
    zlabel = r"$e$"
    cbar.ax.set_ylabel(zlabel)

    ###################
    # Plot scatter

    ax_scatter.scatter(df["ratio_rcirc_rmin"], df["eccentricity_magnitude"], alpha=0.5)

    xlabel = r"$\it{r}_{\mathrm{circ}} / \it{r}_{\mathrm{min}}$"
    ylabel = r"$\it{e}$"

    ax_scatter.set_xscale("log")
    ax_scatter.set_yscale("log")

    ax_scatter.set_xlabel(xlabel)
    ax_scatter.set_ylabel(ylabel)

    ############
    # plot makeup

    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    ##############################################################
    # Configuration
    settings = {
        "scale_range": 10 ** np.arange(0, 2, 0.05),
        # "angle_range": np.arange(0, 2 * np.pi, 0.15),
        "angle_range": np.arange(0, 1 * np.pi, 0.05),
        "system_dict": {
            **standard_system_dict,
        },
    }

    ##############################################################
    # Paper ready plots
    plot_settings = {"show_plot": True}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    # Plot
    basename = "server_comparison_methods_rcirc_stage_1_minimal.pdf"
    plot_eccentricity_distribution_for_position_and_velocity_vectors(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )
