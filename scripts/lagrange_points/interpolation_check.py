import pandas as pd

filename = "results/test/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"


df = pd.read_csv(filename, sep="\s+", skiprows=26)


# import numpy as np

from scipy import interpolate

# x = np.arange(-5.01, 5.01, 0.25)

# y = np.arange(-5.01, 5.01, 0.25)

# xx, yy = np.meshgrid(x, y)

# z = np.sin(xx**2+yy**2)

f = interpolate.interp2d(
    df["A_factor"].to_numpy(),
    df["massratio_accretor_donor"].to_numpy(),
    df[["ratio_RL_RLegg", "roche_lobe_radius"]].to_numpy().T,
    kind="linear",
)
