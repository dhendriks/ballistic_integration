"""
Functions for the plotting of the exploration grid information
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.settings import (
    xlabel_dict,
)


def create_figure_for_alpha_vs_synchronicity(orientation, x_parameter, y_parameter):
    """
    Function to create the figure canvas for the alpha vs synchronicity plot
    """

    if orientation == "vertical":
        # Set up figure
        fig = plt.figure(figsize=(20, 24))

        # Set up gridspec
        gs = fig.add_gridspec(nrows=2, ncols=9)

        # define axes
        ax_mass = fig.add_subplot(gs[0, :-2])
        ax_time = fig.add_subplot(gs[1, :-2])
        ax_cb = fig.add_subplot(gs[:, -2:])

        # Invisible axis:
        ax_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        ax_invisible.set_xticks([])
        ax_invisible.set_yticks([])

        ax_invisible.set_ylabel(
            xlabel_dict[y_parameter],
            labelpad=80,
        )
        ax_time.set_xlabel(xlabel_dict[x_parameter])
        ax_mass.set_xticklabels([])

    elif orientation == "horizontal":
        # Set up figure
        fig = plt.figure(figsize=(20, 10))

        # Set up gridspec
        colsize_per_panel = 6
        gs = fig.add_gridspec(nrows=1, ncols=2 * colsize_per_panel + 2)

        # define axes
        ax_mass = fig.add_subplot(gs[0, 0:colsize_per_panel])
        ax_time = fig.add_subplot(gs[0, colsize_per_panel : 2 * colsize_per_panel])
        ax_cb = fig.add_subplot(gs[:, -2:])

        # Invisible axis:
        ax_invisible = fig.add_subplot(gs[:, :-2], frame_on=False)
        ax_invisible.set_xticks([])
        ax_invisible.set_yticks([])

        ax_mass.set_ylabel(
            xlabel_dict[y_parameter],
        )
        ax_invisible.set_xlabel(
            xlabel_dict[x_parameter],
            labelpad=60,
        )
        ax_time.set_yticklabels([])

    else:
        raise ValueError("Please choose a valid orientation ('horizontal', 'vertical')")

    axes_dict = {
        "mass": ax_mass,
        "time": ax_time,
        "invisible": ax_invisible,
        "cb": ax_cb,
    }

    return fig, axes_dict


def calculate_bins(array):
    """
    function to calculate bins
    """

    # Ensure a unique and sorted array
    sorted_unique_array = np.array(sorted(np.unique(array)))

    #
    min_val = sorted_unique_array.min()
    max_val = sorted_unique_array.max()

    diffs = np.diff(sorted_unique_array)

    # Get smallest diff
    min_diff = diffs.min()

    # construct bins
    bins = np.arange(min_val - min_diff / 2, max_val + min_diff / 2, min_diff)

    return bins


def calculate_bins_and_bincenters(array):
    """
    Function to calculate the bins and the bincenters
    """

    bins = calculate_bins(array)

    bincenters = (bins[1:] + bins[:-1]) / 2

    return bins, bincenters


def min_of_list_of_arrays(list_of_arrays):
    """
    Function to find the smallest element in all the numpy arrays contained in the list
    """

    smallest = np.min([np.min(arr) for arr in list_of_arrays])
    return smallest


def max_of_list_of_arrays(list_of_arrays):
    """
    Function to find the largest element in all the numpy arrays contained in the list
    """

    largest = np.max([np.max(arr) for arr in list_of_arrays])
    return largest


def generate_bins_from_keys(input_array):
    """
    Function to generate the bins that the array of keys represents
    """

    stepsize = np.around(min(input_array[1:] - input_array[:-1]), decimals=10)
    bins = np.arange(
        input_array.min() - 0.5 * stepsize, input_array.max() + 1.5 * stepsize, stepsize
    )

    return bins


def readout_and_filter_exploration_ensemble(filename, extra_query=None):
    """
    Function to read out and filter the exploration ensemble
    """

    # config
    minimum_count = 1
    weight_type_name_dict = {
        "mass": "mass_weighted_probability",
        "time": "time_weighted_probability",
        "count": "unweighted_count",
    }

    # Load the data
    df = pd.read_csv(filename, sep="\s+", header=0)

    if extra_query is not None:
        df = df.query(extra_query)

    # Get the names
    # all_colnames = list(df.columns)

    weight_type_colname = "weight_type"
    probability_colname = "probability"

    gridpoint_colnames = [
        #
        "log10normalised_thermal_velocity",
        "frac_sync_donor",
        "log10q_acc_don",
    ]
    extra_colnames = [
        "log10normalized_L1_stream_area",
        "st_acc",
        "st_don",
        "log10validity_factor_sepinsky_donor",
    ]

    merge_colnames = gridpoint_colnames + extra_colnames

    # Split to different weight types
    mass_df = df[df[weight_type_colname] == "mass"]
    time_df = df[df[weight_type_colname] == "time"]
    count_df = df[df[weight_type_colname] == "unweighted_count"]

    # Delete columns and rename some thing
    del mass_df["weight_type"]
    del time_df["weight_type"]
    del count_df["weight_type"]

    #
    mass_df = mass_df.rename(
        columns={probability_colname: weight_type_name_dict["mass"]}
    )
    time_df = time_df.rename(
        columns={probability_colname: weight_type_name_dict["time"]}
    )
    count_df = count_df.rename(
        columns={probability_colname: weight_type_name_dict["count"]}
    )

    # Merge the dataframes
    combined_df = mass_df.merge(time_df, how="inner", on=merge_colnames)
    combined_df = combined_df.merge(count_df, how="inner", on=merge_colnames)

    #############
    # Filter the combined dataframe

    # has to have an actual probability
    combined_df = combined_df[combined_df.time_weighted_probability > 0]
    combined_df = combined_df[combined_df.mass_weighted_probability > 0]

    # filter on count (1 step is just very little)
    combined_df = combined_df[combined_df.unweighted_count > minimum_count]

    #
    combined_df = combined_df[combined_df.log10normalised_thermal_velocity > -13]
    combined_df = combined_df[combined_df.log10normalised_thermal_velocity > -13]

    ############
    # we calculate the extent of each of the parameters
    parameter_extent_dict = {}
    for parameter in gridpoint_colnames + ["log10validity_factor_sepinsky_donor"]:
        min_val = combined_df[parameter].min()
        max_val = combined_df[parameter].max()

        parameter_extent_dict[parameter] = [min_val, max_val]

    return combined_df, gridpoint_colnames, parameter_extent_dict, weight_type_name_dict
