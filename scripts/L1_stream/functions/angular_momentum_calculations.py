"""
Functions to calculate angular momenta wrt to donors and accretors etc

Function to handle the calculation of the angular momenta with respect to the components and the reference frame

TODO: use system dict to initially scale the position and velocity
TODO: find the box of bounded eccentricities
"""

import astropy.constants as const
import astropy.units as u
import numpy as np
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_accretor,
    calculate_position_donor,
)
from ballistic_integrator.functions.integrator.integration_functions import (
    calc_orbital_angular_frequency,
)


# Functions to handle the multidimensional arguments
def multidim_dot(arr1, arr2):
    """
    Function to do the multidimensional dotproduct
    """

    assert arr1.shape == arr2.shape

    multi_dot = np.zeros(arr1[:, 0].shape)

    # loop over columns
    for i in range(arr1[0, :].shape[0]):
        multi_dot += arr1[:, i] * arr2[:, i]

    return multi_dot


def multidim_cross(arr1, arr2):
    """
    Function to do the multidimensional cross product
    """

    assert arr1.shape == arr2.shape
    assert arr1.shape[1] <= 3

    ###########
    # check if its a 2-d input or 3-d

    # if 2-d I assume its x-y input and z-output.
    if arr1.shape[1] == 2:
        multi_cross = arr1[:, 0] * arr2[:, 1] - arr1[:, 1] * arr2[:, 0]

    # if 3-d I do the full vector
    # elif:
    else:
        raise NotImplementedError("other options not implemented yet")

    return multi_cross


def calculate_circularisation_radius(
    system_dict, reference_frame, position_vector_min, velocity_vector_min
):
    """
    Function to calculate the circularisation radius based on the position
    and velocity at the radius of closest approach in the fame of reference centered on the center of mass

    The steps are as follows:
    - we calculate the angular momentum with respect to the accretor
    - we de-normalise the values (provided in solar units, converted to SI units)
    - calculate the circularisation radius
    - rcirc
    - normalise
    """

    # print("Position vector at rmin: {}".format(position_vector_min))
    # print("Velocity vector at rmin: {}".format(velocity_vector_min))

    ###########
    # Calculate specific angular momentum of stream at radius of closest approach
    specific_angular_momentum = calculate_specific_angular_momenta_with_respect_to(
        to="accretor",
        reference_frame=reference_frame,
        system_dict=system_dict,
        position_vector=position_vector_min,
        velocity_vector=velocity_vector_min,
    )
    # print("Specific angmom stream code units: {}".format(specific_angular_momentum))

    ###########
    # Express in actual units (de-normalise)
    separation = (system_dict["separation"] * u.Rsun).to(u.m)
    orbital_angular_velocity = calc_orbital_angular_frequency(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
        separation=system_dict["separation"],
    )
    mass_accretor = (system_dict["mass_accretor"] * u.Msun).to(u.kg)
    G_SI = const.G

    ###########
    # Calculate Rcirc:
    rcirc = (
        specific_angular_momentum * orbital_angular_velocity * separation**2
    ) ** 2 / (mass_accretor * G_SI)
    rcirc_SI = rcirc.to(u.m)

    # print("Rcirc SI: {}".format(rcirc_SI))

    normalized_rcirc_SI = rcirc_SI / separation
    # print("Rcirc code units: {}".format(normalized_rcirc_SI))

    return normalized_rcirc_SI.value


def calculate_specific_angular_momentum(position_vector, velocity_vector):
    """
    Function to calculate the angular momentum in terms of the natural units of the system
    """

    # calculate cross product
    cross_product = np.cross(position_vector, velocity_vector)

    # calculate inner product
    if position_vector.ndim == 1:
        inner_product = np.dot(position_vector, position_vector)
    else:
        inner_product = multidim_dot(position_vector, position_vector)

    return cross_product + inner_product


def calculate_specific_angular_momenta_with_respect_to(
    to, reference_frame, system_dict, position_vector, velocity_vector
):
    """
    Function to handle calculation of the angular momenta wrt to things.

    currently we only support the center_of_mass reference frame

    the position that is provided should thus be a position with respect to the center of mass

    we can choose to calculate the angular momentum with respect to the donor or the accretor, and based on that we shift the position vector
    """

    ###########
    # Quit when we have the reference frame
    if not reference_frame == "center_of_mass":
        raise ValueError(
            "calling calculate_angular_momenta_with_respect_to without reference_frame=='center_of_mass' is currently not available"
        )

    ###########
    # Handle shift of position wrt objects
    if to == "donor":
        position_donor = calculate_position_donor(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
        )
        shifted_position_vector = position_vector - position_donor
    elif to == "accretor":
        position_accretor = calculate_position_accretor(
            mass_accretor=system_dict["mass_accretor"],
            mass_donor=system_dict["mass_donor"],
            separation=system_dict["separation"],
        )
        shifted_position_vector = position_vector - position_accretor
    elif to == "center_of_mass":
        shifted_position_vector = position_vector
    else:
        raise ValueError(
            "Please choose a correct value for 'to' ('donor', 'accretor', 'center_of_mass')"
        )

    #########
    # Calculate and return
    specific_angular_momentum = calculate_specific_angular_momentum(
        position_vector=shifted_position_vector, velocity_vector=velocity_vector
    )

    return specific_angular_momentum
