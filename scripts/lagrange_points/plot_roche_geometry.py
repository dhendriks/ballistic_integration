"""
Routines to plot the roche geometry and the binary potential as a function of q,f

In this case the potential does take into account the extra forcing
"""

import os

import matplotlib
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)

from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()
matplotlib.rcParams["savefig.dpi"] = 300

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_rochelobe_schematic(system_dict, settings, plot_settings):
    """
    Function to plot the roche lobe schematic
    """

    #
    fig, axes_dict = plot_system(
        system_dict,
        settings=settings,
        frame_of_reference="center_of_mass",
        resultfile=None,
        resultdata=None,
        resultdata_list=None,
        #
        plot_rochelobe_equipotentials=True,
        plot_rochelobe_equipotential_meshgrid=False,
        plot_stars_and_COM=True,
        plot_L_points=True,
        plot_trajectory=False,
        plot_system_information_panel=False,
        plot_system_add_colorbar=False,
        # add_gradients=False,
        # add_velocities_and_accelarations=False
        return_fig=True,
        plot_settings={**plot_settings, "use_acc_don_names": False},
    )

    # Add labels
    axes_dict["ax"].set_xlabel(
        r"$\it{x}/a$", fontsize=plot_settings.get("axis_label_fontsize")
    )
    axes_dict["ax"].set_ylabel(
        r"$\it{y}/a$", fontsize=plot_settings.get("axis_label_fontsize")
    )

    # Remove text that falls out of frame
    fig, axes_dict["ax"] = delete_out_of_frame_text(fig, axes_dict["ax"])

    axes_dict["ax"].set_xlim(plot_settings["plot_bounds_x"])
    axes_dict["ax"].set_ylim(plot_settings["plot_bounds_y"])

    # force the same axes ratio
    axes_dict["ax"].set_aspect("equal")

    # Remove text that falls out of frame
    fig, axes_dict["ax"] = delete_out_of_frame_text(fig, axes_dict["ax"])

    # fig.tight_layout()
    fig.subplots_adjust(wspace=-0.4)

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


#
plot_settings = {
    "star_text_color": "white",
    "show_plot": False,
    "verbosity": 1,
    "plot_bounds_x": [-0.75, 0.75],
    "plot_bounds_y": [-0.75, 0.75],
    "plot_resolution": 1000,
}

# Output dir
output_dir = os.path.join(this_file_dir, "plots")

###########
# basename
basename = "trajectory_classification_overview.pdf"

#
system_dict = {
    **standard_system_dict,
    "mass_donor": 1,
    # "mass_accretor": 0.1,
    # "mass_accretor": 50,
    # "mass_accretor": 0.01,
    "synchronicity_factor": 1.212,
}

grid_point = {
    "massratio_accretor_donor": system_dict["mass_accretor"]
    / system_dict["mass_donor"],
    "synchronicity_factor": system_dict["synchronicity_factor"],
    "log10normalized_thermal_velocity_dispersion": -0.5,
}

settings = {
    **stage_3_settings,
    "system_dict": system_dict,
    "grid_point": grid_point,
    "num_cores": 4,
    "jacobi_error_tol": 1e-2,
    "dt": 0.01,
    "num_samples_area_sampling": 12,
    "return_trajectory_set": True,
    "direction_velocity_asynchronous_offset": +1,
    "include_asynchronous_donor_effects": True,
    "include_asynchronicity_donor_during_integration": True,
}

# q_values = [0.1, 0.1, 10, 10]
# f_values = [0.1, 2, 0.1, 2]

# #
# for q, f in zip(q_values, f_values):
#     system_dict["mass_accretor"] = q
#     system_dict["synchronicity_factor"] = f

#     plot_rochelobe_schematic(
#         system_dict=system_dict,
#         settings=settings,
#         plot_settings={
#             **plot_settings,
#             "output_name": os.path.join(output_dir, "q_{}_f_{}.pdf".format(q, f)),
#         },
#     )

q_values = [0.1, 0.1, 0.1, 0.1, 0.1]
f_values = [0.1, 0.5, 1.0, 1.5, 2]

#
for q, f in zip(q_values, f_values):
    system_dict["mass_accretor"] = q
    system_dict["synchronicity_factor"] = f

    plot_rochelobe_schematic(
        system_dict=system_dict,
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, "new_q_{}_f_{}.pdf".format(q, f)),
        },
    )
