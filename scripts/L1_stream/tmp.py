from enum import Enum


class EXIT_CODES(Enum):
    """
    Enum class to containing simulaton status
    """

    PREEMPTIVE = -1
    NORMAL_FINISH = 0
    INTEGRATOR_TERMINATION = 1
    SELF_INTERSECTION = 2
    FALLBACK_INTO_ROCHELOBE = 3
    JACOBI_ERROR = 4
    WRONG_DIRECTION = 5
    TIMESTEP_TOO_SMALL = 6
    CUSTOM_ERROR = 7


exit_codes = EXIT_CODES
print(exit_codes.INTEGRATOR_TERMINATION.value)
