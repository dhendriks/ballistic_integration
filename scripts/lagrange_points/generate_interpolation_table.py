"""
Function to create an interpolation table for the lagrange point calculations that binary-c could use.
"""

import copy

import pandas as pd
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)

from ballistic_integration_code.scripts.L1_stream.functions.data_header_functions import (
    data_header_write_data_array,
    data_header_write_input_defines,
    data_header_write_meta_data,
    data_header_write_output_defines,
    get_index_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_output_datafile_functions import (
    prepare_df,
)


def generate_interpolation_table(
    input_textfile,
    output_textfile,
    settings,
    parameter_dict,
):
    """
    Function to make a pandas interpolation table from the lagrange and roche lobe volume information
    """

    settings = copy.copy(settings)
    settings["specific_angular_momentum_stream_interpolation_enabled"] = False

    #
    header = "RLOF_HENDRIKS2023_ROCHE_LOBE_INTERPOLATION"
    stage_number = 3

    #
    length_decimals = settings["length_decimals_dataheader"]
    input_parameter_list = copy.copy(
        settings["input_parameter_list_data_header_rochelobe_volume"]
    )
    output_parameter_list = copy.copy(
        settings["output_parameter_list_data_header_rochelobe_volume"]
    )

    # prepare df
    filtered_df, output_parameter_list = prepare_df(
        settings=settings,
        input_interpolation_textfile=input_textfile,
        input_parameter_list=input_parameter_list,
        output_parameter_list=output_parameter_list,
    )

    # Get the number of lines we will output to the table and some others
    num_lines = len(filtered_df.index)
    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    # Get indices for input parameters
    input_index_dict = get_index_dict(
        df=filtered_df, columns=input_parameter_list, offset=0
    )
    output_index_dict = get_index_dict(
        df=filtered_df, columns=output_parameter_list, offset=-num_input_parameters
    )

    # Get git info
    git_info = get_git_info_and_time()

    # Write to file
    with open(output_textfile, "w") as f:
        # Write top-level explanation
        f.write(
            """
// Roche-lobe volume interpolation dataset.
// This dataset provides donor Roche-lobe radii as a function of the A-factor (sepinsky 2007)
// and the mass ratio (q_acc=m_acc/m_don)\n\n
"""
        )

        # Write git revision information
        f.write(
            """
// Generated on: {} with
// \tgit repository: {}
// \tbranch: {}
// \tcommit: {}\n\n
""".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        ######
        # Write data header metadata (parameter descriptions, stage number, number of data lines etc)
        data_header_write_meta_data(
            settings=settings,
            input_parameter_list=input_parameter_list,
            output_parameter_list=output_parameter_list,
            parameter_dict=parameter_dict,
            num_lines=num_lines,
            num_input_parameters=num_input_parameters,
            num_output_parameters=num_output_parameters,
            filehandle=f,
            definition_basename=header,
            write_defines=False,
        )

        ########
        # Write data to output file
        filtered_df.to_csv(
            f,
            index=False,
            sep=settings["csv_output_separator"],
            header=True,
            float_format=lambda x: round(x, length_decimals),
        )

        # # Write to output file
        # if write_to_file:
        #     filtered_df.to_csv(
        #         f,
        #         index=False,
        #         sep=settings["csv_output_separator"],
        #         header=True,
        #         float_format=lambda x: round(x, length_decimals),
        #     )
        # else:
        #     return df, filtered_df


if __name__ == "__main__":

    generate_interpolation_table(
        lagrange_point_data_filename="/home/david/Dropbox/Academic/PHD/projects/ballistic_integration_code/scripts/lagrange_points/results/lagrange_point_data.txt",
        write_to_file=True,
        output_filename="/home/david/Dropbox/Academic/PHD/projects/ballistic_integration_code/scripts/lagrange_points/results/interpolation_table.csv",
    )
