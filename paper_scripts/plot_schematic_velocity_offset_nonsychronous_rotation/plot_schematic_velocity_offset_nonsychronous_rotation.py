"""
Function to plot the non-syncronous rotation velocity offset.
"""

import itertools
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import cm, colors

from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_non_synchronous import (
    calculate_velocity_offset_L1_non_synchronous,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_schematic_velocity_offset_non_synchronous_rotation(
    opposite_asynchronous_offset_direction, plot_settings
):
    """
    Function to plot the non-synchronous rotation offset in terms of the velocity units.
    """

    #
    settings = {
        "non_synch_velocity_offset_distance_to": "donor",
        "include_asynchronicity_donor_for_lagrange_points": True,
        "direction_velocity_asynchronous_offset": -1
        if not opposite_asynchronous_offset_direction
        else 1,
        "frame_of_reference": "center_of_mass",
    }

    stepsize_q = plot_settings["stepsize_q"]
    stepsize_f = plot_settings["stepsize_f"]
    relative_to_orbit = plot_settings["relative_to_orbit"]
    relative_to_orbit_value = -1 if relative_to_orbit else 0

    xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
    ylabel = r"$f_{\mathrm{sync}}$"

    cmap = cm.coolwarm
    color_contour = "black"
    alpha_contour = 0.25

    contours = np.array([0.05, 0.1, 0.25, 0.5, 0.75])
    linestyles = linestyle_list[: len(contours)]

    all_contours = np.concatenate((-contours[::-1], contours))
    all_linestyles = linestyles[::-1] + linestyles
    all_contours += relative_to_orbit_value

    #
    q_range = 10 ** np.arange(-2, 2 + stepsize_q, stepsize_q)
    f_range = np.arange(0 + stepsize_f, 2 + stepsize_f, stepsize_f)

    result_list = []

    # loop over values
    for parameters in itertools.product(
        f_range,
        q_range,
    ):

        # Unpack
        synchronicity_factor = parameters[0]
        massratio_accretor_donor = parameters[1]

        # Set system dict
        settings["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": synchronicity_factor,
            "mass_accretor": massratio_accretor_donor
            * standard_system_dict["mass_donor"],
        }

        # Set grid point
        settings["grid_point"] = {
            "massratio_accretor_donor": massratio_accretor_donor,
            "synchronicity_factor": synchronicity_factor,
        }

        # calculate the offset
        velocity_offset_L1_non_synchronous = (
            calculate_velocity_offset_L1_non_synchronous(
                settings=settings,
                system_dict=settings["system_dict"],
                frame_of_reference=settings["frame_of_reference"],
            )
        )

        # print(velocity_offset_L1_non_synchronous)
        result_list.append(
            [
                synchronicity_factor,
                massratio_accretor_donor,
                velocity_offset_L1_non_synchronous[-1],
            ]
        )

    # Make sure things are sorted
    x_parameter = "massratio"
    y_parameter = "f_sync"
    z_parameter = "v_offset_y"

    # load into df
    df = pd.DataFrame(result_list, columns=[y_parameter, x_parameter, z_parameter])

    # Sort values
    df = df.sort_values(by=[y_parameter, x_parameter])

    ####
    # Handle reshaping
    unique_x = df[x_parameter].unique()
    unique_y = df[y_parameter].unique()

    num_unique_x = len(unique_x)
    num_unique_y = len(unique_y)

    x_values_reshaped = df[x_parameter].to_numpy().reshape((num_unique_y, num_unique_x))
    y_values_reshaped = df[y_parameter].to_numpy().reshape((num_unique_y, num_unique_x))
    z_values_reshaped = df[z_parameter].to_numpy().reshape((num_unique_y, num_unique_x))

    ##########
    # Plot data

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax_plot = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    norm = colors.CenteredNorm()

    # Plot the data
    _ = ax_plot.pcolormesh(
        x_values_reshaped,
        y_values_reshaped,
        z_values_reshaped + relative_to_orbit_value,
        cmap=cmap,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # Plot the contour
    _ = ax_plot.contour(
        unique_x,
        unique_y,
        z_values_reshaped + relative_to_orbit_value,
        levels=all_contours,
        linestyles=all_linestyles,
        colors=color_contour,
        alpha=alpha_contour,
    )

    ##########
    # plot color bar
    cbar = mpl.colorbar.ColorbarBase(ax_cb, cmap=cmap, norm=norm)
    cbar.ax.set_ylabel(r"$v_{\mathrm{offset\ non-synchronous\ rotation,\ y}}$")

    # Plot contourlevels
    for contour_i, contour in enumerate(all_contours):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour] * 2,
            color=color_contour,
            linestyle=all_linestyles[contour_i],
            alpha=alpha_contour,
        )

    ##########
    # plot make-up
    ax_plot.set_xlim([10**-2, 10**2])
    ax_plot.set_xscale("log")

    #
    ax_plot.set_ylabel(ylabel)
    ax_plot.set_xlabel(xlabel)

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def plot_schematic_coriolis_accelaration_non_synchronous_rotation(
    opposite_asynchronous_offset_direction, plot_settings
):
    """
    Function to plot the non-synchronous rotation offset in terms of the velocity units.
    """

    #
    settings = {
        "non_synch_velocity_offset_distance_to": "donor",
        "include_asynchronicity_donor_for_lagrange_points": True,
        "direction_velocity_asynchronous_offset": -1
        if not opposite_asynchronous_offset_direction
        else 1,
        "frame_of_reference": "center_of_mass",
    }

    stepsize_q = plot_settings["stepsize_q"]
    stepsize_f = plot_settings["stepsize_f"]
    relative_to_orbit = plot_settings["relative_to_orbit"]
    relative_to_orbit_value = -1 if relative_to_orbit else 0

    xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
    ylabel = r"$f_{\mathrm{sync}}$"

    cmap = cm.coolwarm
    color_contour = "black"
    alpha_contour = 0.25

    contours = np.array([0.05, 0.1, 0.25, 0.5, 0.75])
    linestyles = linestyle_list[: len(contours)]

    all_contours = np.concatenate((-contours[::-1], contours))
    all_linestyles = linestyles[::-1] + linestyles
    all_contours += relative_to_orbit_value

    #
    q_range = 10 ** np.arange(-2, 2 + stepsize_q, stepsize_q)
    f_range = np.arange(0 + stepsize_f, 2 + stepsize_f, stepsize_f)

    result_list = []

    # loop over values
    for parameters in itertools.product(
        f_range,
        q_range,
    ):

        # Unpack
        synchronicity_factor = parameters[0]
        massratio_accretor_donor = parameters[1]

        # Set system dict
        settings["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": synchronicity_factor,
            "mass_accretor": massratio_accretor_donor
            * standard_system_dict["mass_donor"],
        }

        # Set grid point
        settings["grid_point"] = {
            "massratio_accretor_donor": massratio_accretor_donor,
            "synchronicity_factor": synchronicity_factor,
        }

        # calculate the offset
        velocity_offset_L1_non_synchronous = (
            calculate_velocity_offset_L1_non_synchronous(
                settings=settings,
                system_dict=settings["system_dict"],
                frame_of_reference=settings["frame_of_reference"],
            )
        )

        # print(velocity_offset_L1_non_synchronous)
        result_list.append(
            [
                synchronicity_factor,
                massratio_accretor_donor,
                velocity_offset_L1_non_synchronous[-1],
            ]
        )

    # Make sure things are sorted
    x_parameter = "massratio"
    y_parameter = "f_sync"
    z_parameter = "v_offset_y"

    # load into df
    df = pd.DataFrame(result_list, columns=[y_parameter, x_parameter, z_parameter])

    new_z_parameter = "a_coriolis_non_synchronous_offset_x"
    df[new_z_parameter] = df[z_parameter] * 2
    z_parameter = new_z_parameter

    # Sort values
    df = df.sort_values(by=[y_parameter, x_parameter])

    ####
    # Handle reshaping
    unique_x = df[x_parameter].unique()
    unique_y = df[y_parameter].unique()

    num_unique_x = len(unique_x)
    num_unique_y = len(unique_y)

    x_values_reshaped = df[x_parameter].to_numpy().reshape((num_unique_y, num_unique_x))
    y_values_reshaped = df[y_parameter].to_numpy().reshape((num_unique_y, num_unique_x))
    z_values_reshaped = df[z_parameter].to_numpy().reshape((num_unique_y, num_unique_x))

    ##########
    # Plot data

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax_plot = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    norm = colors.CenteredNorm()

    # Plot the data
    _ = ax_plot.pcolormesh(
        x_values_reshaped,
        y_values_reshaped,
        z_values_reshaped + relative_to_orbit_value,
        cmap=cmap,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # Plot the contour
    _ = ax_plot.contour(
        unique_x,
        unique_y,
        z_values_reshaped + relative_to_orbit_value,
        levels=all_contours,
        linestyles=all_linestyles,
        colors=color_contour,
        alpha=alpha_contour,
    )

    ##########
    # plot color bar
    cbar = mpl.colorbar.ColorbarBase(ax_cb, cmap=cmap, norm=norm)
    cbar.ax.set_ylabel(r"$a_{\mathrm{coriolis\, non-synchronous\ rotation,\ x}}$")

    # Plot contourlevels
    for contour_i, contour in enumerate(all_contours):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour] * 2,
            color=color_contour,
            linestyle=all_linestyles[contour_i],
            alpha=alpha_contour,
        )

    ##########
    # plot make-up
    ax_plot.set_xlim([10**-2, 10**2])
    ax_plot.set_xscale("log")

    #
    ax_plot.set_ylabel(ylabel)
    ax_plot.set_xlabel(xlabel)

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_schematic_velocity_offset_non_synchronous_rotation(
    output_name, show_plot
):
    """
    Function to handle the paper version of the velocity offset due to asynchronous rotation
    """

    #
    plot_schematic_velocity_offset_non_synchronous_rotation(
        opposite_asynchronous_offset_direction=False,
        plot_settings={
            "output_name": output_name,
            "show_plot": show_plot,
            "stepsize_q": 0.025,
            "stepsize_f": 0.025,
            "relative_to_orbit": False,
        },
    )


#
if __name__ == "__main__":
    plot_settings = {"show_plot": False}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    plot_settings["stepsize_q"] = 0.025
    plot_settings["stepsize_f"] = 0.025
    plot_settings["relative_to_orbit"] = False

    #
    basename = "schematic_offset_v_nonsync.pdf"
    plot_schematic_velocity_offset_non_synchronous_rotation(
        opposite_asynchronous_offset_direction=False,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    basename = "schematic_coriolis_nonsync_offset_a_x.pdf"
    plot_schematic_coriolis_accelaration_non_synchronous_rotation(
        opposite_asynchronous_offset_direction=False,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    #
    basename = "schematic_offset_v_nonsync_opposite_asynchronous_offset.pdf"
    plot_schematic_velocity_offset_non_synchronous_rotation(
        opposite_asynchronous_offset_direction=True,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    basename = "schematic_coriolis_nonsync_opposity_asynchronous_offset_a_x.pdf"
    plot_schematic_coriolis_accelaration_non_synchronous_rotation(
        opposite_asynchronous_offset_direction=True,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )
