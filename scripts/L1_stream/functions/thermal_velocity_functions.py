"""
Script that contains functions for the thermal velocity
"""

import numpy as np


def maxwellian_velocity_probability_density(velocity_dispersion, velocity):
    """
    Function to calculate the maxwellian velocity
    """

    maxwellian_probability_density = (
        np.sqrt(2 / np.pi)
        * (velocity**2)
        / (velocity_dispersion**3)
        * np.exp(-(velocity**2) / (2 * velocity_dispersion**2))
    )

    return maxwellian_probability_density


def generate_thermal_velocity_offset_range_and_weights(velocity_dispersion, n_samples):
    """
    Function to generate an array of velocities offsets and weights of a maxwellian velocity distribution given a velocity dispersion.
    In our simulations the velocity dispersion is expressed in terms of the orbital velocity a * omega.
    """

    # Set the bounds
    lower_bound_velocities = velocity_dispersion / 10
    upper_bound_velocities = velocity_dispersion * 4

    # Calculate velocity range
    velocity_edges = np.linspace(
        lower_bound_velocities, upper_bound_velocities, n_samples + 1
    )
    velocity_centers = (velocity_edges[1:] + velocity_edges[:-1]) / 2
    velocity_binsizes = velocity_edges[1:] - velocity_edges[:-1]

    # Calculate weights
    weights_densities = maxwellian_velocity_probability_density(
        velocity_dispersion, velocity_centers
    )
    weights = weights_densities * velocity_binsizes

    # normalise s.t. it totals to 1
    weights = weights / np.sum(weights)

    # print(velocity_centers, velocity_binsizes, weights, np.sum(weights))

    return velocity_centers, weights


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    velocity_dispersion = 0.01
    n_samples = 25

    velocity_centers, weights = generate_thermal_velocity_offset_range_and_weights(
        velocity_dispersion, n_samples
    )

    plt.plot(velocity_centers, weights)
    plt.axvline(velocity_dispersion)
    plt.show()
