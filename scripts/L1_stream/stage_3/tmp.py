import numpy as np
import pandas as pd

data = [[1, 2], [0, 0]]

df = pd.DataFrame(data, columns=["a", "b"])
print(df)


def get_weighted_average(df, col, weight_col):
    """
    Function to get the weighted average of a column
    """

    weighted_average = np.average(df[col], weights=df[weight_col])

    return weighted_average


get_weighted_average(df, col="a", weight_col="b")
