xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
y_label_dict = {
    "rmin": r"Radius of closest approach $\it{r}_{\mathrm{min}}$",
    "rcirc": r"Circularisation radius $\it{r}_{\mathrm{circ}}$",
    "ratio_rcirc_rmin": r"$\it{r}_{\mathrm{circ}}/\it{r}_{\mathrm{min}}$",
    "specific_angular_momentum_multiplier_self_accretion": r"$\left(\it{h}_{\mathrm{f,\ don}}/\it{h}_{\mathrm{i,\ don}}\right)-1$",
}
