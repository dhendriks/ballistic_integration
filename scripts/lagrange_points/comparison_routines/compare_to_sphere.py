"""
Function test the volume calculation with a sphere of known volume.
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial as ss
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

custom_mpl_settings.load_mpl_rc()


def calculate_volume_sphere_analytical(radius):
    """
    Analytic solution to the volume of a sphere
    """

    volume = (4.0 / 3.0) * np.pi * (radius**3)

    return volume


def generate_sphere_points(radius, num_phi, num_theta, add_endpoint=False):
    """
    Function to generate points on a sphere
    """

    phi_step = 2 * np.pi / num_phi
    theta_step = np.pi / num_theta

    if add_endpoint:
        phi_array = np.arange(0, 2 * np.pi + phi_step, phi_step)
        theta_array = np.arange(0, np.pi + theta_step, theta_step)
    else:
        phi_array = np.arange(0, 2 * np.pi, phi_step)
        theta_array = np.arange(0, np.pi, theta_step)

    # ###########
    # # Loop method
    # sphere_points = []

    # for phi in phi_array:
    #     for theta in theta_array:
    #         x = radius * np.sin(theta) * np.cos(phi)
    #         y = radius * np.sin(theta) * np.sin(phi)
    #         z = radius * np.cos(theta)

    #         point = np.array([x, y, z])

    #         sphere_points.append(point)

    # #
    # sphere_points = np.array(sphere_points)
    # # print(sphere_points)

    ###########
    # Numpy method

    sphere_points = np.zeros((3, len(theta_array) * len(phi_array))).T

    # print(np.outer(np.sin(theta_array), np.cos(phi_array)))
    x = radius * np.outer(np.sin(theta_array), np.cos(phi_array))
    y = radius * np.outer(np.sin(theta_array), np.sin(phi_array))
    z = np.repeat(radius * np.cos(theta_array), len(np.sin(phi_array)))

    sphere_points[:, 0] = x.flatten()
    sphere_points[:, 1] = y.flatten()
    sphere_points[:, 2] = z.flatten()

    # print(sphere_points)
    # quit()
    return sphere_points


def calculate_volume_sphere_convexhull(
    radius, num_phi=10, num_theta=10, add_endpoint=False, plot_collection=False
):
    """
    Function to calculate the volume of a sphere with the convex hull method
    """

    points_on_sphere = generate_sphere_points(
        radius=radius, num_phi=num_phi, num_theta=num_theta, add_endpoint=add_endpoint
    )

    #
    if plot_collection:
        x_vals_volume_points = points_on_sphere[:, 0]
        y_vals_volume_points = points_on_sphere[:, 1]
        z_vals_volume_points = points_on_sphere[:, 2]

        # Set up figure
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")

        # Plot scatter
        ax.scatter(
            x_vals_volume_points,
            y_vals_volume_points,
            z_vals_volume_points,
            c="r",
            marker="o",
        )

        # # Make up
        # ax.set_xticklabels([])  # y-axis
        # ax.set_yticklabels([])  # x-axis
        # ax.set_zticklabels([])  # y-axis
        # https://stackoverflow.com/questions/32705931/draw-3d-polygon-using-vertex-array-python-matplotlib

        plt.show()
    # import polytope

    ###########
    # Calculate the volume of the roche lobe
    hull = ss.ConvexHull(points_on_sphere)
    # hull = polytope.qhull(points_on_sphere)

    volume = hull.volume

    return volume


radius = 2
num_theta = 20
num_phi = 2 * num_theta

analytic_solution = calculate_volume_sphere_analytical(radius=radius)
convexhull_solution = calculate_volume_sphere_convexhull(
    radius=radius,
    num_phi=num_phi,
    num_theta=num_theta,
    plot_collection=False,
    add_endpoint=True,
)


convexhull_solution_list = []


num_theta_range = [
    2,
    3,
    5,
    7,
    10,
    15,
    20,
    25,
    30,
    35,
    40,
    45,
    50,
    55,
    60,
    65,
    70,
    75,
    80,
    100,
    150,
    200,
    300,
]

for num_theta in num_theta_range:
    num_phi = 2 * num_theta
    convexhull_solution = calculate_volume_sphere_convexhull(
        radius=radius,
        num_phi=num_phi,
        num_theta=num_theta,
        plot_collection=False,
        add_endpoint=True,
    )

    convexhull_solution_list.append(convexhull_solution)
    print(
        "Analytic solution: {} convexhull solution: {} ratio analytic/convexhull: {}".format(
            analytic_solution,
            convexhull_solution,
            analytic_solution / convexhull_solution,
        )
    )

convexhull_solution_array = np.array(convexhull_solution_list)
ratio = convexhull_solution_array / analytic_solution


absdiff = np.abs(analytic_solution - convexhull_solution_array)

##################
# Set up figure logic
fig = plt.figure(figsize=(20, 20))
gs = fig.add_gridspec(nrows=6, ncols=1)

ax = fig.add_subplot(gs[0:3, :])
ax_ratio = fig.add_subplot(gs[3:6, :])

ax.plot(num_theta_range, convexhull_solution_array, label="Convexhull estimates")
ax.axhline(analytic_solution, label="analytic solution")

ax_ratio.plot(num_theta_range, absdiff, label="Absdiff(convex-analytic)")

ax.set_xscale("log")
ax_ratio.set_xscale("log")

ax.set_yscale("log")
ax_ratio.set_yscale("log")


ax.set_ylabel("Volume")
ax_ratio.set_ylabel("Ratio")

ax_ratio.set_xlabel("Theta resolution")
ax.set_title("Convex hull volume calculation test. Sphere with radius=2.")

plt.show()
