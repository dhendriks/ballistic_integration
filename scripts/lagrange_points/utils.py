import json

import pandas as pd


def call_interpolator(interpolator, *, f_sync, q_acc_don):
    """
    Function to handle calling the interpolator
    """

    return interpolator((f_sync, q_acc_don))


def turn_into_csv_based(input_filename, output_filename, settings):
    """
    Function to turn the dictionary based data to csv
    """

    records = []

    # Readout file and store loaded dict
    with open(input_filename, "r") as f:
        for line in f:
            stripped_line = line.strip()
            loaded_dict = json.loads(stripped_line)
            records.append(loaded_dict)

    # load into dataframe
    df = pd.DataFrame.from_records(records)

    # Write to csv
    df.to_csv(output_filename, index=False, sep=settings["csv_output_separator"])
