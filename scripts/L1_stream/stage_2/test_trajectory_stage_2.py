"""
Main script to run stage 2 of the ballistic integrations:
- grid over initial_velocity/aw, f and q
"""

import json
import os

import numpy as np
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)
from ballistic_integrator.functions.functions import json_encoder

from ballistic_integration_code.scripts.L1_stream.functions.calculate_position_offset_L1_tiny import (
    calculate_position_offset_L1_tiny,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    calculate_trajectory_result_summary,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_non_synchronous import (
    calculate_velocity_offset_L1_non_synchronous,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_soundspeed import (
    calculate_velocity_offset_L1_soundspeed,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.write_to_file import (
    write_to_file,
)
from ballistic_integration_code.scripts.L1_stream.integrator_subclass.subclass_trajectory_integrator import (
    trajectory_integrator_class,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES
from ballistic_integration_code.scripts.L1_stream.stage_2.stage_settings import (
    stage_2_settings,
)

settings = stage_2_settings


from ballistic_integration_code.settings import standard_system_dict

#
settings["q_acc_don"] = 0.01
settings["synchronicity_factor"] = 0.1
settings["normalized_thermal_velocity_dispersion"] = -2.5

settings["system_dict"] = {
    **standard_system_dict,
    "mass_accretor": settings["q_acc_don"] * 1,
}


######
# Initialise
q_acc_don = settings["q_acc_don"]
synchronicity_factor = settings["synchronicity_factor"]
normalized_thermal_velocity_dispersion = settings[
    "normalized_thermal_velocity_dispersion"
]
result_dir = settings["result_dir"]

# trajectory summary filename
trajectory_summary_data_filename = os.path.join(
    result_dir, "trajectory_summary_data.txt"
)

# Create directory
if not os.path.isdir(os.path.abspath(result_dir)):
    os.makedirs(os.path.abspath(result_dir), exist_ok=True)

################
# Calculate velocity offsets

# Velocity offset based on the synchronicity rate
initial_velocity_offset_non_synchronous = calculate_velocity_offset_L1_non_synchronous(
    system_dict=settings["system_dict"],
    frame_of_reference=settings["frame_of_reference"],
)
settings[
    "initial_velocity_offset_non_synchronous"
] = initial_velocity_offset_non_synchronous

# Velocity offset based on initial velocity of material at L1
intial_velocity_offset_soundspeed = calculate_velocity_offset_L1_soundspeed(settings)
settings["intial_velocity_offset_soundspeed"] = intial_velocity_offset_soundspeed

# Combine the velocity offsets
settings["initial_velocity_offset"] = (
    settings["initial_velocity_offset_non_synchronous"]
    + settings["intial_velocity_offset_soundspeed"]
)

# Set initial velocity
initial_v = np.array(settings.get("initial_velocity_offset", [0, 0]))
settings["initial_velocity"] = np.array(initial_v)

################
# Set initial location

# Calculate offset based on tiny shift from L1
position_offset = calculate_position_offset_L1_tiny(
    system_dict=settings["system_dict"],
    frame_of_reference=settings["frame_of_reference"],
)
settings["position_offset_L1_tiny"] = np.array(position_offset)

# Combine the position offsets
settings["initial_position_offset"] = settings["position_offset_L1_tiny"]

# Get base position
L_points = calculate_lagrange_points(
    system_dict=settings["system_dict"],
    frame_of_reference=settings["frame_of_reference"],
)
l1_pos = L_points["L1"]
initial_position_base = np.array(l1_pos[:2])
settings["initial_position_base"] = np.array(initial_position_base)
settings["L_points"] = L_points

# Set offset position
initial_position_offset = np.array(settings.get("initial_position_offset", [0.0, 0.0]))
initial_r = initial_position_base + initial_position_offset
settings["initial_position"] = np.array(initial_r)

#####
# Handle evolution
if settings.get("run", True):

    # Set up the integrator
    integrator = trajectory_integrator_class(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        initial_position=initial_r,
        initial_velocity=initial_v,
        control_settings=settings,
    )

    # Calculate the trajectory
    result = integrator.evolve()

    full_trajectory_data_filename = "test_trajectory.json"
    with open(full_trajectory_data_filename, "w") as f:
        f.write(json.dumps(result, default=json_encoder))

        # json.dump(result, f, default=json_encoder)

    # Plot results
    plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        plot_trajectory=True,
        resultfile=full_trajectory_data_filename,
        plot_rochelobe_equipotential_meshgrid=True,
    )
