"""
Functions for the eccentricity plot
"""

import numpy as np
from numpy import arctan, pi, signbit
from numpy.linalg import norm


def rotate_vector(vector, angle):
    """
    Function to rotate a vector by a certain angle

    angle should be in pi
    """

    rot = np.array(
        [
            [np.cos(angle), -np.sin(angle), 0],
            [np.sin(angle), np.cos(angle), 0],
            [0, 0, 1],
        ]
    )

    rotated_vector = np.dot(rot, vector)

    return rotated_vector


def scale_vector(vector, scale):
    """
    Function to scale the vector
    """

    scaled_vector = vector * scale

    return scaled_vector


def set_magnitude_vector(vector, magnitude):
    """
    Function to set the magnitude of a vector
    """

    new_magnitude_vector = scale_vector(vector / np.linalg.norm(vector), magnitude)

    return new_magnitude_vector


def angle_btw(v1, v2):
    u1 = v1 / norm(v1)
    u2 = v2 / norm(v2)

    y = u1 - u2
    x = u1 + u2

    a0 = 2 * arctan(norm(y) / norm(x))

    if (not signbit(a0)) or signbit(pi - a0):
        return a0
    elif signbit(a0):
        return 0.0
    else:
        return pi


def new_vector():
    """
    Function to return a new vector
    """

    return np.array([1, 1, 0])
