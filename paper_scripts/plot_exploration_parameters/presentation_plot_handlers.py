import os

from plot_exploration_alpha_vs_synchronicity import plot_alpha_f_distributions

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

#
result_root = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "RLOF/")

# #####
# # test
# show_plot = False
# plot_dir = os.path.join(this_file_dir, "plots/")
# filename = os.path.join(
#     result_root,
#     "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
# )
# basename = "exploration_alpha_vs_synchronicity_MS_highres_all.pdf"
# plot_alpha_f_distributions(
#     filename,
#     orientation="horizontal",
#     plot_settings={
#         "show_plot": show_plot,
#         "output_name": os.path.join(plot_dir, basename),
#         "plot_red_dashed_lines": False,
#     },
# )


# #####
# # test
# show_plot = False
# plot_dir = os.path.join(this_file_dir, "plots/")
# filename = os.path.join(
#     result_root,
#     "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
# )

# basename = "exploration_alpha_vs_synchronicity_MS_highres_mainsequence_only.pdf"
# plot_alpha_f_distributions(
#     filename,
#     orientation="horizontal",
#     plot_settings={
#         "show_plot": show_plot,
#         "output_name": os.path.join(plot_dir, basename),
#         "plot_red_dashed_lines": False,
#     },
#     extra_query="st_acc==1",
# )


# #####
# # Figure showing the distributions of (a)sync accretion onto compact objects
# show_plot = False
# plot_dir = os.path.join(this_file_dir, "plots/")
# filename = os.path.join(
#     result_root,
#     "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
# )

# basename = "exploration_alpha_vs_synchronicity_MS_highres_compact_object_accretors.pdf"
# plot_alpha_f_distributions(
#     filename,
#     orientation="horizontal",
#     plot_settings={
#         "show_plot": show_plot,
#         "output_name": os.path.join(plot_dir, basename),
#         "plot_red_dashed_lines": False,
#     },
#     extra_query="st_acc>=10 & st_acc <= 14",
# )

# #####
# # Figure showing the distributions of (a)sync accretion onto compact objects
# show_plot = False
# plot_dir = os.path.join(this_file_dir, "plots/")
# filename = os.path.join(
#     result_root,
#     "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
# )

# basename = "exploration_alpha_vs_synchronicity_MS_highres_compact_object_accretors_no_WD.pdf"
# plot_alpha_f_distributions(
#     filename,
#     orientation="horizontal",
#     plot_settings={
#         "show_plot": show_plot,
#         "output_name": os.path.join(plot_dir, basename),
#         "plot_red_dashed_lines": False,
#     },
#     extra_query="st_acc>=13 & st_acc <= 14",
# )

#####
# Figure showing the distributions of (a)sync accretion onto compact objects
show_plot = False
plot_dir = os.path.join(this_file_dir, "plots/")
filename = os.path.join(
    result_root,
    "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
)

basename = "exploration_alpha_vs_synchronicity_MS_highres_compact_object_accretors_no_WD_low_q.pdf"
plot_alpha_f_distributions(
    filename,
    orientation="horizontal",
    plot_settings={
        "show_plot": show_plot,
        "output_name": os.path.join(plot_dir, basename),
        "plot_red_dashed_lines": False,
    },
    extra_query="st_acc>=13 & st_acc <= 14 & log10q_acc_don <= 0.3010299956639812",
)


# #####
# # Figure showing the distributions of (a)sync accretion onto compact objects low metallicity
# show_plot = False
# plot_dir = os.path.join(this_file_dir, "plots/")
# filename = os.path.join(
#     result_root,
#     "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.0008/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
# )

# basename = "exploration_alpha_vs_synchronicity_MS_highres_compact_object_accretors_no_WD_low_metallicity.pdf"
# plot_alpha_f_distributions(
#     filename,
#     orientation="horizontal",
#     plot_settings={
#         "show_plot": show_plot,
#         "output_name": os.path.join(plot_dir, basename),
#         "plot_red_dashed_lines": False,
#     },
#     extra_query="st_acc>=13 & st_acc <= 14",
# )
