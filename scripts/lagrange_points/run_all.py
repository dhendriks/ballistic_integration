"""
Control function for the lagrange point and roche lobe grid calculations
"""

import os

import numpy as np
from david_phd_functions.backup_functions.functions import backup_if_exists

from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    write_settings_to_file,
)
from ballistic_integration_code.scripts.L1_stream.functions.multiprocessing_routines import (
    multiprocessing_routine,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    parameter_dict,
    project_settings,
)
from ballistic_integration_code.scripts.lagrange_points.generate_data_header import (
    generate_data_header as generate_data_header_function,
)
from ballistic_integration_code.scripts.lagrange_points.generate_interpolation_table import (
    generate_interpolation_table as generate_interpolation_table_function,
)
from ballistic_integration_code.scripts.lagrange_points.generate_lagrange_point_and_roche_lobe_information import (
    generate_lagrange_point_and_roche_lobe_information as generate_lagrange_point_and_roche_lobe_information_function,
)
from ballistic_integration_code.scripts.lagrange_points.grid_generator import (
    grid_generator as grid_generator_lagrange_points,
)
from ballistic_integration_code.scripts.lagrange_points.utils import turn_into_csv_based


def run_all(
    settings,
    parameter_dict,
    generate_lagrange_point_and_roche_lobe_information=True,
    generate_output_interpolation_files=True,
):
    """
    Function to handle running the simulations and all the surrounding tasks. Uses imports and a dict to map the correct function for the correct stage
    """

    ##############################################
    # Run simulations and handle processing of data
    if generate_lagrange_point_and_roche_lobe_information:
        ##############################################
        # Check if the directory of the current simulation exists. If it does, then delete the old dir
        result_dir = settings["result_dir"]
        if settings.get("backup_if_data_exists", True):
            backup_if_exists(result_dir, remove_old_directory_after_backup=True)

        #####################
        # Write settings to file
        write_settings_to_file(
            settings,
            output_filename=os.path.join(
                settings["result_dir"], "settings_lagrange_points.json"
            ),
        )

        ######################
        # Set up grid generator
        grid_generator = grid_generator_lagrange_points(settings=settings)

        ######################
        # Call the multiprocessing routine and let it handle everything
        multiprocessing_routine(
            configuration=settings,
            job_iterable=grid_generator,
            target_function=generate_lagrange_point_and_roche_lobe_information_function,
        )

    #############################################
    # Output interpolation files
    if generate_output_interpolation_files:
        #
        lagrange_point_data_filename = os.path.join(
            settings["result_dir"], settings["lagrange_point_data_file_basename"]
        )

        # load and write to csv
        lagrange_point_data_csv_filename = (
            os.path.splitext(lagrange_point_data_filename)[0] + ".csv"
        )
        turn_into_csv_based(
            input_filename=lagrange_point_data_filename,
            output_filename=lagrange_point_data_csv_filename,
            settings=settings,
        )

        #
        lagrange_point_csv_interpolation_table_filename = os.path.join(
            settings["result_dir"],
            settings["lagrange_point_csv_interpolation_table_file_basename"],
        )
        lagrange_point_header_interpolation_table_filename = os.path.join(
            settings["result_dir"],
            settings["lagrange_point_header_interpolation_table_file_basename"],
        )

        #############################################
        # Generate dataframe -formatted table from the data
        print("Generating CSV output interpolation file")
        generate_interpolation_table_function(
            input_textfile=lagrange_point_data_csv_filename,
            output_textfile=lagrange_point_csv_interpolation_table_filename,
            settings=settings,
            parameter_dict=parameter_dict,
        )

        ######################
        # Call functionality to build the data header for binary_c
        print("Generating data header output interpolation file")
        generate_data_header_function(
            input_textfile=lagrange_point_data_csv_filename,
            output_textfile=lagrange_point_header_interpolation_table_filename,
            settings=settings,
            parameter_dict=parameter_dict,
        )


if __name__ == "__main__":
    settings = {
        **project_settings,
        "result_dir": "results/",
        "max_job_queue_size": 1000,
        "num_cores": 8,
        # "q_range": 10 ** np.linspace(-2, 2, 10),
        # "f_range": np.linspace(0.1, 2, 10),
        "q_range": 10 ** np.linspace(-2, 2, 2),
        "f_range": np.linspace(0.1, 2, 2),
        "verbosity": 1,
        "volume_roche_lobe_xgrid_n": 25,
        "volume_roche_lobe_ygrid_n": 25,
        "volume_roche_lobe_size_factor": 1.001,
    }
    settings["result_dir"] = "test/"
    settings["lagrange_point_data_file_basename"] = "lagrange_point_data.txt"
    settings[
        "lagrange_point_csv_interpolation_table_file_basename"
    ] = "lagrange_point_and_rochelobe_volume_interpolation_table.csv"
    settings[
        "lagrange_point_header_interpolation_table_file_basename"
    ] = "RLOF_hendriks2023_ballistic_stream_interpolation_table.h"

    settings["rochelobe_passed_gridpoint_file_basename"] = "passed_gridpoints.txt"
    settings["rochelobe_failed_gridpoint_file_basename"] = "failed_gridpoints.txt"

    run_all(
        settings=settings,
        parameter_dict=parameter_dict,
        generate_lagrange_point_and_roche_lobe_information=False,
        generate_output_interpolation_files=True,
    )
