"""
Function to plot the inital conditions (position and velocity) schematic

TODO: add XKCD
"""

import os

import matplotlib
import numpy as np
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
    calculate_position_accretor,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)

from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_non_synchronous import (
    calculate_velocity_offset_L1_non_synchronous,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_soundspeed import (
    calculate_velocity_offset_L1_soundspeed,
)
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    generate_L1_area_offset_range_and_weights,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

matplotlib.rcParams["savefig.dpi"] = 300

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


TEXTSIZE = 50
LINEWIDTH = 8


from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_normalised_mass_stream_area,
)


def plot_initial_conditions_schematic(settings, plot_settings):
    """
    Function to plot the roche lobe schematic
    """

    x_bounds = [-0.05, 0.7]
    y_bounds = [-0.3, 0.3]

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    system_dict = {
        **standard_system_dict,
        "mass_donor": 1,
        "mass_accretor": 0.5,
        "synchronicity_factor": 1,
    }
    frame_of_reference = "center_of_mass"
    log10normalized_stream_area = 1e-1
    num_samples_area_sampling = 16

    normalised_mass_stream_area = calculate_normalised_mass_stream_area(
        settings=settings
    )
    log10normalized_stream_area = np.log10(normalised_mass_stream_area) * 0.1

    linestyle_position_components = "--"
    color_position_components = "black"
    alpha_position_components = 0.25
    bbox_position_components = {
        "pad": 0,
        "facecolor": "white",
        "boxstyle": "round",
        "edgecolor": "white",
        "alpha": 0.9,
    }

    linestyle_velocity_components = "-."
    color_velocity_components = "red"
    alpha_velocity_components = 0.25
    bbox_velocity_components = {
        "pad": 0,
        "facecolor": "white",
        "boxstyle": "round",
        "edgecolor": "white",
        "alpha": 0.9,
    }

    # Make xkcd style
    # with plt.xkcd():
    #
    fig, axes_list = plot_system(
        system_dict,
        settings=settings,
        frame_of_reference=frame_of_reference,
        resultfile=None,
        resultdata=None,
        resultdata_list=None,
        #
        plot_rochelobe_equipotentials=True,
        plot_rochelobe_equipotential_meshgrid=False,
        plot_stars_and_COM=True,
        plot_L_points=True,
        plot_trajectory=False,
        plot_system_information_panel=False,
        # add_gradients=False,
        # add_velocities_and_accelarations=False
        return_fig=True,
        plot_settings={
            **plot_settings,
            "plot_bounds_x": x_bounds,
            "plot_bounds_y": y_bounds,
            "plot_resolution": 400,
            "linewidth": LINEWIDTH,
            "textsize": TEXTSIZE,
        },
    )

    #########
    # Get actual L1 point location
    L_points = calculate_lagrange_points(
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
    )

    ###########
    # Construct lines for position
    fraction_sep = 20

    #########################
    # minor offset

    # Get offset
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    L_dict = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    l1_pos = L_dict["L1"][:2]

    # Calculate the offset
    diff_l1_accretor = np.abs(pos_accretor - l1_pos)
    minor_position_offset_L1 = diff_l1_accretor / fraction_sep

    # Construct the offset vector
    minor_offset_vector = np.array([l1_pos, l1_pos + minor_position_offset_L1])

    # Draw the offset vector
    axes_list[0].plot(
        minor_offset_vector[:, 0],
        minor_offset_vector[:, 1],
        linestyle=linestyle_position_components,
        color=color_position_components,
        alpha=alpha_position_components,
        linewidth=LINEWIDTH,
    )

    # add text for minor offset
    axes_list[0].text(
        x=l1_pos[0] + minor_position_offset_L1[0] / 2,
        y=l1_pos[1],
        horizontalalignment="left",
        verticalalignment="top",
        s=r"$\mathbf{x}_{\mathrm{minor\ offset}}$",
        bbox=dict(**bbox_position_components),
        fontsize=TEXTSIZE,
    )

    ##########
    # sampling offset

    # Set up the weights
    (area_sampling_offset_array, _,) = generate_L1_area_offset_range_and_weights(
        log10normalized_stream_area=log10normalized_stream_area,
        n_samples=num_samples_area_sampling,
    )

    # Get offset
    areasampling_offset = np.array([0, area_sampling_offset_array[-7]])

    # Construct vector
    areasampling_offset_vector = np.array(
        [minor_offset_vector[1], minor_offset_vector[1] + areasampling_offset]
    )

    # Draw the offset
    axes_list[0].plot(
        areasampling_offset_vector[:, 0],
        areasampling_offset_vector[:, 1],
        linestyle=linestyle_position_components,
        color=color_position_components,
        alpha=alpha_position_components,
        linewidth=LINEWIDTH,
    )

    # add text for area offset
    axes_list[0].text(
        x=l1_pos[0] + minor_position_offset_L1[0],
        y=l1_pos[1] + areasampling_offset[1] / 2,
        horizontalalignment="left",
        s=r"$\mathbf{x}_{\mathrm{stream\ area\ offset}}$",
        bbox=dict(**bbox_position_components),
        fontsize=TEXTSIZE,
    )

    ##################
    # Draw actual vector
    position_vector_startpoint = l1_pos
    position_vector_endpoint = (
        position_vector_startpoint + minor_position_offset_L1 + areasampling_offset
    )
    position_vector_dxdy = position_vector_endpoint - position_vector_startpoint

    axes_list[0].arrow(
        position_vector_startpoint[0],
        position_vector_startpoint[1],
        position_vector_dxdy[0],
        position_vector_dxdy[1],
        color=color_position_components,
        linewidth=LINEWIDTH,
    )

    # add text for area offset
    axes_list[0].text(
        x=(position_vector_startpoint[0] + position_vector_endpoint[0]) / 2,
        y=(position_vector_startpoint[1] + position_vector_endpoint[1]) / 2,
        horizontalalignment="right",
        s=r"$\mathbf{x}_{init}}$",
        bbox=dict(**bbox_position_components),
        fontsize=TEXTSIZE,
    )

    ###########################
    # Draw velocity offsets

    ###########################
    # Thermal velocity offset

    # Get thermal velocity offset
    thermal_velocity_offset = calculate_velocity_offset_L1_soundspeed(settings=settings)

    # Construct vector
    thermal_velocity_offset_vector = np.array(
        [position_vector_endpoint, position_vector_endpoint + thermal_velocity_offset]
    )

    # Draw the offset
    axes_list[0].plot(
        thermal_velocity_offset_vector[:, 0],
        thermal_velocity_offset_vector[:, 1],
        linestyle=linestyle_velocity_components,
        color=color_velocity_components,
        alpha=alpha_velocity_components,
        linewidth=LINEWIDTH,
    )

    # add text for area offset
    axes_list[0].text(
        x=(thermal_velocity_offset_vector[0] + thermal_velocity_offset_vector[1])[0]
        / 2,
        y=(thermal_velocity_offset_vector[0] + thermal_velocity_offset_vector[1])[1]
        / 2,
        horizontalalignment="center",
        verticalalignment="bottom",
        s=r"$\mathbf{v}_{thermal}}$",
        bbox=dict(**bbox_velocity_components),
        fontsize=TEXTSIZE,
    )

    ###########################
    # get asynchronous velocity offset

    velocity_offset_L1_non_synchronous = calculate_velocity_offset_L1_non_synchronous(
        settings=settings,
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
    )

    # Construct vector
    velocity_offset_L1_non_synchronous_vector = np.array(
        [
            thermal_velocity_offset_vector[1],
            thermal_velocity_offset_vector[1] + velocity_offset_L1_non_synchronous,
        ]
    )

    # Draw the offset
    axes_list[0].plot(
        velocity_offset_L1_non_synchronous_vector[:, 0],
        velocity_offset_L1_non_synchronous_vector[:, 1],
        linestyle=linestyle_velocity_components,
        color=color_velocity_components,
        alpha=alpha_velocity_components,
        linewidth=LINEWIDTH,
    )

    # add text for area offset
    axes_list[0].text(
        x=(
            velocity_offset_L1_non_synchronous_vector[0]
            + velocity_offset_L1_non_synchronous_vector[1]
        )[0]
        / 2,
        y=(
            velocity_offset_L1_non_synchronous_vector[0]
            + velocity_offset_L1_non_synchronous_vector[1]
        )[1]
        / 2,
        horizontalalignment="left",
        verticalalignment="center",
        s=r"$\mathbf{v}_{asynchronous\ offset}}$",
        bbox=dict(**bbox_velocity_components),
        fontsize=TEXTSIZE,
    )

    ##################
    # Draw actual vector
    velocity_vector_startpoint = position_vector_endpoint
    velocity_vector_endpoint = (
        velocity_vector_startpoint
        + thermal_velocity_offset
        + velocity_offset_L1_non_synchronous
    )
    velocity_vector_dxdy = velocity_vector_endpoint - velocity_vector_startpoint

    axes_list[0].arrow(
        velocity_vector_startpoint[0],
        velocity_vector_startpoint[1],
        velocity_vector_dxdy[0],
        velocity_vector_dxdy[1],
        color=color_velocity_components,
        linewidth=LINEWIDTH,
    )

    # add text for area offset
    axes_list[0].text(
        x=(velocity_vector_startpoint[0] + velocity_vector_endpoint[0]) / 2,
        y=(velocity_vector_startpoint[1] + velocity_vector_endpoint[1]) / 2,
        horizontalalignment="left",
        verticalalignment="center",
        s=r"$\mathbf{v}_{init}}$",
        bbox=dict(**bbox_velocity_components),
        fontsize=TEXTSIZE,
    )

    ######
    # Resize
    axes_list[0].set_xlim(plot_settings["x_bounds"])
    axes_list[0].set_ylim(plot_settings["y_bounds"])

    # Add labels
    axes_list[0].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_list[0].set_ylabel(r"$\it{y}$", fontsize=36)

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # force the same axes ratio
    axes_list[0].set_aspect("equal")

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


def handle_paper_plot_initial_conditions_schematic(output_name, show_plot):
    """
    Function to handle the paper version of the initial conditions schematic
    """

    settings = {
        **stage_3_settings,
        "system_dict": {
            **standard_system_dict,
            "mass_accretor": 1,
            "mass_donor": 1,
            "synchronicity_factor": 0.6,
            "separation": 1,
        },
        "include_asynchronicity_donor_for_lagrange_points": True,
        "non_synch_velocity_offset_distance_to": "donor",
        "direction_velocity_asynchronous_offset": +1,
    }
    settings["grid_point"] = {
        "log10normalized_thermal_velocity_dispersion": -1,
        "massratio_accretor_donor": settings["system_dict"]["mass_accretor"]
        / settings["system_dict"]["mass_donor"],
        "synchronicity_factor": settings["system_dict"]["synchronicity_factor"],
    }

    #
    plot_settings = {
        "star_text_color": "white",
        "show_plot": show_plot,
        "x_bounds": [0.15, 0.44],
        "y_bounds": [-0.15, 0.1],
        "offset_text": 0.025,
        "markersize": 10,
        "textsize": 26,
    }

    #
    plot_initial_conditions_schematic(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    #######
    # Config
    show_plot = False

    settings = {
        **stage_3_settings,
        "system_dict": {
            **standard_system_dict,
            "mass_accretor": 1,
            "mass_donor": 1,
            "synchronicity_factor": 0.6,
            "separation": 1,
        },
        "include_asynchronicity_donor_for_lagrange_points": True,
        "non_synch_velocity_offset_distance_to": "donor",
        "direction_velocity_asynchronous_offset": +1,
    }

    settings["grid_point"] = {
        "log10normalized_thermal_velocity_dispersion": -1,
        "massratio_accretor_donor": settings["system_dict"]["mass_accretor"]
        / settings["system_dict"]["mass_donor"],
        "synchronicity_factor": settings["system_dict"]["synchronicity_factor"],
    }

    #
    plot_settings = {
        "star_text_color": "white",
        "show_plot": show_plot,
        "x_bounds": [0.15, 0.44],
        "y_bounds": [-0.15, 0.1],
        "offset_text": 0.025,
        "markersize": 10,
        "textsize": 26,
    }

    # Output dir
    output_dir = "plots/"
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )

    # basename
    basename = "initial_conditions_schematic.pdf"

    #
    plot_initial_conditions_schematic(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )
