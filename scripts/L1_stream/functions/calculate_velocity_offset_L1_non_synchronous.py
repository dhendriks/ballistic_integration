"""
Script that contains the function to calculat the velocity offset due to the non-synchronous rotation
"""

import numpy as np
from ballistic_integrator.functions.frame_of_reference import (
    calculate_lagrange_points,
    calculate_position_accretor,
    calculate_position_donor,
)


def calculate_velocity_offset_L1_non_synchronous(
    settings, system_dict, frame_of_reference
):
    """
    Function to calculate the velocity offset at L1

    Without any velocity offset the particle would not move in the corotating reference frame,
    which means that in the normal frame is would rotate with the orbital rotation


    The velocity offset is then relative to the co-rotating velocity (hence the factor-1)

    We only add a velocity in the y-direction cause we are at y=0 now, the direction of velocity would be only in y now.

    The actual velocity offset formula is:

    velocity_offset_y = -(synch_factor - 1) * distance_accretor_to_L1 * omega_orb

    But in the simulation the velocity is expressed in units of a * omega_orb, so the
    scaled_velocity_offset_y = -(synch_factor - 1) * distance_accretor_to_L1 * omega_orb / a * omega_orb = -(synch_factor - 1) * distance_accretor_to_L1 / a

    so if we express the distance to L1 in terms of the total separation, then the direct formula is

    scaled_velocity_offset_y = -(synch_factor - 1) * distance_accretor_to_L1

    to test things we add another option, i.e. to calculate this with the lever arm wrt the accretor
    """

    # Calculate location of lagrange points
    L_dict = calculate_lagrange_points(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"]
            if settings["include_asynchronicity_donor_for_lagrange_points"]
            else 1,
        },
        frame_of_reference=frame_of_reference,
    )
    l1_pos = L_dict["L1"]

    # Calculate to what object we need to calculate the distance to
    if settings["non_synch_velocity_offset_distance_to"] == "accretor":
        #
        pos_accretor = calculate_position_accretor(
            system_dict=system_dict, frame_of_reference=frame_of_reference
        )
        dist_star_to_l1 = np.linalg.norm(np.abs(pos_accretor - l1_pos[:2]))

    # Calculate to what object we need to calculate the distance to
    if settings["non_synch_velocity_offset_distance_to"] == "donor":
        #
        pos_donor = calculate_position_donor(
            system_dict=system_dict, frame_of_reference=frame_of_reference
        )
        dist_star_to_l1 = np.linalg.norm(np.abs(pos_donor - l1_pos[:2]))
    else:
        raise ValueError(
            "Please choose a valid value for 'non_synch_velocity_offset_distance_to' from ('accretor', 'donor')"
        )

    # NOTE: the choice for direction_velocity_asynchronous_offset makes quite a difference in the outcome but i think the correct choice is +1
    velocity_offset_y = (
        settings["direction_velocity_asynchronous_offset"]
        * (system_dict["synchronicity_factor"] - 1)
        * dist_star_to_l1
    )

    velocity_offset = [0, velocity_offset_y]

    return velocity_offset
