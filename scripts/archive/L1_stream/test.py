import json
import os

import astropy.constants as const
import astropy.units as u
import numpy as np
import scipy
from ballistic_integrator.functions.functions import json_encoder
from ballistic_integrator.functions.integrator.trajectory_integrator_class import (
    trajectory_integrator,
)
from ballistic_integrator.functions.lagrange_points import calculate_lagrange_points

import ballistic_integration_code.settings as settings

settings.settings["output_root"] = "results/"


def main_from_L1(mass_accretor, mass_donor, separation, extra_settings=None):
    """
    Main function to run the trajectory calculator
    """

    if not extra_settings:
        extra_settings = {}

    # Set up integration of particle:
    L_points = calculate_lagrange_points(
        mass_donor, mass_accretor, separation=separation
    )
    l2_pos = L_points["L1"]

    initial_position_base = np.array(l2_pos[:2])
    initial_position_offset = np.array(
        extra_settings.get("initial_position_offset", [0.0, 0.0])
    )

    initial_r = initial_position_base + initial_position_offset
    initial_v = np.array(extra_settings.get("initial_velocity_offset", [0, 0]))

    result_dir = os.path.join(
        os.path.abspath(settings.settings["output_root"]), "testing"
    )
    filename = "L1_q={}_x={}_v={}.json".format(
        mass_accretor / mass_donor,
        "{},{}".format(initial_r[0], initial_r[1]),
        "{},{}".format(initial_v[0], initial_v[1]),
    )
    full_filename = os.path.join(result_dir, filename)

    if extra_settings.get("run", True):
        integrator = trajectory_integrator(
            mass_accretor=mass_accretor,
            mass_donor=mass_donor,
            separation=separation,
            initial_position=initial_r,
            initial_velocity=initial_v,
            control_settings=extra_settings,
        )

        result = integrator.evolve()

        # Write result to file:
        if not os.path.isdir(os.path.abspath(result_dir)):
            os.makedirs(os.path.abspath(result_dir), exist_ok=True)

        with open(full_filename, "w") as f:
            json.dump(result, f, default=json_encoder)

    return full_filename


extra_settings = {
    "max_time": 1500,
    "dt": 0.01,
    "steps_check_self_intersection": -1,
    "jacobi_error_tol": 1e-4,
    "initial_position_offset": [0.1, 0.0],
    "initial_velocity_offset": [0.0, 0.0],
    "print_info": False,
    "run": False,
    "allow_fallback_to_center": True,
    # # custom termination
    # "use_custom_termination_routine": True,
    # "custom_termination_error_string": custom_termination_error_string,
    # "custom_termination_routine": custom_termination_routine,
}


# Set values for a system
q = 1
mass_accretor = 1 * q
mass_donor = 1
separation = 1

# Simulate
resultfile = main_from_L1(
    mass_accretor=mass_accretor,
    mass_donor=mass_donor,
    separation=separation,
    extra_settings=extra_settings,
)

print(resultfile)
