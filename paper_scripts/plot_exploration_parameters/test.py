import matplotlib.ticker as mticker

f = mticker.ScalarFormatter(useMathText=True)
f.set_powerlimits((-3, 3))
ding = "${}$".format(f.format_data(0.00000001))
print(ding)
