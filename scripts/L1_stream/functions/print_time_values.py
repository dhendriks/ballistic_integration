"""
Function to print the time values
"""


def print_time_values(total_time, parts_list, gridpoint):
    """
    Function to print the time analysis
    """

    time_analysis_string = "Time analysis for gridpoint {}\n".format(gridpoint)
    time_analysis_string += "total_calculation for gridpoint took: {:.2f}s\n".format(
        total_time
    )

    for part in parts_list:
        time_analysis_string += "\t{} time: {:.2f}s ({:.2f}%)\n".format(
            part["name"], part["time"], 100 * part["time"] / total_time
        )

    print(time_analysis_string)
