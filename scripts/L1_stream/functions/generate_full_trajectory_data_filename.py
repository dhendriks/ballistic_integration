"""
Function to generate the full trajectory data filename
"""

import os


def generate_full_trajectory_data_filename(
    settings, system_dict, trajectory_result_dir
):
    """
    Function to generate the full trajectory filename
    """

    initial_r = settings["initial_position"]
    initial_v = settings["initial_velocity"]

    trajectory_data_filename = "L1_f={}_q={}_x={}_v={}.json".format(
        settings["grid_point"]["synchronicity_factor"],
        system_dict["mass_accretor"] / system_dict["mass_donor"],
        "{},{}".format(initial_r[0], initial_r[1]),
        "{},{}".format(initial_v[0], initial_v[1]),
    )
    full_trajectory_data_filename = os.path.join(
        trajectory_result_dir, trajectory_data_filename
    )

    return full_trajectory_data_filename
