"""
Collection of functions related to the mass stream
"""

from functools import lru_cache

import astropy.constants as const
import astropy.units as u
import numpy as np
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_donor,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)


def calculate_grav_potential_factor(dl1, q_acc):
    """
    Function to calculate the gravitational potential factor
    """

    donor_term = 1.0 / np.power(dl1, 3)
    accretor_term = q_acc / np.power(1 - dl1, 3)

    return donor_term + accretor_term


def calculate_distance_donor_to_l1(settings):
    """
    Function to calculate the distance of the center of the donor to the L1 point
    """

    #
    system_dict = settings["system_dict"]

    # Calculate lagrange points
    lagrange_points = calculate_lagrange_points(
        system_dict={
            **settings["system_dict"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"]
            if settings["include_asynchronicity_donor_for_lagrange_points"]
            else 1,
        },
        frame_of_reference=settings["frame_of_reference"],
    )

    # Calculate donor position
    position_donor = calculate_position_donor(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
        separation=system_dict["separation"],
    )

    # Calculate distance between L1 and center of donor
    distance_donor_to_l1 = np.linalg.norm(lagrange_points["L1"][:2] - position_donor)

    return distance_donor_to_l1


def calculate_normalised_mass_stream_area(settings):
    """
    Function to calculate the normalized mass stream area $A/a^{2}$. Directly uses te normalized velocity $v/a\omega$ to calculate the area.

    Requires:
    - massratio_accretor_donor
    - synchronicity_factor
    - log10normalized_thermal_velocity_dispersion
    """

    # Unpack
    grid_point = settings["grid_point"]

    q_acc = grid_point["massratio_accretor_donor"]
    f_sync = grid_point["synchronicity_factor"]
    normalized_velocity = (
        10 ** grid_point["log10normalized_thermal_velocity_dispersion"]
    )

    ####
    # calculate the points and locations
    distance_donor_to_l1 = calculate_distance_donor_to_l1(settings)

    ####
    # calculate the terms for the mass stream area and combine
    grav_potential_factor = calculate_grav_potential_factor(
        dl1=distance_donor_to_l1, q_acc=q_acc
    )

    # geometry term
    geometry_term = (1 + q_acc) * np.power(
        grav_potential_factor
        * (grav_potential_factor - (1 + q_acc) * np.power(f_sync, 2)),
        -0.5,
    )

    #
    normalized_stream_area = (
        (2 * np.pi / 3) * np.power(normalized_velocity, 2) * geometry_term
    )

    return normalized_stream_area


@lru_cache
def calculate_normalisation_factor(sigma):
    """
    Function to calculate the normalisation factor
    TODO: perhaps use the num samples here to get exactly 0?
    """

    stepsize = 0.001

    # Set up ltilde edges etc to normalise
    ltilde_edges = np.arange(-1, 1 + stepsize, stepsize)

    ltilde_center = (ltilde_edges[1:] + ltilde_edges[:-1]) / 2
    ltilde_binsizes = ltilde_edges[1:] - ltilde_edges[:-1]

    # Calculate the
    gaussian_values = np.exp(-(ltilde_center**2) / (2 * sigma**2))
    gaussian_values_times_binsize = gaussian_values * ltilde_binsizes

    total = np.sum(gaussian_values_times_binsize)

    normalisation_factor = 1 / total

    return normalisation_factor


def gaussian_weight_density(ltilde):
    """
    Function to assign a gaussian weight factor to the position offset at L1
    """

    sigma = 0.4
    normalisation_factor = calculate_normalisation_factor(sigma)
    weight = normalisation_factor * np.exp(-(ltilde**2) / (2 * sigma**2))

    return weight


def generate_L1_area_offset_range_and_weights(log10normalized_stream_area, n_samples):
    """
    Function to generate an array of offsets and weights given a stream surface area. In our simulations the surface area is expressed in terms of the separation.
    """

    stream_surface_area = 10**log10normalized_stream_area

    sqrt_factor = 0.5 * np.sqrt(stream_surface_area)

    offset_edges = np.linspace(-sqrt_factor, sqrt_factor, n_samples + 1)

    offset_centers = (offset_edges[1:] + offset_edges[:-1]) / 2
    offset_binsizes = offset_edges[1:] - offset_edges[:-1]

    offset_centers_normalized = offset_centers / sqrt_factor
    offset_binsizes_normalized = offset_binsizes / sqrt_factor

    weights_densities = gaussian_weight_density(offset_centers_normalized)
    weights = weights_densities * offset_binsizes_normalized

    # normalise weights again:
    weights = weights / np.sum(weights)

    # print(offset_centers, offset_binsizes, weights, np.sum(weights))

    return (offset_centers, weights)


# if __name__ == "__main__":
#     import matplotlib.pyplot as plt

#     stream_surface_area = 0.01
#     n_samples = 25

#     offset_centers, weights = generate_L1_area_offset_range_and_weights(
#         stream_surface_area=stream_surface_area, n_samples=n_samples
#     )

#     plt.plot(offset_centers, weights)
#     plt.show()
