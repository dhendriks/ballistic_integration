"""
Function to filter the trajectories based on whether they intersect with those that accrete on the accretor
"""


def filter_trajectories_based_on_intersection(trajectory_set, settings):
    """
    Function to identify trajectories that intersect with trajectories from another class

    TODO: finish function
    """

    # Get the different sets
    accreting_onto_accretor_set = [
        trajectory
        for trajectory in trajectory_set
        if trajectory["result_dict"]["accretion_onto_accretor"] == 1
    ]
    # accreting_onto_donor_set = [ trajectory for trajectory in trajectory_set if trajectory['result_dict']['accretion_onto_donor'] == 1]

    # Loop over all the trajectories that supposedly accrete onto the accretor
    for trajectory in trajectory_set:
        if trajectory["result_dict"]["accretion_onto_donor"] == 1:
            print("yo")

    # Do some plotting to check things
    return trajectory_set
