"""
Function to calculate the roche lobe volume of the donor

1) calculate position donor
2) calculate values for L1 potential
3) For a grid of x, y values, calculate the z value where the solution is found for the given potential value
4) Use these grid points and a volume calculator to get the volume
5) Express the volume in terms of the standard RL_edd and create interpolation table

Roche lobe volume filling:
- find l1 and l1 potential value.
- make a grid of x and y values.
- bisect to find z values that fit the potential value there
- then we have all the coordinates, we can use that to run some volume calculator

TODO: Fix the script to calculate the donor roche lobe properties. Currently the accretor values are used..
TODO: our values don't match with sepinksies. I'm not sure why. kind of annoying.
"""

import time

import matplotlib.pyplot as plt
import numpy as np
import scipy
import scipy.spatial as ss
from ballistic_integrator.functions.frame_of_reference import (
    calculate_lagrange_points,
    calculate_position_donor,
    calculate_potential,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    calculate_ratio_RL_RLegg,
)

from ballistic_integration_code.settings import standard_system_dict


#######################
# Bisection functions
def bisect_x(x, settings, phi_l1):
    """
    Function to bisect the x value and find the correct opposite endpoint
    """

    phi_x = calculate_potential(
        settings=settings,
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        x_pos=x,
        y_pos=0,
    )

    return phi_x - phi_l1


def bisect_y(y, settings, x, phi_l1):
    """
    Function to bisect the x value and find the correct opposite endpoint
    """

    phi_y = calculate_potential(
        settings=settings,
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        x_pos=x,
        y_pos=y,
    )

    # print("bisect_y: x: {}, y: {}, phi_y: {}, phi_l1: {}, phi_y-phi_l1: {}".format(
    #     x, y, phi_y, phi_l1, phi_y - phi_l1
    #     )
    # )

    return phi_y - phi_l1


def bisect_z(z, settings, x, y, phi_l1):
    """
    Function to bisect the x value and find the correct opposite endpoint
    """

    phi_y = calculate_potential(
        settings=settings,
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        x_pos=x,
        y_pos=y,
        z_pos=z,
    )

    return phi_y - phi_l1


#######################
# other functions


def compare_sepinsky_rochelobe_volume(mass_donor, mass_accretor, f=1, e=0, nu=0):
    """
    Function to compare RL/RL_egg to the estimate from sepinsy.
    """

    ratio_RL_RLegg_sepinsky = calculate_ratio_RL_RLegg(
        mass_accretor=mass_accretor, mass_donor=mass_donor, f=f, e=e, nu=nu
    )

    return ratio_RL_RLegg_sepinsky


def rochelobe_radius_donor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the donor

    value is normalised by separation

    from FKR 4.6
    """

    top = 0.49 * (mass_donor / mass_accretor) ** (2.0 / 3.0)
    bottom = (0.6 * (mass_donor / mass_accretor) ** (2.0 / 3.0)) + np.log(
        1 + (mass_donor / mass_accretor) ** (1.0 / 3.0)
    )
    R2 = top / bottom

    return R2


def rochelobe_radius_accretor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the accretor

    value is normalised by separation

    from FKR 4.6 modified.
    """

    top = 0.49 * (mass_accretor / mass_donor) ** (2.0 / 3.0)
    bottom = (0.6 * (mass_accretor / mass_donor) ** (2.0 / 3.0)) + np.log(
        1 + (mass_accretor / mass_donor) ** (1.0 / 3.0)
    )
    R2 = top / bottom

    return R2


def calculate_radius_from_sphere_volume(volume):
    """
    Function to calculate the radius that a sphere of the same volume would have
    """

    return np.power((volume / (4 * np.pi / 3)), 1.0 / 3.0)


def calculate_volume_roche_lobe_information(
    system_dict, frame_of_reference, settings, plot_collection=False, print_info=False
):
    """
    Function to calculate the volume of the roche lobe given a system dict and a frame of reference
    """

    #####
    # Unpack settings
    xgrid_n = settings["volume_roche_lobe_xgrid_n"]
    ygrid_n = settings["volume_roche_lobe_ygrid_n"]
    size_factor = settings["volume_roche_lobe_size_factor"]

    volume_points = []

    print("For system {}".format(system_dict))

    ###############
    # Step 1: x-coordinate endpoints of the roche lobe

    ####
    # Get L1 point
    start = time.time()
    L_points = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    x_l1 = L_points["L1"][0]
    stop = time.time()

    ####
    # Get L1 equipotential value
    phi_l1 = calculate_potential(
        settings=settings,
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        x_pos=L_points["L1"][0],
        y_pos=L_points["L1"][1],
    )

    ####
    # Get opposite of L1 point on the line y=0
    x_don = calculate_position_donor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )[0]
    dx_don_l1 = np.abs(x_don - L_points["L1"][0])
    x_l1_opposite_approx = x_don - dx_don_l1

    ####
    # Bisect between x_don and x_l1_opposite_approx to find actual value
    x_l1_opposite = scipy.optimize.bisect(
        bisect_x, x_don, x_l1_opposite_approx, args=(settings, phi_l1)
    )

    print("donor x-location: {}".format(x_don))
    print("L1 x: {}".format(x_l1))
    print("dx_don_l1: {}".format(dx_don_l1))
    print("Opposite x: {}".format(x_l1_opposite))
    quit()

    ####
    # Span a grid of x-points between them (x-grid)
    x_grid = np.linspace(
        x_l1, x_l1_opposite, num=xgrid_n, endpoint=True, dtype=np.float64
    )

    ####
    # Add x-values to volume points
    volume_points.append([x_l1, 0, 0])
    volume_points.append([x_l1_opposite, 0, 0])

    # print("x_don: {} L1_x: {} dx_don_l1: {} x_l1_opposite_approx: {} x_l1_opposite: {}", x_don, L_points["L1"][0], dx_don_l1, x_l1_opposite_approx, x_l1_opposite)
    # print("x_grid: {}".format(x_grid))

    ###############
    # Step 2: Find all the y-points for the points in the x-grid
    x_y_slices = []
    y_bound_dict = {}

    ####
    # For each point in the x-grid (except for the endpoints) start a bisection search for the y-value in both directions
    for x_point in x_grid[1:-1]:

        #######################
        # Newly proposed method. increment y to find first point it exceeds phi_l1 and then do bisection.
        y_stepsize = dx_don_l1 / 20

        # find y bound where phi is higher than phi_l1
        y_bound = 0
        prev_y_bound = 0
        diff = -99

        while diff < 0:

            #
            phi_y = calculate_potential(
                settings=settings,
                system_dict=settings["system_dict"],
                frame_of_reference=settings["frame_of_reference"],
                x_pos=x_point,
                y_pos=y_bound,
            )

            diff = phi_y - phi_l1

            # print("x_point: {}, y_bound: {}, phi_y: {}, phi_l1: {}, phi_y-phi_l1: {}".format(
            #         x_point, y_bound, phi_y, phi_l1, phi_y - phi_l1
            #     )
            # )

            prev_y_bound = y_bound
            y_bound += y_stepsize

            # print(prev_y_bound, y_bound, phi_y, phi_l1, diff)

        # Store in dict as well
        y_bound_dict[x_point] = y_bound

        ####
        # Get negative y value
        neg_y_range = [-y_bound, 0]
        neg_y_point = scipy.optimize.bisect(
            bisect_y, neg_y_range[0], neg_y_range[1], args=(settings, x_point, phi_l1)
        )

        ####
        # Get positive y value
        pos_y_range = [0, y_bound]
        pos_y_point = scipy.optimize.bisect(
            bisect_y, pos_y_range[0], pos_y_range[1], args=(settings, x_point, phi_l1)
        )

        # #####################
        # # Old method

        # ####
        # # Get negative y value
        # neg_y_range = [-size_factor * dx_don_l1, 0]
        # neg_y_point = scipy.optimize.bisect(
        #     bisect_y, neg_y_range[0], neg_y_range[1], args=(settings, x_point, phi_l1)
        # )

        # ####
        # # Get positive y value
        # pos_y_range = [0, size_factor * dx_don_l1]
        # pos_y_point = scipy.optimize.bisect(
        #     bisect_y, pos_y_range[0], pos_y_range[1], args=(settings, x_point, phi_l1)
        # )

        ####
        # Store the endpoints
        volume_points.append([x_point, neg_y_point, 0])
        volume_points.append([x_point, pos_y_point, 0])

        # print("x_point: {} neg_y_point: {} pos_y_point: {}".format(x_point, neg_y_point, pos_y_point))

        ####
        # Save in list to do z-values
        x_y_slices.append([x_point, [neg_y_point, pos_y_point]])

    ###############
    # Go over all the x-values + corresponding y-endpoints
    for x_y_slice in x_y_slices:
        x_point = x_y_slice[0]

        y_endpoints = x_y_slice[1]
        y_grid = np.linspace(
            y_endpoints[0], y_endpoints[1], num=ygrid_n, endpoint=True, dtype=np.float64
        )

        ###############
        # Go over all the values in between the y-grid points
        for y_point in y_grid[1:-1]:

            #####################
            # Old method

            ####
            # Get negative z value
            neg_z_range = [-size_factor * dx_don_l1, 0]
            neg_z_point = scipy.optimize.bisect(
                bisect_z,
                neg_z_range[0],
                neg_z_range[1],
                args=(settings, x_point, y_point, phi_l1),
            )

            ####
            # Get positive z value
            pos_z_range = [0, size_factor * dx_don_l1]
            pos_z_point = scipy.optimize.bisect(
                bisect_z,
                pos_z_range[0],
                pos_z_range[1],
                args=(settings, x_point, y_point, phi_l1),
            )

            ####
            # Add to volume points
            volume_points.append([x_point, y_point, neg_z_point])
            volume_points.append([x_point, y_point, pos_z_point])

            # print("x_point: {} y_point: {} neg_z_point: {} pos_y_point: {}".format(x_point, y_point, neg_z_point, pos_z_point))

    ###########
    # Convert to array and unpack
    volume_points_array = np.array(volume_points, dtype=object)

    x_vals_volume_points = volume_points_array[:, 0]
    y_vals_volume_points = volume_points_array[:, 1]
    z_vals_volume_points = volume_points_array[:, 2]

    ###########
    # Plot collection of points
    if plot_collection:

        # Set up figure
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")

        # Plot scatter
        ax.scatter(
            x_vals_volume_points,
            y_vals_volume_points,
            z_vals_volume_points,
            c="r",
            marker="o",
        )

        # # Make up
        # ax.set_xticklabels([])  # y-axis
        # ax.set_yticklabels([])  # x-axis
        # ax.set_zticklabels([])  # y-axis
        # https://stackoverflow.com/questions/32705931/draw-3d-polygon-using-vertex-array-python-matplotlib

        plt.show()

    ###########
    # Calculate the volume of the roche lobe
    hull = ss.ConvexHull(volume_points_array)

    # Calculate volume
    radius_of_equal_volume_sphere = calculate_radius_from_sphere_volume(
        volume=hull.volume
    )

    # Calculate the eggleton roche lobe radius
    eggleton_roche_lobe_radius = rochelobe_radius_donor(
        mass_donor=system_dict["mass_donor"], mass_accretor=system_dict["mass_accretor"]
    )

    # Calculate ratio
    ratio_roche_lobe_radii = radius_of_equal_volume_sphere / eggleton_roche_lobe_radius

    ratio_sepinsky_eggleton = calculate_ratio_RL_RLegg(
        mass_accretor=system_dict["mass_accretor"],
        mass_donor=system_dict["mass_donor"],
        f=system_dict["synchronicity_factor"],
    )

    # Handling printing of info
    if print_info:
        print("For the given system dict: {}".format(system_dict))
        print("The volume of the roche lobe radius is: {}".format(hull.volume))
        print(
            "The radius of an equal-volume sphere is {}".format(
                radius_of_equal_volume_sphere
            )
        )
        print(
            "The eggleton roche lobe radius is: {}".format(eggleton_roche_lobe_radius)
        )
        print(
            "The ratio between the roche lobe radius with our calculation and the eggleton roche lobe radius is {}".format(
                ratio_roche_lobe_radii
            )
        )

        print("ratio according to sepinsky: {}".format(ratio_sepinsky_eggleton))

    return {
        "roche_lobe_volume": hull.volume,
        "roche_lobe_radius": radius_of_equal_volume_sphere,
        "roche_lobe_radius_eggleton": eggleton_roche_lobe_radius,
        "ratio_new_eggleton": ratio_roche_lobe_radii,
        "ratio_sepinsky_eggleton": ratio_sepinsky_eggleton,
    }


if __name__ == "__main__":
    # Set up system dict
    system_dict = {
        **standard_system_dict,
        "mass_donor": 1,
        "mass_accretor": 0.01,
        # "mass_accretor": 50,
        # "mass_accretor": 0.01,
        "synchronicity_factor": 0.2,
    }
    frame_of_reference = "center_of_mass"

    settings = {}
    settings["volume_roche_lobe_xgrid_n"] = 25
    settings["volume_roche_lobe_ygrid_n"] = 25
    settings["volume_roche_lobe_size_factor"] = 1.1
    settings["include_asynchronicity_donor_during_integration"] = True
    settings["system_dict"] = system_dict
    settings["frame_of_reference"] = frame_of_reference

    calculate_volume_roche_lobe_information(
        system_dict=system_dict,
        frame_of_reference=frame_of_reference,
        settings=settings,
        plot_collection=False,
        print_info=True,
    )
