"""
Function to plot the specific angular momemntum of the stream for accretion onto accretors
"""


import itertools
import json
import os

import astropy.constants as const
import astropy.units as u
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_donor,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import cm, colors
from scipy import interpolate

from ballistic_integration_code.paper_scripts.global_config import xlabel_qacc
from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins_and_bincenters,
)
from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    readout_dataset_and_query,
)
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_normalised_mass_stream_area,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def calculate_rochelobe_radius_accretor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the accretor

    value is normalised by separation

    from FKR 4.6 modified.
    """

    top = 0.49 * (mass_accretor / mass_donor) ** (2.0 / 3.0)
    bottom = (0.6 * (mass_accretor / mass_donor) ** (2.0 / 3.0)) + np.log(
        1 + (mass_accretor / mass_donor) ** (1.0 / 3.0)
    )
    R2 = top / bottom

    return R2


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


def create_bin_dict(array):
    """
    Function to create a dictionary with bin-info given an array of regularly spaced data
    """

    # sort and make unique
    unique_array = np.unique(array)
    sorted_array = np.sort(unique_array)

    # create bincenters
    # bin_centers = create_centers_from_bins(bins=sorted_array)
    bin_edges = create_bins_from_centers(centers=sorted_array)

    return {"centers": sorted_array, "edges": bin_edges}


def plot_specific_angmom_stream_interpolation(
    settings_file, result_file, use_interpolation=False, query=None, plot_settings=None
):
    """
    Function to plot the specific angular momentum of the accretor stream
    """

    if plot_settings is None:
        plot_settings = {}

    #################
    # Handle settings
    with open(settings_file, "r") as f:
        settings = json.loads(f.read())

    # construct radii grid
    specific_angular_momentum_stream_interpolation_lower_bound = settings[
        "specific_angular_momentum_stream_interpolation_lower_bound"
    ]
    specific_angular_momentum_stream_interpolation_upper_bound = settings[
        "specific_angular_momentum_stream_interpolation_upper_bound"
    ]
    specific_angular_momentum_stream_interpolation_num_radii = settings[
        "specific_angular_momentum_stream_interpolation_num_radii"
    ]

    specific_angmom_radius_location_values = np.linspace(
        specific_angular_momentum_stream_interpolation_lower_bound,
        specific_angular_momentum_stream_interpolation_upper_bound,
        specific_angular_momentum_stream_interpolation_num_radii,
    )

    # Construct translation dictionary
    colname_to_yval_dict = {
        "specific_angmom_interpolation_radius_{}".format(
            i
        ): specific_angmom_radius_location_values[i]
        for i in range(specific_angular_momentum_stream_interpolation_num_radii)
    }

    #################
    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
        exit_code_value=EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value,
    )

    # This plot only works if we fix these two parameters
    assert (
        len(result_df["synchronicity_factor"].unique()) == 1
    ), "Currently present unique synchronicity_factor values: {}".format(
        np.unique(result_df["synchronicity_factor"].to_numpy())
    )
    assert (
        len(result_df["log10normalized_thermal_velocity_dispersion"].unique()) == 1
    ), "Currently present unique log10normalized_thermal_velocity_dispersion values: {}".format(
        np.unique(result_df["log10normalized_thermal_velocity_dispersion"].to_numpy())
    )

    result_df = result_df.sort_values(by="massratio_accretor_donor")
    specific_angmom_cols = sorted(
        [
            col
            for col in result_df.columns
            if col.startswith("specific_angmom_interpolation_radius_")
        ],
        key=lambda x: x.split("_")[-1],
    )

    if use_interpolation:
        x_vals_list = []
        y_vals_list = []
        z_vals_list = []

        y_vals = np.linspace(0, 1.0, 100)

        # loop over result_df
        for row in result_df.to_dict(orient="records"):
            if row["fraction_onto_accretor"] == 0:
                continue

            #######
            # Fill the values with the stored data
            try:
                radius_vals = []
                z_vals = []

                for col in specific_angmom_cols:
                    radius_vals.append(colname_to_yval_dict[col])
                    z_vals.append(row[col] / row["h_min_acc"])

                radius_vals = np.array(radius_vals)
                z_vals = np.array(z_vals)

                #######
                # Add information of rmin

                # Convert rmin to be in units of the roche lobe radius of the accretor
                rochelobe_radius_accretor = calculate_rochelobe_radius_accretor(
                    mass_donor=1, mass_accretor=row["massratio_accretor_donor"]
                )
                rmin_in_terms_of_rochelobe_radius_accretor = (
                    row["rmin"] / rochelobe_radius_accretor
                )

                # slice off the interpolation radii that are below the rmin
                reverse = (radius_vals < rmin_in_terms_of_rochelobe_radius_accretor)[
                    ::-1
                ]
                slice_i = len(reverse) - np.argmax(reverse)

                sliced_radius_vals = radius_vals[slice_i:]
                sliced_z_vals = z_vals[slice_i:]

                # prepend the rmin information
                sliced_radius_vals = np.insert(
                    sliced_radius_vals, 0, rmin_in_terms_of_rochelobe_radius_accretor
                )
                sliced_z_vals = np.insert(sliced_z_vals, 0, 1.0)

                ######
                # construct interpolation table from this
                specific_angmom_interp = interpolate.interp1d(
                    sliced_radius_vals, sliced_z_vals, bounds_error=False
                )

                interp_z_vals = specific_angmom_interp(y_vals)

                #
                x_vals_list += list(
                    np.zeros(interp_z_vals.shape) + row["massratio_accretor_donor"]
                )
                y_vals_list += list(y_vals)
                z_vals_list += list(interp_z_vals)
            except ValueError:
                print("valueerror")
    else:

        ################
        # Construct the values
        x_vals_list = []
        y_vals_list = []
        z_vals_list = []

        #
        for specific_angmom_col in specific_angmom_cols:
            x_vals = result_df["massratio_accretor_donor"].to_numpy()
            y_vals = np.zeros(x_vals.shape) + colname_to_yval_dict[specific_angmom_col]
            z_vals = (
                result_df[specific_angmom_col].to_numpy()
                / result_df["h_min_acc"].to_numpy()
            )

            #########
            # turn into list and append
            x_vals_list += list(x_vals)
            y_vals_list += list(y_vals)
            z_vals_list += list(z_vals)

    #########
    # turn into numpy arrays again
    x_arrays = np.array(x_vals_list)
    y_arrays = np.array(y_vals_list)
    z_arrays = np.array(z_vals_list)

    ######
    # Create bin structures
    x_bin_dict = create_bin_dict(x_arrays)
    y_bin_dict = create_bin_dict(y_arrays)

    X, Y = np.meshgrid(x_bin_dict["centers"], y_bin_dict["centers"])

    ######
    # construct the histogram
    z_hist = np.histogram2d(
        x_arrays,
        y_arrays,
        bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
        weights=z_arrays,
    )

    z_hist_vals = z_hist[0]

    min_plot_value = z_hist_vals[z_hist_vals > 0].min()
    max_plot_value = z_hist_vals[z_hist_vals > 0].max()

    #
    norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)

    #####################
    # Construct plot

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax_plot = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    hist_data = ax_plot.hist2d(
        x_arrays,
        y_arrays,
        bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
        weights=z_arrays,
        antialiased=True,
        rasterized=True,
        norm=norm,
    )

    # make colorbar
    cbar = mpl.colorbar.ColorbarBase(
        ax_cb,
        cmap=mpl.cm.viridis,
        norm=norm,
    )

    # Draw contourlines
    contour_edgevals = np.linspace(min_plot_value, max_plot_value, 6)
    contour_levels = (contour_edgevals[1:] + contour_edgevals[:-1]) / 2
    contour_color = "orange"
    CS = ax_plot.contour(
        X, Y, hist_data[0].T, levels=contour_levels, colors=contour_color
    )
    # ax_plot.clabel(CS, fontsize=16, inline=True)

    # Draw the contour levels on the colorbar
    for contour_i, _ in enumerate(contour_levels):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour_levels[contour_i]] * 2,
            contour_color,
        )

    ####
    # Plot the radius of closest approach
    rochelobe_radii_accretor = calculate_rochelobe_radius_accretor(
        mass_donor=1, mass_accretor=result_df["massratio_accretor_donor"].to_numpy()
    )
    ax_plot.plot(
        result_df["massratio_accretor_donor"].to_numpy(),
        result_df["rmin"].to_numpy() / rochelobe_radii_accretor,
        color="red",
        linestyle="--",
        linewidth=8,
        label="Radius of closest approach",
    )

    ax_plot.set_xscale("log")

    xlabel = xlabel_qacc
    ylabel = (
        "Distance stream accretor\n"
        + r"$d_{\mathrm{stream-accretor}} / R_{\mathrm{Roche-Lobe,\ accretor}\,(q)}$"
    )
    zlabel = r"$h_{\mathrm{stream}}(d_{\mathrm{stream-accretor}}) / h_{\mathrm{stream}}(r_{\mathrm{min}})$"
    ax_plot.set_xlabel(xlabel, fontsize=plot_settings.get("label_fontsize", 24))
    ax_plot.set_ylabel(ylabel, fontsize=plot_settings.get("label_fontsize", 24))
    cbar.ax.set_ylabel(zlabel, fontsize=plot_settings.get("label_fontsize", 24))

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def generate_specific_angmom_stream_interpolation_plot(simdir, output_dir):
    """
    Handler function for the plotting
    """

    result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    settings_file = os.path.join(simdir, "settings.json")

    # plot
    basename = "specific_angular_momentum_of_the_stream.pdf"
    plot_specific_angmom_stream_interpolation(
        settings_file=settings_file,
        result_file=result_file,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(output_dir, basename),
            "label_fontsize": 42,
        },
        query="synchronicity_factor == 1. & log10normalized_thermal_velocity_dispersion == -3.",
        use_interpolation=True,
    )


def handle_paper_plot_specific_angmom_stream_interpolation(
    input_filename, output_name, show_plot, query_string
):
    """
    General function to handle the paper version of the stream interpolation
    """

    simdir = os.path.dirname(input_filename)
    settings_file = os.path.join(simdir, "settings.json")

    # plot
    plot_specific_angmom_stream_interpolation(
        settings_file=settings_file,
        result_file=input_filename,
        plot_settings={
            "show_plot": show_plot,
            "output_name": output_name,
            "label_fontsize": 42,
        },
        query=query_string,
        use_interpolation=True,
    )


def handle_paper_plot_specific_angmom_stream_interpolation_low_v_synchronous(
    input_filename, output_name, show_plot
):
    """
    General function to handle the paper version of the stream interpolation for low thermal velocity and synchronous
    """

    query_string = "synchronicity_factor == 1. & log10normalized_thermal_velocity_dispersion == -3."

    handle_paper_plot_specific_angmom_stream_interpolation(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


if __name__ == "__main__":

    #
    simdir = "/home/david/data_projects/ballistic_data/L1_stream/ballistic_stream_integration_results_stage_3_individual_trajectories_testing"

    result_root = "/home/david/data_projects/ballistic_data/L1_stream/server_results/"

    simdir = os.path.join(
        result_root,
        "ballistic_stream_integration_results_stage_3_INTER______________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    )

    result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    settings_file = os.path.join(simdir, "settings.json")

    # #

    # # Output dir
    # output_dir = os.path.join(
    #     os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    # )
    # output_dir = os.path.join(this_file_dir, "plots/")

    # # plot
    # basename = "specific_angular_momentum_of_the_stream.pdf"
    # plot_specific_angmom_stream_interpolation(
    #     settings_file=settings_file,
    #     result_file=result_file,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(output_dir, basename),
    #         "label_fontsize": 42,
    #     },
    #     use_interpolation=True,
    # )

    ##################################
    # Full simulation dataset

    simdir = os.path.join(
        result_root,
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION/",
    )
    result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    settings_file = os.path.join(simdir, "settings.json")

    # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # output_dir = os.path.join(this_file_dir, "plots/")

    # plot
    basename = "specific_angular_momentum_of_the_stream.pdf"
    plot_specific_angmom_stream_interpolation(
        settings_file=settings_file,
        result_file=result_file,
        plot_settings={
            "show_plot": False,
            "output_name": os.path.join(output_dir, basename),
            "label_fontsize": 42,
        },
        query="synchronicity_factor == 1.0 & log10normalized_thermal_velocity_dispersion == -3.",
        use_interpolation=True,
    )

    # # low-f set
    # simdir = os.path.join(
    #     result_root,
    #     "ballistic_stream_integration_results_stage_3_INTER_LOW_F_____________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    # )
    # result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    # settings_file = os.path.join(simdir, "settings.json")

    # #
    # plot_specific_angmom_stream_interpolation(
    #     settings_file=settings_file,
    #     result_file=result_file,
    #     plot_settings={"show_plot": True},
    #     use_interpolation=True,
    # )

    # # high-f set
    # simdir = os.path.join(
    #     result_root,
    #     "ballistic_stream_integration_results_stage_3_INTER_HIGH_F_____________OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    # )
    # result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    # settings_file = os.path.join(simdir, "settings.json")

    # #
    # plot_specific_angmom_stream_interpolation(
    #     settings_file=settings_file,
    #     result_file=result_file,
    #     plot_settings={"show_plot": True},
    #     use_interpolation=True,
    # )
