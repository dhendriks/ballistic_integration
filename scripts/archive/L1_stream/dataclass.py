"""
Script that contains the data class
"""

import json

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import colors

custom_mpl_settings.load_mpl_rc()


class trajectoryDataclass:
    """
    Trajectory data class object
    """

    def __init__(self, filename):
        self.filename = filename

        with open(filename, "r") as f:
            self.data = json.loads(f.read())

    def plot_trajectory():
        """
        Function to plot the trajectory of the particle
        """
