"""
Function to run the trajectories. Handles setting up the trajectory class, running the integration and processing the values
"""

import json
import os
import time

from ballistic_integrator.functions.functions import json_encoder

from ballistic_integration_code.scripts.L1_stream.functions.calculate_and_store_lagrange_points import (
    calculate_and_store_lagrange_points,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_and_store_roche_lobe_information import (
    calculate_and_store_roche_lobe_information,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    calculate_trajectory_result_summary,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    write_failed_passed_systems,
)
from ballistic_integration_code.scripts.L1_stream.functions.generate_full_trajectory_data_filename import (
    generate_full_trajectory_data_filename,
)
from ballistic_integration_code.scripts.L1_stream.functions.generate_result_dict import (
    generate_result_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.handle_plotting_at_gridpoint import (
    handle_plotting_at_gridpoint,
)
from ballistic_integration_code.scripts.L1_stream.functions.handle_trajectory_analysis_plot_at_gridpoint import (
    handle_trajectory_analysis_plot_at_gridpoint,
)
from ballistic_integration_code.scripts.L1_stream.functions.print_time_values import (
    print_time_values,
)
from ballistic_integration_code.scripts.L1_stream.functions.rerun_failed_systems_functions import (
    handle_plotting_at_re_run,
)
from ballistic_integration_code.scripts.L1_stream.functions.set_initial_location import (
    set_initial_location,
)
from ballistic_integration_code.scripts.L1_stream.functions.set_initial_velocity import (
    set_initial_velocity,
)
from ballistic_integration_code.scripts.L1_stream.functions.write_to_file import (
    write_to_file,
)
from ballistic_integration_code.scripts.L1_stream.integrator_subclass.subclass_trajectory_integrator import (
    trajectory_integrator_class,
)


def integrate_trajectories_from_L1_stage_1(
    settings,
):
    """
    Main function to run the trajectory calculator
    """

    # List to store set of trajectories
    trajectory_set = []

    #####
    # handle output filenames

    # Set output filenames
    result_dir = settings["result_dir"]
    trajectory_summary_data_filename = os.path.join(
        result_dir, "trajectory_summary_data.txt"
    )

    failed_systems_filename = os.path.join(result_dir, "failed_systems.txt")
    passed_systems_filename = os.path.join(result_dir, "passed_systems.txt")

    # Create directory of trajectories
    trajectory_result_dir = os.path.join(result_dir, "trajectory_data")
    os.makedirs(os.path.abspath(trajectory_result_dir), exist_ok=True)

    ################
    # Set initial location
    settings = set_initial_location(settings)

    ################
    # Set initial velocity
    settings = set_initial_velocity(settings)

    # Full trajectory filename
    full_trajectory_data_filename = generate_full_trajectory_data_filename(
        settings=settings,
        system_dict=settings["system_dict"],
        trajectory_result_dir=trajectory_result_dir,
    )

    #####
    # Handle evolution
    if settings.get("run", True):
        ##############
        # Integrate system
        parts_list = []
        start_general = time.time()

        # Set up the integrator
        start_integrator = time.time()
        integrator = trajectory_integrator_class(
            system_dict=settings["system_dict"],
            frame_of_reference=settings["frame_of_reference"],
            initial_position=settings["initial_position"],
            initial_velocity=settings["initial_velocity"],
            control_settings=settings,
        )

        # Integrate the trajectory
        result = integrator.evolve()

        stop_integrator = time.time()
        parts_list.append(
            {
                "name": "Trajectory integration",
                "time": stop_integrator - start_integrator,
            }
        )

        # Write trajectory to file
        if settings["write_full_output_to_file"]:
            with open(full_trajectory_data_filename, "w") as f:
                json.dump(result, f, default=json_encoder)

        ##############
        # Handle data extraction
        start_data_extraction = time.time()

        # Extract the summarized information from the trajectory
        result_summary = calculate_trajectory_result_summary(result)
        result_summary["weight"] = 1

        # Set up result dict that contains the results of the trajectories sampled around the L1 point for this specific grid-point
        result_dict = generate_result_dict(
            settings=settings,
            weight=1,
            result_summary=result_summary,
            grid_point=settings["grid_point"],
        )
        del result_dict["initial_position"]
        del result_dict["initial_velocity"]

        print("Stream orientation: {}".format(result_dict))

        # Store all the trajectory data s.t. we can go filter them
        trajectory_set.append(
            {"result_dict": result_dict, "result_summary": result_summary}
        )

        stop_data_extraction = time.time()
        parts_list.append(
            {
                "name": "Data extraction",
                "time": stop_data_extraction - start_data_extraction,
            }
        )

        ############
        # Handle storing lagrange points in the dict
        if settings["store_lagrange_point_info_in_gridpoint"]:
            start_lagrange_point = time.time()
            result_dict = calculate_and_store_lagrange_points(
                settings=settings, result_dict=result_dict
            )
            stop_lagrange_point = time.time()
            parts_list.append(
                {
                    "name": "Lagrange point calculation",
                    "time": stop_lagrange_point - start_lagrange_point,
                }
            )

        ############
        # Handle storing the roche lobe volume (and fraction)
        if settings["store_roche_lobe_volumes"]:
            start_roche_lobe = time.time()
            result_dict = calculate_and_store_roche_lobe_information(
                settings=settings, result_dict=result_dict
            )
            stop_roche_lobe = time.time()
            parts_list.append(
                {
                    "name": "Roche lobe calculation",
                    "time": stop_roche_lobe - start_roche_lobe,
                }
            )

        ##############
        # Handle plotting
        start_plotting = time.time()
        # Handle plotting of the system during re-run
        handle_plotting_at_re_run(
            settings=settings,
            system_dict=settings["system_dict"],
            result=result,
            full_trajectory_data_filename=full_trajectory_data_filename,
        )

        ###################
        # Handle plotting of the trajectory
        if settings["generate_plot_at_gridpoint"]:
            handle_plotting_at_gridpoint(
                settings=settings,
                grid_point=settings["grid_point"],
                trajectory_set=trajectory_set,
            )

        ###################
        # Handle plotting of the trajectory
        if settings["generate_trajectory_analysis_plot_at_gridpoint"]:
            handle_trajectory_analysis_plot_at_gridpoint(
                settings=settings,
                trajectory_set=trajectory_set,
            )
        stop_plotting = time.time()
        parts_list.append({"name": "Plotting", "time": stop_plotting - start_plotting})

        ##############
        # Handle writing
        start_writing = time.time()

        ############
        # Fractions of classes for accepted trajectories
        result_dict["fraction_onto_accretor"] = result_dict["accretion_onto_accretor"]
        result_dict["fraction_onto_donor"] = result_dict["accretion_onto_donor"]
        result_dict["fraction_undefined"] = result_dict["undefined_accretion"]

        # Write output to file
        write_to_file(settings, trajectory_summary_data_filename, result_dict)

        # Handle writing of failed or passed systems
        write_failed_passed_systems(
            grid_point=settings["grid_point"],
            result_summary=result_summary,
            settings=settings,
            failed_systems_filename=failed_systems_filename,
            passed_systems_filename=passed_systems_filename,
        )
        stop_writing = time.time()
        parts_list.append({"name": "Writing", "time": stop_writing - start_writing})

        stop_general = time.time()

        print_time_values(
            total_time=stop_general - start_general,
            parts_list=parts_list,
            gridpoint=settings["grid_point"],
        )
