"""
Routine to build a fit based on the public dataset of the rochelobe data

steps:
1: download dataset
2: load into pandas dataframe and modify columns to match desired structure
3: fit using some function
4: compare fit against original data
"""

import inspect
import os

import numpy as np
import pandas as pd
import wget

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def download_dataset(output_directory):
    """
    Function to download the dataset
    """
    os.makedirs(output_directory, exist_ok=True)

    #
    url = "https://zenodo.org/records/8256319/files/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv?download=1"

    if not os.path.isfile(
        os.path.join(
            output_directory, "RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"
        )
    ):
        wget.download(url, out=output_directory)


output_directory = os.path.join(this_file_dir, "results/")
download_dataset(output_directory=output_directory)


df = pd.read_csv(
    os.path.join(
        output_directory, "RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"
    ),
    skiprows=26,
    sep="\s+",
)

df["fsync"] = np.sqrt(df["A_factor"])
df["log10massratio_accretor_donor"] = np.log10(df["massratio_accretor_donor"])

#
unique_log10massratio_accretor_donor = np.unique(df["log10massratio_accretor_donor"])
unique_fsync = np.unique(df["fsync"])

num_unique_log10massratio_accretor_donor = len(unique_log10massratio_accretor_donor)
num_unique_fsync = len(unique_fsync)
ratio_RL_RLegg_grid = np.reshape(
    df["ratio_RL_RLegg"].to_numpy(),
    (num_unique_log10massratio_accretor_donor, num_unique_fsync),
)

#
X = df["log10massratio_accretor_donor"]
Y = df["fsync"]
Z = ratio_RL_RLegg_grid


def functional_form_fit(x, y):
    """
    Function to calculate the polynomial function in components
    """

    poly = [x * 0 + 1, x, y, x * y, x**2, y**2]

    return poly


def calculate_fit(x, y, coeffs):
    """
    Function to calculate the values of the fit
    """

    poly = np.array(functional_form_fit(x, y))
    values = np.sum(poly.T * coeffs, axis=1)

    return values


# Construct (we can remake this with sympy)
A = np.array(functional_form_fit(X, Y)).T
B = Z.flatten()

# Calculate coefficients
coeff, r, rank, s = np.linalg.lstsq(A, B)

# Calculate values of the fit and compare
fit_values = calculate_fit(X, Y, coeff)
fractional_difference = (fit_values - B) / B
abs_fractional_difference = np.abs(fractional_difference)
max_abs_fractional_difference = np.max(abs_fractional_difference)

print(
    "using {} function:\n\n{}".format(
        functional_form_fit.__name__, inspect.getsource(functional_form_fit)
    )
)
print("With coefficients {}".format(coeff))
print(
    "With the current fit the maximum absolute fractional error is {}".format(
        max_abs_fractional_difference
    )
)
