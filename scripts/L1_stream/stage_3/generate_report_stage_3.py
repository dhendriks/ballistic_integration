"""
Function to generate the simulation report for stage 3
TODO: i dont think we need this here
"""

import os

import pandas as pd


def generate_report_stage_3(data_file, report_file):
    """
    Function to generate the simulation report for stage 3
    """

    # Read out interpolation textfile
    df = pd.read_csv(data_file, sep="\s+", header=0)

    # # Store and save information to file
    # exit_code_count_string = df.groupby(["exit_code"])["exit_code"].count().__repr__()

    # #
    # with open(report_file, "w") as f:
    #     f.write(exit_code_count_string)

    # # print to screen
    # with open(report_file, "r") as f:
    #     print(f.read())


if __name__ == "__main__":
    input_filename = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_3",
        "trajectory_summary_data.txt",
    )
    report_file = "results/test_report_file.txt"
    generate_report_stage_3(data_file=input_filename, report_file=report_file)
