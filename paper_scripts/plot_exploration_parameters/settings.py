#################
# Global configuration
xlabel_dict = {
    "log10q_acc_don": r"$\mathrm{log}_{10}\left(\it{q}_{\mathrm{acc}}\right)$",
    "frac_sync_donor": r"$\it{f}_{\mathrm{sync}}$",
    "log10normalised_thermal_velocity": r"$\mathrm{log}_{10}\left(\it{v}_{\mathrm{thermal}}/\it{a \omega}\right)$",
    "log10normalized_L1_stream_area": r"$\mathrm{log}_{10}\left(\it{A}_{\mathrm{stream}}\right)$",
    "log10validity_factor_sepinsky_donor": r"$\mathrm{log}_{10}\left(\it{\eta}_{\mathrm{static}}\right)$",
}

ylabel_dict = {
    "log10validity_factor_sepinsky_donor": r"$\mathrm{d}/\mathrm{d}\mathrm{log}_{10}\left(\it{\eta}_{\mathrm{static}}\right)$",
    "log10normalised_thermal_velocity": r"$\mathrm{d}/\mathrm{d}\mathrm{log}_{10}\left(\it{v}_{\mathrm{thermal}}/\it{a}\omega\right)$",
    "log10q_acc_don": r"$\mathrm{d}/\mathrm{d}\mathrm{log}_{10}\left(\it{q}_{\mathrm{acc}}\right)$",
    "frac_sync_donor": r"$\mathrm{d}/\mathrm{d}\it{f}_{\mathrm{sync}}$",
}


#
label_dict = {"time": "Time spent", "mass": "Mass transferred", "count": "Count"}
colors = ["blue", "orange", "green", "red"]
hatch_list = ["/", "\\", "|", "-", "+", "x", "o", "O", ".", "*"]
fontsize_ylabel = 36
fontsize_xlabel = 36
