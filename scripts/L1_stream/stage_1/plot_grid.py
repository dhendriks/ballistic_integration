"""
Script containing functions to plot the grid of results of the integration
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integrator.functions.plot_functions import fkr, ulrich_kolb
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

custom_mpl_settings.load_mpl_rc()


def plot_grid(input_interpolation_textfile, plot_settings={}):
    """
    function to plot the grid of results
    """

    # Read out interpolation textfile
    result_df = pd.read_csv(input_interpolation_textfile, sep="\s+", header=0)
    result_df = result_df[result_df.exit_code == 7]

    #
    unique_synchronicity_factors = sorted(result_df["synchronicity_factor"].unique())

    #
    mass_accretor_array = 10 ** np.linspace(-2, 2, 100)
    mass_donor_array = np.ones(mass_accretor_array.shape)

    # get results # TODO: check if these are correct
    rcirc_ulrich = ulrich_kolb(mass_donor_array, mass_accretor_array)
    rcirc_fkr = fkr(mass_donor_array, mass_accretor_array)

    # Calculte mass ratio
    q_acc_don = mass_accretor_array / mass_donor_array
    result_df["rcirc"] = result_df["rmin"] * 1.7

    #
    fig, axes = plt.subplots(figsize=(16, 16))
    axes.plot(q_acc_don, rcirc_ulrich, "--", label="Ulrich \& Kolb")
    axes.plot(q_acc_don, rcirc_fkr, "--", label="FKR")

    # Plot results for each synchronicity factor
    cmap = plt.cm.get_cmap("viridis", len(unique_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    for i, synchronicity_factor in enumerate(unique_synchronicity_factors):
        filtered_df = result_df[result_df.synchronicity_factor == synchronicity_factor]
        axes.plot(
            filtered_df["massratio_accretor_donor"],
            filtered_df["rcirc"],
            "r",
            color=cmaplist[i],
            label="f = {}".format(synchronicity_factor),
        )

    axes.legend()
    axes.set_xscale("log")
    axes.set_yscale("log")
    axes.set_ylabel("Radius of closest approach r_min")
    axes.set_xlabel("massratio accretor/donor")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    input_interpolation_textfile = "/home/david/projects/binary_c_root/results/BALLISTIC/L1_stream/ballistic_stream_integration_results_stage_1/trajectory_summary_data.txt"
    plot_grid(input_interpolation_textfile, plot_settings={"show_plot": True})
