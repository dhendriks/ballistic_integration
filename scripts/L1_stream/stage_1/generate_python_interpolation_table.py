"""
Script containing function to convert the interpolation table file to the structure for binary_c

The read-out file contains the summarized results of the ballistic trajectories. instead of first saving the trajectory data we now just process that immediately
"""

import os

import pandas as pd
from ballistic_integrator.functions.integrator.trajectory_integrator_class import (
    EXIT_CODES,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)
from scipy.interpolate import RegularGridInterpolator

from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    get_git_info_and_time,
)


def generate_python_interpolation_table(input_interpolation_textfile):
    """
    Function to write the interpolation data as a header array
    """

    # Read out interpolation textfile
    df = pd.read_csv(input_interpolation_textfile, sep="\s+", header=0)

    # Add columns to df
    df["A_factor"] = A_formula(f=df["synchronicity_factor"])

    ############
    # Prepare for write-out

    # Filter the dataframe on those that have a correct exit code
    filtered_df = df[df.exit_code == EXIT_CODES.CUSTOM_ERROR.value]

    # Make sure the order is correct
    filtered_df = filtered_df[["A_factor", "massratio_accretor_donor", "rmin", "rcirc"]]

    interpolation_table = "x"

    return interpolation_table


if __name__ == "__main__":
    # Get input file
    input_filename = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_1",
        "trajectory_summary_data.txt",
    )

    # set output file
    if os.getenv("BINARY_C"):
        output_dir = os.path.join(os.getenv("BINARY_C"), "src", "RLOF")

    output_dir = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_1",
    )

    output_dir = "results/"

    #
    output_filename = os.path.join(
        output_dir, "RLOF_Hendriks2023_ballistic_stream_table.h"
    )
    os.makedirs(output_dir, exist_ok=True)

    stage_1_interpolation_table = generate_python_interpolation_table(
        input_interpolation_textfile=input_filename,
    )

    stage_1_interpolation_table
