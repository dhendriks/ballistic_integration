"""
Script that contains the function to plot the alpha vs f distribution
https://tex.stackexchange.com/questions/23487/how-can-i-get-roman-numerals-in-text
"""

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    add_labels_subplots,
    align_both_axes,
    show_and_save_plot,
)

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins_and_bincenters,
    create_figure_for_alpha_vs_synchronicity,
    readout_and_filter_exploration_ensemble,
)
from ballistic_integration_code.paper_scripts.plot_exploration_parameters.settings import (
    colors,
    hatch_list,
    label_dict,
    xlabel_dict,
)

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"
# mp.rcParams['text.usetex']=True
mpl.rcParams[
    "text.latex.preamble"
] = r"\makeatletter \newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@} \makeatother"


this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def capitalize_first(string):
    """
    Function to capitalise first letter of string
    """

    return string[0].upper() + string[1:]


def return_section_queries(x_parameter, y_parameter, left_y, right_y):
    """
    Function to return the queries for the sections
    """

    section_queries = {
        "1": f"{x_parameter} > {right_y} & {y_parameter} > 0",
        "2": f"{left_y} <= {x_parameter} <= {right_y} & {y_parameter} > 0",
        "3": f"{x_parameter} < {left_y} & {y_parameter} > 0",
        "4": f"{x_parameter} < {left_y} & {y_parameter} < 0",
        "5": f"{left_y} <= {x_parameter} <= {right_y} & {y_parameter} < 0",
        "6": f"{x_parameter} > {right_y} & {y_parameter} < 0",
    }

    return section_queries


def return_section_plot_properties(left_y, right_y):
    """
    Function to return some plot properties for the sections

    locations are in normal units (not transformed)
    """

    section_plot_properties = {
        "1": {"x": 2.5, "y": 4, "text": "I"},
        "2": {"x": 1, "y": 9, "text": "II"},
        "3": {"x": 0.5, "y": 4, "text": "III"},
        "4": {"x": 0.5, "y": -1, "text": "IV"},
        "5": {"x": 1, "y": -2, "text": "V"},
        "6": {"x": 2.5, "y": -1, "text": "VI"},
    }

    return section_plot_properties


def plot_sections(ax, left_y, right_y):
    """
    Function to add lines to indicate each section.

    on the x-axis (validity of static) we section off everything either above or below 1

    on the y-axis (synchronicity) we create a section around f=1 surrounded by the most nearby bin-edges that are not 1.
        and the other sections are left and right of that.
    """

    ################
    # Add lines
    line_config = dict(color="red", linestyle="dotted", linewidth=3)

    # x
    # margin = 5e-2
    # ax.axhline(0+margin, color='blue', linestyle='dotted', linewidth=2)
    # ax.axhline(0-margin, color='blue', linestyle='dotted', linewidth=2)
    ax.axhline(0, **line_config)

    # y
    ax.axvline(left_y, **line_config)
    ax.axvline(right_y, **line_config)

    ################
    # add symbols for sections
    box_configuration = {"pad": 0.4, "facecolor": "#e6b3cc", "boxstyle": "round"}

    # Plot all section text
    plot_properties = return_section_plot_properties(left_y, right_y)
    for section_key in sorted(plot_properties.keys()):
        section_properties = plot_properties[section_key]
        ax.text(
            section_properties["x"],
            section_properties["y"],
            section_properties["text"],
            bbox=box_configuration,
            horizontalalignment="center",
            verticalalignment="center",
            fontsize=14,
        )


def plot_sections_legend(ax, section_results, weight_type, weight_type_name_dict):
    """
    Function to add the sections legend
    """

    f = mticker.ScalarFormatter(useMathText=True)
    f.set_powerlimits((-3, 3))

    bbox_dict = {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"}

    plot_properties = return_section_plot_properties(left_y=0, right_y=0)
    section_legend_string = ""

    # print(section_results)
    for section in section_results.keys():
        total_percent_section = (
            section_results[section]["df"][weight_type_name_dict[weight_type]] * 100
        )

        #
        if total_percent_section < 0.01:
            formatted_total_percent_section = "< 0.01%"
        else:
            formatted_total_percent_section = f"{total_percent_section:2.2f}%"

        #
        section_text = plot_properties[section]["text"]
        section_string_text = f"{section_text}:"

        num_spaces = 12
        spaces_string = " " * (
            num_spaces - len(section_string_text) - len(formatted_total_percent_section)
        )

        #
        section_legend_string += "{}{}{}\n".format(
            section_string_text, spaces_string, formatted_total_percent_section
        )

    # clean
    section_legend_string = section_legend_string[:-1]

    # add to plot
    ax.text(
        0.9,
        0.7,
        section_legend_string,
        # horizontalalignment='left',
        horizontalalignment="right",
        verticalalignment="center",
        transform=ax.transAxes,
        bbox=bbox_dict,
        fontsize=14,
        fontdict={"family": "monospace"},
    )


def calculate_section_results(
    grouped_df,
    x_parameter,
    y_parameter,
    left_of_one,
    right_of_one,
    weight_type_name_dict,
):
    """
    Function to calculate the section results
    """

    #######################
    # Get the probabilities for all the sections

    # Calculate sums for sections
    weight_cols = [
        weight_type_name_dict["mass"],
        weight_type_name_dict["time"],
        weight_type_name_dict["count"],
    ]

    ########
    # normalize the columns first
    normalized_df = grouped_df.copy(deep=True)
    for column in weight_cols:
        normalized_df[column] = normalized_df[column] / normalized_df[column].sum()

    ########
    # Perform queries and sums
    section_queries = return_section_queries(
        x_parameter, y_parameter, left_y=left_of_one, right_y=right_of_one
    )
    section_results = {}
    for section, section_query in section_queries.items():
        queried_df = normalized_df.query(section_query)
        weight_columns_df = queried_df[
            [
                weight_type_name_dict["mass"],
                weight_type_name_dict["time"],
                weight_type_name_dict["count"],
            ]
        ]
        section_results[section] = {
            "df": weight_columns_df.sum(),
            "query": section_query,
        }
        print(section_results[section])

    ########
    # check if the total is good enough
    total = {"mass": 0, "time": 0}
    for section in section_results.keys():
        df = section_results[section]["df"]
        for weight_type in ["mass", "time"]:
            total[weight_type] += df[weight_type_name_dict[weight_type]]

    ########
    # assert if false
    margin = 1e-3
    for weight_type in total.keys():
        assert np.abs(total[weight_type] - 1) < margin

    return section_results


def plot_alpha_f_distributions(
    filename, orientation, extra_query=None, plot_settings=None
):
    """
    Function to generate a plot based on the unique values of these columns
    """

    if plot_settings is None:
        plot_settings = {}

    #
    x_parameter = "frac_sync_donor"
    y_parameter = "log10validity_factor_sepinsky_donor"

    # Set color normalisation
    norm = mpl.colors.LogNorm(
        vmax=1,
        vmin=10**-5,
    )

    # norm = mpl.colors.Normalize(
    #     vmax=1,
    #     vmin=10**-5,
    # )

    # Read out the data and filter
    (
        combined_df,
        _,
        _,
        weight_type_name_dict,
    ) = readout_and_filter_exploration_ensemble(filename, extra_query=extra_query)

    ######
    # Construct the figure
    fig, axes_dict = create_figure_for_alpha_vs_synchronicity(
        orientation=orientation, x_parameter=x_parameter, y_parameter=y_parameter
    )

    ax_mass = axes_dict["mass"]
    ax_time = axes_dict["time"]
    ax_cb = axes_dict["cb"]
    axes_list = [ax_mass, ax_time]

    #######################
    # Calculate quantities

    # construct aggregate dict
    parameters = [x_parameter, y_parameter]
    aggregate_dict = {parameter: "first" for parameter in parameters}
    for _, (_, weight_colname) in enumerate(weight_type_name_dict.items()):
        aggregate_dict[weight_colname] = "sum"

    # groupby and aggregate
    grouped_df = combined_df[parameters + list(weight_type_name_dict.values())]
    grouped_df = grouped_df.groupby(parameters, as_index=False).aggregate(
        aggregate_dict
    )
    grouped_df = grouped_df.sort_values(by=parameters)

    #
    x_bins, x_bincenters = calculate_bins_and_bincenters(grouped_df[x_parameter])
    y_bins, y_bincenters = calculate_bins_and_bincenters(grouped_df[y_parameter])
    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    # For the y-bins: get the 2 nearest bins around y!=1
    index_one = np.argmin(np.abs(y_bins - 1.0))
    left_of_one, right_of_one = y_bins[index_one - 1], y_bins[index_one + 1]

    #######################
    # Get the probabilities for all the sections
    section_results = calculate_section_results(
        grouped_df=grouped_df,
        x_parameter=x_parameter,
        y_parameter=y_parameter,
        left_of_one=left_of_one,
        right_of_one=right_of_one,
        weight_type_name_dict=weight_type_name_dict,
    )

    ##
    ## TODO: create method to loop over the types and handle them the same
    ##

    #######################
    # Plot mass

    # Calculate the results
    hist_mass, _, _ = np.histogram2d(
        grouped_df[x_parameter],
        grouped_df[y_parameter],
        bins=[x_bins, y_bins],
        weights=grouped_df[weight_type_name_dict["mass"]],
        density=True,
    )

    #
    _ = ax_mass.pcolormesh(
        X,
        Y,
        hist_mass.T,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #######################
    # Optional parts for the plot

    # Plot red dashed lines
    if plot_settings.get("plot_red_dashed_lines", True):
        ax_mass.axhline(0, linestyle="--", color="r", alpha=0.5)
        ax_mass.axvline(1, linestyle="--", color="r", alpha=0.5)

    # add text in-figure for weight-type
    if plot_settings.get("plot_weight_type_text", True):
        ax_mass.text(
            0.9,
            0.9,
            capitalize_first("mass transferred"),
            horizontalalignment="right",
            transform=ax_mass.transAxes,
            bbox={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        )

    # add section plot
    if plot_settings.get("plot_sections", True):
        plot_sections(ax_mass, left_y=left_of_one, right_y=right_of_one)

    # add section fraction legend
    if plot_settings.get("plot_sections_legend", True):
        # add the legend that contains the data to the plot
        plot_sections_legend(
            ax=ax_mass,
            section_results=section_results,
            weight_type="mass",
            weight_type_name_dict=weight_type_name_dict,
        )

    #######################
    # Plot time

    # Calculate the results
    hist_time, _, _ = np.histogram2d(
        grouped_df[x_parameter],
        grouped_df[y_parameter],
        bins=[x_bins, y_bins],
        weights=grouped_df[weight_type_name_dict["time"]],
        density=True,
    )

    #
    _ = ax_time.pcolormesh(
        X,
        Y,
        hist_time.T,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    #######################
    # Optional parts for the plot

    # Plot red dashed lines
    if plot_settings.get("plot_red_dashed_lines", True):
        ax_time.axhline(0, linestyle="--", color="r", alpha=0.5)
        ax_time.axvline(1, linestyle="--", color="r", alpha=0.5)

    # add text in-figure for weight-type
    if plot_settings.get("add_weight_type_text", True):
        ax_time.text(
            0.9,
            0.9,
            capitalize_first("time spent"),
            horizontalalignment="right",
            transform=ax_time.transAxes,
            bbox={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        )

    # add section plot
    if plot_settings.get("plot_sections", True):
        plot_sections(ax_time, left_y=left_of_one, right_y=right_of_one)

    # add section fraction legend
    if plot_settings.get("plot_sections_legend", True):
        # add the legend that contains the data to the plot
        plot_sections_legend(
            ax=ax_time,
            section_results=section_results,
            weight_type="time",
            weight_type_name_dict=weight_type_name_dict,
        )

    ##########
    # plot color bar
    cbar = mpl.colorbar.ColorbarBase(ax_cb, norm=norm, extend="min")
    cbar.ax.set_ylabel("Normalized weight")

    ##########
    # plot make up

    # Add labels to subplots
    add_labels_subplots(
        fig,
        axes_list,
        label_function_kwargs={
            "x_loc": 0.05,
            "bbox_dict": {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        },
    )

    #
    axes_list[0].set_xlim([0, 4])
    axes_list[1].set_xlim([0, 4])

    # Align axes
    align_both_axes(fig=fig, axes_list=axes_list)

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_alpha_f_distributions(input_filename, output_name, show_plot):
    """
    Function to handle the paper version of the plot that shows the alpha vs synchronicity parameter distribution
    """

    include_weight_type = ["mass", "time"]

    plot_alpha_f_distributions(
        input_filename,
        orientation="horizontal",
        plot_settings={
            "show_plot": show_plot,
            "output_name": output_name,
            "plot_red_dashed_lines": False,
        },
    )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    show_plot = False
    include_weight_type = ["mass", "time"]

    #
    result_root = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "RLOF/")

    # #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # plot_dir = os.path.join(this_file_dir, "plots/")

    # ######
    # # TESTING
    # filename = os.path.join(
    #     result_root,
    #     "TEST_RES_EXPLORATION_BALLISTIC_RLOF_2021_SANA/population_results/Z0.001/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # basename = "exploration_alpha_vs_synchronicity_test.pdf"
    # plot_alpha_f_distributions(
    #     filename,
    #     orientation="horizontal",
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # #####
    # # FIDUCIAL
    # filename = os.path.join(
    #     result_root,
    #     "server_results/EVENTS_V2.2.2_HIGH_RES_RLOF_2021_SANA_BALLISTIC_EXPLORATION/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # basename = "exploration_alpha_vs_synchronicity.pdf"
    # plot_alpha_f_distributions(
    #     filename,
    #     orientation="horizontal",
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    #####
    # testing new data
    show_plot = False
    # plot_dir = os.path.join(this_file_dir, "plots/")

    # filename = os.path.join(
    #     result_root,
    #     "server_results/EVENTS_V2.2.4_MID_RES_EXPLORATION_BALLISTIC_RLOF_2021_SANA/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # basename = "exploration_alpha_vs_synchronicity_test.pdf"
    # plot_alpha_f_distributions(
    #     filename,
    #     orientation="horizontal",
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # #####
    # # test
    # filename = os.path.join(
    #     result_root,
    #     "server_results/EVENTS_V2.2.4_MID_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # basename = "exploration_alpha_vs_synchronicity_MS.pdf"
    # plot_alpha_f_distributions(
    #     filename,
    #     orientation="horizontal",
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "plot_red_dashed_lines": False,
    #     },
    # )

    #####
    # test
    show_plot = False
    plot_dir = os.path.join(this_file_dir, "plots/")
    filename = os.path.join(
        result_root,
        "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )
    basename = "exploration_alpha_vs_synchronicity_MS_highres.pdf"
    plot_alpha_f_distributions(
        filename,
        orientation="horizontal",
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
            "plot_red_dashed_lines": False,
        },
    )
