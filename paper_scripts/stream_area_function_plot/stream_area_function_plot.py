"""
Function to plot some stuff about the mass stream area

TODO: consider plotting the stream diameter instead of area
TODO: stuff here can be optimised.
"""

import itertools
import os

import astropy.constants as const
import astropy.units as u
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_donor,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import add_label_subplot, show_and_save_plot
from matplotlib import cm, colors

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins_and_bincenters,
)
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_normalised_mass_stream_area,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def construct_contours(value_arr, fraction_levels):
    """
    Function to generate the
    """

    contourlevels = []

    #
    for fraction in fraction_levels:
        min_val = value_arr.min()
        max_val = value_arr.max()
        diff = max_val - min_val

        #
        contourlevel = min_val + fraction * diff

        #
        contourlevels.append(contourlevel)

    return contourlevels


def handle_calculation_normalised_stream_area(
    settings,
    synchronicity_factor,
    massratio_accretor_donor,
    log10normalized_thermal_velocity_dispersion,
):
    """
    Wrapper function that handles the calculation of the normalised stream area
    """

    # Set system dict
    settings["system_dict"] = {
        **standard_system_dict,
        "synchronicity_factor": synchronicity_factor,
        "mass_accretor": massratio_accretor_donor * standard_system_dict["mass_donor"],
    }

    # Set grid point
    settings["grid_point"] = {
        "massratio_accretor_donor": massratio_accretor_donor,
        "synchronicity_factor": synchronicity_factor,
        "log10normalized_thermal_velocity_dispersion": log10normalized_thermal_velocity_dispersion,
    }

    # Calculate stream
    normalised_mass_stream_area = calculate_normalised_mass_stream_area(settings)

    return normalised_mass_stream_area


def diameter_of_stream(normalised_mass_stream_area):
    """
    Function to calculate the diameter of the stream
    """

    diameter = 2 * np.sqrt(normalised_mass_stream_area / np.pi)

    return diameter


def calculate_a_over_v_grid_q_f(
    settings, f_range, q_range, normalized_velocity, verbose
):
    """
    Function to calculate a over v for a grid of q and f
    """

    all_results = []

    # Loop over parameters and yield the settings
    for parameters in itertools.product(
        f_range,
        q_range,
    ):

        # Unpack
        synchronicity_factor = parameters[0]
        massratio_accretor_donor = parameters[1]
        log10normalized_thermal_velocity_dispersion = np.log10(normalized_velocity)

        # calculate the stream area
        normalised_mass_stream_area = handle_calculation_normalised_stream_area(
            settings=settings,
            synchronicity_factor=synchronicity_factor,
            massratio_accretor_donor=massratio_accretor_donor,
            log10normalized_thermal_velocity_dispersion=log10normalized_thermal_velocity_dispersion,
        )

        # else:
        all_results.append(
            [
                synchronicity_factor,
                massratio_accretor_donor,
                normalised_mass_stream_area / normalized_velocity**2,
            ]
        )

        if verbose:
            print(
                [
                    synchronicity_factor,
                    massratio_accretor_donor,
                    normalised_mass_stream_area / normalized_velocity**2,
                ]
            )

    all_results = np.array(all_results)

    return all_results


def plot_area_over_velocity_squared(
    calculate_diameter_of_stream,
    settings,
    plot_settings,
    verbose,
    external_plot_dict=None,
):
    """
    Function to plot the mass stream area divided by velocity squared. this basically shows the geometry factor.
    """

    #################################
    # setting up range
    q_stepsize = settings["q_stepsize"]
    f_stepsize = settings["f_stepsize"]
    log10normalized_thermal_velocity_dispersion = settings[
        "log10normalized_thermal_velocity_dispersion"
    ]

    q_range = 10 ** np.arange(-2, 2 + q_stepsize, q_stepsize)
    f_range = np.arange(f_stepsize, 2 + f_stepsize, f_stepsize)
    normalized_velocity = 10**log10normalized_thermal_velocity_dispersion

    cmap = cm.coolwarm
    color_contour = "black"
    alpha_contour = 0.25

    xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
    ylabel = r"$f_{\mathrm{sync}}$"

    #################################
    # Calculate array of a over v for grid of q and f
    all_results_array = calculate_a_over_v_grid_q_f(
        settings=settings,
        f_range=f_range,
        q_range=q_range,
        normalized_velocity=normalized_velocity,
        verbose=verbose,
    )

    # Unpack and modify
    f_array = all_results_array[:, 0]
    q_array = all_results_array[:, 1]
    area_over_velocity_squared_array = all_results_array[:, 2]
    if plot_settings["use_log10"]:
        area_over_velocity_squared_array = np.log10(area_over_velocity_squared_array)

    # turn into diameter vs velocity
    if calculate_diameter_of_stream:
        area_over_velocity_squared_array = diameter_of_stream(
            normalised_mass_stream_area=area_over_velocity_squared_array
        )

    #
    contours = construct_contours(
        value_arr=area_over_velocity_squared_array,
        fraction_levels=[0.01, 0.10, 0.25, 0.50, 0.75],
    )
    linestyles = linestyle_list[: len(contours)]

    # reshape
    shape = (len(f_range), len(q_range))
    f_array_reshaped = f_array.reshape(shape)
    q_array_reshaped = q_array.reshape(shape)
    area_over_velocity_squared_array_reshaped = (
        area_over_velocity_squared_array.reshape(shape)
    )

    #
    x_values_reshaped = q_array_reshaped
    y_values_reshaped = f_array_reshaped
    z_values_reshaped = area_over_velocity_squared_array_reshaped

    unique_x = q_range
    unique_y = f_range

    ##########
    # Plot data

    if not external_plot_dict is None:
        ax_plot = external_plot_dict["ax_plot"]
        ax_cb = external_plot_dict["ax_cb"]
        fig = external_plot_dict["fig"]
    else:
        # Set up figure
        fig = plt.figure(figsize=(20, 12))
        fig.subplots_adjust(hspace=0.7)

        # Set up gridspec
        gs = fig.add_gridspec(nrows=1, ncols=9)

        ax_plot = fig.add_subplot(gs[:, :-1])
        ax_cb = fig.add_subplot(gs[:, -1:])

    # Set norm
    norm = colors.Normalize()

    # Plot the data
    _ = ax_plot.pcolormesh(
        x_values_reshaped,
        y_values_reshaped,
        z_values_reshaped,
        cmap=cmap,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # Plot the contour
    cs = ax_plot.contour(
        unique_x,
        unique_y,
        z_values_reshaped,
        levels=contours,
        linestyles=linestyles,
        colors=color_contour,
        alpha=alpha_contour,
    )
    ax_plot.clabel(cs, contours)

    ##########
    # plot color bar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, cmap=cmap, norm=norm)

    if calculate_diameter_of_stream:
        zlabel = r"$(D_{\mathrm{stream}} / a) / (v_{\mathrm{thermal}}/ a \omega)$"
    else:
        zlabel = r"$A_{\mathrm{stream}} / v_{\mathrm{thermal}}^{2}$"

    #
    if plot_settings["use_log10"]:
        zlabel = r"$\mathrm{log}_{10}$" + zlabel

    cbar.ax.set_ylabel(zlabel)

    # Plot contourlevels
    for contour_i, contour in enumerate(contours):
        cbar.ax.plot(
            [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
            [contour] * 2,
            color=color_contour,
            linestyle=linestyles[contour_i],
            alpha=alpha_contour,
        )

    ##########
    # plot make-up
    ax_plot.set_xlim([10**-2, 10**2])
    ax_plot.set_xscale("log")

    #
    ax_plot.set_ylabel(ylabel)
    ax_plot.set_xlabel(xlabel)

    #
    if external_plot_dict is None:
        fig.tight_layout()

        show_and_save_plot(fig, plot_settings)


def plot_area_versus_velocity(
    calculate_diameter_of_stream,
    settings,
    plot_settings,
    verbose,
    external_plot_dict=None,
):
    """
    Function to plot the stream area as a function of velocity, using the
    """

    ##########################
    # Configuration
    q_stepsize = settings["q_stepsize"]
    f_stepsize = settings["f_stepsize"]
    log10v_stepsize = settings["log10v_stepsize"]
    log10a_stepsize = settings["log10a_stepsize"]

    #
    q_range = 10 ** np.arange(-2, 2 + q_stepsize, q_stepsize)
    f_range = np.arange(f_stepsize, 2 + f_stepsize, f_stepsize)
    log10v_range = np.arange(-3, -0.5 + log10v_stepsize, log10v_stepsize)

    cmap = cm.coolwarm
    color_contour = "black"
    alpha_contour = 0.25

    #
    xlabel = r"$v_{\mathrm{thermal}} / a \omega$"

    if calculate_diameter_of_stream:
        ylabel = r"$D_{\mathrm{stream}} / a$"
    else:
        ylabel = r"$A_{\mathrm{stream}}$"

    #################
    # Get the value for q = 1 f = 1
    line_results = []
    for log10v in log10v_range:
        # Set the values and get the normalised stream area
        synchronicity_factor = 1
        massratio_accretor_donor = 1

        # Calculate normalised mass stream
        normalised_mass_stream_area = handle_calculation_normalised_stream_area(
            settings=settings,
            synchronicity_factor=synchronicity_factor,
            massratio_accretor_donor=massratio_accretor_donor,
            log10normalized_thermal_velocity_dispersion=log10v,
        )

        #
        if calculate_diameter_of_stream:
            line_results.append(
                diameter_of_stream(
                    normalised_mass_stream_area=normalised_mass_stream_area
                )
            )
        else:
            line_results.append(normalised_mass_stream_area)

    #################
    # Get the A results for all the combinations with a trick
    normalized_velocity = 10**-3
    all_results_ratio_array = calculate_a_over_v_grid_q_f(
        settings=settings,
        f_range=f_range,
        q_range=q_range,
        normalized_velocity=normalized_velocity,
        verbose=verbose,
    )

    area_over_velocity_squared_array = all_results_ratio_array[:, 2]
    # area_array = area_over_velocity_squared_array * (normalized_velocity**2)

    # if calculate_diameter_of_stream:
    #     print(area_array)
    #     area_array = diameter_of_stream(normalised_mass_stream_area=area_array)
    #     print(area_array)

    all_area_array = np.array([])
    all_velocity_array = np.array([])

    for log10v in log10v_range:
        area_array = area_over_velocity_squared_array * ((10**log10v) ** 2)

        if calculate_diameter_of_stream:
            area_array = diameter_of_stream(normalised_mass_stream_area=area_array)

        velocity_array = np.ones(area_array.shape) * (10**log10v)

        #
        all_area_array = np.concatenate((all_area_array, area_array))
        all_velocity_array = np.concatenate((all_velocity_array, velocity_array))

    ####################
    # set up bins
    all_log10velocity_array = np.log10(all_velocity_array)
    all_log10area_array = np.log10(all_area_array)

    # create bins
    log10v_bins, log10v_bincenters = calculate_bins_and_bincenters(log10v_range)
    log10area_bins = np.arange(
        all_log10area_array.min(), all_log10area_array.max(), log10a_stepsize
    )
    log10area_bincenter = (log10area_bins[1:] + log10area_bins[:-1]) / 2

    # Calculate the results
    hist_A, _, _ = np.histogram2d(
        all_log10velocity_array,
        all_log10area_array,
        bins=[log10v_bins, log10area_bins],
        density=True,
    )

    # Change everything that is not 0 to 1
    hist_A[hist_A != 0] = 1

    # Set norm
    norm = colors.Normalize()

    # # #
    # # contours = construct_contours(
    # #     value_arr=area_over_velocity_squared_array,
    # #     fraction_levels=[0.01, 0.10, 0.25, 0.50, 0.75],
    # # )
    # # linestyles = linestyle_list[: len(contours)]

    #
    X, Y = np.meshgrid(10**log10v_bins, 10**log10area_bins)

    ##########
    # Plot data
    if not external_plot_dict is None:
        ax_plot = external_plot_dict["ax_plot"]
        fig = external_plot_dict["fig"]
    else:
        # Set up figure
        fig = plt.figure(figsize=(20, 12))
        fig.subplots_adjust(hspace=0.7)

        # Set up gridspec
        gs = fig.add_gridspec(nrows=1, ncols=9)

        ax_plot = fig.add_subplot(gs[:, :-1])
        ax_cb = fig.add_subplot(gs[:, -1:])

    ###############
    # Plot the straight line
    ax_plot.plot(10**log10v_range, line_results, linestyle="--", color="k")

    # # Plot the data of the histogram
    # _ = ax_plot.pcolormesh(
    #     X,
    #     Y,
    #     hist_A.T,
    #     cmap=cmap,
    #     norm=norm,
    #     shading="auto",
    #     antialiased=True,
    #     rasterized=True,
    # )
    # log10v_bincenters
    # log10area_bincenter
    # Plot the contour
    _ = ax_plot.contourf(
        10**log10v_bincenters,
        10**log10area_bincenter,
        hist_A.T,
        # levels=contours,
        # linestyles=linestyles,
        # colors=color_contour,
        alpha=alpha_contour,
        levels=[0.9, 1.1],
        hatches=["-"],
        cmap="gray",
    )

    ##########
    # plot make-up
    # ax_plot.set_xlim([10**-2, 10**2])
    # ax_plot.set_xscale("log")

    #
    ax_plot.set_ylabel(ylabel)
    ax_plot.set_xlabel(xlabel)

    ax_plot.set_yscale("log")
    ax_plot.set_xscale("log")

    #
    if external_plot_dict is None:
        fig.tight_layout()

        show_and_save_plot(fig, plot_settings)


def plot_two_panel_geometry_and_stream_area(
    calculate_diameter_of_stream, settings, plot_settings, verbose
):
    """
    Main function to plot both the plots of the geometry effects as well as the stream area for the
    """

    panel_i = 0
    fontsize = plot_settings.get("panel_label_fontsize", 36)

    ################
    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=10)

    ax_versus = fig.add_subplot(gs[:, 0:4])
    ax_over = fig.add_subplot(gs[:, 5:9])
    ax_cb = fig.add_subplot(gs[:, 9:])

    # Call plot routine for relation A and v
    plot_area_versus_velocity(
        calculate_diameter_of_stream=calculate_diameter_of_stream,
        settings=settings,
        plot_settings=plot_settings,
        external_plot_dict={
            "ax_plot": ax_versus,
            "fig": fig,
        },
        verbose=verbose,
    )

    # L3
    fig, ax_versus = add_label_subplot(
        fig=fig,
        ax=ax_versus,
        label_i=panel_i,
        x_loc=0.85,
        y_loc=0.10,
        fontsize=fontsize,
        bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
    )
    panel_i += 1

    # Call plot routine for the ratio
    plot_area_over_velocity_squared(
        calculate_diameter_of_stream=calculate_diameter_of_stream,
        settings=settings,
        plot_settings=plot_settings,
        external_plot_dict={
            "ax_plot": ax_over,
            "ax_cb": ax_cb,
            "fig": fig,
        },
        verbose=verbose,
    )

    # L3
    fig, ax_over = add_label_subplot(
        fig=fig,
        ax=ax_over,
        label_i=panel_i,
        x_loc=0.85,
        y_loc=0.10,
        fontsize=fontsize,
        bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
    )
    panel_i += 1

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_two_panel_geometry_and_stream_area(output_name, show_plot):
    """
    Function to handle the paper version of the geometry and stream area plot
    """

    #
    factor = 1
    synchronicity_factor = 1
    massratio_accretor_donor = 1
    log10normalized_thermal_velocity_dispersion = -3

    # Construct settings
    settings = {
        "include_asynchronicity_donor_for_lagrange_points": True,
        "frame_of_reference": "center_of_mass",
    }

    # Set system dict
    settings["system_dict"] = {
        **standard_system_dict,
        "synchronicity_factor": synchronicity_factor,
        "mass_accretor": massratio_accretor_donor * standard_system_dict["mass_donor"],
    }

    # Set grid point
    settings["grid_point"] = {
        "massratio_accretor_donor": massratio_accretor_donor,
        "synchronicity_factor": synchronicity_factor,
        "log10normalized_thermal_velocity_dispersion": log10normalized_thermal_velocity_dispersion,
    }

    settings["q_stepsize"] = 0.025 * factor
    settings["f_stepsize"] = 0.025 * factor
    settings["log10a_stepsize"] = 0.025 * factor
    settings["log10v_stepsize"] = 0.025 * factor
    settings[
        "log10normalized_thermal_velocity_dispersion"
    ] = log10normalized_thermal_velocity_dispersion

    #
    plot_two_panel_geometry_and_stream_area(
        calculate_diameter_of_stream=True,
        settings=settings,
        plot_settings={
            "use_log10": False,
            "output_name": output_name,
            "show_plot": show_plot,
        },
        verbose=0,
    )


#
if __name__ == "__main__":
    plot_settings = {"show_plot": False, "use_log10": False}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    synchronicity_factor = 1
    massratio_accretor_donor = 1
    log10normalized_thermal_velocity_dispersion = -3

    # Construct settings
    settings = {
        "include_asynchronicity_donor_for_lagrange_points": True,
        "frame_of_reference": "center_of_mass",
    }

    # Set system dict
    settings["system_dict"] = {
        **standard_system_dict,
        "synchronicity_factor": synchronicity_factor,
        "mass_accretor": massratio_accretor_donor * standard_system_dict["mass_donor"],
    }

    # Set grid point
    settings["grid_point"] = {
        "massratio_accretor_donor": massratio_accretor_donor,
        "synchronicity_factor": synchronicity_factor,
        "log10normalized_thermal_velocity_dispersion": log10normalized_thermal_velocity_dispersion,
    }

    # #
    # settings["q_stepsize"] = 0.25
    # settings["f_stepsize"] = 0.25
    # settings["log10a_stepsize"] = 0.25
    # settings["log10v_stepsize"] = 0.25

    factor = 1
    settings["q_stepsize"] = 0.025 * factor
    settings["f_stepsize"] = 0.025 * factor
    settings["log10a_stepsize"] = 0.025 * factor
    settings["log10v_stepsize"] = 0.025 * factor
    settings[
        "log10normalized_thermal_velocity_dispersion"
    ] = log10normalized_thermal_velocity_dispersion

    #
    basename = "combined_area_plot.pdf"
    plot_two_panel_geometry_and_stream_area(
        calculate_diameter_of_stream=True,
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
        verbose=1,
    )

    # basename = "area_over_velocity_schematic.pdf"
    # plot_area_over_velocity_squared(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #         "use_log10": False,
    #     },
    # )

    # #
    # basename = "area_vs_velocity_marginalised_rest.pdf"
    # plot_area_versus_velocity(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )
