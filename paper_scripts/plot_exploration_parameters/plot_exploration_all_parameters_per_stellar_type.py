"""
Script to call the function to plot the distributions per stellar type
"""

import os

from ballistic_integration_code.paper_scripts.plot_exploration_parameters_1d.main_plot_function import (
    plot_alpha_distributions,
    plot_exploration_parameter_distributions,
    readout_and_filter_exploration_ensemble,
)

if __name__ == "__main__":

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = "plots/"

    # test res
    filename = os.path.join(
        os.getenv("BINARYC_DATA_ROOT"),
        "RLOF/TEST_RES_EXPLORATION_BALLISTIC_RLOF_2021_SANA/population_results/Z0.001/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )

    # # low res
    # filename = os.path.join(
    #     os.getenv("BINARYC_DATA_ROOT"),
    #     "RLOF/LOW_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.002/subproject_ballistic_ensemble_data.csv",
    # )

    # # Mid res and metallicities
    # filename = os.path.join(
    #     os.getenv("BINARYC_DATA_ROOT"),
    #     "RLOF/server_results/MID_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # filename = os.path.join(
    #     os.getenv("BINARYC_DATA_ROOT"),
    #     "RLOF/server_results/MID_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.004/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # filename = os.path.join(
    #     os.getenv("BINARYC_DATA_ROOT"),
    #     "RLOF/server_results/MID_RES_EXPLORATION_BALLISTIC_SANA/population_results/Z0.0008/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )

    # Get the df and see what we can do to loop over the stellar types
    # Read out the data and filter
    (
        df,
        count_df,
        parameter_cols,
        parameter_extent_dict,
    ) = readout_and_filter_exploration_ensemble(filename)
    preloaded_dict = {
        "df": df,
        "count_df": count_df,
        "parameter_cols": parameter_cols,
        "parameter_extent_dict": parameter_extent_dict,
    }

    # Loop over all stellar accretor types
    for stellar_type_accretor in df["st_acc"].unique():
        basename = "testing/exploration_parameter_ranges_st_acc_{}.pdf".format(
            stellar_type_accretor
        )
        plot_exploration_parameter_distributions(
            filename,
            stellar_type_accretor=stellar_type_accretor,
            pre_loaded_data=preloaded_dict,
            plot_settings={
                "show_plot": False,
                "output_name": os.path.join(plot_dir, basename),
            },
        )

    # Loop over all stellar donor types
    for stellar_type_donor in df["st_don"].unique():
        basename = "testing/exploration_parameter_ranges_st_don_{}.pdf".format(
            stellar_type_donor
        )
        plot_exploration_parameter_distributions(
            filename,
            stellar_type_donor=stellar_type_donor,
            pre_loaded_data=preloaded_dict,
            plot_settings={
                "show_plot": False,
                "output_name": os.path.join(plot_dir, basename),
            },
        )

    for stellar_type_accretor in df["st_acc"].unique():
        for stellar_type_donor in df["st_don"].unique():

            basename = (
                "testing/exploration_parameter_ranges_st_acc_{}_st_don_{}.pdf".format(
                    stellar_type_accretor, stellar_type_donor
                )
            )
            plot_exploration_parameter_distributions(
                filename,
                stellar_type_accretor=stellar_type_accretor,
                stellar_type_donor=stellar_type_donor,
                pre_loaded_data=preloaded_dict,
                plot_settings={
                    "show_plot": False,
                    "output_name": os.path.join(plot_dir, basename),
                },
            )

    # #
    # basename = "exploration_parameter_ranges.pdf"
    # basename_alpha = "exploration_parameter_ranges_alpha.pdf"

    # plot_exploration_parameter_distributions(
    #     filename,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )
    # plot_alpha_distributions(
    #     filename,
    #     plot_settings={
    #         "show_plot": False,
    #         "output_name": os.path.join(plot_dir, basename_alpha),
    #     },
    # )
