"""
Function to create an interpolation table for the lagrange point calculations that binary-c could use.

We set up a grid of log10 q_acc/don and f.

We calcalate the following properties:
- L1 point w.r.t donor (i.e. including asynchronicity)
- Roche-Lobe volume of donor
- Radius of equal-volume sphere of donor
- Ratio to eggleton's estimate for the roche lobe radius
"""

import itertools
import json
import os
import traceback

import numpy as np
import pandas as pd
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
    calculate_lagrange_points_sepinsky,
    calculate_ratio_RL_RLegg,
)

from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)


def generate_lagrange_point_and_roche_lobe_information(settings):
    """
    Function to create an interpolation table for the lagrange point calculations that binary-c could use
    """

    #
    output_filename = os.path.join(
        settings["result_dir"], settings["lagrange_point_data_file_basename"]
    )
    passed_gridpoint_filename = os.path.join(
        settings["result_dir"], settings["rochelobe_passed_gridpoint_file_basename"]
    )
    failed_gridpoint_filename = os.path.join(
        settings["result_dir"], settings["rochelobe_failed_gridpoint_file_basename"]
    )

    #
    synchronicity_factor = settings["system_dict"]["synchronicity_factor"]
    mass_donor = settings["system_dict"]["mass_donor"]
    mass_accretor = settings["system_dict"]["mass_accretor"]
    massratio_accretor_donor = mass_accretor / mass_donor

    # Set up gridpoint
    gridpoint = {
        "mass_donor": mass_donor,
        "mass_accretor": mass_accretor,
        "synchronicity_factor": synchronicity_factor,
    }
    if settings.get("verbosity", 0) > 1:
        print("Handling {}".format(gridpoint))

    # Catch failing gridpoints and write to file
    try:
        ###################
        # Calculate lagrange points for current q and f
        lagrange_points = calculate_lagrange_points_sepinsky(
            mass_accretor=mass_accretor,
            mass_donor=mass_donor,
            f=synchronicity_factor,
            original=False,
        )

        ######
        # Calculate Roche lobe information
        volume_roche_lobe_information = calculate_volume_roche_lobe_information(
            system_dict=settings["system_dict"],
            frame_of_reference=settings["frame_of_reference"],
            settings=settings,
            plot_collection=False,
            print_info=settings["verbosity"] > 2,
        )

        ###################
        # Construct output dictionary
        output_dict = {
            **gridpoint,
            #
            "L1_x": lagrange_points["L1"][0],
            **volume_roche_lobe_information,
        }

        output_dict["massratio_accretor_donor"] = (
            gridpoint["mass_accretor"] / gridpoint["mass_donor"]
        )
        output_dict["ratio_RL_RLegg"] = (
            volume_roche_lobe_information["roche_lobe_radius"]
            / volume_roche_lobe_information["roche_lobe_radius_eggleton"]
        )

        ###########
        # write to file
        with open(output_filename, "a+") as f:
            f.write(json.dumps(output_dict) + "\n")

        ###########
        # write passed gridpoint to file
        with open(passed_gridpoint_filename, "a+") as f:
            f.write(json.dumps(gridpoint) + "\n")

    except BaseException as exception:
        print("System {} failed".format(gridpoint))

        print(settings["system_dict"])
        print(settings)

        print("An exception occurred: {}".format(exception))
        print(f"Exception Name: {type(exception).__name__}")
        print(f"Exception Desc: {exception}")
        print(f"Exception traceback: {traceback.print_exc()}")

        ###########
        # write failed gridpoint to file
        with open(failed_gridpoint_filename, "a+") as f:
            f.write(json.dumps(gridpoint) + "\n")


if __name__ == "__main__":
    settings = {}

    output_dir = "results/"
    os.makedirs(output_dir, exist_ok=True)

    output_filename = os.path.join(
        output_dir, "lagrange_point_roche_lobe_information_output_file.txt"
    )
    generate_lagrange_interpolation_table(
        settings=settings,
    )
