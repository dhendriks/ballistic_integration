"""
Script to plot the lagrange point locations with certain lagrange point calculators
"""

import matplotlib.pyplot as plt

#
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

custom_mpl_settings.load_mpl_rc()

from ballistic_integrator.functions.lagrange_points.lagrange_points_binary_c import (
    calculate_lagrange_points_binary_c,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_hubova import (
    calculate_lagrange_points_hubova,
)

resolution = 0.01
massratios_accretor_donor = 10 ** np.arange(-1, 1, resolution)
mass_donor = 1


def calculate_all_Lagrange_points_binary_c(massratios_accretor_donor):
    """
    Function to calculate all the Lagrange points for a range of mass ratios of m_acc/m_don with the binary_c prescription
    """

    L1_locations = []
    L2_locations = []
    L3_locations = []

    for massratio_accretor_donor in massratios_accretor_donor:

        mass_accretor = massratio_accretor_donor * mass_donor

        # Calculate L points and store
        L_points = calculate_lagrange_points_binary_c(
            mass_accretor=mass_accretor, mass_donor=mass_donor, separation=1
        )

        L1_locations.append(L_points["L1"])
        L2_locations.append(L_points["L2"])
        L3_locations.append(L_points["L3"])

    L1_locations = np.array(L1_locations)
    L2_locations = np.array(L2_locations)
    L3_locations = np.array(L3_locations)

    return (L1_locations, L2_locations, L3_locations)


def calculate_all_Lagrange_points_hubova(massratios_accretor_donor):
    """
    Function to calculate all the Lagrange points for a range of mass ratios of m_acc/m_don with the binary_c prescription
    """

    L1_locations = []
    L2_locations = []
    L3_locations = []

    for massratio_accretor_donor in massratios_accretor_donor:

        mass_accretor = massratio_accretor_donor * mass_donor
        print(massratio_accretor_donor)
        # Calculate L points and store
        L_points = calculate_lagrange_points_hubova(
            mass_accretor=mass_accretor, mass_donor=mass_donor, separation=1, f=1
        )

        L1_locations.append(L_points["L1"])
        L2_locations.append(L_points["L2"])
        L3_locations.append(L_points["L3"])

    L1_locations = np.array(L1_locations)
    L2_locations = np.array(L2_locations)
    L3_locations = np.array(L3_locations)

    return (L1_locations, L2_locations, L3_locations)


# (
#     L1_locations_binary_c,
#     L2_locations_binary_c,
#     L3_locations_binary_c,
# ) = calculate_all_Lagrange_points_binary_c(massratios_accretor_donor)

(
    L1_locations_hubova,
    L2_locations_hubova,
    L3_locations_hubova,
) = calculate_all_Lagrange_points_hubova(massratios_accretor_donor)


# ##################
# # Set up figure
# fig = plt.figure(figsize=(18, 9))
# gs = fig.add_gridspec(nrows=1, ncols=9)

# ax = fig.add_subplot(gs[:, :])

# # Plot locations of L points: points
# ax.plot(
#     massratios_accretor_donor,
#     L1_locations_binary_c[:, 0],
#     "o",
#     label="L1 binary_c",
#     color="blue",
# )
# ax.plot(
#     massratios_accretor_donor,
#     L2_locations_binary_c[:, 0],
#     "o",
#     label="L2 binary_c",
#     color="orange",
# )
# ax.plot(
#     massratios_accretor_donor,
#     L3_locations_binary_c[:, 0],
#     "o",
#     label="L3 binary_c",
#     color="green",
# )

# # Plot locations of L points: thin lines
# ax.plot(
#     massratios_accretor_donor, L1_locations_binary_c[:, 0], linewidth=0.5, color="blue"
# )
# ax.plot(
#     massratios_accretor_donor,
#     L2_locations_binary_c[:, 0],
#     linewidth=0.5,
#     color="orange",
# )
# ax.plot(
#     massratios_accretor_donor, L3_locations_binary_c[:, 0], linewidth=0.5, color="green"
# )


# ax.set_xscale("log")
# ax.legend()
# plt.show()


##################
# Set up figure
fig = plt.figure(figsize=(18, 9))
gs = fig.add_gridspec(nrows=1, ncols=9)

ax = fig.add_subplot(gs[:, :])

# Plot locations of L points: points
ax.plot(
    massratios_accretor_donor,
    L1_locations_hubova[:, 0],
    "o",
    label="L1 hubova",
    color="blue",
)
ax.plot(
    massratios_accretor_donor,
    L2_locations_hubova[:, 0],
    "o",
    label="L2 hubova",
    color="orange",
)
ax.plot(
    massratios_accretor_donor,
    L3_locations_hubova[:, 0],
    "o",
    label="L3 hubova",
    color="green",
)

# Plot locations of L points: thin lines
ax.plot(
    massratios_accretor_donor, L1_locations_hubova[:, 0], linewidth=0.5, color="blue"
)
ax.plot(
    massratios_accretor_donor,
    L2_locations_hubova[:, 0],
    linewidth=0.5,
    color="orange",
)
ax.plot(
    massratios_accretor_donor, L3_locations_hubova[:, 0], linewidth=0.5, color="green"
)


ax.set_xscale("log")
ax.legend()
plt.show()
