"""
Global settings of the entire codebase
"""

settings = {
    # Some directory stuff
    # "output_root": "/home/david/Desktop/results_ballistic_L2/",
    # Some plotting stuff
    "plot_system_at_re_run": False
}

# some global standards
standard_system_dict = {
    "mass_donor": 1,
    "mass_accretor": 1,
    "separation": 1,
    "synchronicity_factor": 1,
    "eccentricity": 0,
    "mean_anomaly": 0,
}
