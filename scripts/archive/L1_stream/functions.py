"""
Script containing some extra functions for the L1 stream code
"""

import json
import os

import numpy as np
from ballistic_integrator.functions.functions import json_encoder
from ballistic_integrator.functions.integrator.trajectory_integrator_class import (
    trajectory_integrator,
)
from ballistic_integrator.functions.lagrange_points.calculate_lagrange_points import (
    calculate_lagrange_points,
)

from ballistic_integration_code import settings

settings.settings["output_root"] = "results/"


def main_from_L1(
    mass_accretor,
    mass_donor,
    separation,
    extra_settings={},
    custom_termination_routine=None,
):
    """
    Main function to run the trajectory calculator
    """

    ######
    # Initialise

    # Get L1 location
    L_points = calculate_lagrange_points(
        mass_donor, mass_accretor, separation=separation
    )
    l1_pos = L_points["L1"]

    # Set initial position
    initial_position_base = np.array(l1_pos[:2])
    initial_position_offset = np.array(
        extra_settings.get("initial_position_offset", [0.0, 0.0])
    )
    initial_r = initial_position_base + initial_position_offset

    # Set initial velocity
    initial_v = np.array(extra_settings.get("initial_velocity_offset", [0, 0]))

    #####
    # handle filename and directory
    result_dir = extra_settings["result_dir"]
    filename = "L1_q={}_x={}_v={}.json".format(
        mass_accretor / mass_donor,
        "{},{}".format(initial_r[0], initial_r[1]),
        "{},{}".format(initial_v[0], initial_v[1]),
    )
    full_filename = os.path.join(result_dir, filename)

    # Create directory
    if not os.path.isdir(os.path.abspath(result_dir)):
        os.makedirs(os.path.abspath(result_dir), exist_ok=True)

    #####
    # Handle evolution
    if extra_settings.get("run", True):
        # Set up the integrator
        integrator = trajectory_integrator(
            mass_accretor=mass_accretor,
            mass_donor=mass_donor,
            separation=separation,
            initial_position=initial_r,
            initial_velocity=initial_v,
            control_settings=extra_settings,
        )

        if extra_settings["use_custom_termination_routine"]:
            integrator.custom_termination_routine = extra_settings[
                "custom_termination_routine"
            ]

        # Calculate the trajectory
        result = integrator.evolve()

        with open(full_filename, "w") as f:
            json.dump(result, f, default=json_encoder)

    return full_filename
