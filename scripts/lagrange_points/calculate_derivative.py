import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import align_axes, show_and_save_plot
from matplotlib import colors
from scipy import interpolate

from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()
from scipy.interpolate import LinearNDInterpolator, RegularGridInterpolator


def load_df(filename):
    """
    Function to load the dataframe
    """

    #
    headerline = []
    datalines = []

    with open(filename, "r") as f:
        lineno = 0
        for line in f:
            if line.strip() and not line.startswith("//"):

                cleaned_line = line.strip().split()

                if lineno == 0:
                    headerline.append(cleaned_line)
                else:
                    values = [float(el) for el in cleaned_line]
                    datalines.append(values)

                lineno += 1

    df = pd.DataFrame(datalines, columns=headerline[0])

    df["f_sync"] = np.sqrt(df["A_factor"])

    return df


def calculate_derivatives(massratios, rochelobe_radii):
    """
    Function to calculate the derivatives
    """

    d_massratios = massratios[1:] - massratios[:-1]

    d_radii = rochelobe_radii[1:] - rochelobe_radii[:-1]

    d_radii_d_massratios = d_radii / d_massratios

    return d_radii_d_massratios


def calculate_derivative_and_comparison_to_synchronous(filename, plot_settings):
    """
    Function to calculate the derivative d(RL/a)/dq as function of synchronicity and compare it to the synchronous case
    """

    #
    df = load_df(filename)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(12, 24))
    gs = fig.add_gridspec(nrows=2, ncols=1)

    #
    ax_derivative = fig.add_subplot(gs[0, 0])
    ax_derivative_relative = fig.add_subplot(gs[1, 0])

    #############
    # Calculate sync
    df_ = df.query("f_sync=={}".format(1))
    df_ = df_.sort_values(by="massratio_accretor_donor")

    massratios = 1.0 / df_["massratio_accretor_donor"].to_numpy()
    roche_lobe_radii = df_["roche_lobe_radius"].to_numpy()

    d_radii_d_massratios_sync = calculate_derivatives(massratios, roche_lobe_radii)

    unique_fsyncs = df["f_sync"].unique()

    cmap = plt.cm.get_cmap("viridis", len(unique_fsyncs))

    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    for i, unique_fsync in enumerate(unique_fsyncs):
        df_ = df.query("f_sync=={}".format(unique_fsync))
        df_ = df_.sort_values(by="massratio_accretor_donor")

        massratios = 1.0 / df_["massratio_accretor_donor"].to_numpy()
        roche_lobe_radii = df_["roche_lobe_radius"].to_numpy()

        massratios_ = (massratios[1:] + massratios[:-1]) / 2

        d_radii_d_massratios = calculate_derivatives(massratios, roche_lobe_radii)
        d_radii_d_massratios_relative_to_sync = (
            d_radii_d_massratios / d_radii_d_massratios_sync
        )

        label = "f = {:1.1f}".format(unique_fsync)

        ax_derivative.plot(
            massratios_, d_radii_d_massratios, label=label, color=cmaplist[i]
        )
        ax_derivative_relative.plot(
            massratios_,
            d_radii_d_massratios_relative_to_sync,
            label="f_sync {}".format(unique_fsync),
            color=cmaplist[i],
        )

    ax_derivative.set_ylabel(r"d($R_{\mathrm{RL}}/a$)/d$q_{\mathrm{don}}$")
    ax_derivative.legend(ncol=2, loc=3)
    ax_derivative.set_yscale("log")
    ax_derivative.set_xscale("log")
    ax_derivative_relative.set_xscale("log")

    ax_derivative.set_title(r"d($R_{\mathrm{RL}}/a$)/d$q_{\mathrm{don}}$")
    ax_derivative_relative.set_title("derivative relative to sync")
    ax_derivative_relative.set_xlabel(r"Mass ratio $q_{\mathrm{don}}$")

    fig.subplots_adjust(hspace=0.2)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def calculate_logarithmic_derivative_and_comparison_to_synchronous(
    filename, plot_settings
):
    """
    Function to calculate the derivative d(RL/a)/dq as function of synchronicity and compare it to the synchronous case
    """

    #
    df = load_df(filename)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(12, 24))
    gs = fig.add_gridspec(nrows=2, ncols=1)

    #
    ax_derivative = fig.add_subplot(gs[0, 0])
    ax_derivative_relative = fig.add_subplot(gs[1, 0])

    #############
    # Calculate sync
    df_ = df.query("f_sync=={}".format(1))
    df_ = df_.sort_values(by="massratio_accretor_donor")

    massratios = 1.0 / df_["massratio_accretor_donor"].to_numpy()
    roche_lobe_radii = df_["roche_lobe_radius"].to_numpy()

    massratios_ = (massratios[1:] + massratios[:-1]) / 2
    roche_lobe_radii_ = (roche_lobe_radii[1:] + roche_lobe_radii[:-1]) / 2

    d_radii_d_massratios_sync = calculate_derivatives(massratios, roche_lobe_radii)
    d_ln_radii_d_ln_massratios_sync = d_radii_d_massratios_sync * (
        massratios_ / roche_lobe_radii_
    )

    unique_fsyncs = df["f_sync"].unique()

    cmap = plt.cm.get_cmap("viridis", len(unique_fsyncs))

    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    for i, unique_fsync in enumerate(unique_fsyncs):
        df_ = df.query("f_sync=={}".format(unique_fsync))
        df_ = df_.sort_values(by="massratio_accretor_donor")

        massratios = 1.0 / df_["massratio_accretor_donor"].to_numpy()
        roche_lobe_radii = df_["roche_lobe_radius"].to_numpy()

        massratios_ = (massratios[1:] + massratios[:-1]) / 2
        roche_lobe_radii_ = (roche_lobe_radii[1:] + roche_lobe_radii[:-1]) / 2

        d_radii_d_massratios = calculate_derivatives(massratios, roche_lobe_radii)
        d_ln_radii_d_ln_massratios = d_radii_d_massratios * (
            massratios_ / roche_lobe_radii_
        )

        d_ln_radii_d_ln_massratios_relative_to_sync = (
            d_ln_radii_d_ln_massratios / d_ln_radii_d_ln_massratios_sync
        )

        label = "f = {:1.1f}".format(unique_fsync)

        ax_derivative.plot(
            massratios_, d_ln_radii_d_ln_massratios, label=label, color=cmaplist[i]
        )
        ax_derivative_relative.plot(
            massratios_,
            d_ln_radii_d_ln_massratios_relative_to_sync,
            label="f_sync {}".format(unique_fsync),
            color=cmaplist[i],
        )

    ax_derivative.set_ylabel(r"dln($R_{\mathrm{RL}}/a$)/dln$q_{\mathrm{don}}$")
    ax_derivative.legend(ncol=2, loc=3)
    ax_derivative.set_yscale("log")
    ax_derivative.set_xscale("log")
    ax_derivative_relative.set_xscale("log")

    ax_derivative.set_title(r"dln($R_{\mathrm{RL}}/a$)/dln$q_{\mathrm{don}}$")
    ax_derivative_relative.set_title("logarithmic derivative relative to sync")
    ax_derivative_relative.set_xlabel(r"Mass ratio $q_{\mathrm{don}}$")

    fig.subplots_adjust(hspace=0.2)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def add_derivative_to_df(df):
    """
    Function to add the derivative to the df
    """

    # #
    # df = load_df(filename)

    df = df.sort_values(by=["f_sync", "massratio_accretor_donor"])

    unique_f_sync_values = df["f_sync"].unique()

    all_derivative_values = []

    ############
    # loop over unique synchronicity factors
    for unique_f_sync in unique_f_sync_values:
        # Select df
        cur_df = df.query("f_sync == {}".format(unique_f_sync))

        # extract data
        massratios = cur_df["massratio_accretor_donor"].to_numpy()
        roche_lobe_radii = cur_df["roche_lobe_radius"].to_numpy()

        ##########
        # calculate dR/dq at the centers of the mass-ratios (it takes the input as edges, and calculates the derivatives in the centers)
        d_roche_lobe_radii_d_q_acc_don = calculate_derivatives(
            massratios=massratios, rochelobe_radii=roche_lobe_radii
        )
        center_massratios = (massratios[1:] + massratios[:-1]) / 2

        # print("at massratios: {}".format(center_massratios))
        # print("derivatives: {}".format(d_roche_lobe_radii_d_q_acc_don))

        # create interpolation table to reconstruct it at the correct massratios
        interpolator = interpolate.interp1d(
            center_massratios,
            d_roche_lobe_radii_d_q_acc_don,
            bounds_error=False,
            fill_value="extrapolate",
        )
        derivative_values = interpolator(massratios)

        # print("at massratios: {}".format(massratios))
        # print("derivatives: {}".format(derivative_values))

        all_derivative_values += list(derivative_values)

    # Load into df
    df.loc[:, "d_roche_lobe_radii_d_q_acc_don"] = all_derivative_values

    return df


def create_roche_lobe_derivative_interpolator_dict(df):
    """
    Function to create interpolators for the roche-lobe radius and the derivative of the roche lobe radius as functions of q and f
    """

    ##########
    # Sort values
    df = df.sort_values(by=["f_sync", "massratio_accretor_donor"])

    ##########
    # Extract values
    f_sync_values = df["f_sync"].to_numpy()
    massratio_accretor_donor_values = df["massratio_accretor_donor"].to_numpy()

    roche_lobe_radius_values = df["roche_lobe_radius"].to_numpy()
    d_roche_lobe_radii_d_q_acc_don_values = df[
        "d_roche_lobe_radii_d_q_acc_don"
    ].to_numpy()

    # for fsync, q, rl in zip(f_sync_values, massratio_accretor_donor_values, roche_lobe_radius_values):
    #     print(fsync, q, rl)

    ##########
    # Determine shape
    unique_f_sync_values = np.unique(f_sync_values)
    unique_massratio_accretor_donor_values = np.unique(massratio_accretor_donor_values)
    newshape = (len(unique_f_sync_values), len(unique_massratio_accretor_donor_values))

    # Set up RL radius calculator
    roche_lobe_radius_values_mesh = np.reshape(roche_lobe_radius_values, newshape)
    roche_lobe_radius_interpolator = RegularGridInterpolator(
        (unique_f_sync_values, unique_massratio_accretor_donor_values),
        roche_lobe_radius_values_mesh,
        bounds_error=False,
        fill_value=None,
    )

    # Set up derivative calculator
    d_roche_lobe_radii_d_q_acc_don_values_mesh = np.reshape(
        d_roche_lobe_radii_d_q_acc_don_values, newshape
    )
    d_roche_lobe_radii_d_q_acc_don_interpolator = RegularGridInterpolator(
        (unique_f_sync_values, unique_massratio_accretor_donor_values),
        d_roche_lobe_radii_d_q_acc_don_values_mesh,
        bounds_error=False,
        fill_value=None,
    )

    # set up interpolator dict
    interpolator_dict = {
        "roche_lobe_radius_interpolator": roche_lobe_radius_interpolator,
        "d_roche_lobe_radii_d_q_acc_don_interpolator": d_roche_lobe_radii_d_q_acc_don_interpolator,
    }

    #######
    # lets test the interpolators
    num_tests = 5
    for _ in range(num_tests):
        rand_i = np.random.randint(len(f_sync_values))
        test_val_f_sync = f_sync_values[rand_i]
        test_val_massratio_accretor_donor = massratio_accretor_donor_values[rand_i]
        test_val_roche_lobe_radius = roche_lobe_radius_values[rand_i]
        test_val_d_roche_lobe_radii_d_q_acc_don = d_roche_lobe_radii_d_q_acc_don_values[
            rand_i
        ]

        test_coords = (test_val_f_sync, test_val_massratio_accretor_donor)
        interpolated_val_roche_lobe_radius = roche_lobe_radius_interpolator(test_coords)
        interpolated_val_d_roche_lobe_radii_d_q_acc_don = (
            d_roche_lobe_radii_d_q_acc_don_interpolator(test_coords)
        )

        # print(test_coords)
        # print(test_val_roche_lobe_radius, test_val_d_roche_lobe_radii_d_q_acc_don)
        # print(interpolated_val_roche_lobe_radius, interpolated_val_d_roche_lobe_radii_d_q_acc_don)

    return interpolator_dict


if __name__ == "__main__":
    import os

    #
    filename = "results/test/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"

    this_file = os.path.abspath(__file__)
    this_file_dir = os.path.dirname(this_file)

    #
    df = load_df(filename=filename)
    df = add_derivative_to_df(df=df)

    #
    interpolator_dict = create_roche_lobe_derivative_interpolator_dict(df=df)

    rl_int = interpolator_dict["roche_lobe_radius_interpolator"]
    # print(rl_int((1.4, 0.01)))

    # interpolator_dict(1.4, 0.01)


# (1.4, 0.01)
# 0.618761 -2.1070563627241015
# 0.618761 -2.1070563627241015
# [Finished in 1.7s]


# print(df)
# quit()

# #
# calculate_derivative_and_comparison_to_synchronous(
#     filename=filename,
#     plot_settings={
#         "show_plot": False,
#         "output_name": os.path.join(this_file_dir, "plots/derivative_analysis.pdf"),
#     },
# )

# #
# calculate_logarithmic_derivative_and_comparison_to_synchronous(
#     filename=filename,
#     plot_settings={
#         "show_plot": False,
#         "output_name": os.path.join(
#             this_file_dir, "plots/logarithmic_derivative_analysis.pdf"
#         ),
#     },
# )
