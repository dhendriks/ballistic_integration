"""
Routine that handles selecting the trajectory categories based on a trajectory set
"""


def select_trajectory_categories(trajectory_set):
    """
    Routine that handles selecting the trajectory categories based on a trajectory set
    """

    # Separate trajectory sets:
    accretor_trajectory_list = [
        el["result_summary"]
        for el in trajectory_set
        if el["result_dict"]["accretion_onto_accretor"] == 1
    ]
    donor_trajectory_list = [
        el["result_summary"]
        for el in trajectory_set
        if el["result_dict"]["accretion_onto_donor"] == 1
    ]
    undefined_trajectory_list = [
        el["result_summary"]
        for el in trajectory_set
        if el["result_dict"]["undefined_accretion"] == 1
    ]
    terminated_trajectory_list = [
        el["result_summary"]
        for el in trajectory_set
        if el["result_dict"]["terminated"] == 1
    ]
    rejected_trajectory_list = [
        el["result_summary"]
        for el in trajectory_set
        if el["result_dict"]["rejected_based_on_intersection"] == 1
    ]

    #
    accretor_trajectory_set = {
        "name": "onto_accretor",
        "trajectory_list": accretor_trajectory_list,
        "plot_kwargs": {"label": "Onto accretor", "color": "red"},
    }
    donor_trajectory_set = {
        "name": "onto_donor",
        "trajectory_list": donor_trajectory_list,
        "plot_kwargs": {"label": "Onto donor", "color": "blue"},
    }
    undefined_trajectory_set = {
        "name": "undefined",
        "trajectory_list": undefined_trajectory_list,
        "plot_kwargs": {"label": "Undefined", "color": "pink"},
    }
    terminated_trajectory_set = {
        "name": "terminated",
        "trajectory_list": terminated_trajectory_list,
        "plot_kwargs": {"label": "Terminated", "color": "orange"},
    }
    rejected_trajectory_set = {
        "name": "rejected",
        "trajectory_list": rejected_trajectory_list,
        "plot_kwargs": {"label": "Rejected", "color": "purple"},
    }

    #
    trajectory_sets = [
        accretor_trajectory_set,
        donor_trajectory_set,
        undefined_trajectory_set,
        terminated_trajectory_set,
        rejected_trajectory_set,
    ]

    return trajectory_sets
