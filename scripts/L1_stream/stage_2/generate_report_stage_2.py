"""
Function to generate the report of the summarized results of the stage 1
"""

import os

import pandas as pd

from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    filter_df_on_exit_codes,
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    EXIT_CODE_MESSAGE_DICT,
    EXIT_CODES,
)
from ballistic_integration_code.scripts.L1_stream.stage_2.stage_settings import (
    stage_2_settings,
)


def generate_report_stage_2(data_file, report_file):
    """
    Function to generate the report of the datafile. analyses how much of the trajectories are actually succesfull and analyzed.
    """

    #
    total_count = 0
    total_count_passed = 0
    total_count_failed = 0

    # Read out interpolation textfile
    df = pd.read_csv(data_file, sep="\s+", header=0)

    # Function to find systems or lines with error codes
    _, _, passed = filter_df_on_exit_codes(
        df, allowed_exit_codes=stage_2_settings["allowed_exit_codes"]
    )

    # Get git info
    git_info = get_git_info_and_time()

    # Tally totals
    for _, row in df.iterrows():
        # tally total succesful and total failed
        total_count += 1

        if row["exit_code"] in stage_2_settings["allowed_exit_codes"]:
            total_count_passed += 1
        else:
            total_count_failed += 1

    # Write outputs to file
    with open(report_file, "w") as f:
        ###############
        # Write top-level explanation
        f.write(
            "// Ballistic stream trajectory integration stage 1 unfiltered dataset. This dataset aims to provide an interpolation dataset to determine rmin and racc for the stream based on the mass ratio q and the A factor that captures synchronicity, eccentricty and mean anomaly (see sepinksy 2007), to improve RLOF calculations and estimates for interactions and specifically for accretion disks.\n\n"
        )

        # Write git revision information
        f.write(
            "// Generated on: {} with git repository: {} branch: {} commit: {}\n\n".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        # Found not-allowed exit codes
        f.write("Passed the exit-code test: {}\n\n".format(passed))

        # Print the exit code, the count, the name and the message
        for exit_code, count in (
            df.groupby(["exit_code"])["exit_code"].count().iteritems()
        ):
            exit_code_string = "Exit code: {} count: {} name: {} message: {}".format(
                exit_code,
                count,
                EXIT_CODES(exit_code).name,
                EXIT_CODE_MESSAGE_DICT[exit_code],
            )
            f.write(exit_code_string + "\n")

        # Write total
        f.write(
            "Total: {} (passed: {} failed: {})".format(
                total_count, total_count_passed, total_count_failed
            )
        )

    # print to screen
    print("\n\n##############################################")
    print("# Report")
    with open(report_file, "r") as f:
        print(f.read())
    print("##############################################\n\n")


if __name__ == "__main__":
    input_filename = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_1",
        "trajectory_summary_data.txt",
    )
    report_file = "results/test_report_file.txt"
    generate_report_stage_2(data_file=input_filename, report_file=report_file)
