"""
Script containing the function to calculate the velocity offset at L1 due to soundspeed of material
"""

import numpy as np


def calculate_velocity_offset_L1_soundspeed(settings):
    """
    Function to calculate the velocity offset at L1 due to soundspeed of material

    For now we have this velocity entirely in the x-direction
    """

    if "log10normalized_thermal_velocity_dispersion" in settings["grid_point"]:
        velocity_offset = np.array(
            [
                10
                ** settings["grid_point"][
                    "log10normalized_thermal_velocity_dispersion"
                ],
                0,
            ]
        )
    else:
        velocity_offset = np.array([0, 0])

    return velocity_offset
