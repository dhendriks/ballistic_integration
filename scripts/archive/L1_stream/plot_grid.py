"""
Script containing functions to plot the grid of results of the integration
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integration_codes.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    readout_resultfile_and_calculate_trajectory_result_summary,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import add_plot_info, show_and_save_plot

custom_mpl_settings.load_mpl_rc()


def plot_grid(result_dir):
    """
    function to plot the grid of results
    """

    # get all datafiles
    datafiles = []
    for file in os.listdir(result_dir):
        if file.endswith(".json"):
            datafiles.append(os.path.join(result_dir, file))

    # sort datafiles on mass ratio
    q_dict = {}
    for file in datafiles:
        split = file.split("q=")[-1].split("_x=")[0]
        q_val = float(split)
        q_dict[q_val] = file

    # get information out of datafiles
    combined_results = {
        "mass_accretor_list": [],
        "mass_donor_list": [],
        "min_distance_to_accretor_list": [],
    }

    result_list = []

    for q in sorted(q_dict):
        filename = q_dict[q]

        # Plot the trajectory to check
        # plot_resultfile(file, plot_settings={"show_plot": True})

        # Readout data and find closest file
        result_dict = readout_resultfile_and_calculate_trajectory_result_summary(
            filename
        )
        result_list.append(result_dict)

    # Convert to pandas and convert to
    df = pd.DataFrame(result_list)

    df["massratio_accretor_donor"] = df["mass_accretor"] / df["mass_donor"]

    plot_settings = {"show_plot": True}

    #
    mass_donor = 1
    mass_accretor_array = 10 ** np.linspace(-2, 2, 100)
    mass_donor_array = np.ones(mass_accretor_array.shape)

    # get results
    res_ulrich = ulrich_kolb(mass_donor_array, mass_accretor_array)
    res_fkr = fkr(mass_donor_array, mass_accretor_array)

    # Calculte mass ratio
    q_acc_don = mass_accretor_array / mass_donor_array

    #
    fig, axes = plt.subplots(figsize=(16, 16))
    axes.plot(q_acc_don, res_ulrich, label="Ulrich \& Kolb")
    axes.plot(q_acc_don, res_fkr, label="FKR")
    axes.plot(df["massratio_accretor_donor"], df["min_distance_to_accretor"], "ro")

    axes.legend()
    axes.set_xscale("log")
    axes.set_yscale("log")
    axes.set_ylabel("Radius of closest approach r_min")
    axes.set_xlabel("massratio accretor/donor")

    # Add info and plot the figure
    fig = add_plot_info(fig, plot_settings)
    show_and_save_plot(fig, plot_settings)


plot_grid("results/main")
