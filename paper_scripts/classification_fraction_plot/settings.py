xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
y_label_dict = {
    "synchronicity_factor": r"$\it{f}_{\mathrm{sync}}$",
    "massratio_accretor_donor": r"$\mathrm{log}_{10}\left(\it{q}_{\mathrm{acc}}\right)$",
}
