"""
Function to plot the roche lobe radius ratio for asynchronous donors vs eggleton value
"""

import itertools
import os

import astropy.constants as const
import astropy.units as u
import matplotlib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ballistic_integrator.functions.frame_of_reference.center_of_mass import (
    calculate_position_donor,
)
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import cm, colors

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins_and_bincenters,
)
from ballistic_integration_code.scripts.L1_stream.functions.mass_stream_area_functions import (
    calculate_normalised_mass_stream_area,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    parameter_dict,
    project_settings,
)
from ballistic_integration_code.scripts.lagrange_points.run_all import run_all
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


def create_bin_dict(array):
    """
    Function to create a dictionary with bin-info given an array of regularly spaced data
    """

    # sort and make unique
    unique_array = np.unique(array)
    sorted_array = np.sort(unique_array)

    # create bincenters
    # bin_centers = create_centers_from_bins(bins=sorted_array)
    bin_edges = create_bins_from_centers(centers=sorted_array)

    return {"centers": sorted_array, "edges": bin_edges}


def plot_RL_ratio(settings, df_filename, plot_settings):
    """
    Functo
    """

    df = pd.read_csv(df_filename, header=0, sep="\s+")

    x_arrays = df["massratio_accretor_donor"].to_numpy()
    y_arrays = df["synchronicity_factor"].to_numpy()
    z_arrays = df["ratio_new_eggleton"].to_numpy()

    ######
    # Create bin structures
    x_bin_dict = create_bin_dict(x_arrays)
    y_bin_dict = create_bin_dict(y_arrays)

    X, Y = np.meshgrid(x_bin_dict["centers"], y_bin_dict["centers"])

    ######
    # construct the histogram
    z_hist = np.histogram2d(
        x_arrays,
        y_arrays,
        bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
        weights=z_arrays,
    )

    min_plot_value = z_hist[0].min()
    max_plot_value = z_hist[0].max()

    norm = colors.Normalize(vmin=min_plot_value, vmax=max_plot_value)

    ##########
    # Plot data

    # Set up figure
    fig = plt.figure(figsize=(20, 12))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=9)

    ax_plot = fig.add_subplot(gs[:, :-1])
    ax_cb = fig.add_subplot(gs[:, -1:])

    ax_plot.hist2d(
        x_arrays,
        y_arrays,
        bins=[x_bin_dict["edges"], y_bin_dict["edges"]],
        weights=z_arrays,
        antialiased=True,
        rasterized=True,
    )

    # make colorbar
    cbar = mpl.colorbar.ColorbarBase(
        ax_cb,
        cmap=mpl.cm.viridis,
        norm=norm,
    )

    # # Plot the contour
    # cs = ax_plot.contour(
    #     unique_x,
    #     unique_y,
    #     z_values_reshaped,
    #     levels=contours,
    #     linestyles=linestyles,
    #     colors=color_contour,
    #     alpha=alpha_contour,
    # )
    # ax_plot.clabel(cs, contours)

    # ##########
    # # plot color bar
    # cbar = matplotlib.colorbar.ColorbarBase(ax_cb, cmap=cmap, norm=norm)

    # zlabel = r"$\left(A_{\mathrm{stream}} / a^{2} \right) / \left( v_{\mathrm{thermal}} / a\omega \right)^{2}$"
    # if plot_settings["use_log10"]:
    #     zlabel = r"$\mathrm{log}_{10}$" + zlabel

    # cbar.ax.set_ylabel(zlabel)

    # # Plot contourlevels
    # for contour_i, contour in enumerate(contours):
    #     cbar.ax.plot(
    #         [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
    #         [contour] * 2,
    #         color=color_contour,
    #         linestyle=linestyles[contour_i],
    #         alpha=alpha_contour,
    #     )

    xlabel = r"$\it{q}_{\mathrm{acc}} = \it{M}_{\mathrm{acc}}/\it{M}_{\mathrm{don}}$"
    ylabel = r"$f_{\mathrm{sync}}$"
    ##########
    # plot make-up
    ax_plot.set_xlim([10**-2, 10**2])
    ax_plot.set_xscale("log")

    #
    ax_plot.set_ylabel(ylabel)
    ax_plot.set_xlabel(xlabel)

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    settings = {
        **project_settings,
        "result_dir": "results/debugging/",
        "max_job_queue_size": 1000,
        "num_cores": 8,
        "q_range": 10 ** np.linspace(-2, 2, 20),
        # "f_range": [1],
        "f_range": np.linspace(0.1, 2, 10),
        "verbosity": 3,
        "volume_roche_lobe_xgrid_n": 25,
        "volume_roche_lobe_ygrid_n": 25,
        "volume_roche_lobe_size_factor": 1.001,
    }
    settings["result_dir"] = "test/"
    settings["lagrange_point_data_file_basename"] = "lagrange_point_data.txt"
    settings[
        "lagrange_point_csv_interpolation_table_file_basename"
    ] = "lagrange_point_and_rochelobe_volume_interpolation_table.csv"
    settings[
        "lagrange_point_header_interpolation_table_file_basename"
    ] = "RLOF_sepinsky2007_lagrange_interpolation_table.h"

    settings["rochelobe_passed_gridpoint_file_basename"] = "passed_gridpoints.txt"
    settings["rochelobe_failed_gridpoint_file_basename"] = "failed_gridpoints.txt"

    run_all(
        settings=settings,
        parameter_dict=parameter_dict,
        generate_lagrange_point_and_roche_lobe_information=True,
        generate_output_interpolation_files=True,
    )

    df_filename = os.path.join(
        settings["result_dir"],
        settings["lagrange_point_csv_interpolation_table_file_basename"],
    )

    rochelobe_data_filename = os.path.join(
        settings["result_dir"], settings["lagrange_point_data_file_basename"]
    )

    # plot_RL_ratio(
    #     settings=settings,
    #     df_filename=df_filename,
    #     plot_settings={"show_plot": True},
    # )
