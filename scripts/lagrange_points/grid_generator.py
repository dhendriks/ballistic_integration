"""
Grid generator for the roche lobe and lagrange point calculations
"""

import copy
import itertools

from ballistic_integration_code.settings import standard_system_dict


def grid_generator(settings):
    """
    Grid generator for the roche lobe and lagrange point calculations
    """

    # readout settings
    massratios_accretor_donor = settings["q_range"]
    synchronicity_factors = settings["f_range"]

    # Loop over parameters and yield the settings
    for parameters in itertools.product(
        synchronicity_factors, massratios_accretor_donor
    ):
        synchronicity_factor = parameters[0]
        massratio_accretor_donor = parameters[1]

        # Set system dict
        settings["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": synchronicity_factor,
            "mass_accretor": massratio_accretor_donor
            * standard_system_dict["mass_donor"],
        }

        # Set grid point
        settings["grid_point"] = {
            "massratio_accretor_donor": massratio_accretor_donor,
            "synchronicity_factor": synchronicity_factor,
        }

        # Copy the settings
        returned_settings = copy.deepcopy(settings)

        yield returned_settings
