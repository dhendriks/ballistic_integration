"""
Script to run some tests
"""

import json
import os

import numpy as np
from ballistic_integrator.functions.frame_of_reference.wrapper import (
    calculate_lagrange_points,
)
from ballistic_integrator.functions.functions import json_encoder

from ballistic_integration_code import settings
from ballistic_integration_code.scripts.L1_stream.functions.calculate_position_offset_L1_tiny import (
    calculate_position_offset_L1_tiny,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_trajectory_result_summary import (
    calculate_trajectory_result_summary,
)
from ballistic_integration_code.scripts.L1_stream.functions.calculate_velocity_offset_L1_non_synchronous import (
    calculate_velocity_offset_L1_non_synchronous,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.stage_1.subclass_trajectory_integrator import (
    stage_1_trajectory_integrator_class,
)
from ballistic_integration_code.settings import standard_system_dict

# Control settings
settings = {
    ############
    # Global settings
    "frame_of_reference": "center_of_mass",
    # Grid settings
    "result_dir": os.path.join(
        os.environ["BINARYC_DATA_ROOT"],
        "BALLISTIC",
        "L1_stream",
        "TESTS",
    ),
    "backup_if_data_exists": True,
    "run": True,
    "write_full_output_to_file": True,
    "write_summary_output_to_file": False,
    #
    ######################
    # trajectory integration settings
    "max_time": 10,
    "dt": 0.01,
    "steps_check_self_intersection": -1,
    "jacobi_error_tol": 1e-5,
    "initial_velocity_offset": [0.0, 0.0],
    "print_info": False,
    "allow_fallback_to_center": True,
    #
    ######################
    # custom termination control
    "use_custom_termination_routine": True,
    #
    ######################
    # custom timestep control
    "use_custom_timestep_control_routine": True,
    "max_position_diff_timestep_control": 0.001,
    "global_minimum_timestep": 1e-8,
    "timestep_increase_factor": 1.1,
}

settings["q_acc_don"] = 1
settings["synchronicity_factor"] = 1.0

######
# Initialise
q_acc_don = settings["q_acc_don"]
synchronicity_factor = settings["synchronicity_factor"]
result_dir = settings["result_dir"]
trajectory_result_dir = os.path.join(result_dir, "trajectory_data")

# Create directory
os.makedirs(os.path.abspath(trajectory_result_dir), exist_ok=True)

system_dict = {
    **standard_system_dict,
    "synchronicity_factor": synchronicity_factor,
    "mass_accretor": 1 * q_acc_don,
}

################
# Set initial location

# Calculate offset based on tiny shift from L1
position_offset = calculate_position_offset_L1_tiny(
    system_dict=system_dict, frame_of_reference=settings["frame_of_reference"]
)
settings["position_offset_L1_tiny"] = np.array(position_offset)

# Combine the position offsets
settings["initial_position_offset"] = settings["position_offset_L1_tiny"]

# Get base position
L_points = calculate_lagrange_points(
    system_dict=system_dict, frame_of_reference=settings["frame_of_reference"]
)
l1_pos = L_points["L1"]
initial_position_base = np.array(l1_pos[:2])
settings["initial_position_base"] = np.array(initial_position_base)
settings["L_points"] = L_points

# Set offset position
initial_position_offset = np.array(settings.get("initial_position_offset", [0.0, 0.0]))
initial_r = initial_position_base + initial_position_offset
settings["initial_position"] = np.array(initial_r)

################
# Velocity offset based on the synchronicity rate
initial_velocity_offset_non_synchronous = calculate_velocity_offset_L1_non_synchronous(
    system_dict=system_dict, frame_of_reference=settings["frame_of_reference"]
)
settings[
    "initial_velocity_offset_non_synchronous"
] = initial_velocity_offset_non_synchronous

# Combine the velocity offsets
settings["initial_velocity_offset"] = settings[
    "initial_velocity_offset_non_synchronous"
]

# Set initial velocity
initial_v = np.array(settings.get("initial_velocity_offset", [0, 0]))
settings["initial_velocity"] = np.array(initial_v)

#####
# handle output filenames

# Full trajectory
trajectory_data_filename = "L1_f={}_q={}_x={}_v={}.json".format(
    settings["synchronicity_factor"],
    system_dict["mass_accretor"] / system_dict["mass_donor"],
    "{},{}".format(initial_r[0], initial_r[1]),
    "{},{}".format(initial_v[0], initial_v[1]),
)
full_trajectory_data_filename = os.path.join(
    trajectory_result_dir, trajectory_data_filename
)

# trajectory summary filename
trajectory_summary_data_filename = os.path.join(
    result_dir, "trajectory_summary_data.txt"
)

#####
# Handle evolution
if settings.get("run", True):
    # Set up the integrator
    integrator = stage_1_trajectory_integrator_class(
        system_dict=system_dict,
        frame_of_reference=settings["frame_of_reference"],
        initial_position=initial_r,
        initial_velocity=initial_v,
        control_settings=settings,
    )

    # Calculate the trajectory
    result = integrator.evolve()

    if settings["write_full_output_to_file"]:
        with open(full_trajectory_data_filename, "w") as f:
            json.dump(result, f, default=json_encoder)


############
# Print results
trajectory_result_summary = calculate_trajectory_result_summary(result)

# Plot results
plot_system(
    system_dict=system_dict,
    frame_of_reference=settings["frame_of_reference"],
    plot_trajectory=True,
    resultfile=full_trajectory_data_filename,
    plot_rochelobe_equipotential_meshgrid=True,
)
