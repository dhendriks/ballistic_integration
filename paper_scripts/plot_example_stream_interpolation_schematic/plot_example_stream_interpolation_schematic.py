"""
Function to handle plotting an example set of trajectories with different classifications
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from ballistic_integrator.functions.frame_of_reference import (
    calculate_position_accretor,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)
from scipy import interpolate

from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    rochelobe_radius_accretor,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_example_stream_interpolation_schematic(settings, plot_settings):
    """
    Function to plot different sets of trajectories
    """

    grid_point = {
        "massratio_accretor_donor": settings["system_dict"]["mass_accretor"]
        / settings["system_dict"]["mass_donor"],
        "synchronicity_factor": settings["system_dict"]["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -3,
    }
    settings["grid_point"] = grid_point

    assert settings["num_samples_area_sampling"] == 1

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    # Get trajectory set for the configuration passed by settings
    trajectory_set, averaged_result_dict = integrate_trajectories_from_L1_stage_3(
        settings=settings
    )

    # Get category sets
    trajectory_sets_per_category = select_trajectory_categories(trajectory_set)

    #
    fig = plt.figure(figsize=(20, 20))
    fig, axes_list = plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        plot_trajectory=True,
        plot_rochelobe_equipotential_meshgrid=False,
        plot_L_points=False,
        plot_system_information_panel=False,
        resultdata_sets=trajectory_sets_per_category,
        return_fig=True,
        plot_settings={
            **plot_settings,
            "plot_bounds_x": plot_settings["x_bounds"],
            "plot_bounds_y": plot_settings["y_bounds"],
            "plot_resolution": settings["plot_resolution"],
            "star_text_color": "white",
            "COM_text_color": "white",
        },
        settings=settings,
        fig=fig,
    )

    #######
    # Add rings to plot
    interpolation_radii = np.linspace(
        settings["specific_angular_momentum_stream_interpolation_lower_bound"],
        settings["specific_angular_momentum_stream_interpolation_upper_bound"],
        settings["specific_angular_momentum_stream_interpolation_num_radii"],
    )
    angle_dstep = 0.01
    angles = np.arange(0, 2 * np.pi + angle_dstep, angle_dstep)

    radii_linestyle = "--"
    radii_alpha = 0.5
    radii_color = "k"

    intersection = False
    intersection_coordinates = []

    system_dict = settings["system_dict"]

    # with respect to what
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=settings["frame_of_reference"]
    )

    RL_accretor = rochelobe_radius_accretor(
        mass_donor=system_dict["mass_donor"], mass_accretor=system_dict["mass_accretor"]
    )

    # Extract information for the interpolation
    trajectory_info = trajectory_set[0]["result_summary"]

    positions = trajectory_info["positions"]
    distances_to_accretor = trajectory_info["distances_to_accretor"]

    # Combine
    positions_and_velocities = np.zeros((positions.shape[0], positions.shape[1]))
    positions_and_velocities[:, : positions.shape[1]] = positions

    ########
    # Set up interpolation table
    specific_angular_momentum_stream_interpolator = interpolate.interp1d(
        distances_to_accretor,
        positions_and_velocities.T,
    )

    # loop over radii
    num_radii = len(interpolation_radii)
    for interpolation_radius_fraction_i, interpolation_radius_fraction in enumerate(
        interpolation_radii
    ):
        interpolation_radius = interpolation_radius_fraction * RL_accretor

        # Get x and y values of ring
        x_vals = interpolation_radius * np.cos(angles) + pos_accretor[0]
        y_vals = interpolation_radius * np.sin(angles) + pos_accretor[1]

        # Plot rings
        axes_list[0].plot(
            x_vals,
            y_vals,
            linestyle=radii_linestyle,
            alpha=radii_alpha,
            color=radii_color,
        )

        # # plot text
        # # text = "R/RL$_{{acc}} = {:.2f}$".format(interpolation_radius_fraction)
        # text = "R= {}/{} RL$_{{acc}} $".format(interpolation_radius_fraction_i, num_radii)
        # axes_list[0].text(
        #     pos_accretor[0],
        #     pos_accretor[1] + interpolation_radius,
        #     text,
        #     ha="center",
        #     color="black",
        #     # weight="bold",
        #     bbox=dict(
        #         facecolor="white", alpha=1, edgecolor="white", boxstyle="round,pad=0.2"
        #     ),
        #     fontsize=15,
        # )

        ################
        # interpolate the stream and find exact locations of the intersection
        try:
            # Get the data
            interpolated_data = specific_angular_momentum_stream_interpolator(
                interpolation_radius
            )

            # unpack data
            position_data_at_interpolated_radius = interpolated_data[
                : positions.shape[1]
            ]

            intersection = True

            intersection_coordinates.append(position_data_at_interpolated_radius)

        # Catch error when out of bounds
        except ValueError:
            pass

    # plot intersection points
    if intersection:
        # unpack
        intersection_coordinates_arr = np.array(intersection_coordinates)
        intersection_x = intersection_coordinates_arr[:, 0]
        intersection_y = intersection_coordinates_arr[:, 1]

        # plot
        _ = axes_list[0].plot(
            intersection_x,
            intersection_y,
            marker="o",
            linestyle="None",
            markeredgewidth=4,
            markersize=20,
            markeredgecolor="#9933ff",
            markerfacecolor="None",
        )

    # Draw arrow on stream
    arrow_position_along_stream = 0.05
    size_arrow = 0.015

    positions_stream = trajectory_sets_per_category[0]["trajectory_list"][0][
        "positions"
    ]
    velocities_stream = trajectory_sets_per_category[0]["trajectory_list"][0][
        "velocities"
    ]
    initial_position = positions_stream[0]
    distance_along_stream = 0
    previous_position = initial_position
    for position, velocity in zip(positions_stream[1:], velocities_stream[1:]):
        distance_along_stream += np.linalg.norm(position - previous_position)
        if distance_along_stream > arrow_position_along_stream:
            scaled_velocity = (velocity / np.linalg.norm(velocity)) * size_arrow
            axes_list[0].arrow(
                position[0],
                position[1],
                scaled_velocity[0],
                scaled_velocity[1],
                length_includes_head=True,
                head_width=size_arrow,
                head_length=size_arrow,
                color="k",
                zorder=2000,
            )
            break

        # Store previous position
        previous_position = position

    ######
    # Resize
    for ax in axes_list:
        ax.set_xlim(plot_settings["x_bounds"])
        ax.set_ylim(plot_settings["y_bounds"])

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # remove legend
    legend = axes_list[0].get_legend()
    legend.remove()

    # Add labels
    axes_list[0].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_list[0].set_ylabel(r"$\it{y}$", fontsize=36)

    # force the same axes ratio
    axes_list[0].set_aspect("equal")

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


def plot_example_multiple_stream_interpolation_schematic(
    settings, system_dicts, plot_settings
):
    """
    Function to plot different sets of trajectories
    """

    assert settings["num_samples_area_sampling"] == 1

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=8)

    axes_dict = {}
    axes_dict["ax"] = fig.add_subplot(gs[:, :])

    ########
    #
    trajectory_sets, trajectory_sets_per_category_list, averaged_result_dicts = (
        [],
        [],
        [],
    )

    for system_dict_no, system_dict in enumerate(system_dicts):
        settings["system_dict"] = system_dict

        grid_point = {
            "massratio_accretor_donor": settings["system_dict"]["mass_accretor"]
            / settings["system_dict"]["mass_donor"],
            "synchronicity_factor": settings["system_dict"]["synchronicity_factor"],
            "log10normalized_thermal_velocity_dispersion": -3,
        }
        settings["grid_point"] = grid_point

        # Get trajectory set for the configuration passed by settings
        trajectory_set, averaged_result_dict = integrate_trajectories_from_L1_stage_3(
            settings=settings
        )
        trajectory_sets.append(trajectory_set)
        averaged_result_dicts.append(averaged_result_dict)

        # Get category sets
        trajectory_sets_per_category = select_trajectory_categories(trajectory_set)
        trajectory_sets_per_category_list.append(trajectory_sets_per_category)

        #
        fig, axes_dict = plot_system(
            system_dict=settings["system_dict"],
            frame_of_reference=settings["frame_of_reference"],
            plot_trajectory=True,
            plot_rochelobe_equipotential_meshgrid=False,
            plot_L_points=False,
            plot_system_information_panel=False,
            plot_rochelobe_equipotentials=True if system_dict_no == 0 else False,
            plot_stars_and_COM=True if system_dict_no == 0 else False,
            resultdata_sets=trajectory_sets_per_category,
            return_fig=True,
            plot_settings={
                **plot_settings,
                "plot_bounds_x": plot_settings["x_bounds"],
                "plot_bounds_y": plot_settings["y_bounds"],
                "plot_resolution": settings["plot_resolution"],
                "star_text_color": "white",
                "COM_text_color": "white",
            },
            settings=settings,
            fig=fig,
            axes_dict=axes_dict,
        )

    #######
    # Add rings to plot
    interpolation_radii = np.linspace(
        settings["specific_angular_momentum_stream_interpolation_lower_bound"],
        settings["specific_angular_momentum_stream_interpolation_upper_bound"],
        settings["specific_angular_momentum_stream_interpolation_num_radii"],
    )
    angle_dstep = 0.01
    angles = np.arange(0, 2 * np.pi + angle_dstep, angle_dstep)

    radii_linestyle = "--"
    radii_alpha = 0.5
    radii_color = "k"

    system_dict = settings["system_dict"]

    # with respect to what
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=settings["frame_of_reference"]
    )

    RL_accretor = rochelobe_radius_accretor(
        mass_donor=system_dict["mass_donor"], mass_accretor=system_dict["mass_accretor"]
    )

    #############
    # Draw the radii
    num_radii = len(interpolation_radii)
    for interpolation_radius_fraction_i, interpolation_radius_fraction in enumerate(
        interpolation_radii
    ):
        interpolation_radius = interpolation_radius_fraction * RL_accretor

        # Get x and y values of ring
        x_vals = interpolation_radius * np.cos(angles) + pos_accretor[0]
        y_vals = interpolation_radius * np.sin(angles) + pos_accretor[1]

        # Plot rings
        axes_dict["ax"].plot(
            x_vals,
            y_vals,
            linestyle=radii_linestyle,
            alpha=radii_alpha,
            color=radii_color,
        )

    #####################
    # Loop over trajectories and find intersections
    for trajectory_set_i, trajectory_set in enumerate(trajectory_sets):

        intersection = False
        intersection_coordinates = []

        # Extract information for the interpolation
        trajectory_info = trajectory_set[0]["result_summary"]

        positions = trajectory_info["positions"]
        distances_to_accretor = trajectory_info["distances_to_accretor"]

        # Combine
        positions_and_velocities = np.zeros((positions.shape[0], positions.shape[1]))
        positions_and_velocities[:, : positions.shape[1]] = positions

        ########
        # Set up interpolation table
        specific_angular_momentum_stream_interpolator = interpolate.interp1d(
            distances_to_accretor,
            positions_and_velocities.T,
        )

        ##############
        # Loop over radii
        for interpolation_radius_fraction_i, interpolation_radius_fraction in enumerate(
            interpolation_radii
        ):
            interpolation_radius = interpolation_radius_fraction * RL_accretor

            # Get x and y values of ring
            x_vals = interpolation_radius * np.cos(angles) + pos_accretor[0]
            y_vals = interpolation_radius * np.sin(angles) + pos_accretor[1]

            # Plot rings
            axes_dict["ax"].plot(
                x_vals,
                y_vals,
                linestyle=radii_linestyle,
                alpha=radii_alpha,
                color=radii_color,
            )

            # # plot text
            # # text = "R/RL$_{{acc}} = {:.2f}$".format(interpolation_radius_fraction)
            # text = "R= {}/{} RL$_{{acc}} $".format(interpolation_radius_fraction_i, num_radii)
            # axes_list[0].text(
            #     pos_accretor[0],
            #     pos_accretor[1] + interpolation_radius,
            #     text,
            #     ha="center",
            #     color="black",
            #     # weight="bold",
            #     bbox=dict(
            #         facecolor="white", alpha=1, edgecolor="white", boxstyle="round,pad=0.2"
            #     ),
            #     fontsize=15,
            # )

            ################
            # interpolate the stream and find exact locations of the intersection
            try:
                # Get the data
                interpolated_data = specific_angular_momentum_stream_interpolator(
                    interpolation_radius
                )

                # unpack data
                position_data_at_interpolated_radius = interpolated_data[
                    : positions.shape[1]
                ]

                intersection = True

                intersection_coordinates.append(position_data_at_interpolated_radius)

            # Catch error when out of bounds
            except ValueError:
                pass

        # plot intersection points
        if intersection:
            # unpack
            intersection_coordinates_arr = np.array(intersection_coordinates)
            intersection_x = intersection_coordinates_arr[:, 0]
            intersection_y = intersection_coordinates_arr[:, 1]

            # plot
            _ = axes_dict["ax"].plot(
                intersection_x,
                intersection_y,
                marker="o",
                linestyle="None",
                markeredgewidth=4,
                markersize=20,
                markeredgecolor="#9933ff",
                markerfacecolor="None",
            )

    #############
    # # Draw arrow on stream
    # arrow_position_along_stream = 0.05
    # size_arrow = 0.015

    # positions_stream = trajectory_sets_per_category[0]["trajectory_list"][0][
    #     "positions"
    # ]
    # velocities_stream = trajectory_sets_per_category[0]["trajectory_list"][0][
    #     "velocities"
    # ]
    # initial_position = positions_stream[0]
    # distance_along_stream = 0
    # previous_position = initial_position
    # for position, velocity in zip(positions_stream[1:], velocities_stream[1:]):
    #     distance_along_stream += np.linalg.norm(position - previous_position)
    #     if distance_along_stream > arrow_position_along_stream:
    #         scaled_velocity = (velocity / np.linalg.norm(velocity)) * size_arrow
    #         axes_list[0].arrow(
    #             position[0],
    #             position[1],
    #             scaled_velocity[0],
    #             scaled_velocity[1],
    #             length_includes_head=True,
    #             head_width=size_arrow,
    #             head_length=size_arrow,
    #             color="k",
    #             zorder=2000,
    #         )
    #         break

    #     # Store previous position
    #     previous_position = position

    ######
    # Resize
    axes_dict["ax"].set_xlim(plot_settings["x_bounds"])
    axes_dict["ax"].set_ylim(plot_settings["y_bounds"])

    # Remove text that falls out of frame
    fig, axes_dict["ax"] = delete_out_of_frame_text(fig, axes_dict["ax"])

    # remove legend
    legend = axes_dict["ax"].get_legend()
    legend.remove()

    # Add labels
    axes_dict["ax"].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_dict["ax"].set_ylabel(r"$\it{y}$", fontsize=36)

    # force the same axes ratio
    axes_dict["ax"].set_aspect("equal")

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


def handle_paper_plot_example_stream_interpolation_schematic(output_name, show_plot):
    """
    Function to handle the paper version of the stream interpolation schematic
    """

    #######
    # Config
    plot_settings = {
        "star_text_color": "white",
        "show_plot": show_plot,
        "verbosity": 1,
        "x_bounds": [0, 1],
        "y_bounds": [-0.5, 0.5],
        "textsize": 26,
        "markersize": 10,
    }

    ###########
    system_dict = {
        **standard_system_dict,
        "synchronicity_factor": 1,
        "mass_accretor": 10**0,
        "mass_donor": 1,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -3,
    }

    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 1,
        "jacobi_error_tol": 1e-3,
        "num_samples_area_sampling": 1,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "specific_angular_momentum_stream_interpolation_enabled": False,
        "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_upper_bound": 0.9,  # in terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_num_radii": 6,  # number of radii to calculate properties at
        "verbosity": 0,
        "generate_plot_at_gridpoint": False,
    }

    plot_example_stream_interpolation_schematic(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    #######
    # Config

    plot_settings = {
        "star_text_color": "white",
        "show_plot": False,
        "verbosity": 1,
        "x_bounds": [0, 1],
        "y_bounds": [-0.5, 0.5],
        "textsize": 26,
        "markersize": 10,
    }

    # # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    output_dir = os.path.join(this_file_dir, "plots/")

    # basename
    basename = "stream_interpolation_schematic.pdf"

    ###########
    #
    system_dict = {
        **standard_system_dict,
        "synchronicity_factor": 1,
        "mass_accretor": 10**0,
        "mass_donor": 1,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -3,
    }

    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 1,
        "jacobi_error_tol": 1e-3,
        "num_samples_area_sampling": 1,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "specific_angular_momentum_stream_interpolation_enabled": False,
        "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_upper_bound": 0.9,  # in terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_num_radii": 6,  # number of radii to calculate properties at
        "verbosity": 3,
    }

    plot_example_stream_interpolation_schematic(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )
