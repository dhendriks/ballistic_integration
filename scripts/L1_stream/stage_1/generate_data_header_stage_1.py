"""
Script containing function to convert the interpolation table file to the structure for binary_c

The read-out file contains the summarized results of the ballistic trajectories. instead of first saving the trajectory data we now just process that immediately

TODO: currently we do not have the option to handle the interpolation of the data if there are holes. We can create a function to do this
"""

import os

import pandas as pd
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)

from ballistic_integration_code.scripts.L1_stream.functions.data_header_functions import (
    data_header_write_data_array,
    data_header_write_input_defines,
    data_header_write_meta_data,
    data_header_write_output_defines,
    get_index_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    filter_df_on_exit_codes,
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import parameter_dict
from ballistic_integration_code.scripts.L1_stream.stage_1.stage_settings import (
    stage_1_settings,
)


def generate_data_header_stage_1(
    input_interpolation_textfile, output_interpolation_textfile, settings
):
    """
    Function to write the interpolation data as a header array
    """

    #####################
    # Config and readout
    definition_basename = "RLOF_HENDRIKS2023_BALLISTIC_STREAM_INTERPOLATION"
    stage_number = 1

    #
    length_decimals = settings["length_decimals_dataheader"]
    input_parameter_list = settings["input_parameter_list_data_header"]
    output_parameter_list = settings["output_parameter_list_data_header"]

    # TODO: move to function and update to all stages
    # Add stream interpolation radii to output parameters if flag is set
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:

        interpolation_radius_fractions = np.linspace(
            settings["specific_angular_momentum_stream_interpolation_lower_bound"],
            settings["specific_angular_momentum_stream_interpolation_upper_bound"],
            settings["specific_angular_momentum_stream_interpolation_num_radii"],
        )
        interpolation_radius_output_parameter_list = []

        # loop over stream interpolation radii
        for stream_interpolation_radius_i in range(
            settings["specific_angular_momentum_stream_interpolation_num_radii"]
        ):
            interpolation_radius_i_string = "interpolation_radius_{}".format(
                stream_interpolation_radius_i
            )

            # Add to interpolation radius output parameter list
            interpolation_radius_output_parameter_list.append(
                interpolation_radius_i_string
            )

            # add to parameter dict
            parameter_dict[interpolation_radius_i_string] = {
                "shortname": interpolation_radius_i_string,
                "longname": interpolation_radius_i_string,
                "description": "Stream interpolation radius at R/RL_acc = {}".format(
                    interpolation_radius_fractions[interpolation_radius_i_string]
                ),
                "unit": dimensionless_unit,
            }

        # Add to total output parameter list
        output_parameter_list += interpolation_radius_output_parameter_list

    # Read out interpolation textfile
    df = pd.read_csv(input_interpolation_textfile, sep="\s+", header=0)

    # Add columns to df
    df["A_factor"] = A_formula(f=df["synchronicity_factor"])

    #####################
    # Prepare for write-out
    filtered_df, _, _ = filter_df_on_exit_codes(
        df,
        allowed_exit_codes=settings["allowed_exit_codes"],
        fail_silently=False,
    )

    # Make sure the order is correct
    filtered_df = filtered_df[input_parameter_list + output_parameter_list]

    # make sure to sort the columns
    filtered_df = filtered_df.sort_values(by=input_parameter_list)

    # Write the number of lines we will output to the table and some others
    num_lines = len(filtered_df.index)
    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    # Get indices for input parameters
    input_index_dict = get_index_dict(
        df=filtered_df, columns=input_parameter_list, offset=0
    )
    output_index_dict = get_index_dict(
        df=filtered_df, columns=output_parameter_list, offset=-num_input_parameters
    )

    # Get git info
    git_info = get_git_info_and_time()

    #####################
    # Write header info to file
    with open(output_interpolation_textfile, "w") as f:
        ########################
        # Write top-level explanation
        f.write(
            """// Ballistic stream trajectory integration stage {} dataset.
// This dataset aims to provide an interpolation dataset to determine rmin and racc for the stream based on the mass ratio q and the A factor
// that captures synchronicity, eccentricty and mean anomaly (see sepinksy 2007), to improve RLOF calculations and estimates for interactions
// and specifically for accretion disks.\n
""".format(
                stage_number
            )
        )

        # Write git revision information
        f.write(
            "// Generated on: {} with git repository: {} branch: {} commit: {}\n\n".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        ######
        # Write data header metadata (parameter descriptions, stage number, number of data lines etc)
        data_header_write_meta_data(
            input_parameter_list=input_parameter_list,
            output_parameter_list=output_parameter_list,
            parameter_dict=parameter_dict,
            stage_number=stage_number,
            num_lines=num_lines,
            num_input_parameters=num_input_parameters,
            num_output_parameters=num_output_parameters,
            filehandle=f,
            definition_basename=definition_basename,
        )

        ##########
        # Write the input defines
        data_header_write_input_defines(
            parameter_list=input_parameter_list,
            index_dict=input_index_dict,
            filehandle=f,
            definition_basename=definition_basename,
        )

        ##########
        # Write the output defines
        data_header_write_output_defines(
            parameter_list=output_parameter_list,
            index_dict=output_index_dict,
            filehandle=f,
            definition_basename=definition_basename,
        )

        #########
        # Write the data
        data_header_write_data_array(
            df=filtered_df,
            length_decimals=length_decimals,
            num_lines=num_lines,
            filehandle=f,
            definition_basename=definition_basename,
        )


if __name__ == "__main__":
    # Get input file
    input_filename = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_1",
        "trajectory_summary_data.txt",
    )

    # set output file
    if os.getenv("BINARY_C"):
        output_dir = os.path.join(os.getenv("BINARY_C"), "src", "RLOF")

    # output_dir = os.path.join(
    #     os.getenv("PROJECT_DATA_ROOT"),
    #     "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_1",
    # )

    output_dir = "results/"

    #
    output_filename = os.path.join(
        output_dir, "RLOF_Hendriks2023_ballistic_stream_table.h"
    )
    os.makedirs(output_dir, exist_ok=True)

    generate_data_header_stage_1(
        input_interpolation_textfile=input_filename,
        output_interpolation_textfile=output_filename,
        settings=stage_1_settings,
    )
