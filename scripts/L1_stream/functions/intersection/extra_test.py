from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_detection import (
    combine_intersection_dicts,
)

dict_1 = {"a": [(1, 2), (3, 4), (5, 6)], "b": [(1, 2), (3, 4)]}
dict_2 = {"a": [(5, 6), (7, 8), (9, 10)], "c": [(1, 2), (3, 4)]}
all_dicts = [dict_1, dict_2]

combine_intersection_dicts(intersection_dicts=[dict_1, dict_2], settings={})

for dicto in all_dicts:
    dicto["d"] = 10
print(all_dicts)
