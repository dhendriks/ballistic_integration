"""
Script to run the grid of roche lobe volume calculations
"""

import numpy as np

from ballistic_integration_code.scripts.L1_stream.project_settings import (
    parameter_dict,
    project_settings,
)
from ballistic_integration_code.scripts.lagrange_points.run_all import run_all

#####################
# General settings for this
settings = {
    **project_settings,
    "result_dir": "results/",
    "max_job_queue_size": 1000,
    "num_cores": 8,
    "verbosity": 1,
    "volume_roche_lobe_xgrid_n": 25,
    "volume_roche_lobe_ygrid_n": 25,
    "volume_roche_lobe_size_factor": 1,
    "include_asynchronicity_donor_during_integration": True,
}
settings["result_dir"] = "results/test/"
settings["lagrange_point_data_file_basename"] = "lagrange_point_data.txt"
settings[
    "lagrange_point_csv_interpolation_table_file_basename"
] = "RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"
settings[
    "lagrange_point_header_interpolation_table_file_basename"
] = "RLOF_Hendriks2023_roche_lobe_interpolation_table.h"

settings["rochelobe_passed_gridpoint_file_basename"] = "passed_gridpoints.txt"
settings["rochelobe_failed_gridpoint_file_basename"] = "failed_gridpoints.txt"
settings["frame_of_reference"] = "center_of_mass"

settings["input_parameter_list_data_header_rochelobe_volume"] = [
    "A_factor",
    "massratio_accretor_donor",
]
settings["output_parameter_list_data_header_rochelobe_volume"] = [
    "ratio_RL_RLegg",
    "roche_lobe_radius",
]


# ##################
# # test grid
# settings["q_range"] = 10 ** np.linspace(-2, 2, 2)
# settings["f_range"] = np.linspace(0.1, 2, 2)


# ##################
# # low res grid
# settings["q_range"] = 10 ** np.linspace(-2, 2, 5)
# settings["f_range"] = np.linspace(0.1, 2, 5)


##################
# mid res grid
# settings["q_range"] = 10 ** np.linspace(-2, 2, 10)
# settings["f_range"] = np.linspace(0.1, 2, 10)


# ##################
# # High res grid
settings["q_range"] = np.concatenate(
    [10 ** np.linspace(-2, 0, 20), 10 ** np.linspace(0, 2, 10)[1:]]
)
settings["f_range"] = np.linspace(0.1, 2, 20)

#
run_all(
    settings=settings,
    parameter_dict=parameter_dict,
    generate_lagrange_point_and_roche_lobe_information=False,
    generate_output_interpolation_files=True,
)
