"""
Function to plot the ratio of rcirc and rmin
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import pandas as pd
from ballistic_integrator.functions.plot_functions import (
    fkr_rcirc,
    return_lubow_shu_table_2_data,
    ulrich_kolb,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib.lines import Line2D
from matplotlib.ticker import ScalarFormatter

from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    check_correct_gridpoint_queryresult,
    handle_adding_all_comparison_plots,
    handle_query,
    readout_dataset_and_query,
)
from ballistic_integration_code.paper_scripts.radii_plots.settings import (
    xlabel,
    y_label_dict,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

fontsize_lines = 14


def plot_ratio_rcirc_rmin_grid(
    result_file,
    query=None,
    plot_settings={},
):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    radius_quantity = "ratio_rcirc_rmin"

    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
        exit_code_value=EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value,
    )

    # calculate ratio between rcirc and rmin
    result_df["ratio_rcirc_rmin"] = result_df["rcirc"] / result_df["rmin"]

    ##################
    # Plot our results

    #
    fig, axes = plt.subplots(figsize=(16, 16))

    # Get unique mass synchronicity factors
    unique_nonzero_synchronicity_factors = result_df.query(
        "{} > 0".format(radius_quantity)
    )["synchronicity_factor"].unique()

    # Set up colormap
    cmap = plt.cm.get_cmap("viridis", len(unique_nonzero_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    data_lines = []

    # Plot results for each synchronicity factor
    for i, synchronicity_factor in enumerate(unique_nonzero_synchronicity_factors):
        filtered_df = result_df[result_df.synchronicity_factor == synchronicity_factor]

        # Plot data
        marker = "o"
        linestyle = ":"
        label = "f = {:1.1f}".format(synchronicity_factor)

        axes.plot(
            filtered_df["massratio_accretor_donor"],
            filtered_df["ratio_rcirc_rmin"],
            marker=marker,
            linestyle=linestyle,
            color=cmaplist[i],
            label=label,
            alpha=0.8,
        )

        data_lines.append(
            Line2D(
                [0],
                [0],
                marker=marker,
                color=cmaplist[i],
                label=label,
                lw=4,
            )
        )

        # Add f-label text
        if plot_settings["add_f_label_text"]:
            rightmost_q_val = filtered_df["massratio_accretor_donor"].tolist()[-1]
            rightmost_y_val = filtered_df[radius_quantity].tolist()[-1]

            axes.text(
                rightmost_q_val * 1.1,
                rightmost_y_val,
                "f = {:1.1f}".format(synchronicity_factor),
                fontsize=fontsize_lines,
            )

    ########## TODO: move this to the other functions that handle this stuff
    # Add data of ulrich & kolb and lubow & shu
    add_paper_legend = True
    q_acc_don = 10 ** np.linspace(-2, 2, 100)
    lines_legend_papers = []

    ## Ulrich & Kolb
    label = r"$r_{\mathrm{circ}}/r_{\mathrm{min}}$ Ulrich & Kolb 1976"
    color = "b"
    linestyle = "--"

    ulrich_kolb_data = ulrich_kolb(
        mass_donor=np.ones(q_acc_don.shape), mass_accretor=q_acc_don
    )
    axes.plot(
        q_acc_don,
        ulrich_kolb_data["rcirc"] / ulrich_kolb_data["rmin"],
        linestyle,
        color=color,
    )

    lines_legend_papers.append(
        Line2D([0], [0], linestyle=linestyle, color=color, lw=4, label=label)
    )

    ## Lubow & Shu
    label = r"$r_{\mathrm{circ}}/r_{\mathrm{min}}$ Lubow & Shu 1975"
    color = "red"
    marker = "d"

    lubow_shu_datapoints = return_lubow_shu_table_2_data()
    axes.plot(
        lubow_shu_datapoints["q_acc_don"],
        np.array(lubow_shu_datapoints["rcirc"])
        / np.array(lubow_shu_datapoints["rmin"]),
        marker=marker,
        linestyle="None",
        color=color,
        zorder=200,
    )
    lines_legend_papers.append(
        Line2D([0], [0], marker=marker, linestyle="None", color=color, label=label)
    )

    ######
    # Make up

    # Add legend for the data
    legend_data = plt.legend(
        handles=data_lines,
        loc=plot_settings.get("loc_fsync_legend", 4),
        ncol=2,
    )
    axes.add_artist(legend_data)

    # Add legend for the other references
    if add_paper_legend:
        legend_papers = plt.legend(
            handles=lines_legend_papers,
            loc=plot_settings.get("loc_comparison_legend", 1),
        )
        axes.add_artist(legend_papers)

    #
    axes.set_xscale("log")
    axes.set_yscale(plot_settings.get("yscale", "linear"))

    if plot_settings.get("yscale", "linear") == "log":
        axes.yaxis.set_minor_formatter(
            ticker.FuncFormatter(lambda y, _: "{:g}".format(y))
        )
    else:
        axes.yaxis.set_major_formatter(
            ticker.FuncFormatter(lambda y, _: "{:g}".format(y))
        )

    #
    axes.set_ylabel(y_label_dict[radius_quantity])
    axes.set_xlabel(xlabel)

    current_xlim = axes.get_xlim()
    axes.set_xlim([current_xlim[0], current_xlim[1] * 2])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_ratio_rcirc_rmin(
    input_filename, output_name, show_plot, query_string, plot_settings
):
    """
    General function to handle the paper version of plot for the ratio of the circularisation radius to the radius of closest approach
    """

    plot_settings = {
        "show_plot": show_plot,
        "add_f_label_text": False,
        "add_ulrich_plot": True,
        "add_fkr_plot": True,
        "add_lubow_plot": True,
        "add_failed_fraction_measure": True,
        "add_orientation_legend": True,
        **plot_settings,
    }

    plot_ratio_rcirc_rmin_grid(
        input_filename,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


def handle_paper_plot_ratio_rcirc_rmin_low_v(input_filename, output_name, show_plot):
    """
    Function to handle the paper version of plot for the ratio of the circularisation radius to the radius of closest approach for low thermal velocity
    """

    plot_settings = {
        "loc_fsync_legend": 3,
        "loc_comparison_legend": 1,
    }

    #
    query_string = "log10normalized_thermal_velocity_dispersion==-3"

    handle_paper_plot_ratio_rcirc_rmin(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
        plot_settings=plot_settings,
    )


def handle_paper_plot_ratio_rcirc_rmin_high_v(input_filename, output_name, show_plot):
    """
    Function to handle the paper version of plot for the ratio of the circularisation radius to the radius of closest approach for high thermal velocity
    """

    plot_settings = {"loc_fsync_legend": 1, "loc_comparison_legend": 4, "yscale": "log"}

    #
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5"

    handle_paper_plot_ratio_rcirc_rmin(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
        plot_settings=plot_settings,
    )


def handle_paper_plot_ratio_rcirc_rmin_manual(
    input_filename, output_name, show_plot, log10v
):
    """
    Function to handle the paper version of plot for the ratio of the circularisation radius to the radius of closest approach for high thermal velocity
    """

    #
    query_string = "log10normalized_thermal_velocity_dispersion=={}".format(log10v)

    handle_paper_plot_ratio_rcirc_rmin(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


if __name__ == "__main__":
    ###########
    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    ############
    # Set up ballistic data filename
    ballistic_data_filename = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
        "trajectory_summary_data.txt",
    )

    ###########
    #
    output_name = os.path.join(plot_dir, "manual_test.pdf")
    show_plot = False
    handle_paper_plot_ratio_rcirc_rmin_manual(
        input_filename=ballistic_data_filename,
        output_name=output_name,
        show_plot=show_plot,
        log10v=-1.0,
    )
