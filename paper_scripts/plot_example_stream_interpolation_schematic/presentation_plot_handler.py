import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from ballistic_integrator.functions.frame_of_reference import (
    calculate_position_accretor,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)
from scipy import interpolate

from ballistic_integration_code.paper_scripts.plot_example_stream_interpolation_schematic.plot_example_stream_interpolation_schematic import (
    plot_example_multiple_stream_interpolation_schematic,
    plot_example_stream_interpolation_schematic,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    rochelobe_radius_accretor,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_for_presentation():

    plot_settings = {
        "star_text_color": "white",
        "show_plot": False,
        "verbosity": 1,
        "x_bounds": [0.25, 0.75],
        "y_bounds": [-0.2, 0.2],
        "textsize": 26,
        "markersize": 10,
    }

    # # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    output_dir = os.path.join(this_file_dir, "plots/")

    # basename
    basename = "PRESENTATION_SCHEMATIC_MULTIPLE.pdf"

    ###########
    #
    system_dict_1 = {
        **standard_system_dict,
        "synchronicity_factor": 0.75,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    system_dict_2 = {
        **standard_system_dict,
        "synchronicity_factor": 1.0,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    system_dict_3 = {
        **standard_system_dict,
        "synchronicity_factor": 0.6,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    #
    settings = {
        **stage_3_settings,
        "num_cores": 1,
        "jacobi_error_tol": 1e-2,
        "num_samples_area_sampling": 1,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "specific_angular_momentum_stream_interpolation_enabled": False,
        "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_upper_bound": 0.9,  # in terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_num_radii": 6,  # number of radii to calculate properties at
        "verbosity": 3,
        "generate_plot_at_gridpoint": False,
    }

    # plot_example_stream_interpolation_schematic(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )

    plot_example_multiple_stream_interpolation_schematic(
        system_dicts=[system_dict_1, system_dict_2, system_dict_3],
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )


def plot_for_mathieu():

    plot_settings = {
        "star_text_color": "white",
        "show_plot": False,
        "verbosity": 1,
        "x_bounds": [-0.2, 0.75],
        "y_bounds": [-0.4, 0.5],
        "textsize": 26,
        "markersize": 10,
    }

    # # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    output_dir = os.path.join(this_file_dir, "plots/")

    # basename
    basename = "PRESENTATION_SCHEMATIC_MULTIPLE.pdf"

    ###########
    #
    system_dict_1 = {
        **standard_system_dict,
        "synchronicity_factor": 0.75,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    system_dict_2 = {
        **standard_system_dict,
        "synchronicity_factor": 1.0,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    system_dict_3 = {
        **standard_system_dict,
        "synchronicity_factor": 0.6,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    system_dict_4 = {
        **standard_system_dict,
        "synchronicity_factor": 1.75,
        "mass_accretor": 10**0,
        "mass_donor": 2,
    }

    #
    settings = {
        **stage_3_settings,
        "num_cores": 1,
        "jacobi_error_tol": 1e-2,
        "num_samples_area_sampling": 1,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "specific_angular_momentum_stream_interpolation_enabled": False,
        "specific_angular_momentum_stream_interpolation_lower_bound": 0.1,  # In terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_upper_bound": 0.9,  # in terms of roche lobe radius of accretor
        "specific_angular_momentum_stream_interpolation_num_radii": 6,  # number of radii to calculate properties at
        "verbosity": 3,
        "generate_plot_at_gridpoint": False,
    }

    # plot_example_stream_interpolation_schematic(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )

    plot_example_multiple_stream_interpolation_schematic(
        system_dicts=[system_dict_1, system_dict_2, system_dict_3, system_dict_4],
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )


plot_for_mathieu()
