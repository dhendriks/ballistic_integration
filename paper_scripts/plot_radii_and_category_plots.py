"""
Wrapper function to plot all the functions
"""

import os

from ballistic_integration_code.paper_scripts.classification_fraction_plot.plot_classification_fractions import (
    plot_classification_fractions,
)
from ballistic_integration_code.paper_scripts.radii_plots.plot_ratio_rcirc_rmin import (
    plot_ratio_rcirc_rmin_grid,
)
from ballistic_integration_code.paper_scripts.radii_plots.plot_rcirc_for_non_synchronous_rotators import (
    plot_circularisation_radius_grid,
)
from ballistic_integration_code.paper_scripts.radii_plots.plot_rmin_for_non_synchronous_rotators import (
    plot_minimum_radius_grid,
)
from ballistic_integration_code.paper_scripts.specific_angular_momentum_increase_self_accretion.plot_specific_angular_momentum_factor_self_accretion import (
    plot_specific_angular_momentum_factor_self_accretion,
)

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_all_radii_and_category(
    result_file,
    query_string,
    plot_settings,
    handle_circularisation_plot,
    handle_minimum_radius_plot,
    handle_ratio_rcirc_rmin_plot,
    handle_self_accretion_plot,
    handle_classification_plot,
):
    """
    Function to run a set of plots
    """

    ######################
    # circularisation radius plot
    if handle_circularisation_plot:
        plot_circularisation_radius_grid(
            result_file,
            query=query_string,
            add_ulrich_plot=plot_settings["add_ulrich_plot"],
            add_fkr_plot=plot_settings["add_fkr_plot"],
            add_lubow_plot=plot_settings["add_lubow_plot"],
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(
                    plot_settings["plot_dir"],
                    "circularisation_radii_" + plot_settings["basename"],
                ),
            },
        )

    ######################
    # minimum radius plot
    if handle_minimum_radius_plot:
        plot_minimum_radius_grid(
            result_file,
            query=query_string,
            add_ulrich_plot=plot_settings["add_ulrich_plot"],
            add_fkr_plot=False,
            add_lubow_plot=plot_settings["add_lubow_plot"],
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(
                    plot_settings["plot_dir"],
                    "radii_of_closest_approach_" + plot_settings["basename"],
                ),
            },
        )

    ######################
    # Radii minimum radius and circularisation radius
    if handle_ratio_rcirc_rmin_plot:
        plot_ratio_rcirc_rmin_grid(
            result_file,
            query=query_string,
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(
                    plot_settings["plot_dir"],
                    "ratio_rcirc_rmin_" + plot_settings["basename"],
                ),
                "loc_fsync_legend": 1,
                "loc_comparison_legend": 4,
            },
        )

    ######################
    # Radii minimum radius and circularisation radius
    if handle_self_accretion_plot:
        plot_specific_angular_momentum_factor_self_accretion(
            result_file,
            query=query_string,
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(
                    plot_settings["plot_dir"],
                    "self_accretion_h_factor_" + plot_settings["basename"],
                ),
            },
        )

    ######################
    # Radii minimum radius and circularisation radius
    if handle_classification_plot:
        plot_classification_fractions(
            result_file,
            query=query_string,
            add_failed_fraction_measure=plot_settings["add_failed_fraction_measure"],
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(
                    plot_settings["plot_dir"],
                    "classification_fractions_" + plot_settings["basename"],
                ),
            },
        )


def generate_radii_and_classification_plots(simdir, output_dir):
    """
    Handler function for the plotting of the radi and the classification plots
    """

    plot_rmin = True
    plot_rcirc = True
    plot_classification = True
    plot_ratio = True
    plot_self_accretion = True

    function_call_set = []

    #
    result_file = os.path.join(simdir, "trajectory_summary_data.txt")

    # low v
    function_call_set.append(
        {
            "result_file": result_file,
            "query_string": "log10normalized_thermal_velocity_dispersion==-3",
            "plot_settings": {
                "plot_dir": output_dir,
                "basename": "stage_3_low_v.pdf",
                "show_plot": False,
                "add_f_label_text": False,
                "add_ulrich_plot": True,
                "add_fkr_plot": True,
                "add_lubow_plot": True,
                "add_failed_fraction_measure": True,
            },
        }
    )

    # mid v
    function_call_set.append(
        {
            "result_file": result_file,
            "query_string": "log10normalized_thermal_velocity_dispersion==-1.75",
            "plot_settings": {
                "plot_dir": output_dir,
                "basename": "stage_3_mid_v.pdf",
                "show_plot": False,
                "add_f_label_text": False,
                "add_ulrich_plot": True,
                "add_fkr_plot": True,
                "add_lubow_plot": True,
                "add_failed_fraction_measure": True,
            },
        }
    )

    # high v
    function_call_set.append(
        {
            "result_file": result_file,
            "query_string": "log10normalized_thermal_velocity_dispersion==-0.5",
            "plot_settings": {
                "plot_dir": output_dir,
                "basename": "stage_3_high_v.pdf",
                "show_plot": False,
                "add_f_label_text": False,
                "add_ulrich_plot": True,
                "add_fkr_plot": True,
                "add_lubow_plot": True,
                "add_failed_fraction_measure": True,
            },
        }
    )

    ###############
    #
    for function_call in function_call_set:
        plot_all_radii_and_category(
            result_file=function_call["result_file"],
            query_string=function_call["query_string"],
            plot_settings=function_call["plot_settings"],
            handle_circularisation_plot=plot_rcirc,
            handle_minimum_radius_plot=plot_rmin,
            handle_ratio_rcirc_rmin_plot=plot_ratio,
            handle_self_accretion_plot=plot_self_accretion,
            handle_classification_plot=plot_classification,
        )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    plot_rmin = True
    plot_rcirc = False
    plot_classification = False
    plot_ratio = False
    plot_self_accretion = False

    # plot_settings = {"show_plot": False, "add_f_label_text": False}
    add_orientation_legend = True
    show_plot = False

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    #######################
    # Server stage 3 results
    #######################

    ##############
    # calls
    function_call_set = []

    simnames = [
        # "ballistic_stream_integration_results_stage_3",
        # "ballistic_stream_integration_results_stage_3_NO_AREA_SAMPLING",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET",
        # "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_NO_AREA_SAMPLING",
    ]

    ##############
    for simname in simnames:
        result_dir = os.path.join(
            os.environ["PROJECT_DATA_ROOT"],
            "ballistic_data",
            "L1_stream",
            "server_results",
            simname,
        )
        result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

        simname_plot_dir = os.path.join(plot_dir, simname)
        os.makedirs(simname_plot_dir, exist_ok=True)

        # low v
        function_call_set.append(
            {
                "result_file": result_file,
                "query_string": "log10normalized_thermal_velocity_dispersion==-3",
                "plot_settings": {
                    "plot_dir": simname_plot_dir,
                    "basename": "stage_3_low_v.pdf",
                    "show_plot": show_plot,
                    "add_f_label_text": False,
                    "add_ulrich_plot": True,
                    "add_fkr_plot": True,
                    "add_lubow_plot": True,
                    "add_failed_fraction_measure": True,
                    "add_orientation_legend": add_orientation_legend,
                },
            }
        )

        # mid v
        function_call_set.append(
            {
                "result_file": result_file,
                "query_string": "log10normalized_thermal_velocity_dispersion==-1.75",
                "plot_settings": {
                    "plot_dir": simname_plot_dir,
                    "basename": "stage_3_mid_v.pdf",
                    "show_plot": show_plot,
                    "add_f_label_text": False,
                    "add_ulrich_plot": True,
                    "add_fkr_plot": True,
                    "add_lubow_plot": True,
                    "add_failed_fraction_measure": True,
                    "add_orientation_legend": add_orientation_legend,
                },
            }
        )

        # high v
        function_call_set.append(
            {
                "result_file": result_file,
                "query_string": "log10normalized_thermal_velocity_dispersion==-0.5",
                "plot_settings": {
                    "plot_dir": simname_plot_dir,
                    "basename": "stage_3_high_v.pdf",
                    "show_plot": show_plot,
                    "add_f_label_text": False,
                    "add_ulrich_plot": True,
                    "add_fkr_plot": True,
                    "add_lubow_plot": True,
                    "add_failed_fraction_measure": True,
                    "add_orientation_legend": add_orientation_legend,
                },
            }
        )

    ###############
    #
    for function_call in function_call_set:
        plot_all_radii_and_category(
            result_file=function_call["result_file"],
            query_string=function_call["query_string"],
            plot_settings=function_call["plot_settings"],
            handle_circularisation_plot=plot_rcirc,
            handle_minimum_radius_plot=plot_rmin,
            handle_ratio_rcirc_rmin_plot=plot_ratio,
            handle_self_accretion_plot=plot_self_accretion,
            handle_classification_plot=plot_classification,
        )
