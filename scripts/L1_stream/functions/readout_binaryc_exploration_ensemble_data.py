"""
Function to read out the ballistic parameter exploration ensemble
"""

import pandas as pd


def readout_binaryc_exploration_ensemble_data(filename):
    """
    Function to read out the ballistic parameter exploration ensemble
    """

    result_df = pd.read_csv(filename, sep="\s+", header=0)
    print(result_df[result_df.probability > 0])


if __name__ == "__main__":
    filename = "/home/david/projects/binary_c_root/results/RLOF/CUSTOM_RES_SANA/population_results/Z0.001/subproject_ballistic_ensemble_data.csv"

    readout_binaryc_exploration_ensemble_data(filename)
