"""
Function to perform some quality checks
"""


def check_rectangular(df):
    """
    Function to check if the df is rectangular
    """

    unique_massratios = df["massratio_accretor_donor"].unique().tolist()
    unique_A_factors = df["A_factor"].unique().tolist()

    # get all possible unique combinations
    possible_combinations = [
        (x, y) for x in unique_massratios for y in unique_A_factors
    ]

    # Get the present combinations
    present_combinations = [
        tuple(el) for el in df[["massratio_accretor_donor", "A_factor"]].values.tolist()
    ]

    # get missing combinations
    missing_combinations = set(possible_combinations) - set(present_combinations)

    if len(missing_combinations) > 0:
        print("not rectangular. missing data")

    pass


def quality_check(df, settings):
    """
    Function to manage the quality checks
    """

    # check rectangular
    check_rectangular(df=df)


if __name__ == "__main__":

    print("yo")

    set1 = set([(1, 2), (3, 4)])

    set2 = set([(3, 4), (5, 6)])

    print(set1 - set2)
