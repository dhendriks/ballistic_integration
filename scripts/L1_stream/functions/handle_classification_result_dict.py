"""
Function to handle the 'classification' of the result dict based on the exit codes
"""

from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES


def handle_classification_result_dict(result_dict):
    """
    Function to handle the 'classification' of the result dict
    """

    # Check for accretion onto accretor
    result_dict["accretion_onto_accretor"] = (
        1 if result_dict["exit_code"] == EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value else 0
    )
    result_dict["accretion_onto_donor"] = (
        1 if result_dict["exit_code"] == EXIT_CODES.ACCRETION_ONTO_DONOR.value else 0
    )
    result_dict["undefined_accretion"] = (
        1
        if result_dict["exit_code"] == EXIT_CODES.LOST_FROM_SYSTEM_UNDEFINED.value
        else 0
    )
    result_dict["terminated"] = (
        1
        if result_dict["exit_code"]
        not in [
            EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value,
            EXIT_CODES.ACCRETION_ONTO_DONOR.value,
            EXIT_CODES.LOST_FROM_SYSTEM_UNDEFINED.value,
        ]
        else 0
    )
    result_dict["rejected_based_on_intersection"] = 0

    return result_dict
