import copy

import numpy as np
import pandas as pd
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    A_formula,
)


def add_warningflag(dataframe):
    """
    Function to add a warning flag to the pandas dataframe. If 0: all is fine. If 1: something within that row is wrong, and contained a bad value
    """

    # Set default
    dataframe["warning_flag"] = 0

    # Find indices in the df where the row contains nan values
    ind = dataframe.loc[dataframe.isnull().any(axis=1)].index
    dataframe.iloc[ind, dataframe.columns.get_loc("warning_flag")] = 1

    return dataframe


def fix_nan_df(dataframe, defaultval=0):
    """
    Function to fix nan value in the dataframe and give a default to 0
    """

    for col_i, col in enumerate(dataframe.columns):
        nanvals = np.isnan(dataframe[col].to_numpy())
        dataframe.iloc[nanvals, col_i] = defaultval

    return dataframe


def prepare_df(
    settings, input_interpolation_textfile, input_parameter_list, output_parameter_list
):
    """
    Function to prepare the dataframe to write
    """

    # Read out interpolation textfile
    df = pd.read_csv(input_interpolation_textfile, sep="\s+", header=0)
    print(df)
    # Add columns to df
    df["A_factor"] = A_formula(f=df["synchronicity_factor"])

    # Add warning flag
    df = add_warningflag(dataframe=df)
    output_parameter_list += ["warning_flag"]

    # fix bad values
    df = fix_nan_df(dataframe=df)

    ######################
    # Abort when too many of the rows contain bad values
    # TODO: implement some user input here or flag to enable this
    if np.sum(df["warning_flag"] > settings["warning_flag_threshold"]):
        # Abort
        raise ValueError(
            "Too many rows in the interpolation table dataframe seem to contain bad values"
        )

    ############
    # Prepare for write-out

    # Make sure the order is correct
    filtered_df = df[input_parameter_list + output_parameter_list]

    # make sure to sort the columns
    filtered_df = filtered_df.sort_values(by=input_parameter_list)

    return filtered_df, output_parameter_list


def handle_interpolation_radii_parameter(
    output_parameter_list, settings, parameter_dict
):
    """
    Function a to add the interpolation radii to the pameter dict and output parameter list
    """

    #
    parameter_dict = copy.deepcopy(parameter_dict)

    # Add stream interpolation radii to output parameters if flag is set
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:

        interpolation_radius_fractions = np.linspace(
            settings["specific_angular_momentum_stream_interpolation_lower_bound"],
            settings["specific_angular_momentum_stream_interpolation_upper_bound"],
            settings["specific_angular_momentum_stream_interpolation_num_radii"],
        )
        interpolation_radius_output_parameter_list = []

        # loop over stream interpolation radii
        for stream_interpolation_radius_i in range(
            settings["specific_angular_momentum_stream_interpolation_num_radii"]
        ):
            interpolation_radius_i_string = (
                "specific_angmom_interpolation_radius_{}".format(
                    stream_interpolation_radius_i
                )
            )

            # Add to interpolation radius output parameter list
            interpolation_radius_output_parameter_list.append(
                interpolation_radius_i_string
            )

            # add to parameter dict
            parameter_dict[interpolation_radius_i_string] = {
                "shortname": interpolation_radius_i_string,
                "longname": interpolation_radius_i_string,
                "description": "Stream interpolation radius at R/RL_acc = {}".format(
                    interpolation_radius_fractions[stream_interpolation_radius_i]
                ),
            }

        # Add to total output parameter list
        output_parameter_list += interpolation_radius_output_parameter_list

    return output_parameter_list, parameter_dict
