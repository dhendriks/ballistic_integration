"""
Tmp script
"""

import os

import matplotlib
import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)

from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


system_dict = {
    **standard_system_dict,
    "mass_donor": 1,
    "mass_accretor": 0.01,
    # "mass_accretor": 50,
    # "mass_accretor": 0.01,
    "synchronicity_factor": 1.5,
}

"""
Function to handle plotting an example set of trajectories with different classifications
"""


def plot_trajectory_classification_example(settings, plot_settings):
    """
    Function to plot different sets of trajectories
    """

    x_bounds = [-0.05, 0.7]
    y_bounds = [-0.3, 0.3]

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    #
    fig = plt.figure(figsize=(20, 20))
    fig, axes_list = plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        plot_rochelobe_equipotential_meshgrid=False,
        plot_system_information_panel=False,
        return_fig=True,
        plot_settings={
            **plot_settings,
            "plot_bounds_x": plot_settings["x_bounds"],
            "plot_bounds_y": plot_settings["y_bounds"],
            "plot_resolution": settings["plot_resolution"],
            "star_text_color": "white",
        },
        settings=settings,
        fig=fig,
    )

    ######
    # Resize
    for ax in axes_list:
        ax.set_xlim(plot_settings["x_bounds"])
        ax.set_ylim(plot_settings["y_bounds"])

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # Add labels
    axes_list[0].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_list[0].set_ylabel(r"$\it{y}$", fontsize=36)

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


if __name__ == "__main__":
    #######
    # Config

    plot_settings = {
        "star_text_color": "white",
        "show_plot": False,
        "verbosity": 1,
        "x_bounds": [-0.8, 0.8],
        "y_bounds": [-1, 1],
    }

    # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )

    ###########
    # basename
    basename = "trajectory_classification_overview.pdf"

    #
    system_dict = {
        **standard_system_dict,
        "mass_donor": 1,
        "mass_accretor": 0.01,
        # "mass_accretor": 50,
        # "mass_accretor": 0.01,
        "synchronicity_factor": 1.8,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -0.5,
    }

    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 4,
        "jacobi_error_tol": 1e-2,
        "dt": 0.01,
        "num_samples_area_sampling": 12,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "include_asynchronous_donor_effects": True,
        "include_asynchronicity_donor_during_integration": True,
    }

    plot_trajectory_classification_example(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )
