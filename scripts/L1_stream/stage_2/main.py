"""
Main script to run stage 2 of the ballistic integrations:
- grid over initial_velocity/aw, f and q
"""

import os

import numpy as np

from ballistic_integration_code.scripts.L1_stream.functions.run_all import run_all
from ballistic_integration_code.scripts.L1_stream.stage_2.stage_settings import (
    stage_2_settings,
)

######################
# Run grid           #
######################

settings = stage_2_settings

############
# Test run
settings = {
    **settings,
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_2_TEST",
    ),
    "q_range": 10 ** np.linspace(-2, 2, 2),
    "f_range": np.linspace(0.1, 1, 2),
    "jacobi_error_tol": 1e-3,
    "dt": 0.000001,
    "verbosity": 3,
    "num_cores": 1,
    "log10normalized_thermal_velocity_dispersion_range": np.linspace(-3.5, -0.5, 2),
}
run_all(
    settings=settings,
    stage='stage_2',
    run_trajectories=True,
    generate_report=True,
    create_data_header=True,
    generate_description_table=True
)
# re_run_settings = {"result_dir": settings['result_dir'] + "RE_RUN", "plot_system_at_re_run": True}
# run_all(settings=settings, stage='stage_1', run_trajectories=False, generate_report=False, create_data_header=False, generate_description_table=False
#     re_run_failed_systems=True, re_run_settings=re_run_settings
# )


# ############
# # Production run
# run_all(
#     settings=settings,
#     stage='stage_2',
#     run_trajectories=True,
#     generate_report=True,
#     create_data_header=True,
#     generate_description_table=True
# )
# # re_run_settings = {"result_dir": settings['result_dir'] + "_RE_RUN", "plot_system_at_re_run": True, "write_failed_passed_systems": False, "verbosity": 2, "timestep_increase_factor": 1.05, "max_position_diff_timestep_control": 0.000025}
# # run_all(settings=settings, stage='stage_2', run_trajectories=False, generate_report=False, create_data_header=False, generate_description_table=False
# #     re_run_failed_systems=True, re_run_settings=re_run_settings
# # )
