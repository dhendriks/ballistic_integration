"""
Script to read out the exploration ensemble and handle/filter out some of the bad stuff
"""

import copy
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import add_label_subplot, show_and_save_plot

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins,
    readout_and_filter_exploration_ensemble,
)
from ballistic_integration_code.paper_scripts.plot_exploration_parameters.settings import (
    colors,
    hatch_list,
    label_dict,
    xlabel_dict,
    ylabel_dict,
)

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_exploration_parameter_distributions(
    filename,
    include_weight_type,
    stellar_type_accretor=None,
    stellar_type_donor=None,
    pre_loaded_data=None,
    plot_settings={},
):
    """
    Function to generate a plot based on the unique values of these columns
    """

    # Get the data
    if pre_loaded_data is not None:
        combined = copy.deepcopy(pre_loaded_data["combined"])
        parameter_cols = pre_loaded_data["parameter_cols"]
        parameter_extent_dict = pre_loaded_data["parameter_extent_dict"]
        weight_type_name_dict = pre_loaded_data["weight_type_name_dict"]
    else:
        (
            combined_df,
            parameter_cols,
            parameter_extent_dict,
            weight_type_name_dict,
        ) = readout_and_filter_exploration_ensemble(filename)

    ##########
    # Perform additional filter on stellar type
    if stellar_type_accretor is not None:
        combined = combined[combined.st_acc == stellar_type_accretor]
    if stellar_type_donor is not None:
        combined = combined[combined.st_don == stellar_type_accretor]

    combined_df = combined_df.sort_values(by="log10q_acc_don")
    print(combined_df.head())
    quit()

    ##########
    # Set up figure
    fig = plt.figure(figsize=(20, 24))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=len(parameter_cols), ncols=1)

    # Invisible axis:
    ax_invisible = fig.add_subplot(gs[:, :], frame_on=False)
    ax_invisible.set_xticks([])
    ax_invisible.set_yticks([])

    ax_invisible.set_ylabel(
        "Normalised weight",
        labelpad=150,
        fontsize=plot_settings.get("fontsize_ylabel", 36),
    )

    # Loop over all the relevant parameter and generate the plots
    for col_i, col in enumerate(parameter_cols):
        ax = fig.add_subplot(gs[col_i, :])

        aggregate_dict = {col: "first"}
        for weight_i, (weight_type, weight_colname) in enumerate(
            weight_type_name_dict.items()
        ):
            aggregate_dict[weight_colname] = "sum"

        # Calculate regardless of weight type
        grouped_df = combined_df.groupby(col, as_index=False).aggregate(aggregate_dict)
        grouped_df = grouped_df.sort_values(by=col)

        # normalise all prob columns
        for weight_i, (weight_type, weight_colname) in enumerate(
            weight_type_name_dict.items()
        ):
            grouped_df[weight_colname] = (
                grouped_df[weight_colname] / grouped_df[weight_colname].sum()
            )

        # Calculate smallest probability
        all_weight_probability_values = grouped_df[list(weight_type_name_dict.values())]
        min_min_val = all_weight_probability_values.min().min()

        # Loop over weight type
        for weight_i, (weight_type, weight_colname) in enumerate(
            weight_type_name_dict.items()
        ):

            # Only handle the correct weight types
            if weight_type not in include_weight_type:
                continue

            # Get the bins
            bins = calculate_bins(array=grouped_df[col].to_numpy())

            # create histogram
            _ = ax.hist(
                grouped_df[col],
                bins=bins,
                weights=grouped_df[weight_colname],
                label=label_dict[weight_type],
                histtype="step",
                hatch=hatch_list[weight_i],
                edgecolor="k",
                facecolor=colors[weight_i],
                fill=True,
                alpha=0.25,
                density=True,
            )

        ######
        # Make-up

        # add lines
        ax.axhline(10**-5)

        # labels, resize and scale
        ax.set_yscale("log")
        ax.set_xlabel(
            xlabel_dict[col], fontsize=plot_settings.get("fontsize_xlabel", 36)
        )
        ax.set_ylabel(
            ylabel_dict[col], fontsize=plot_settings.get("fontsize_ylabel", 36)
        )

        # Shift the xlims
        xlim_pad_factor = (
            np.abs((parameter_extent_dict[col][1] - parameter_extent_dict[col][0])) / 8
        )
        ax.set_xlim(
            [
                parameter_extent_dict[col][0] - xlim_pad_factor,
                parameter_extent_dict[col][1] + xlim_pad_factor,
            ]
        )

        # Place legend
        if col_i == 0:
            ax.legend(loc=2, fontsize=plot_settings.get("fontsize_legend", 24))

        # Add label subplot
        fig, ax = add_label_subplot(
            fig=fig,
            ax=ax,
            label_i=col_i,
            x_loc=0.95,
            y_loc=0.95,
            bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        )

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_exploration_parameter_distributions(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of exploration parameter distributions
    """

    include_weight_type = ["mass", "time"]

    plot_exploration_parameter_distributions(
        input_filename,
        include_weight_type=include_weight_type,
        plot_settings={
            "show_plot": show_plot,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    show_plot = False
    include_weight_type = ["mass", "time"]

    #
    result_root = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "RLOF/")

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # plot_dir = os.path.join(this_file_dir, "plots/")

    # ######
    # # TESTING
    # filename = os.path.join(
    #     result_root,
    #     "TEST_RES_EXPLORATION_BALLISTIC_RLOF_2021_SANA/population_results/Z0.001/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv"
    # )
    # basename = "exploration_parameter_ranges_test.pdf"
    # plot_exploration_parameter_distributions(
    #     filename,
    #     include_weight_type=include_weight_type,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # #####
    # # FIDUCIAL
    # filename = os.path.join(
    #     result_root,
    #     "server_results/EVENTS_V2.2.2_HIGH_RES_RLOF_2021_SANA_BALLISTIC_EXPLORATION/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    # )
    # basename = "exploration_parameter_ranges.pdf"
    # plot_exploration_parameter_distributions(
    #     filename,
    #     include_weight_type=include_weight_type,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    #####
    # FIDUCIAL
    filename = os.path.join(
        result_root,
        "server_results/EVENTS_V2.2.4_HIGH_RES_EXPLORATION_BALLISTIC_RLOF_2021_MOE_DISTEFANO/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )

    basename = "exploration_parameter_ranges.pdf"
    plot_exploration_parameter_distributions(
        filename,
        include_weight_type=include_weight_type,
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
        },
    )
