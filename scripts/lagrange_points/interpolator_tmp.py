import numpy as np
from scipy.interpolate import RegularGridInterpolator


def f(x, y):

    return x - y


x = np.linspace(1, 4, 4)
y = np.linspace(4, 8, 5)

xg, yg = np.meshgrid(x, y, indexing="ij", sparse=True)


print(xg, yg)

data = f(xg, yg)

print(data)

print(data.shape)

interp = RegularGridInterpolator((x, y), data)
