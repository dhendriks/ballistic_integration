"""
Functions to compare zeta_L.

The idea is to fix a gamma (e.g. isotropic), set f_sync value bounds (f_sync_lower, f_sync_upper)

- start with very general zeta_L
- make new function that fixes gamma
- generate zeta_L based on synchronous donor
- generate zeta_L based on sub-synchronous donor
- generate zeta_L based on super-synchronous donor

Plots (all for isotropic)
- plot zeta_L
- relative to sync, how does a very sub-synchronous rotation affect the zeta_L?
- relative to sync, how does a very sub-synchronous rotation affect the zeta_L?
- plot relative extent change total size


The plots need to be clear
TODO: fix 1 clear plotting routine that can handle whatever and then tinker with the colorbar things
TODO: fix plotting routine
TODO: explicitly test whether things are correct in comparison to the zeta_L calculation
TODO: increase resolution a bit more and then


TODO: plot

"""

import os
from functools import partial

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from calculate_derivative import (
    add_derivative_to_df,
    create_roche_lobe_derivative_interpolator_dict,
    load_df,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)
from grav_waves.single_star_evolution.scripts.create_initial_final_mass_grid.functions.plot_functions.utility_functions import (
    linestyle_tuple,
)
from matplotlib import colors
from plotting import plot_2d_episode_distribution
from utils import call_interpolator

custom_mpl_settings.load_mpl_rc()
matplotlib.rcParams["savefig.dpi"] = 300

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


plot_output_dir = os.path.join(this_file_dir, "plots/")


#
filename = "results/test/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"

#
df = load_df(filename=filename)
df = add_derivative_to_df(df=df)

#
interpolator_dict = create_roche_lobe_derivative_interpolator_dict(df=df)


def find_all_min_max(array_list):
    """
    function to find all mins and maxes
    """

    mins = []
    maxs = []

    for arr in array_list:
        mins.append(np.min(array_list))
        maxs.append(np.max(array_list))

    return np.min(mins), np.max(maxs)


def logarithmic_derivative_scale_factor_roche_lobe(q_acc_don, fsync):
    """
    Function that calculates the scale factor on the logarithmic derivative of the roche-lobe radius as a result of asynchronous donor rotation
    """

    coordinate = (fsync, q_acc_don)
    sync_coordinate = (1, q_acc_don)

    # unpack interpolator
    roche_lobe_radius_interpolator = interpolator_dict["roche_lobe_radius_interpolator"]
    d_roche_lobe_radii_d_q_acc_don_interpolator = interpolator_dict[
        "d_roche_lobe_radii_d_q_acc_don_interpolator"
    ]

    # calculate logarithmic derivative for given fsync
    roche_lobe_radius = call_interpolator(
        roche_lobe_radius_interpolator, f_sync=fsync, q_acc_don=q_acc_don
    )
    d_roche_lobe_radii_d_q_acc_don = call_interpolator(
        d_roche_lobe_radii_d_q_acc_don_interpolator, f_sync=fsync, q_acc_don=q_acc_don
    )
    log_derivative_for_given_fsync = d_roche_lobe_radii_d_q_acc_don * (
        q_acc_don / roche_lobe_radius
    )
    # print(coordinate)
    # print(roche_lobe_radius)
    # print(d_roche_lobe_radii_d_q_acc_don)
    # print(log_derivative_for_given_fsync)

    # calculate logarithmic derivative for synchronous system
    roche_lobe_radius_sync = call_interpolator(
        roche_lobe_radius_interpolator, f_sync=1, q_acc_don=q_acc_don
    )
    d_roche_lobe_radii_d_q_acc_don_sync = call_interpolator(
        d_roche_lobe_radii_d_q_acc_don_interpolator, f_sync=1, q_acc_don=q_acc_don
    )
    log_derivative_for_synchronous = d_roche_lobe_radii_d_q_acc_don_sync * (
        q_acc_don / roche_lobe_radius_sync
    )
    # print(sync_coordinate)
    # print(roche_lobe_radius_sync)
    # print(d_roche_lobe_radii_d_q_acc_don_sync)
    # print(log_derivative_for_synchronous)

    # calculate scale factor
    logarithmic_derivative_scale_factor = (
        log_derivative_for_given_fsync / log_derivative_for_synchronous
    )
    # print(logarithmic_derivative_scale_factor)

    return logarithmic_derivative_scale_factor


def zeta_L(beta, q_acc_don, gamma, fsync):
    """
    General function to calculate zeta_L
    """

    # calculate conventional
    first_term = -2 * (
        1 - (beta / q_acc_don) + (beta - 1) * (gamma + 0.5) * (1 / (1 + q_acc_don))
    )
    second_term = (
        (1 / 3)
        * (
            2
            - ((1.2 + (q_acc_don ** (-1 / 3) + q_acc_don ** (-2 / 3)) ** -1))
            / (0.6 + q_acc_don ** (2 / 3) * np.log(1 + q_acc_don ** (-1 / 3)))
        )
        * ((beta / q_acc_don) - 1)
    )

    # add scaling
    logarithmic_derivative_scale_factor_roche_lobe_val = (
        logarithmic_derivative_scale_factor_roche_lobe(q_acc_don=q_acc_don, fsync=fsync)
    )
    second_term = second_term * logarithmic_derivative_scale_factor_roche_lobe_val

    # print("beta: ", beta)
    # print("q_acc_don: ", q_acc_don)
    # print("gamma: ", gamma)
    # print("fsync: ", fsync)
    # print(
    #     "logarithmic_derivative_scale_factor_roche_lobe_val: ",
    #     logarithmic_derivative_scale_factor_roche_lobe_val,
    # )
    # print("first term (change of orbit): ", first_term)
    # print("second term (change of RL): ", second_term)

    return first_term + second_term


def gamma_isotropic(q_acc_don):
    """
    Function to calculate the gamma for isotropic mass-loss
    """

    return 1.0 / q_acc_don


def gamma_jeans(q_acc_don):
    """
    Function to calculate the gamma for isotropic mass-loss
    """

    return q_acc_don


def zeta_L_isotropic(beta, q_acc_don, fsync):
    """
    Function to calculate zeta_L for isotropic mass loss
    """

    gamma_isotropic_val = gamma_isotropic(q_acc_don=q_acc_don)

    return zeta_L(
        beta=beta, q_acc_don=q_acc_don, gamma=gamma_isotropic_val, fsync=fsync
    )


def calculate_q_beta_grid(q_acc_don_arr, beta_arr, func):
    """
    Function to calculate values on a grid given a function that is assumed to only accept q_acc_don and beta
    """

    #
    q_acc_don_arr_mesh, beta_arr_mesh = np.meshgrid(q_acc_don_arr, beta_arr)
    val_arr = []

    # TODO: consider multiprocessing this LOL
    for q_acc_don, beta in zip(q_acc_don_arr_mesh.flatten(), beta_arr_mesh.flatten()):
        val = func(q_acc_don=q_acc_don, beta=beta)
        val_arr.append(val)

    val_arr = np.array(val_arr)
    val_arr = val_arr.reshape(q_acc_don_arr_mesh.shape)

    return val_arr


def plot_zeta_grid(
    q_arr,
    beta_arr,
    zeta_grid,
    colorbar_scale,
    plot_settings,
    custom_limits,
    contourlevels=None,
):
    """
    Function to plot the zeta_L grid
    """

    # print(normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid)

    # filtered = normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid[
    #     ~np.isnan(normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid)
    # ]
    # normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid
    # print(np.max(normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid))

    #
    parameter_1 = r"$q_{acc}$"
    parameter_2 = r"$\beta_{rlof}$"

    #
    ratio = 6 / 7
    main_size = 20

    fig = plt.figure(figsize=(main_size, main_size * ratio))
    gs = fig.add_gridspec(nrows=6, ncols=7)
    axis_dict = {}

    # Configure all the subpanels
    axis_dict["ax_main"] = fig.add_subplot(gs[:, :-2])

    #
    axis_dict["ax_cb"] = fig.add_subplot(gs[:, -1])

    #
    fig, axis_dict = plot_2d_episode_distribution(
        fig=fig,
        axis_dict=axis_dict,
        x_arr=q_arr,
        y_arr=beta_arr,
        result_arr=zeta_grid,
        parameter_1=parameter_1,
        parameter_2=parameter_2,
        plot_settings=plot_settings,
        box_corners=None,
        custom_limits=custom_limits,
        colorbar_scale=colorbar_scale,
    )

    #
    axis_dict["ax_main"].set_xscale("log")

    #
    fontsize = 32

    #
    axis_dict["ax_main"].set_xlabel(parameter_1, fontsize=fontsize)
    axis_dict["ax_main"].set_ylabel(parameter_2, fontsize=fontsize)
    axis_dict["ax_main"].set_title(plot_settings["title"])

    #
    if contourlevels is not None:
        # contourlevels = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
        linestyles = [el[1] for el in linestyle_tuple]
        _ = axis_dict["ax_main"].contour(
            q_arr,
            beta_arr,
            zeta_grid,
            levels=contourlevels,
            linestyles=linestyles,
            colors="k",
        )
        for contour_i, _ in enumerate(contourlevels):
            axis_dict["ax_cb"].plot(
                [axis_dict["ax_cb"].get_xlim()[0], axis_dict["ax_cb"].get_xlim()[1]],
                [contourlevels[contour_i]] * 2,
                "black",
                linestyle=linestyles[contour_i],
            )

    #######################
    # Save and finish
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ##############
    # Configuration

    #
    f_sync_lower = 0.1
    f_sync_upper = 1.9

    q_step = 0.025
    beta_step = 0.01

    #
    q_acc_don_arr = 10 ** np.arange(-2, 2 + q_step, q_step)
    beta_arr = np.arange(0, 1 + beta_step, beta_step)

    ##############
    # Make function binds
    zeta_L_isotropic_lower_fsync = partial(zeta_L_isotropic, fsync=f_sync_lower)
    zeta_L_isotropic_sync_fsync = partial(zeta_L_isotropic, fsync=1)
    zeta_L_isotropic_upper_fsync = partial(zeta_L_isotropic, fsync=f_sync_upper)

    ##############
    # Calculate the grids
    zeta_L_isotropic_lower_fsync_grid = calculate_q_beta_grid(
        q_acc_don_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        func=zeta_L_isotropic_lower_fsync,
    )
    zeta_L_isotropic_sync_fsync_grid = calculate_q_beta_grid(
        q_acc_don_arr=q_acc_don_arr, beta_arr=beta_arr, func=zeta_L_isotropic_sync_fsync
    )
    zeta_L_isotropic_upper_fsync_grid = calculate_q_beta_grid(
        q_acc_don_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        func=zeta_L_isotropic_upper_fsync,
    )

    all_min, all_max = find_all_min_max(
        [
            zeta_L_isotropic_lower_fsync_grid,
            zeta_L_isotropic_sync_fsync_grid,
            zeta_L_isotropic_upper_fsync_grid,
        ]
    )

    ##############
    # plot normal zeta_L
    # symlog
    plot_zeta_grid(
        q_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        zeta_grid=zeta_L_isotropic_sync_fsync_grid,
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "isotropic_sync.pdf"),
            "title": "isotropic mass-loss. relative extent",
        },
        colorbar_scale="symlog",
        custom_limits=[all_min, all_max],
    )

    # ##############
    # # plot upper zeta_L
    # # symlog
    # plot_zeta_grid(
    #     q_arr=q_acc_don_arr,
    #     beta_arr=beta_arr,
    #     zeta_grid=zeta_L_isotropic_upper_fsync_grid,
    #     plot_settings={
    #         "output_name": os.path.join(plot_output_dir, "isotropic_upper.pdf"),
    #         "title": "isotropic mass-loss. Fsync upper",
    #     },
    #     colorbar_scale="symlog",
    #     custom_limits=[all_min, all_max],
    # )

    # ##############
    # # plot upper zeta_L
    # # symlog
    # plot_zeta_grid(
    #     q_arr=q_acc_don_arr,
    #     beta_arr=beta_arr,
    #     zeta_grid=zeta_L_isotropic_lower_fsync_grid,
    #     plot_settings={
    #         "output_name": os.path.join(plot_output_dir, "isotropic_lower.pdf"),
    #         "title": "isotropic mass-loss. Fsync lower",
    #     },
    #     colorbar_scale="symlog",
    #     custom_limits=[all_min, all_max],
    # )

    ##############
    # Calculate extent of changes and normalize to sync: |(zeta_L_subsync-zeta_L_supersync)/zeta_L_sync|
    absdiff_zeta_L_isotropic_lower_upper_fsync_grid = np.abs(
        zeta_L_isotropic_lower_fsync_grid - zeta_L_isotropic_upper_fsync_grid
    )
    normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid = np.abs(
        absdiff_zeta_L_isotropic_lower_upper_fsync_grid
        / zeta_L_isotropic_sync_fsync_grid
    )

    #
    plot_zeta_grid(
        q_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        zeta_grid=normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid,
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir, "isotropic_normalized_absdiff.pdf"
            ),
            "title": r"isotropic mass-loss. $\|(\zeta_{L, lower}-\zeta_{L, upper})/\zeta_{L, sync}\|$",
        },
        colorbar_scale="log",
        custom_limits=[
            np.min(normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid) + 1e-2,
            np.max(normalized_absdiff_zeta_L_isotropic_lower_upper_fsync_grid),
        ],
    )

    # ##############
    # # Calculate extent of changes and normalize to sync: |(zeta_L_subsync-zeta_L_sync)|
    # absdiff_zeta_L_isotropic_lower_sync_fsync_grid = np.abs(
    #     zeta_L_isotropic_lower_fsync_grid - zeta_L_isotropic_sync_fsync_grid
    # )

    # #
    # plot_zeta_grid(
    #     q_arr=q_acc_don_arr,
    #     beta_arr=beta_arr,
    #     zeta_grid=absdiff_zeta_L_isotropic_lower_sync_fsync_grid,
    #     plot_settings={
    #         "output_name": "isotropic_absdiff_subsync_sync.pdf",
    #         "title": "isotropic mass-loss. abs diff subsync sync",
    #     },
    # )

    # ##############
    # # TODO: plot values

    # ##############
    # # perhaps its better to just take fsync lower and fsync upper and compare that to the normal one
    # fractional_diff_zeta_L_isotropic_lower_fsync_grid = (
    #     zeta_L_isotropic_lower_fsync_grid / zeta_L_isotropic_sync_fsync_grid
    # ) - 1
    # print(fractional_diff_zeta_L_isotropic_lower_fsync_grid)

    # filtered = fractional_diff_zeta_L_isotropic_lower_fsync_grid[
    #     ~np.isnan(fractional_diff_zeta_L_isotropic_lower_fsync_grid)
    # ]
    # print(np.max(filtered))
    # print(np.min(filtered))
    # print(np.min(np.abs(filtered)))

    ##############
    # perhaps its better to just take fsync lower and fsync upper and compare that to the normal one
    fractional_diff_zeta_L_isotropic_upper_fsync_grid = (
        zeta_L_isotropic_upper_fsync_grid / zeta_L_isotropic_sync_fsync_grid
    ) - 1

    filtered = fractional_diff_zeta_L_isotropic_upper_fsync_grid[
        ~np.isnan(fractional_diff_zeta_L_isotropic_upper_fsync_grid)
    ]

    #
    plot_zeta_grid(
        q_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        zeta_grid=fractional_diff_zeta_L_isotropic_upper_fsync_grid,
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "isotropic_upper_sync.pdf"),
            "title": r"isotropic mass-loss $(\zeta_{\mathrm{L, iso, upper}}/\zeta_{\mathrm{L, iso, sync}})-1$",
        },
        colorbar_scale="symlog",
        custom_limits=[
            -36.632624247503244,
            2.4943105175164,
        ],
    )

    ##############
    # perhaps its better to just take fsync lower and fsync upper and compare that to the normal one
    fractional_diff_zeta_L_isotropic_lower_fsync_grid = (
        zeta_L_isotropic_lower_fsync_grid / zeta_L_isotropic_sync_fsync_grid
    ) - 1

    filtered = fractional_diff_zeta_L_isotropic_lower_fsync_grid[
        ~np.isnan(fractional_diff_zeta_L_isotropic_lower_fsync_grid)
    ]

    #
    plot_zeta_grid(
        q_arr=q_acc_don_arr,
        beta_arr=beta_arr,
        zeta_grid=fractional_diff_zeta_L_isotropic_lower_fsync_grid,
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "isotropic_lower_sync.pdf"),
            "title": r"isotropic mass-loss $(\zeta_{\mathrm{L, iso, lower}}/\zeta_{\mathrm{L, iso, sync}})-1$",
        },
        colorbar_scale="linear",
        custom_limits=[
            -1.1627173144848595,
            16.545908502423377,
        ],
    )
