"""
Function to handle plotting an example set of trajectories with different classifications
"""

import os

import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)

from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_detection import (
    get_all_self_and_other_intersections,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

from ballistic_integration_code.scripts.L1_stream.functions.utils import timing


def plot_trajectory_classification_example(settings, plot_settings):
    """
    Function to plot different sets of trajectories
    """

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    with timing("trajectory calculation", verbosity=plot_settings.get("verbosity", 0)):
        # Get trajectory set for the configuration passed by settings
        trajectory_set, averaged_result_dict = integrate_trajectories_from_L1_stage_3(
            settings=settings
        )
    if plot_settings.get("verbose", False):
        print(averaged_result_dict)

    # Get category sets
    trajectory_sets_per_category = select_trajectory_categories(trajectory_set)

    # get the intersections
    (
        combined_all_self_intersections,
        combined_all_other_intersections,
    ) = get_all_self_and_other_intersections(
        trajectory_set=trajectory_set, settings=settings
    )

    # NOTE to plot some of the intersections by eye again
    # for trajectory in trajectory_set:
    #     other_intersections_list = trajectory['intersection_data']['other_intersection_data']['other_intersections_list']
    #     if other_intersections_list:
    #         for intersection in other_intersections_list:
    #             for location, segment_list in intersection['intersection_dict'].items():
    #                 print(location, segment_list)

    #                 #
    #                 segment_1 = segment_list[0][0]
    #                 segment_2 = segment_list[0][1]

    #                 #
    #                 plt.plot([segment_1[0][0], segment_1[1][0]], [segment_1[0][1], segment_1[1][1]])
    #                 plt.plot([segment_2[0][0], segment_2[1][0]], [segment_2[0][1], segment_2[1][1]])
    #                 plt.gca().set_aspect('equal')
    #                 plt.show()

    #
    fig = plt.figure(figsize=(20, 20))
    fig, axes_list = plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        plot_trajectory=True,
        plot_rochelobe_equipotential_meshgrid=True,
        resultdata_sets=trajectory_sets_per_category,
        plot_system_information_panel=False,
        thin_trajectories=plot_settings["thin_trajectories"],
        return_fig=True,
        plot_settings={
            **plot_settings,
            "plot_bounds_x": plot_settings["x_bounds"],
            "plot_bounds_y": plot_settings["y_bounds"],
            "plot_resolution": settings["plot_resolution"],
            "star_text_color": "white",
        },
        settings=settings,
        fig=fig,
    )

    ##################
    # Intersections
    linestyle_intersections = "None"
    markeredgewidth_intersections = 4
    markersize_intersections = 20
    markerfacecolor_intersections = "None"

    marker_self_intersections = "o"
    marker_other_intersections = "s"

    markeredgecolor_self_intersection = "#00cc66"
    markeredgecolor_other_intersection = "#00ffff"

    # Other-intersections
    if plot_settings.get("plot_other_intersections", True):
        if plot_settings.get("verbosity", 0) > 0:
            print(
                "Found {} unique other-intersection locations".format(
                    len(combined_all_other_intersections)
                )
            )
        for other_intersection_location in combined_all_other_intersections:
            # plot
            _ = axes_list[0].plot(
                other_intersection_location[0],
                other_intersection_location[1],
                marker=marker_other_intersections,
                linestyle=linestyle_intersections,
                markeredgewidth=markeredgewidth_intersections,
                markersize=markersize_intersections,
                markeredgecolor=markeredgecolor_other_intersection,
                markerfacecolor=markerfacecolor_intersections,
            )

    # Self-intersections
    if plot_settings.get("plot_self_intersections", True):
        # Draw on the intersections
        if plot_settings.get("verbosity", 0) > 0:
            print(
                "Found {} unique self-intersection locations".format(
                    len(combined_all_self_intersections)
                )
            )
        for self_intersection_location in combined_all_self_intersections:
            # plot
            _ = axes_list[0].plot(
                self_intersection_location[0],
                self_intersection_location[1],
                marker=marker_self_intersections,
                linestyle=linestyle_intersections,
                markeredgewidth=markeredgewidth_intersections,
                markersize=markersize_intersections,
                markeredgecolor=markeredgecolor_self_intersection,
                markerfacecolor=markerfacecolor_intersections,
            )

    ######
    # Resize
    for ax in axes_list:
        ax.set_xlim(plot_settings["x_bounds"])
        ax.set_ylim(plot_settings["y_bounds"])

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # Add labels
    axes_list[0].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_list[0].set_ylabel(r"$\it{y}$", fontsize=36)

    # force the same axes ratio
    axes_list[0].set_aspect("equal")

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


def handle_paper_plot_trajectory_classification_example(output_name, show_plot):
    """
    Function to handle the paper version of trajectory classification example with intersections
    """

    #######
    # Config
    plot_settings = {
        "star_text_color": "white",
        "show_plot": show_plot,
        "verbosity": 0,
        "x_bounds": [0.5, 1.2],
        "y_bounds": [-0.5, 0.5],
        "thin_trajectories": False,
    }

    # ###########
    # # basename
    # basename = "trajectory_classification_overview.pdf"
    # #
    # system_dict = {
    #     **standard_system_dict,
    #     "synchronicity_factor": 1.75,
    #     "mass_accretor": 10**-1.5,
    #     "mass_donor": 1,
    # }

    # grid_point = {
    #     "massratio_accretor_donor": system_dict["mass_accretor"]
    #     / system_dict["mass_donor"],
    #     "synchronicity_factor": system_dict["synchronicity_factor"],
    #     "log10normalized_thermal_velocity_dispersion": -0.5,
    # }

    # settings = {
    #     **stage_3_settings,
    #     "system_dict": system_dict,
    #     "grid_point": grid_point,
    #     "num_cores": 1,
    #     "jacobi_error_tol": 1e-2,
    #     "num_samples_area_sampling": 4,
    #     "return_trajectory_set": True,
    #     "direction_velocity_asynchronous_offset": +1,
    # }

    # plot_trajectory_classification_example(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )

    ###########
    # basename
    basename = "trajectory_classification_overview.pdf"

    #
    system_dict = {
        **standard_system_dict,
        "synchronicity_factor": 1.75,
        "mass_accretor": 10**-1.2,
        "mass_donor": 1,
        "synchronicity_factor": 0.22,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -0.5,
    }

    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 4,
        "jacobi_error_tol": 1e-6,
        "dt": 0.01,
        "num_samples_area_sampling": 12,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "include_asynchronous_donor_effects": True,
        "include_asynchronicity_donor_during_integration": False,
        "limit_timestep_on_stepsize": True,
        "max_position_diff_timestep_rejection": 0.001,
        #
        "verbosity": 0,
        "intersection_angle_threshold": 10,
    }

    plot_trajectory_classification_example(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    #######
    # Config

    plot_settings = {
        "star_text_color": "white",
        "show_plot": False,
        "verbosity": 1,
        "x_bounds": [0.5, 1],
        "y_bounds": [-0.5, 0.5],
    }

    # Output dir
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # output_dir = os.path.join(this_file_dir, "plots/")

    # ###########
    # # basename
    # basename = "trajectory_classification_overview.pdf"
    # #
    # system_dict = {
    #     **standard_system_dict,
    #     "synchronicity_factor": 1.75,
    #     "mass_accretor": 10**-1.5,
    #     "mass_donor": 1,
    # }

    # grid_point = {
    #     "massratio_accretor_donor": system_dict["mass_accretor"]
    #     / system_dict["mass_donor"],
    #     "synchronicity_factor": system_dict["synchronicity_factor"],
    #     "log10normalized_thermal_velocity_dispersion": -0.5,
    # }

    # settings = {
    #     **stage_3_settings,
    #     "system_dict": system_dict,
    #     "grid_point": grid_point,
    #     "num_cores": 1,
    #     "jacobi_error_tol": 1e-2,
    #     "num_samples_area_sampling": 4,
    #     "return_trajectory_set": True,
    #     "direction_velocity_asynchronous_offset": +1,
    # }

    # plot_trajectory_classification_example(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )

    # ###########
    # # basename
    # basename = "trajectory_classification_overview.pdf"
    # #
    # system_dict = {
    #     **standard_system_dict,
    #     "synchronicity_factor": 1.75,
    #     "mass_accretor": 10**-1.2,
    #     "mass_donor": 1,
    #     "synchronicity_factor": 0.22,
    # }

    # grid_point = {
    #     "massratio_accretor_donor": system_dict["mass_accretor"]
    #     / system_dict["mass_donor"],
    #     "synchronicity_factor": system_dict["synchronicity_factor"],
    #     "log10normalized_thermal_velocity_dispersion": -0.5,
    # }

    # settings = {
    #     **stage_3_settings,
    #     "system_dict": system_dict,
    #     "grid_point": grid_point,
    #     "num_cores": 4,
    #     "jacobi_error_tol": 1e-6,
    #     "dt": 0.01,
    #     "num_samples_area_sampling": 12,
    #     "return_trajectory_set": True,
    #     "direction_velocity_asynchronous_offset": +1,
    #     "include_asynchronous_donor_effects": True,
    #     "include_asynchronicity_donor_during_integration": False,
    #     "limit_timestep_on_stepsize": True,
    #     "max_position_diff_timestep_rejection": 0.001,
    #     #
    #     "verbosity": 0,
    #     "intersection_angle_threshold": 10,
    # }

    # plot_trajectory_classification_example(
    #     settings=settings,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )

    ###########
    # PLOTTING TO CHECK STATEMENTS IN THE PAPER

    plot_settings = {
        "star_text_color": "white",
        "show_plot": True,
        "verbosity": 1,
        "x_bounds": [-2, 2],
        "y_bounds": [-2, 2],
        "thin_trajectories": True,
    }

    output_dir = os.path.join("plots/")
    basename = "test.pdf"

    # #
    # system_dict = {
    #     **standard_system_dict,
    #     "mass_accretor": 1,
    #     "mass_donor": 1,
    #     "synchronicity_factor": 0.4,
    # }
    # grid_point = {
    #     "massratio_accretor_donor": system_dict["mass_accretor"]
    #     / system_dict["mass_donor"],
    #     "synchronicity_factor": system_dict["synchronicity_factor"],
    #     "log10normalized_thermal_velocity_dispersion": -3.0,
    # }

    #
    system_dict = {
        **standard_system_dict,
        "mass_accretor": 6.15848211e-01,
        "mass_donor": 1,
        "synchronicity_factor": 1.0,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -3,
    }

    #
    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 4,
        # "jacobi_error_tol": 1e-6,
        # "dt": 0.01,
        # "num_samples_area_sampling": 25,
        "return_trajectory_set": True,
        "generate_plot_at_gridpoint": False,
        # "direction_velocity_asynchronous_offset": +1,
        # "include_asynchronous_donor_effects": True,
        # "include_asynchronicity_donor_during_integration": False,
        # "limit_timestep_on_stepsize": True,
        # "max_position_diff_timestep_rejection": 0.001,
        #
        "verbosity": 0,
        # "intersection_angle_threshold": 30,
        # "trajectory_thinning_length": 100,
        "thin_trajectory": True,
        "thin_by_index": False,
        "thin_by_distance": False,
        "thin_by_angle": True,
        "trajectory_thinning_angle_threshold": 2.5,
    }

    plot_trajectory_classification_example(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )
