"""
David Hendriks 2019

Main script for the L2 mass loss project:
    Script to integrate ballistic trajectories of particles leaving L2

    this file is the main file for the scripts to visualise the orbit and potential of the binary and the accretion disk.

TASKS:
    Plot roche-lobe surface
    Put in some real stuff:
    Plot contour where the coriolis force acts on particles to push them out
    Input scale bar
    Add rmin
    Add rmax
    Fix Solar units
    Fix scaling of lagrange poits correctly
    Fix all the locations. things dont add up lol.
    Change all the calculations to explcitly have m_accretor m_donor

    Change the structure so that the results are generated and stored in pdf files
    add verbosity
    add multiprocessing to this
    rewrite the integration to c
"""

import json
import os

import astropy.constants as const
import astropy.units as u
import numpy as np
import scipy
from ballistic_integrator.functions.functions import json_encoder
from ballistic_integrator.functions.integration_functions import calculate_trajectory
from ballistic_integrator.functions.lagrange_points import calculate_lagrange_points

import ballistic_integration_code.settings as settings


def main_from_L2(mass_accretor, mass_donor, separation, extra_settings=None):
    """
    Main function to run the trajectory calculator
    """

    if not extra_settings:
        extra_settings = {}

    # Set up integration of particle:
    L_points = calculate_lagrange_points(
        mass_donor, mass_accretor, separation=separation
    )
    l2_pos = L_points["L2"]

    initial_position_base = np.array(l2_pos[:2])
    initial_position_offset = np.array(
        extra_settings.get("initial_position_offset", [0.0, 0.0])
    )

    initial_r = initial_position_base + initial_position_offset
    initial_v = np.array(extra_settings.get("initial_velocity_offset", [0, 0]))

    result_dir = os.path.join(
        os.path.abspath(settings.settings["output_root"]), "testing"
    )
    filename = "L2_q={}_x={}_v={}.json".format(
        mass_accretor / mass_donor,
        "{},{}".format(initial_r[0], initial_r[1]),
        "{},{}".format(initial_v[0], initial_v[1]),
    )
    full_filename = os.path.join(result_dir, filename)

    if extra_settings.get(
        "run", True
    ):  # If we already calculate the results then we can just return only the resultfile
        # Calculate the trajectory
        result = calculate_trajectory(
            mass_donor=mass_donor,
            mass_accretor=mass_accretor,
            separation=separation,
            initial_position=initial_r,
            initial_velocity=initial_v,
            extra_settings=extra_settings,
        )

        # Write result to file:
        if not os.path.isdir(os.path.abspath(result_dir)):
            os.makedirs(os.path.abspath(result_dir), exist_ok=True)

        with open(full_filename, "w") as f:
            json.dump(result, f, default=json_encoder)

    return full_filename


if __name__ == "__main__":
    ###########################
    # Generate the data
    ###########################

    extra_settings = {
        "max_time": 1500,
        "dt": 0.01,
        "steps_check_self_intersection": -1,
        "jacobi_error_tol": 1e-6,
        "initial_position_offset": [0.0, 0.0],
        "initial_velocity_offset": [0.0, 0.0],
        "print_info": False,
        "run": True,
    }

    # # Set values for a system
    # mass_accretor = 1 * 0.02
    # mass_donor = 1
    # separation = 1
    # resultfile = main_from_L2(mass_accretor=mass_accretor, mass_donor=mass_donor, separation=separation, extra_settings=extra_settings)
    # plot_resultfile(resultfile)
    # quit()

    q_step = 0.2
    q_range = np.arange(q_step, 1 + q_step, q_step)

    max_distances = []
    initial_angmoms = []
    final_angmoms = []
    final_times = []
    exit_codes = []

    for q in q_range:
        # Set values for a system
        mass_accretor = 1 * q
        mass_donor = 1
        separation = 1

        # Simulate
        resultfile = main_from_L2(
            mass_accretor=mass_accretor,
            mass_donor=mass_donor,
            separation=separation,
            extra_settings=extra_settings,
        )

        with open(resultfile, "r") as f:
            data = json.loads(f.read())

        #
        times = np.array(data["times"])
        distances = np.array(data["distances_from_center"])
        angmoms = np.array(data["angular_momenta"])

        exit_codes.append(data["exit_code"])
        max_distances.append(max(distances))
        initial_angmoms.append(angmoms[0])
        final_angmoms.append(angmoms[-1])
        final_times.append(times[-1])

    ###########################
    # Generate the results
    ###########################

    # plt.title("Exit code of simulation")
    # plt.scatter(q_range, exit_codes, marker='s', s=200)
    # plt.xlabel("q: accretor/donor")
    # plt.ylabel("Exit code simulation")
    # plt.savefig('plots/exit_codes.png')
    # plt.show()

    # plt.title("Maximum distance at end of simulation.\nGreen ones are succesful")
    # plt.plot(q_range, max_distances)
    # plt.xlabel("q: accretor/donor")
    # plt.ylabel("distance from center")
    # plt.savefig('plots/max_distances_codes.png')
    # plt.show()

    # fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(16,8))
    # plt.suptitle("Final angular momenta.\nGreen ones are succesful")
    # axes[0].plot(q_range, final_angmoms, label='Final angmoms')
    # axes[1].plot(q_range, np.array(final_angmoms)-np.array(initial_angmoms), label='Diff initial final')
    # axes[1].set_xlabel("q: accretor/donor", fontsize=18)
    # axes[0].set_ylabel("Final angular momentum", fontsize=18)
    # axes[1].set_ylabel("difference angular momentum", fontsize=18)

    # plt.legend()
    # plt.savefig('plots/angmoms.png')
    # plt.show()

    # plt.title("Final time of simulation")
    # plt.plot(q_range, final_times)
    # plt.xlabel("q: accretor/donor")
    # plt.ylabel("distance from center")
    # plt.axhline(extra_settings['max_time'], linestyle='--', color='red')
    # plt.savefig('plots/final_times.png')
    # plt.show()

    # import pandas as pd
    # df = pd.DataFrame(np.array([q_range, final_times, final_angmoms, initial_angmoms, max_distances, exit_codes]).T, columns=["q", "final_times", "final_angmoms", "initial_angmoms", "max_distances", "exit_codes"])

    # total_dict = {}
    # total_dict['dataframe'] = df.to_json()
    # total_dict['settings'] = extra_settings

    # with open('results_max_time_{}.json'.format(extra_settings['max_time']), 'w') as f:
    #     json.dump(total_dict, f)

    # plot_distance_info(resultfile)

    # plot_angmom_info(resultfile)

    # plot_resultfile(resultfile)
