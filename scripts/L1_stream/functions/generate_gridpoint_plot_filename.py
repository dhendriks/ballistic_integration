"""
Function to generate the plot filename for a given gridpoint
"""


def generate_gridpoint_plot_filename(gridpoint):
    """
    Function to generate a filename for the plot for the current gridpoint
    """

    # Construct filename from the relevant grid point properties
    base_string = ""
    for key in sorted(gridpoint.keys()):
        base_string += "{}_{}_".format(key, gridpoint[key])

    return base_string + ".pdf"
