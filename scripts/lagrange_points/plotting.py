"""
General function that handles the plotting of the 2-d distribution of 2 parameters in the episode data.
"""

import copy

import matplotlib
import numpy as np
from matplotlib import colors
from RLOF.paper_scripts.functions.episode_plot_functions.utility import (
    add_cdf_percentage_line,
)


def calculate_vmin(main_hist, magnitude_lim=3):
    """
    function to calculate vmin
    """

    min_val = main_hist[main_hist > 0].min()
    max_val = main_hist[main_hist > 0].max()

    log_min_val = np.log10(min_val)
    log_max_val = np.log10(max_val)

    if log_max_val - log_min_val > magnitude_lim:
        return 10 ** (log_max_val - magnitude_lim), True

    return 10**log_min_val, False


def plot_2d_episode_distribution(
    fig,
    axis_dict,
    x_arr,
    y_arr,
    result_arr,
    parameter_1,
    parameter_2,
    plot_settings,
    box_corners=None,
    colorbar_scale="linear",
    custom_limits=None,
):
    """
    This general function plots certain things based on whether the axis_dict contains an panel for this data.

    e.g. if axis_dict contains 'pdf_p1', it will plot the probability distribution function of parameter 1

    parameter_1 is assumed to be the x-axis
    parameter_2 is assumed to be the y-axis

    bins_parameter_* are assumed to be the bin-edges
    """

    #####################
    #
    X, Y = np.meshgrid(x_arr, y_arr)

    ###############
    # TODO: fix that we can take different scales

    # # Configure color scaling
    # vmin, limited = calculate_vmin(result_arr, magnitude_lim=10)
    # vmax = result_arr.max()

    # if not custom_limits is None:
    vmin, vmax = custom_limits[0], custom_limits[1]

    ########
    #
    if colorbar_scale == "linear":
        norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)
    elif colorbar_scale == "log":
        norm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax)
    elif colorbar_scale == "symlog":
        min_val = np.min(np.abs(result_arr))
        min_val = 5e-1

        #
        norm = colors.SymLogNorm(
            linthresh=min_val,
            linscale=1,
            vmin=vmin,
            vmax=vmax,
        )

    #
    cmap = plot_settings.get("cmap", matplotlib.cm.viridis)
    cmap = copy.copy(cmap)
    cmap.set_under(color="white")

    #####################
    # Plot main 2-d histogram
    if "ax_main" in axis_dict:
        # plot values
        axis_dict["ax_main"].pcolormesh(
            X,
            Y,
            result_arr,
            cmap=plot_settings.get("cmap", matplotlib.cm.viridis),
            norm=norm,
            # vmin=vmin,
            # vmax=vmax,
            antialiased=True,
            rasterized=True,
        )

        # handle query for
        if box_corners is not None:
            #
            box_query = "({p1_left} <= {p1} <= {p1_right}) & ({p2_bottom} <= {p2} <= {p2_top})".format(
                p1_left=box_corners["p1_left"],
                p1=parameter_1,
                p1_right=box_corners["p1_right"],
                p2_bottom=box_corners["p2_bottom"],
                p2=parameter_2,
                p2_top=box_corners["p2_top"],
            )

            # create df
            box_query_df = queried_df.query(box_query)

            # get total sum
            box_sum = box_query_df["number_per_solar_mass"].sum()

            # draw inset
            axis_dict["ax_main"].plot(
                [
                    box_corners["p1_left"],
                    box_corners["p1_right"],
                    box_corners["p1_right"],
                    box_corners["p1_left"],
                    box_corners["p1_left"],
                ],
                [
                    box_corners["p2_bottom"],
                    box_corners["p2_bottom"],
                    box_corners["p2_top"],
                    box_corners["p2_top"],
                    box_corners["p2_bottom"],
                ],
                linestyle="--",
                color="red",
            )

            # calculate how much of fraction it is from the main plot
            box_fraction = box_sum / main_hist.sum()

            #
            if "loc" in box_corners:
                if box_corners["loc"] == "top":
                    location_text = np.array(
                        [
                            (box_corners["p1_right"] + box_corners["p1_left"]) / 2,
                            1.25 * box_corners["p2_top"],
                        ]
                    )
                    # location_text += np.array([0, 0.25 * box_corners["p2_top"]])
                    ha = "center"
                    va = "bottom"
                elif box_corners["loc"] == "left":
                    location_text = np.array(
                        [
                            (0.9 * box_corners["p1_left"]),
                            (box_corners["p2_top"] + box_corners["p2_bottom"]) / 2,
                        ]
                    )
                    ha = "right"
                    va = "bottom"

                else:
                    raise ValueError("unsupported location provided")

                #
                format_ = "{:.2f}"

                #
                axis_dict["ax_main"].text(
                    location_text[0],
                    location_text[1],
                    format_.format(box_fraction),
                    bbox={"pad": 0.5, "facecolor": "red", "boxstyle": "square"},
                    ha=ha,
                    va=va,
                )

    #####################
    # Plot colorbar
    if "ax_cb" in axis_dict:
        # make colorbar
        cbar = matplotlib.colorbar.ColorbarBase(
            axis_dict["ax_cb"],
            cmap=plot_settings.get("cmap", matplotlib.cm.viridis),
            norm=norm,
            # extend="min" if limited else None,
            # extend="min" if limited else None,
        )
        # cbar.ax.set_ylabel(r"Fraction of $\dot{J}_{\mathrm{stream}}$ returned to the orbit")

        # for contour_i in range(len(contour_levels)):
        #     cbar.ax.plot(
        #         [0, 1],
        #         [contour_levels[contour_i]] * 2,
        #         "black",
        #         linestyle=linestyles[contour_i],
        #     )

    #####################
    # Plot histogram of first parameter
    if "ax_pdf_p1" in axis_dict:

        pdf_1 = axis_dict["ax_pdf_p1"].plot(
            bincenters_parameter_1,
            hist_p1,
            # lw=2,
            # c='k',
            # zorder=13,
            # label="Z = {} ({:.2f})".format(
            #     metallicity,
            #     ratio_dict[metallicity]["number_per_solar_mass"].sum(),
            # ),
            # linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
        )

    #####################
    # Plot cumulative density of first parameter
    if "ax_cdf_p1" in axis_dict:

        cdf_1 = axis_dict["ax_cdf_p1"].plot(
            bincenters_parameter_1,
            cdf_p1
            # alpha=0.75,
            # label="Z = {}".format(metallicity),
            # linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
            # linewidth=4,
        )

        ####
        # Add 50% lines
        add_cdf_percentage_line(
            axis_dict["ax_cdf_p1"],
            {"cdf": cdf_p1, "centers": bincenters_parameter_1},
            fraction=0.5,
            index_i=0,
            plot_kwargs={},
            reverse=False,
        )

    if "ax_pdf_p2" in axis_dict:
        pdf_2 = axis_dict["ax_pdf_p2"].plot(
            hist_p2,
            bincenters_parameter_2,
            # lw=2,
            # c='k',
            # zorder=13,
            # label="Z = {} ({:.2f})".format(
            #     metallicity,
            #     ratio_dict[metallicity]["number_per_solar_mass"].sum(),
            # ),
            # linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
        )

    if "ax_cdf_p2" in axis_dict:

        cdf_2 = axis_dict["ax_cdf_p2"].plot(
            cdf_p2,
            bincenters_parameter_2,
            # alpha=0.75,
            # label="Z = {}".format(metallicity),
            # linestyle=linestyle_list[index_i] if not index_i == 0 else "-",
            # linewidth=4,
        )

        ####
        # Add 50% lines
        add_cdf_percentage_line(
            axis_dict["ax_cdf_p2"],
            {"cdf": cdf_p2, "centers": bincenters_parameter_2},
            fraction=0.5,
            index_i=0,
            plot_kwargs={},
            reverse=True,
        )

    return fig, axis_dict
