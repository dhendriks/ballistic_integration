"""
Function to plot the classification fractions for a given A, v

TODO: tidy up method entirely
TODO: split the function into components
TODO: create method to allow plotting a range of velocities to show the evolution.
TODO: Add method to fill in the region where no intersections occur
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings

#
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import add_labels_subplots, show_and_save_plot
from matplotlib import colors

from ballistic_integration_code.paper_scripts.classification_fraction_plot.settings import (
    xlabel,
    y_label_dict,
)
from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    handle_query,
    readout_dataset_and_query,
)

custom_mpl_settings.load_mpl_rc()
plt.rcParams.update({"hatch.color": "black", "hatch.linewidth": 2})

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

fontsize_lines = 14


def create_bins(value_array):
    """
    Function to create bins for the value array
    """

    # create bins with shift
    midpoints = (value_array[1:] + value_array[:-1]) / 2

    bins = np.concatenate(
        (
            np.array([midpoints[0] - np.diff(midpoints)[0]]),
            midpoints,
            np.array([midpoints[-1] + np.diff(midpoints)[-1]]),
        )
    )

    return bins


def plot_intersection_overview(
    result_file,
    query=None,
    plot_settings={},
):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    x_parameter = "massratio_accretor_donor"
    y_parameter = "synchronicity_factor"

    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
    )

    # get unique values for the mass ratio and f
    unique_q_acc = np.sort(result_df[x_parameter].unique())
    unique_f_sync = np.sort(result_df[y_parameter].unique())

    # create bins
    bins_q_acc = create_bins(unique_q_acc)
    bins_f_sync = create_bins(unique_f_sync)

    x_bins = bins_q_acc
    y_bins = bins_f_sync

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    ############
    # Set up figure logic
    fig = plt.figure(figsize=(12, 10))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=4)

    # set up axes
    axis_intersections = fig.add_subplot(gs[0, :-1])
    ax_cb = fig.add_subplot(gs[0, -1:])
    ax_cb.set_ylim([0, 1])
    ax_cb.yaxis.tick_right()

    #
    axes_list = [axis_intersections]
    ##############
    # handle plotting

    # define contourlevels and add these to each plot, and to the colorbar
    contourlevels = [0.1, 0.5, 0.9]
    linewidths = 4
    color_self_intersection = "red"
    color_other_intersection = "blue"
    linestyles = linestyle_list[: len(contourlevels)][::-1]
    contourf_alpha = 0.2

    #####
    # Self intersection

    # create histogram
    fraction_self_intersection_hist, _, _ = np.histogram2d(
        result_df["massratio_accretor_donor"],
        result_df["synchronicity_factor"],
        bins=[bins_q_acc, bins_f_sync],
        weights=result_df["fraction_self_intersection"],
    )

    #
    if np.any(fraction_self_intersection_hist):
        _ = axis_intersections.contour(
            X,
            Y,
            fraction_self_intersection_hist.T,
            levels=contourlevels,
            colors=color_self_intersection,
            linewidths=linewidths,
            linestyles=linestyles,
        )
        _ = axis_intersections.contourf(
            X,
            Y,
            fraction_self_intersection_hist.T,
            levels=contourlevels + [100],
            colors=color_self_intersection,
            linestyles=linestyles,
            hatches="//",
            alpha=contourf_alpha,
        )

    #####
    # Other intersection

    # create histogram
    fraction_other_intersection_hist, _, _ = np.histogram2d(
        result_df["massratio_accretor_donor"],
        result_df["synchronicity_factor"],
        bins=[bins_q_acc, bins_f_sync],
        weights=result_df["fraction_other_intersection"],
    )

    # Check if its filled with all zeros
    if np.any(fraction_other_intersection_hist):
        _ = axis_intersections.contour(
            X,
            Y,
            fraction_other_intersection_hist.T,
            levels=contourlevels,
            colors=color_other_intersection,
            linewidths=linewidths,
            linestyles=linestyles,
        )
        _ = axis_intersections.contourf(
            X,
            Y,
            fraction_other_intersection_hist.T,
            levels=contourlevels + [100],
            colors=color_other_intersection,
            linestyles=linestyles,
            hatches="\\",
            alpha=contourf_alpha,
        )

    #####
    # no intersection
    if plot_settings.get("plot_no_intersection", True):
        # create histogram
        fraction_no_intersection_hist, _, _ = np.histogram2d(
            result_df["massratio_accretor_donor"],
            result_df["synchronicity_factor"],
            bins=[bins_q_acc, bins_f_sync],
            weights=result_df["fraction_other_intersection"]
            + result_df["fraction_self_intersection"],
        )

        fraction_no_intersection_hist[fraction_no_intersection_hist > 0] = 1

        # locations with no
        result_df.loc[:, "combined_fraction_self_intersection"] = (
            result_df["fraction_self_intersection"]
            + result_df["fraction_other_intersection"]
        )
        no_intersection_df = result_df.query("combined_fraction_self_intersection == 0")

        #
        markercolor = "#9933ff"
        axis_intersections.plot(
            no_intersection_df["massratio_accretor_donor"],
            no_intersection_df["synchronicity_factor"],
            marker="o",
            linestyle="None",
            markeredgewidth=3,
            markersize=20,
            markeredgecolor=markercolor,
            markerfacecolor=markercolor,
        )

        # Check if its filled with all zeros
        # if np.any(fraction_no_intersection_hist):
        # _ = axis_intersections.contour(
        #     X,
        #     Y,
        #     fraction_other_intersection_hist.T,
        #     levels=contourlevels,
        #     colors=color_other_intersection,
        #     linewidths=linewidths,
        #     linestyles=linestyles,
        # )
        # axis_intersections.contourf(
        #     fraction_no_intersection_hist.T,
        #     levels=np.linspace(0.0-0.1, 0.0+0.1, 2)
        # )

    ########
    # display info of failed systems

    # Draw the contourlines on the colorbar
    for contour_i, contourlevel in enumerate(contourlevels):
        ax_cb.plot(
            [0, 1],
            [contourlevel] * 2,
            color="k",
            linestyle=linestyles[contour_i],
        )
    ax_cb.set_xlim([0, 1])

    #####
    # make up

    # labels
    axes_list[0].set_xlabel(xlabel)
    axes_list[0].set_ylabel(y_label_dict[y_parameter])
    axes_list[0].set_xscale("log")
    axes_list[0].set_ylim([0.1, 2])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def generate_plot_intersection_overview(result_file):
    """
    Function that handles the configuration of the plotting and calls the plot routine
    """

    ##############################################################
    # Paper ready plots
    plot_settings = {
        "show_plot": False,
        "fontsize_title": 26,
        "plot_no_intersection": False,
    }

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots_new/")

    #######################
    # Server results
    #######################

    #
    result_dir = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION/",
    )
    result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    #
    velocities = [-3, -2.75, -2.5, -2.25, -2, -1.75, -1.5, -1.25, -1, -0.75, -0.5]
    # velocities = [-3]

    # High velocity High A
    for velocity in velocities:
        query_string = "log10normalized_thermal_velocity_dispersion=={}".format(
            velocity
        )
        basename = "intersection_overview_log10v={}.pdf".format(velocity)
        plot_intersection_overview(
            result_file,
            query=query_string,
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(plot_dir, basename),
            },
        )


def handle_paper_plot_intersection_overview(
    input_filename, output_name, show_plot, query_string
):
    """
    General function to handle the paper version of the intersection overview plot
    """

    ##############################################################
    # Paper ready plots
    plot_settings = {
        "show_plot": False,
        "fontsize_title": 26,
        "plot_no_intersection": False,
    }

    plot_intersection_overview(
        input_filename,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


def handle_paper_plot_intersection_overview_low_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of the intersection overview plot for low thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-3.0"

    handle_paper_plot_intersection_overview(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


def handle_paper_plot_intersection_overview_high_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of the intersection overview plot for high thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-0.5"

    handle_paper_plot_intersection_overview(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    plot_settings = {
        "show_plot": False,
        "fontsize_title": 26,
        "plot_no_intersection": False,
    }

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots_new/")

    #######################
    # Server results
    #######################

    #
    result_dir = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION/",
    )
    result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    #
    velocities = [-3, -2.75, -2.5, -2.25, -2, -1.75, -1.5, -1.25, -1, -0.75, -0.5]
    # velocities = [-3]

    # High velocity High A
    for velocity in velocities:
        query_string = "log10normalized_thermal_velocity_dispersion=={}".format(
            velocity
        )
        basename = "intersection_overview_log10v={}.pdf".format(velocity)
        plot_intersection_overview(
            result_file,
            query=query_string,
            plot_settings={
                **plot_settings,
                "output_name": os.path.join(plot_dir, basename),
            },
        )
