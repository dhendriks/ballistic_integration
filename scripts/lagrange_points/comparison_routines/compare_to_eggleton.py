"""
Functions to test our calculations and compare them to the eggleton radius.

We first compare to the eggleton radius from the approximation formula of eggleton
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy
import scipy.spatial as ss
from ballistic_integrator.functions.frame_of_reference import (
    calculate_lagrange_points,
    calculate_position_donor,
    calculate_potential,
)
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    calculate_ratio_RL_RLegg,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot

from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()


from david_phd_functions.plotting.linestyle_hatch_colorlist import (
    color_list,
    linestyle_list,
)
from david_phd_functions.plotting.utils import show_and_save_plot


def rochelobe_radius_donor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the donor

    value is normalised by separation

    from FKR 4.6
    """

    top = 0.49 * (mass_donor / mass_accretor) ** (2.0 / 3.0)
    bottom = (0.6 * (mass_donor / mass_accretor) ** (2.0 / 3.0)) + np.log(
        1 + (mass_donor / mass_accretor) ** (1.0 / 3.0)
    )
    R2 = top / bottom

    return R2


def compare_to_eggleton_prescription(
    settings, massratio_accretor_list, resolution_list, plot_settings
):
    """
    Function to compare to the eggleton prescription for a couple of mass ratios
    """

    approximation_result_dict = {}

    color_list = []
    cmap = plt.cm.get_cmap(
        plot_settings.get("colormap_variations", "viridis"),
        len(massratio_accretor_list),
    )
    color_list += [cmap(i) for i in range(cmap.N)]

    #
    for massratio_accretor in massratio_accretor_list:
        settings["system_dict"]["mass_accretor"] = massratio_accretor

        radii = []
        radii_eggleton = []

        for resolution in resolution_list:

            print("calculating q={} res={}".format(massratio_accretor, resolution))

            settings["volume_roche_lobe_xgrid_n"] = resolution
            settings["volume_roche_lobe_ygrid_n"] = resolution

            result = calculate_volume_roche_lobe_information(
                system_dict=system_dict,
                frame_of_reference=frame_of_reference,
                settings=settings,
                plot_collection=False,
                print_info=False,
            )

            # Store
            radii.append(result["roche_lobe_radius"])
            radii_eggleton.append(result["roche_lobe_radius_eggleton"])

        radii_array = np.array(radii)
        radii_eggleton_array = np.array(radii_eggleton)

        diff_radii_eggleton_array = radii_array - radii_eggleton_array
        absdiff_radii_eggleton_array = np.abs(diff_radii_eggleton_array)

        absdiff_radii_eggleton_array_scaled = (
            absdiff_radii_eggleton_array / radii_eggleton_array
        )

        # Store in results
        approximation_result_dict[massratio_accretor] = {
            "radii_array": radii_array,
            "radii_eggleton_array": radii_eggleton_array,
            "absdiff_radii_eggleton_array_scaled": absdiff_radii_eggleton_array_scaled,
            "resolution_array": np.array(resolution_list),
        }

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=6, ncols=1)

    ax = fig.add_subplot(gs[0:3, :])
    ax_absdiff = fig.add_subplot(gs[3:6, :])

    # loop over the results
    for massratio_accretor_i, massratio_accretor in enumerate(
        approximation_result_dict.keys()
    ):
        res = approximation_result_dict[massratio_accretor]

        #
        approximation_result_dict[massratio_accretor] = {
            "radii_array": radii_array,
            "radii_eggleton_array": radii_eggleton_array,
            "resolution_array": np.array(resolution_list),
        }

        #
        ax.plot(
            res["resolution_array"],
            res["radii_array"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
        )

        #
        ax.plot(
            res["resolution_array"],
            res["radii_eggleton_array"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
            alpha=0.25,
        )

        #
        ax_absdiff.plot(
            res["resolution_array"],
            res["absdiff_radii_eggleton_array_scaled"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
            label="q={}".format(massratio_accretor),
        )

    # Make up
    ax.set_xscale("log")
    ax_absdiff.set_xscale("log")

    ax.set_yscale("log")
    ax_absdiff.set_yscale("log")

    ax.set_xticklabels([])
    ax.set_xticks([])

    ax_absdiff.legend(loc=3)
    #
    ax.set_ylabel("Volume")
    ax_absdiff.set_ylabel("|convexhull-eggleton prescription|/eggleton prescription")

    ax_absdiff.set_xlabel("Resolution")
    ax.set_title("Convex hull volume test comparing to Eggleton approximation")

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def compare_to_eggleton_integration(settings, resolution_list, plot_settings):
    """
    Function to compare to the eggleton integration for the mass ratios he provides in his paper
    """

    #
    massratio_accretor_integration_dict = {
        # 1000:   0.7817,
        100: 0.7182,
        10: 0.5803,
        2.5: 0.4621,
        1: 0.3799,
        0.4: 0.3026,
        0.1: 0.2054,
        0.01: 0.1012,
        # 0.001:  0.0482,
    }

    color_list = []
    cmap = plt.cm.get_cmap(
        plot_settings.get("colormap_variations", "viridis"),
        len(list(massratio_accretor_integration_dict.keys())),
    )
    color_list += [cmap(i) for i in range(cmap.N)]

    #
    integration_result_dict = {}

    #
    for massratio_accretor in massratio_accretor_integration_dict.keys():
        settings["system_dict"]["mass_accretor"] = 1 / massratio_accretor

        radii = []
        radii_eggleton = []

        for resolution in resolution_list:

            print("calculating q={} res={}".format(massratio_accretor, resolution))

            settings["volume_roche_lobe_xgrid_n"] = resolution
            settings["volume_roche_lobe_ygrid_n"] = resolution

            result = calculate_volume_roche_lobe_information(
                system_dict=system_dict,
                frame_of_reference=frame_of_reference,
                settings=settings,
                plot_collection=False,
                print_info=False,
            )

            # Store
            radii.append(result["roche_lobe_radius"])
            radii_eggleton.append(
                massratio_accretor_integration_dict[massratio_accretor]
            )

        radii_array = np.array(radii)
        radii_eggleton_array = np.array(radii_eggleton)

        diff_radii_eggleton_array = radii_array - radii_eggleton_array
        absdiff_radii_eggleton_array = np.abs(diff_radii_eggleton_array)

        absdiff_radii_eggleton_array_scaled = (
            absdiff_radii_eggleton_array / radii_eggleton_array
        )

        # Store in results
        integration_result_dict[massratio_accretor] = {
            "radii_array": radii_array,
            "radii_eggleton_array": radii_eggleton_array,
            "absdiff_radii_eggleton_array_scaled": absdiff_radii_eggleton_array_scaled,
            "resolution_array": np.array(resolution_list),
        }

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=6, ncols=1)

    ax = fig.add_subplot(gs[0:3, :])
    ax_absdiff = fig.add_subplot(gs[3:6, :])

    # loop over the results
    for massratio_accretor_i, massratio_accretor in enumerate(
        integration_result_dict.keys()
    ):
        res = integration_result_dict[massratio_accretor]

        #
        integration_result_dict[massratio_accretor] = {
            "radii_array": radii_array,
            "radii_eggleton_array": radii_eggleton_array,
            "resolution_array": np.array(resolution_list),
        }

        #
        ax.plot(
            res["resolution_array"],
            res["radii_array"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
        )

        #
        ax.plot(
            res["resolution_array"],
            res["radii_eggleton_array"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
            alpha=0.25,
        )

        #
        ax_absdiff.plot(
            res["resolution_array"],
            res["absdiff_radii_eggleton_array_scaled"],
            color=color_list[massratio_accretor_i],
            linestyle=linestyle_list[massratio_accretor_i],
            label="q={}".format(massratio_accretor),
        )

    # Make up
    ax.set_xscale("log")
    ax_absdiff.set_xscale("log")

    # ax.set_yscale("log")
    ax_absdiff.set_yscale("log")

    ax.set_xticklabels([])
    ax.set_xticks([])

    ax_absdiff.legend(loc=3)
    #
    ax.set_ylabel("Volume")
    ax_absdiff.set_ylabel("|convexhull-eggleton integration|/eggleton integration")

    ax_absdiff.set_xlabel("Resolution")
    ax.set_title("Convex hull volume test comparing to Eggleton integration")

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    # Set up system dict
    system_dict = {
        **standard_system_dict,
        "mass_donor": 1,
        "mass_accretor": 1,
        "synchronicity_factor": 1.0,
    }
    frame_of_reference = "center_of_mass"

    settings = {}
    settings["volume_roche_lobe_xgrid_n"] = 5
    settings["volume_roche_lobe_ygrid_n"] = 5
    settings["volume_roche_lobe_size_factor"] = 1
    settings["include_asynchronicity_donor_during_integration"] = True
    settings["system_dict"] = system_dict
    settings["frame_of_reference"] = frame_of_reference

    #
    compare_to_eggleton_prescription(
        settings=settings,
        massratio_accretor_list=[
            # 0.1,
            # 1.0,
            # 10
            100,
            10,
            2.5,
            1,
            0.4,
            0.1,
            0.01,
        ],
        resolution_list=[5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
        plot_settings={"output_name": "plots/comparison_to_eggleton_prescription.pdf"},
    )

    # compare_to_eggleton_integration(
    #     settings=settings,
    #     resolution_list = [
    #         5, 10, 15, 20,
    #         25, 30, 35,
    #         40, 45, 50
    #     ],
    #     plot_settings={'output_name': 'plots/comparison_to_eggleton_integration.pdf', 'show_plot': False}
    # )
