"""
Script that contains the function to offset the initial position of the particle by a tiny fraction to ensure the particle is not exactly on L1
"""

import numpy as np
from ballistic_integrator.functions.frame_of_reference import (
    calculate_lagrange_points,
    calculate_position_accretor,
)


def calculate_position_offset_L1_tiny(
    system_dict, frame_of_reference, fraction_sep=100.0
):
    """
    Function to calculate the position offset from L1

    This is to place the particle slightly towards the accretor, so it is not exactly on L1
    """

    # Get the positions of the accretor and the L1 lagrange point
    pos_accretor = calculate_position_accretor(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    L_dict = calculate_lagrange_points(
        system_dict=system_dict, frame_of_reference=frame_of_reference
    )
    l1_pos = L_dict["L1"]

    # Calculate the offset
    diff_l1_accretor = np.abs(pos_accretor - l1_pos[:2])
    position_offset_L1 = diff_l1_accretor / fraction_sep

    return np.array(position_offset_L1)
