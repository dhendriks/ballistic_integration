import os

from ballistic_integration_code.paper_scripts.radii_plots.plot_rmin_for_non_synchronous_rotators import (
    plot_minimum_radius_grid,
)


def handle_paper_plot_radius_of_closest_approach(
    input_filename, output_name, show_plot, query_string
):
    """
    General function to handle the paper version of plot for the radius of closest approach
    """

    plot_settings = {
        "show_plot": show_plot,
        "add_f_label_text": False,
        "add_ulrich_plot": True,
        "add_fkr_plot": True,
        "add_lubow_plot": True,
        "add_failed_fraction_measure": True,
        "add_orientation_legend": True,
        "add_synchronous_rochelobe_accretor": True,
    }

    #
    plot_minimum_radius_grid(
        input_filename,
        query=query_string,
        add_ulrich_plot=plot_settings["add_ulrich_plot"],
        add_fkr_plot=False,
        add_lubow_plot=plot_settings["add_lubow_plot"],
        add_synchronous_rochelobe_accretor=plot_settings[
            "add_synchronous_rochelobe_accretor"
        ],
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


def handle_paper_plot_radius_of_closest_approach_low_v(
    input_filename, output_name, show_plot
):
    """
    General function to handle the paper version of plot for the radius of closest approach for low thermal velocity
    """

    #
    handle_paper_plot_radius_of_closest_approach(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string="log10normalized_thermal_velocity_dispersion==-3",
    )


# Set up ballistic data filename
ballistic_data_filename = os.path.join(
    os.environ["PROJECT_DATA_ROOT"],
    "ballistic_data",
    "L1_stream",
    "server_results",
    "ballistic_stream_integration_results_stage_3_OPPOSITE_ASYNCHRONOUS_OFFSET_WITH_INTERPOLATION",
    "trajectory_summary_data.txt",
)

print(ballistic_data_filename)


handle_paper_plot_radius_of_closest_approach_low_v(
    ballistic_data_filename, "/home/david/presi_plot_rob.pdf", False
)
