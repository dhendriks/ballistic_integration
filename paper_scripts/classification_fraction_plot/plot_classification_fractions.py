"""
Function to plot the classification fractions for a given A, v
"""

import copy
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import add_labels_subplots, show_and_save_plot
from matplotlib import colors

from ballistic_integration_code.paper_scripts.classification_fraction_plot.settings import (
    xlabel,
    y_label_dict,
)
from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    handle_query,
    readout_dataset_and_query,
)

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

fontsize_lines = 14


def create_bins(value_array):
    """
    Function to create bins for the value array
    """

    # create bins with shift
    midpoints = (value_array[1:] + value_array[:-1]) / 2

    bins = np.concatenate(
        (
            np.array([midpoints[0] - np.diff(midpoints)[0]]),
            midpoints,
            np.array([midpoints[-1] + np.diff(midpoints)[-1]]),
        )
    )

    return bins


def plot_classification_fractions(
    result_file,
    add_failed_fraction_measure,
    query=None,
    plot_settings={},
):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    x_parameter = "massratio_accretor_donor"
    y_parameter = "synchronicity_factor"

    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
    )

    # check if we have the correct column:
    if not "weight_valid" in result_df.columns:
        result_df.loc[:, "weight_valid"] = 1

    # get unique values for the mass ratio and f
    unique_q_acc = np.sort(result_df[x_parameter].unique())
    unique_f_sync = np.sort(result_df[y_parameter].unique())

    # create bins
    bins_q_acc = create_bins(unique_q_acc)
    bins_f_sync = create_bins(unique_f_sync)

    x_bins = bins_q_acc
    y_bins = bins_f_sync

    # make bincenters
    x_bincenters = (x_bins[1:] + x_bins[:-1]) / 2
    y_bincenters = (y_bins[1:] + y_bins[:-1]) / 2

    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    ############
    # Set up figure logic
    fig = plt.figure(figsize=(30, 10))
    fig.subplots_adjust(hspace=0.7)

    # Set up gridspec
    gs = fig.add_gridspec(nrows=1, ncols=10)

    # Set up axes
    axis_accretion_onto_accretor_fractions = fig.add_subplot(gs[0, 0:3])
    axis_accretion_onto_donor_fractions = fig.add_subplot(gs[0, 3:6])
    axis_lost_from_system_fractions = fig.add_subplot(gs[0, 6:9])
    ax_cb = fig.add_subplot(gs[0, -1:])

    axes_list = [
        axis_accretion_onto_accretor_fractions,
        axis_accretion_onto_donor_fractions,
        axis_lost_from_system_fractions,
    ]

    ######
    # Set norm
    norm = colors.Normalize(
        vmin=1e-10,
        vmax=1,
    )

    cmap = copy.copy(plt.cm.viridis)
    cmap.set_under(color="white")

    ##############
    # handle plotting

    #####
    # accretion onto accretor

    # create histogram
    accretion_onto_accretor_fractions_hist, _, _ = np.histogram2d(
        result_df["massratio_accretor_donor"],
        result_df["synchronicity_factor"],
        bins=[bins_q_acc, bins_f_sync],
        weights=result_df["fraction_onto_accretor"],
    )

    # plot colormesh
    _ = axis_accretion_onto_accretor_fractions.pcolormesh(
        X,
        Y,
        accretion_onto_accretor_fractions_hist.T,
        norm=norm,
        cmap=cmap,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    #####
    # accretion onto donor

    # create histogram
    accretion_onto_donor_fractions_hist, _, _ = np.histogram2d(
        result_df["massratio_accretor_donor"],
        result_df["synchronicity_factor"],
        bins=[bins_q_acc, bins_f_sync],
        weights=result_df["fraction_onto_donor"],
    )

    # plot colormesh
    _ = axis_accretion_onto_donor_fractions.pcolormesh(
        X,
        Y,
        accretion_onto_donor_fractions_hist.T,
        norm=norm,
        cmap=cmap,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    #####
    # lost from system

    # create histogram
    lost_from_system_fractions_hist, _, _ = np.histogram2d(
        result_df["massratio_accretor_donor"],
        result_df["synchronicity_factor"],
        bins=[bins_q_acc, bins_f_sync],
        weights=result_df["fraction_undefined"],
    )

    # plot colormesh
    _ = axis_lost_from_system_fractions.pcolormesh(
        X,
        Y,
        lost_from_system_fractions_hist.T,
        norm=norm,
        cmap=cmap,
        shading="auto",
        antialiased=plot_settings.get("antialiased", True),
        rasterized=plot_settings.get("rasterized", True),
    )

    # make colorbar
    cbar = matplotlib.colorbar.ColorbarBase(ax_cb, norm=norm, cmap=cmap)
    cbar.ax.set_ylabel(r"Fraction per category")

    ########
    # display info of failed systems
    if add_failed_fraction_measure:
        fraction_valid_hist, _, _ = np.histogram2d(
            result_df["massratio_accretor_donor"],
            result_df["synchronicity_factor"],
            bins=[bins_q_acc, bins_f_sync],
            weights=result_df["weight_valid"],
        )

        #
        full_hist = np.ones(accretion_onto_accretor_fractions_hist.shape)

        # calculate difference of correctly classified systems (taking into account their actual total fraction) and 100%
        difference_hist = full_hist - fraction_valid_hist * (
            accretion_onto_accretor_fractions_hist
            + accretion_onto_donor_fractions_hist
            + lost_from_system_fractions_hist
        )
        all_zeros = not np.any(difference_hist)

        # define contourlevels and add these to each plot, and to the colorbar

        # TODO: we can generate these contourlevels automatically by inspecting the actual unique values in here and splitting that up into some sections and doing something fancy
        contourlevels = [0.1, 0.5, 0.9]
        linewidths = 2
        color_contour = "red"
        linestyles = ["-.", "--", "solid"]

        #
        if not all_zeros:
            _ = axis_accretion_onto_accretor_fractions.contour(
                X,
                Y,
                difference_hist.T,
                levels=contourlevels,
                colors=color_contour,
                linewidths=linewidths,
                linestyles=linestyles,
            )
            _ = axis_accretion_onto_donor_fractions.contour(
                X,
                Y,
                difference_hist.T,
                levels=contourlevels,
                colors=color_contour,
                linewidths=linewidths,
                linestyles=linestyles,
            )
            _ = axis_lost_from_system_fractions.contour(
                X,
                Y,
                difference_hist.T,
                levels=contourlevels,
                colors=color_contour,
                linewidths=linewidths,
                linestyles=linestyles,
            )

            for contour_i, contourlevel in enumerate(contourlevels):
                cbar.ax.plot(
                    [cbar.ax.get_xlim()[0], cbar.ax.get_xlim()[1]],
                    [contourlevel] * 2,
                    color_contour,
                    linestyle=linestyles[contour_i],
                )

    #####
    # make up

    # Add labels to subplots
    add_labels_subplots(
        fig,
        axes_list,
        label_function_kwargs={
            "x_loc": 0.90,
            "bbox_dict": {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        },
    )

    # labels
    axes_list[1].set_xlabel(xlabel)
    axes_list[0].set_ylabel(y_label_dict[y_parameter])

    #
    axes_list[1].set_yticklabels([])
    axes_list[2].set_yticklabels([])

    axes_list[0].set_xscale("log")
    axes_list[1].set_xscale("log")
    axes_list[2].set_xscale("log")

    #
    axes_list[0].set_title(
        "Accretion onto accretor", fontsize=plot_settings.get("fontsize_title", 26)
    )
    axes_list[1].set_title(
        "Accretion onto donor", fontsize=plot_settings.get("fontsize_title", 26)
    )
    axes_list[2].set_title(
        "Lost from system", fontsize=plot_settings.get("fontsize_title", 26)
    )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_classification_fractions(
    input_filename, output_name, show_plot, query_string
):
    """
    General function to handle the paper version of the trajectory classification plot
    """

    plot_settings = {
        "show_plot": show_plot,
        "add_failed_fraction_measure": True,
    }

    plot_classification_fractions(
        input_filename,
        query=query_string,
        add_failed_fraction_measure=plot_settings["add_failed_fraction_measure"],
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


def handle_paper_plot_classification_fractions_low_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of the trajectory classification plot for low thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-3"

    handle_paper_plot_classification_fractions(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


def handle_paper_plot_classification_fractions_high_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of the trajectory classification plot for high thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-0.5"

    handle_paper_plot_classification_fractions(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    add_failed_fraction_measure = True
    plot_settings = {"show_plot": False, "fontsize_title": 26}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    #######################
    # Server results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "server_comparison_methods_rcirc_stage_1_minimal.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "server_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "server_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=False,
    #     add_lubow_plot=False,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    ##############
    # Stage 3 results
    result_dir = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_TEST",
    )
    result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # Low velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-3 & log10normalized_stream_area==-4"
    basename = "server_comparison_methods_rmin_stage_3_low_v_low_A.pdf"
    plot_classification_fractions(
        result_file,
        query=query_string,
        add_failed_fraction_measure=add_failed_fraction_measure,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # Low velocity large A
    query_string = "log10normalized_thermal_velocity_dispersion==-3 & log10normalized_stream_area==-1"
    basename = "server_comparison_methods_rmin_stage_3_low_v_high_A.pdf"
    plot_classification_fractions(
        result_file,
        query=query_string,
        add_failed_fraction_measure=add_failed_fraction_measure,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # High velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5 & log10normalized_stream_area==-4"
    basename = "server_comparison_methods_rmin_stage_3_high_v_low_A.pdf"
    plot_classification_fractions(
        result_file,
        query=query_string,
        add_failed_fraction_measure=add_failed_fraction_measure,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # High velocity High A
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5 & log10normalized_stream_area==-1"
    basename = "server_comparison_methods_rmin_stage_3_high_v_high_A.pdf"
    plot_classification_fractions(
        result_file,
        query=query_string,
        add_failed_fraction_measure=add_failed_fraction_measure,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    #######################
    # local results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "local_classification_fractions_stage_1_minimal.pdf"
    # plot_classification_fractions(
    #     result_file,
    #     add_failed_fraction_measure=add_failed_fraction_measure,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "local_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "local_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=False,
    #     add_lubow_plot=False,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    ##############################################################
    # TESTING
    ##############################################################

    # #######################################
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1_TEST",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # #
    # basename = "testing_comparison_methods_rcirc_stage_1_TEST.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # #######################################
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2_TEST",
    # )

    # #
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "testing_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "testing_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=True,
    #     add_lubow_plot=True,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )
