"""
Utility functions for the radii plots
"""

import numpy as np
import pandas as pd
from ballistic_integrator.functions.plot_functions import (
    fkr_rcirc,
    return_lubow_shu_table_2_data,
    ulrich_kolb,
)
from matplotlib.lines import Line2D


def readout_dataset_and_query(result_file, query, exit_code_value=None):
    """
    Function to read the datafile, filter and query
    """

    # Those that we have to choose a specific value for
    first_grid_parameters = [
        "log10normalized_stream_area",
    ]

    # those that we always display
    second_grid_parameters = ["synchronicity_factor", "massratio_accretor_donor"]

    # Read out interpolation textfile
    result_df = pd.read_csv(result_file, sep="\s+", header=0)

    # Filter on exit code if present
    if exit_code_value is not None:
        if "exit_code" in result_df.columns:
            result_df = result_df[result_df.exit_code == exit_code_value]

    # Print available values
    for first_grid_parameter in first_grid_parameters:
        if first_grid_parameter in result_df.columns:
            unique_values = result_df[first_grid_parameter].unique()

            print(
                "{} has the following unique values to choose from: {}".format(
                    first_grid_parameter, unique_values
                )
            )

    # Handle query
    result_df = handle_query(result_df, query)

    # Check if we queried correctly
    check_correct_gridpoint_queryresult(
        df=result_df,
        parameter_list=first_grid_parameters,
    )

    # Make sure the values are sorted for each column
    result_df = result_df.sort_values(by=second_grid_parameters)

    if len(result_df.index) == 0:
        raise ValueError("dataframe is empty. Abort")

    return result_df


def check_correct_gridpoint_queryresult(df, parameter_list):
    """
    Function to handle checking if the dataframe has been queried correctly and does not contain multiple values for any of the parameters in parameter_list
    """

    # Check if we queried correctly
    for parameter in parameter_list:
        if parameter in df.columns:
            unique_parameter_values = df[parameter].unique()
            if len(unique_parameter_values) > 1:
                raise ValueError(
                    "Please query so that '{}' only has 1 value. Currently has {}".format(
                        parameter, unique_parameter_values
                    )
                )


def handle_query(df, query_string):
    """
    Function  to handle query
    """

    if query_string:
        df = df.query(query_string)

    return df


def handle_ulrich_kolb_plot(fig, ax, q_acc_don, radius_quantity, legend_dict):
    """
    Function to handle adding the ulrich & kolb data to the plot
    TODO: generalise these functions to handle the ratio between the two
    """

    quantity_label = (
        r"$r_{\mathrm{min}}$ Ulrich & Kolb 1976"
        if radius_quantity == "rmin"
        else r"$r_{\mathrm{circ}}$ Ulrich & Kolb 1976"
    )
    color = "b"
    linestyle = "--"

    ulrich_kolb_data = ulrich_kolb(
        mass_donor=np.ones(q_acc_don.shape), mass_accretor=q_acc_don
    )
    ax.plot(q_acc_don, ulrich_kolb_data[radius_quantity], linestyle, color=color)

    legend_dict["labels_legend_papers"].append(quantity_label)
    legend_dict["lines_legend_papers"].append(
        Line2D([0], [0], linestyle=linestyle, color=color, lw=4, label=quantity_label)
    )
    legend_dict["add_paper_legend"] = True

    return fig, ax, legend_dict


def handle_lubow_shu_plot(fig, ax, q_acc_don, radius_quantity, legend_dict):
    """
    Function to handle adding the lubow & shu data to the plot
    """

    quantity_label = (
        r"$r_{\mathrm{min}}$ Lubow & Shu 1975"
        if radius_quantity == "rmin"
        else r"$r_{\mathrm{circ}}$ Lubow & Shu 1975"
    )

    color = "red"
    marker = "d"

    lubow_shu_datapoints = return_lubow_shu_table_2_data()
    ax.plot(
        lubow_shu_datapoints["q_acc_don"],
        lubow_shu_datapoints[radius_quantity],
        marker=marker,
        linestyle="None",
        color=color,
        zorder=200,
    )

    legend_dict["labels_legend_papers"].append(quantity_label)
    legend_dict["lines_legend_papers"].append(
        Line2D(
            [0], [0], marker=marker, linestyle="None", color=color, label=quantity_label
        )
    )
    legend_dict["add_paper_legend"] = True

    return fig, ax, legend_dict


def handle_fkr_plot(fig, ax, q_acc_don, radius_quantity, legend_dict):
    """
    Function to handle adding the lubow & shu data to the plot
    """

    label = "Frank, King & Raine 2002"
    color = "orange"
    linestyle = "--"

    fkr_curve = fkr_rcirc(mass_donor=np.ones(q_acc_don.shape), mass_accretor=q_acc_don)
    ax.plot(q_acc_don, fkr_curve[radius_quantity], linestyle=linestyle, color=color)

    legend_dict["labels_legend_papers"].append(label)
    legend_dict["lines_legend_papers"].append(
        Line2D(
            [0],
            [0],
            linestyle=linestyle,
            color=color,
            lw=4,
            label=label,
        )
    )
    legend_dict["add_paper_legend"] = True

    return fig, ax, legend_dict


def calculate_rochelobe_radius_accretor(mass_donor, mass_accretor):
    """
    Function to calculate the roche lobe radius of the accretor

    value is normalised by separation

    from FKR 4.6 modified.
    """

    top = 0.49 * (mass_accretor / mass_donor) ** (2.0 / 3.0)
    bottom = (0.6 * (mass_accretor / mass_donor) ** (2.0 / 3.0)) + np.log(
        1 + (mass_accretor / mass_donor) ** (1.0 / 3.0)
    )
    R2 = top / bottom

    return R2


def handle_adding_all_comparison_plots(
    fig,
    ax,
    q_acc_don,
    radius_quantity,
    legend_dict,
    add_ulrich_plot=False,
    add_lubow_plot=False,
    add_fkr_plot=False,
    add_synchronous_rochelobe_accretor=False,
):
    """
    Function to handle adding all the comparison plots
    """

    # Add data ulrich & Kolb
    if add_ulrich_plot:
        fig, ax, legend_dict = handle_ulrich_kolb_plot(
            fig=fig,
            ax=ax,
            q_acc_don=q_acc_don,
            radius_quantity=radius_quantity,
            legend_dict=legend_dict,
        )

    # Add data Lubow & Shu
    if add_lubow_plot:
        fig, ax, legend_dict = handle_lubow_shu_plot(
            fig=fig,
            ax=ax,
            q_acc_don=q_acc_don,
            radius_quantity=radius_quantity,
            legend_dict=legend_dict,
        )

    # add fkr data to plot
    if add_fkr_plot:
        fig, ax, legend_dict = handle_fkr_plot(
            fig=fig,
            ax=ax,
            q_acc_don=q_acc_don,
            radius_quantity=radino us_quantity,
            legend_dict=legend_dict,
        )

    ###############
    # Add rochelobe radius accretor for scale
    if add_synchronous_rochelobe_accretor:
        label = r"$r_{\mathrm{RL\ acc}}$"
        color = "orange"
        linestyle = "-."

        # Calculate the rochelobe radius of the accretor
        rochelobe_radius_accretor = calculate_rochelobe_radius_accretor(
            mass_donor=1, mass_accretor=q_acc_don
        )

        #
        ax.plot(q_acc_don, rochelobe_radius_accretor, color=color, linestyle=linestyle)

        #
        legend_dict["labels_legend_papers"].append(label)
        legend_dict["lines_legend_papers"].append(
            Line2D(
                [0],
                [0],
                linestyle=linestyle,
                color=color,
                lw=4,
                label=label,
            )
        )
        legend_dict["add_paper_legend"] = True

    return fig, ax, legend_dict
