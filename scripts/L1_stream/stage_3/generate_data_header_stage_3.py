"""
Script containing function to convert the interpolation table file to the structure for binary_c

The read-out file contains the summarized results of the ballistic trajectories. instead of first saving the trajectory data we now just process that immediately
"""

import copy
import os

import numpy as np
import pandas as pd

from ballistic_integration_code.scripts.L1_stream.functions.data_header_functions import (
    data_header_write_data_array,
    data_header_write_input_defines,
    data_header_write_meta_data,
    data_header_write_output_defines,
    get_index_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    filter_df_on_exit_codes,
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import (
    dimensionless_unit,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_output_datafile_functions import (
    handle_interpolation_radii_parameter,
    prepare_df,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)


def generate_data_header_stage_3(
    input_interpolation_textfile,
    output_interpolation_textfile,
    settings,
    parameter_dict,
):
    """
    Function to write the interpolation data as a header array
    """

    #####################
    # Config
    definition_basename = "RLOF_HENDRIKS2023_BALLISTIC_STREAM_INTERPOLATION"
    stage_number = 3
    parameter_dict = copy.deepcopy(parameter_dict)

    #
    length_decimals = settings["length_decimals_dataheader"]
    input_parameter_list = settings["input_parameter_list_data_header"]
    output_parameter_list = copy.copy(settings["output_parameter_list_data_header"])

    # update with interpolation radii information
    output_parameter_list, parameter_dict = handle_interpolation_radii_parameter(
        output_parameter_list=output_parameter_list,
        settings=settings,
        parameter_dict=parameter_dict,
    )

    # prepare df
    filtered_df, output_parameter_list = prepare_df(
        settings=settings,
        input_interpolation_textfile=input_interpolation_textfile,
        input_parameter_list=input_parameter_list,
        output_parameter_list=output_parameter_list,
    )

    # Get the number of lines we will output to the table and some others
    num_lines = len(filtered_df.index)
    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    # Get indices for input parameters
    input_index_dict = get_index_dict(
        df=filtered_df, columns=input_parameter_list, offset=0
    )
    output_index_dict = get_index_dict(
        df=filtered_df, columns=output_parameter_list, offset=-num_input_parameters
    )

    # Get git info
    git_info = get_git_info_and_time()

    #####################
    # Write header info to file
    # TODO: put this in a function and abstract. Do the same for the other stages
    with open(output_interpolation_textfile, "w") as f:

        ########################
        # Write top-level explanation
        f.write(
            """// Ballistic stream trajectory integration stage {} dataset.
// This dataset aims to provide an interpolation dataset to determine rmin and racc for the stream based on the mass ratio q and the A factor
// that captures synchronicity, eccentricty and mean anomaly (see sepinksy 2007), to improve RLOF calculations and estimates for interactions
// and specifically for accretion disks.\n
""".format(
                stage_number
            )
        )

        # Write git revision information
        f.write(
            "// Generated on: {} with git repository: {} branch: {} commit: {}\n\n".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        ######
        # Write data header metadata (parameter descriptions, stage number, number of data lines etc)
        data_header_write_meta_data(
            settings=settings,
            input_parameter_list=input_parameter_list,
            output_parameter_list=output_parameter_list,
            parameter_dict=parameter_dict,
            stage_number=stage_number,
            num_lines=num_lines,
            num_input_parameters=num_input_parameters,
            num_output_parameters=num_output_parameters,
            filehandle=f,
            definition_basename=definition_basename,
        )

        ##########
        # Write the input defines
        data_header_write_input_defines(
            parameter_list=input_parameter_list,
            index_dict=input_index_dict,
            filehandle=f,
            definition_basename=definition_basename,
        )

        ##########
        # Write the output defines
        data_header_write_output_defines(
            settings=settings,
            parameter_list=output_parameter_list,
            index_dict=output_index_dict,
            filehandle=f,
            definition_basename=definition_basename,
        )

        #########
        # Write the data
        data_header_write_data_array(
            df=filtered_df,
            length_decimals=length_decimals,
            num_lines=num_lines,
            filehandle=f,
            definition_basename=definition_basename,
        )


if __name__ == "__main__":
    # Get input file
    input_filename = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_3",
        "trajectory_summary_data.txt",
    )

    # set output file
    if os.getenv("BINARY_C"):
        output_dir = os.path.join(os.getenv("BINARY_C"), "src", "RLOF")

    output_dir = os.path.join(
        os.getenv("PROJECT_DATA_ROOT"),
        "ballistic_data/L1_stream/ballistic_stream_integration_results_stage_3",
    )

    output_dir = "results/"

    #
    output_filename = os.path.join(
        output_dir, "RLOF_Hendriks2023_ballistic_stream_table.h"
    )
    os.makedirs(output_dir, exist_ok=True)

    generate_data_header_stage_3(
        input_interpolation_textfile=input_filename,
        output_interpolation_textfile=output_filename,
    )
