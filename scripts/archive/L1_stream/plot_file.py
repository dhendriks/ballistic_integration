"""
Script to plot the trajectory of a particle
"""
import json

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib import colors

custom_mpl_settings.load_mpl_rc()

# filename = "results/testing/L1_q=0.2_x=0.5977410760702523,0.0_v=0.0,0.0.json"

filename = "/home/david/Dropbox/Academic/PHD/projects/ballistic_integration_code/scripts/L1_stream/results/testing/L1_q=1.0_x=0.16955250222699747,0.0_v=0.0,0.0.json"
filename = "/home/david/Dropbox/Academic/PHD/projects/ballistic_integration_code/scripts/L1_stream/results/testing/L1_q=0.4_x=0.4245663249849051,0.0_v=0.0,0.0.json"
filename = "/home/david/Dropbox/Academic/PHD/projects/ballistic_integration_code/scripts/L1_stream/results/testing/L1_q=0.5_x=0.27114438683079567,0.0_v=0.0,0.0.json"

with open(filename, "r") as f:
    data = json.loads(f.read())

x_positions = np.array(data["x_positions"])
y_positions = np.array(data["y_positions"])

print(x_positions)
print(y_positions)

plt.plot(x_positions, y_positions)
plt.show()
