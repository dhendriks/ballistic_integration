"""
Function to plot the vthermal vs a stream
"""

import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    add_labels_subplots,
    align_both_axes,
    show_and_save_plot,
)

from ballistic_integration_code.paper_scripts.plot_exploration_parameters.functions import (
    calculate_bins_and_bincenters,
    create_figure_for_alpha_vs_synchronicity,
    readout_and_filter_exploration_ensemble,
)
from ballistic_integration_code.paper_scripts.plot_exploration_parameters.settings import (
    colors,
    hatch_list,
    label_dict,
    xlabel_dict,
)

custom_mpl_settings.load_mpl_rc()
mpl.rcParams["axes.facecolor"] = ".9"

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_v_a_distributions(filename, orientation, plot_settings={}):
    """
    Function to generate a plot based on the unique values of these columns
    """

    #
    x_parameter = "log10normalised_thermal_velocity"
    y_parameter = "log10normalized_L1_stream_area"

    # Set color normalisation
    norm = mpl.colors.LogNorm(
        vmax=1,
        vmin=10**-5,
    )

    # Read out the data and filter
    (
        combined_df,
        parameter_cols,
        parameter_extent_dict,
        weight_type_name_dict,
    ) = readout_and_filter_exploration_ensemble(filename)

    ######
    # Construct the figure
    fig, axes_dict = create_figure_for_alpha_vs_synchronicity(
        orientation=orientation, x_parameter=x_parameter, y_parameter=y_parameter
    )

    ax_mass = axes_dict["mass"]
    ax_time = axes_dict["time"]
    ax_cb = axes_dict["cb"]
    axes_list = [ax_mass, ax_time]

    #######################
    # Calculate quantities

    # construct aggregate dict
    parameters = [x_parameter, y_parameter]
    aggregate_dict = {parameter: "first" for parameter in parameters}
    for _, (_, weight_colname) in enumerate(weight_type_name_dict.items()):
        aggregate_dict[weight_colname] = "sum"

    # groupby and aggregate
    grouped_df = combined_df[parameters + list(weight_type_name_dict.values())]
    grouped_df = grouped_df.groupby(parameters, as_index=False).aggregate(
        aggregate_dict
    )
    grouped_df = grouped_df.sort_values(by=parameters)

    #
    x_bins, x_bincenters = calculate_bins_and_bincenters(grouped_df[x_parameter])
    y_bins, y_bincenters = calculate_bins_and_bincenters(grouped_df[y_parameter])
    X, Y = np.meshgrid(x_bincenters, y_bincenters)

    #######################
    # Plot mass

    # Calculate the results
    hist_mass, _, _ = np.histogram2d(
        grouped_df[x_parameter],
        grouped_df[y_parameter],
        bins=[x_bins, y_bins],
        weights=grouped_df[weight_type_name_dict["mass"]],
        density=True,
    )

    _ = ax_mass.pcolormesh(
        X,
        Y,
        hist_mass.T,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # ax_mass.axhline(0, linestyle="--", color="r", alpha=0.5)
    # ax_mass.axvline(1, linestyle="--", color="r", alpha=0.5)

    #######################
    # Plot time

    # Calculate the results
    hist_time, _, _ = np.histogram2d(
        grouped_df[x_parameter],
        grouped_df[y_parameter],
        bins=[x_bins, y_bins],
        weights=grouped_df[weight_type_name_dict["time"]],
        density=True,
    )

    _ = ax_time.pcolormesh(
        X,
        Y,
        hist_time.T,
        norm=norm,
        shading="auto",
        antialiased=True,
        rasterized=True,
    )

    # ax_time.axhline(0, linestyle="--", color="r", alpha=0.5)
    # ax_time.axvline(1, linestyle="--", color="r", alpha=0.5)

    ##########
    # plot color bar
    cbar = mpl.colorbar.ColorbarBase(ax_cb, norm=norm, extend="min")
    cbar.ax.set_ylabel("Normalized probability\ndensity")

    ##########
    # plot make up

    # Add labels to subplots
    add_labels_subplots(
        fig,
        axes_list,
        label_function_kwargs={
            "x_loc": 0.05,
            "bbox_dict": {"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
        },
    )

    axes_list[0].set_xlim([-3.5, -0.5])
    axes_list[0].set_ylim([-7, -1])

    axes_list[1].set_xlim([-3.5, -0.5])
    axes_list[1].set_ylim([-7, -1])

    # Align axes
    align_both_axes(fig=fig, axes_list=axes_list)

    #
    fig.tight_layout()

    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    show_plot = False
    include_weight_type = ["mass", "time"]

    #
    result_root = os.path.join(os.getenv("BINARYC_DATA_ROOT"), "RLOF/")

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    ######
    # TESTING
    filename = os.path.join(
        result_root,
        "TEST_RES_EXPLORATION_BALLISTIC_RLOF_2021_SANA/population_results/Z0.001/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )
    basename = "exploration_vthermal_vs_Astream_low_res.pdf"
    plot_v_a_distributions(
        filename,
        orientation="horizontal",
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # quit()

    #####
    # FIDUCIAL
    filename = os.path.join(
        result_root,
        "server_results/EVENTS_V2.2.2_HIGH_RES_RLOF_2021_SANA_BALLISTIC_EXPLORATION/population_results/Z0.02/subproject_ballistic_exploration_velocity_dispersion_ensemble_data.csv",
    )
    basename = "exploration_vthermal_vs_Astream_high_res.pdf"
    plot_v_a_distributions(
        filename,
        orientation="horizontal",
        plot_settings={
            "show_plot": show_plot,
            "output_name": os.path.join(plot_dir, basename),
        },
    )
