"""
Some uncategorized utility functions for the trajectory calculations
"""

import time

import numpy as np


def get_weighted_average(df, col, weight_col="weight", ignore_value=None):
    """
    Function to get the weighted average of a column

    Optionally we can ignore a specific value. This leads to the weighted average only being calculated with the elements of the arrays that do not match the indices of the elements in the value array that match the ignore_value.
    """

    #
    values = df[col].to_numpy()
    weights = df[weight_col].to_numpy()

    # handle ignore value
    if ignore_value is not None:
        _values = np.copy(values)

        values = values[_values != 0]
        weights = weights[_values != 0]

    # Handle calculating the average
    if len(values) > 0:
        weighted_average = np.average(values, weights=weights)
    else:
        weighted_average = 0

    return weighted_average


class timing:
    def __init__(self, name, verbosity=0):
        self.name = name
        self.verbosity = verbosity

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc, value, exc_traceback):
        self.end = time.time()
        if self.verbosity > 0:
            print(f"{self.name} took\n\t{self.end-self.start:.4e}s")
