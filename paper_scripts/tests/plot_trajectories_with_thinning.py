"""
Function to handle plotting an example set of trajectories with different classifications
"""

import copy
import os

import matplotlib
import matplotlib.pyplot as plt
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)

from ballistic_integration_code.scripts.L1_stream.functions.intersection.intersection_detection import (
    get_all_self_and_other_intersections,
)
from ballistic_integration_code.scripts.L1_stream.functions.intersection.thinning_functions import (
    thin_trajectory_by_angle,
    thin_trajectory_by_distance,
    thin_trajectory_by_index,
)
from ballistic_integration_code.scripts.L1_stream.functions.plot_functions import (
    plot_system,
)
from ballistic_integration_code.scripts.L1_stream.functions.select_trajectory_categories import (
    select_trajectory_categories,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.integrate_trajectories_from_L1_stage_3 import (
    integrate_trajectories_from_L1_stage_3,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.stage_settings import (
    stage_3_settings,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

import pickle

from ballistic_integration_code.scripts.L1_stream.functions.utils import timing


def plot_trajectory_classification_example(
    settings, plot_settings, recalculate_trajectory_set=False
):
    """
    Function to plot different sets of trajectories
    """

    x_bounds = plot_settings["x_bounds"]
    y_bounds = plot_settings["y_bounds"]

    output_file = "trajectory_set.pickle"

    ##############################
    # Calculate trajectory set for the configuration passed by settings
    if recalculate_trajectory_set or not os.path.isfile(output_file):
        trajectory_set = integrate_trajectories_from_L1_stage_3(settings=settings)
        with open(output_file, "wb") as f:
            pickle.dump(trajectory_set, f)

    # load from pickle
    with open(output_file, "rb") as f:
        trajectory_set = pickle.load(f)

    # Select original
    original_trajectory = trajectory_set[0]["result_summary"]
    original_trajectory["weight"] = 0.5

    trajectory_sets_per_category = []

    ####################
    # Manually add to trajectory set
    trajectory_sets_per_category.append(
        {
            "name": "full trajectory",
            "trajectory_list": [original_trajectory],
            "plot_kwargs": {
                "label": "Full trajectory: {}".format(
                    len(original_trajectory["positions"])
                ),
                "color": "red",
            },
        }
    )

    ####################
    # Add thinned versions of the trajectories

    # Thin by index
    if plot_settings.get("handle_thinning_by_index", True):
        index_thinned_trajectory = copy.deepcopy(original_trajectory)
        with timing("Thinning by index"):
            index_thinned_trajectory["positions"] = thin_trajectory_by_index(
                trajectory=index_thinned_trajectory["positions"], settings=settings
            )
            trajectory_sets_per_category.append(
                {
                    "name": "Thin by index",
                    "trajectory_list": [index_thinned_trajectory],
                    "plot_kwargs": {
                        "label": "Thin by index: {}".format(
                            len(index_thinned_trajectory["positions"])
                        ),
                        "color": "blue",
                    },
                }
            )

    # Thin by distance
    if plot_settings.get("handle_thinning_by_distance", True):
        distance_thinned_trajectory = copy.deepcopy(original_trajectory)
        with timing("Thinning by distance"):
            distance_thinned_trajectory["positions"] = thin_trajectory_by_distance(
                trajectory=distance_thinned_trajectory["positions"], settings=settings
            )
            trajectory_sets_per_category.append(
                {
                    "name": "Thin by distance",
                    "trajectory_list": [distance_thinned_trajectory],
                    "plot_kwargs": {
                        "label": "Thin by distance: {}".format(
                            len(distance_thinned_trajectory["positions"])
                        ),
                        "color": "black",
                    },
                }
            )

    # Thin by angle
    if plot_settings.get("handle_thinning_by_angle", True):
        angle_thinned_trajectory = copy.deepcopy(original_trajectory)
        with timing("Thinning by angle"):
            angle_thinned_trajectory["positions"] = thin_trajectory_by_angle(
                trajectory=angle_thinned_trajectory["positions"], settings=settings
            )
            trajectory_sets_per_category.append(
                {
                    "name": "Thin by angle change",
                    "trajectory_list": [angle_thinned_trajectory],
                    "plot_kwargs": {
                        "label": "Thin by angle change: {}".format(
                            len(angle_thinned_trajectory["positions"])
                        ),
                        "color": "green",
                    },
                }
            )

    ###################
    # Handle plotting #

    #
    fig = plt.figure(figsize=(20, 20))
    fig, axes_list = plot_system(
        system_dict=settings["system_dict"],
        frame_of_reference=settings["frame_of_reference"],
        plot_trajectory=True,
        plot_rochelobe_equipotential_meshgrid=False,
        resultdata_sets=trajectory_sets_per_category,
        plot_system_information_panel=False,
        return_fig=True,
        plot_stars_and_COM=False,
        plot_L_points=False,
        plot_settings={
            **plot_settings,
            "plot_bounds_x": plot_settings["x_bounds"],
            "plot_bounds_y": plot_settings["y_bounds"],
            "plot_resolution": settings["plot_resolution"],
            "star_text_color": "white",
        },
        settings=settings,
        fig=fig,
    )

    # ##################
    # # Intersections
    # linestyle_intersections = "None"
    # markeredgewidth_intersections = 4
    # markersize_intersections = 20
    # markerfacecolor_intersections = "None"

    # marker_self_intersections = "o"
    # marker_other_intersections = "s"

    # markeredgecolor_self_intersection = "#00cc66"
    # markeredgecolor_other_intersection = "#00ffff"

    # # Other-intersections
    # if plot_settings.get("plot_other_intersections", True):
    #     print(
    #         "Found {} unique other-intersection locations".format(
    #             len(combined_all_other_intersections)
    #         )
    #     )
    #     for other_intersection_location in combined_all_other_intersections:
    #         # plot
    #         _ = axes_list[0].plot(
    #             other_intersection_location[0],
    #             other_intersection_location[1],
    #             marker=marker_other_intersections,
    #             linestyle=linestyle_intersections,
    #             markeredgewidth=markeredgewidth_intersections,
    #             markersize=markersize_intersections,
    #             markeredgecolor=markeredgecolor_other_intersection,
    #             markerfacecolor=markerfacecolor_intersections,
    #         )

    # # Self-intersections
    # if plot_settings.get("plot_self_intersections", True):
    #     # Draw on the intersections
    #     print(
    #         "Found {} unique self-intersection locations".format(
    #             len(combined_all_self_intersections)
    #         )
    #     )
    #     for self_intersection_location in combined_all_self_intersections:
    #         # plot
    #         _ = axes_list[0].plot(
    #             self_intersection_location[0],
    #             self_intersection_location[1],
    #             marker=marker_self_intersections,
    #             linestyle=linestyle_intersections,
    #             markeredgewidth=markeredgewidth_intersections,
    #             markersize=markersize_intersections,
    #             markeredgecolor=markeredgecolor_self_intersection,
    #             markerfacecolor=markerfacecolor_intersections,
    #         )

    ######
    # Resize
    for ax in axes_list:
        ax.set_xlim(plot_settings["x_bounds"])
        ax.set_ylim(plot_settings["y_bounds"])

    # Remove text that falls out of frame
    fig, axes_list[0] = delete_out_of_frame_text(fig, axes_list[0])

    # Add labels
    axes_list[0].set_xlabel(r"$\it{x}$", fontsize=36)
    axes_list[0].set_ylabel(r"$\it{y}$", fontsize=36)

    #
    show_and_save_plot(fig=fig, plot_settings=plot_settings)


if __name__ == "__main__":
    ###########
    # PLOTTING TO CHECK STATEMENTS IN THE PAPER

    plot_settings = {
        "star_text_color": "white",
        "show_plot": True,
        "verbosity": 1,
        "x_bounds": [-1, 1],
        "y_bounds": [-1, 1],
        "thin_trajectories": True,
        "handle_thinning_by_distance": True,
        "handle_thinning_by_index": True,
        "handle_thinning_by_angle": True,
    }

    output_dir = os.path.join("plots/")
    basename = "test.pdf"
    #
    system_dict = {
        **standard_system_dict,
        "mass_accretor": 1,
        "mass_donor": 1,
        "synchronicity_factor": 1,
    }

    grid_point = {
        "massratio_accretor_donor": system_dict["mass_accretor"]
        / system_dict["mass_donor"],
        "synchronicity_factor": system_dict["synchronicity_factor"],
        "log10normalized_thermal_velocity_dispersion": -3.0,
    }

    settings = {
        **stage_3_settings,
        "system_dict": system_dict,
        "grid_point": grid_point,
        "num_cores": 4,
        "jacobi_error_tol": 1e-6,
        "dt": 0.001,
        "num_samples_area_sampling": 1,
        "return_trajectory_set": True,
        "direction_velocity_asynchronous_offset": +1,
        "include_asynchronous_donor_effects": True,
        "include_asynchronicity_donor_during_integration": False,
        "limit_timestep_on_stepsize": True,
        "max_position_diff_timestep_rejection": 0.001,
        #
        "verbosity": 0,
        "intersection_angle_threshold": 30,
        "trajectory_thinning_length": 100,
        "handle_intersecting_trajectories": False,
        "trajectory_thinning_distance_threshold": 0.05,
        "trajectory_thinning_angle_threshold": 2,
    }

    plot_trajectory_classification_example(
        settings=settings,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
        recalculate_trajectory_set=True,
    )
