"""
File containing the stage level settings. Used by stage_1/main.py
"""

import os

import numpy as np

from ballistic_integration_code.scripts.L1_stream.project_settings import (
    EXIT_CODES,
    project_settings,
)

#######################
# Control settings
stage_1_settings = {
    **project_settings,
    ############
    # Grid settings
    "result_dir": os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "ballistic_stream_integration_results_stage_1",
    ),
    "q_range": 10 ** np.linspace(-2, 2, 20),
    "f_range": np.linspace(0.1, 1, 10),
    "allowed_exit_codes": [EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value],
    ############
    # Data header output control
    "input_parameter_list_data_header": ["A_factor", "massratio_accretor_donor"],
    "output_parameter_list_data_header": ["rmin", "rcirc"],
}
