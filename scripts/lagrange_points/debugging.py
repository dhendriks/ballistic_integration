"""
Script to try to debug the difference with the roche lobe volume difference between the sepinsky implementation and my implementation
"""

import numpy as np

from ballistic_integration_code.scripts.L1_stream.project_settings import (
    parameter_dict,
    project_settings,
)
from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)
from ballistic_integration_code.scripts.lagrange_points.run_all import run_all
from ballistic_integration_code.settings import standard_system_dict

settings = {
    **project_settings,
    "result_dir": "results/debugging/",
    "max_job_queue_size": 1000,
    "num_cores": 8,
    "q_range": 10 ** np.linspace(-2, 2, 20),
    "f_range": [1],
    # "f_range": np.linspace(0.1, 2, 2),
    "verbosity": 1,
    "volume_roche_lobe_xgrid_n": 25,
    "volume_roche_lobe_ygrid_n": 25,
    "volume_roche_lobe_size_factor": 1.005,
}


synchronicity_factor = 0.8
massratio_accretor_donor = 0.1


# Set system dict
settings["system_dict"] = {
    **standard_system_dict,
    "synchronicity_factor": synchronicity_factor,
    "mass_accretor": massratio_accretor_donor * standard_system_dict["mass_donor"],
}


######
# Calculate Roche lobe information
volume_roche_lobe_information = calculate_volume_roche_lobe_information(
    system_dict=settings["system_dict"],
    frame_of_reference=settings["frame_of_reference"],
    settings=settings,
    plot_collection=False,
    print_info=True,
)

print(volume_roche_lobe_information)


# if __name__ == "__main__":
#     settings = {
#         **project_settings,
#         "result_dir": "results/debugging/",
#         "max_job_queue_size": 1000,
#         "num_cores": 8,
#         "q_range": 10 ** np.linspace(-2, 2, 20),
#         "f_range": [1],
#         # "f_range": np.linspace(0.1, 2, 2),
#         "verbosity": 1,
#         "volume_roche_lobe_xgrid_n": 25,
#         "volume_roche_lobe_ygrid_n": 25,
#         "volume_roche_lobe_size_factor": 1.001,
#     }
#     settings["result_dir"] = "test/"
#     settings["lagrange_point_data_file_basename"] = "lagrange_point_data.txt"
#     settings[
#         "lagrange_point_csv_interpolation_table_file_basename"
#     ] = "lagrange_point_and_rochelobe_volume_interpolation_table.csv"
#     settings[
#         "lagrange_point_header_interpolation_table_file_basename"
#     ] = "RLOF_sepinsky2007_lagrange_interpolation_table.h"

#     settings["rochelobe_passed_gridpoint_file_basename"] = "passed_gridpoints.txt"
#     settings["rochelobe_failed_gridpoint_file_basename"] = "failed_gridpoints.txt"

#     run_all(
#         settings=settings,
#         parameter_dict=parameter_dict,
#         generate_lagrange_point_and_roche_lobe_information=True,
#         generate_output_interpolation_files=True,
#     )
