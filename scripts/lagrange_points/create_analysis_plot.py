"""
Function to create an analysis plot to inspect the datatable
"""

import json

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.linestyle_hatch_colorlist import linestyle_list
from david_phd_functions.plotting.utils import align_axes, show_and_save_plot
from matplotlib import colors

from ballistic_integration_code.scripts.lagrange_points.calculate_roche_lobe_volume import (
    calculate_volume_roche_lobe_information,
)
from ballistic_integration_code.settings import standard_system_dict

custom_mpl_settings.load_mpl_rc()


def load_df(filename):
    """
    Function to load the dataframe
    """

    #
    headerline = []
    datalines = []

    with open(filename, "r") as f:
        lineno = 0
        for line in f:
            if line.strip() and not line.startswith("//"):

                cleaned_line = line.strip().split()

                if lineno == 0:
                    headerline.append(cleaned_line)
                else:
                    values = [float(el) for el in cleaned_line]
                    datalines.append(values)

                lineno += 1

    df = pd.DataFrame(datalines, columns=headerline[0])

    df["f_sync"] = np.sqrt(df["A_factor"])

    return df


def load_settings(settings_filename):
    """
    Function to load the settings
    """

    with open(settings_filename, "r") as f:
        settings = json.loads(f.read())

    return settings


def create_bins_from_centers(centers):
    """
    Function to create a set of bin edges from a set of bin centers. Assumes the two endpoints have the same binwidth as their neighbours
    """

    # Create bin edges minus the outer two
    bin_edges = (centers[1:] + centers[:-1]) / 2

    # Add to left
    bin_edges = np.append(np.array(bin_edges[0] - np.diff(centers)[0]), bin_edges)

    # Add to right
    bin_edges = np.append(bin_edges, np.array(bin_edges[-1] + np.diff(centers)[-1]))

    return bin_edges


def get_combinations(df, settings):
    """
    Function to get the combinations
    """

    # readout present combinations
    present_combinations = df[["massratio_accretor_donor", "f_sync"]].values.tolist()

    # Get all combinations
    all_combinations = []
    for i in range(len(settings["q_range"])):
        for j in range(len(settings["f_range"])):
            all_combinations.append([settings["q_range"][i], settings["f_range"][j]])
    all_combinations_array = np.array(all_combinations)

    #
    missing_combinations = []
    for el in all_combinations:
        if not el in present_combinations:
            missing_combinations.append(el)
    missing_combinations_array = np.array(missing_combinations)

    return present_combinations, all_combinations_array, missing_combinations_array


###############

plot_settings = {"show_plot": False, "output_name": "plots/analysis_plot.pdf"}

# Load file
filename = "results/test/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"
settings_filename = "results/test/settings_lagrange_points.json"


df = load_df(filename)
settings = load_settings(settings_filename)

(
    present_combinations,
    all_combinations_array,
    missing_combinations_array,
) = get_combinations(df, settings)


massratios = df["massratio_accretor_donor"]

##################
# Create bins x-axis and y-axis
x_bins = create_bins_from_centers(np.array(settings["q_range"]))
y_bins = create_bins_from_centers(np.array(settings["f_range"]))

#
X, Y = np.meshgrid(np.array(settings["q_range"]), np.array(settings["f_range"]))

##################
# Set up figure logic
fig = plt.figure(figsize=(12, 24))
gs = fig.add_gridspec(nrows=3, ncols=6)

# Set up axes
ax_coverage = fig.add_subplot(gs[0, :5])
ax_radius = fig.add_subplot(gs[1, :5])
ax_ratio_radius = fig.add_subplot(gs[2, :5])

# Set up colorbar axes
ax_radius_cb = fig.add_subplot(gs[1, 5])
ax_ratio_radius_cb = fig.add_subplot(gs[2, 5])

ax_radius_cb.yaxis.set_label_position("right")
ax_radius_cb.yaxis.tick_right()

ax_radius_cb.yaxis.set_label_position("right")
ax_ratio_radius_cb.yaxis.tick_right()

##################
# Plot coverage
ax_coverage.scatter(
    all_combinations_array[:, 0], all_combinations_array[:, 1], s=100, c="red"
)

ax_coverage.scatter(massratios, df["f_sync"], s=100, c="green")

##################
# Plot radius and colorbar
radius_cmap = plt.get_cmap("viridis")
radius_norm = colors.Normalize(
    vmin=df["roche_lobe_radius"].min(), vmax=df["roche_lobe_radius"].max()
)

# Create the 2-d hist
hist, _, _ = np.histogram2d(
    massratios,
    df["f_sync"],
    bins=[x_bins, y_bins],
    weights=df["roche_lobe_radius"],
)

# Plot the results
_ = ax_radius.pcolormesh(
    X,
    Y,
    hist.T,
    norm=radius_norm,
    shading="auto",
    antialiased=plot_settings.get("antialiased", True),
    rasterized=plot_settings.get("rasterized", True),
    cmap=radius_cmap,
)

# make colorbar
cbar = mpl.colorbar.ColorbarBase(ax_radius_cb, norm=radius_norm, cmap=radius_cmap)

##################
# Plot ratio and colorbar
ratio_radius_cmap = plt.get_cmap("viridis")
ratio_radius_norm = colors.Normalize(
    vmin=df["ratio_RL_RLegg"].min(), vmax=df["ratio_RL_RLegg"].max()
)

# Create the 2-d hist
ratio_hist, _, _ = np.histogram2d(
    massratios,
    df["f_sync"],
    bins=[x_bins, y_bins],
    weights=df["ratio_RL_RLegg"],
)

# Plot the results
_ = ax_ratio_radius.pcolormesh(
    X,
    Y,
    ratio_hist.T,
    norm=ratio_radius_norm,
    shading="auto",
    antialiased=plot_settings.get("antialiased", True),
    rasterized=plot_settings.get("rasterized", True),
    cmap=ratio_radius_cmap,
)

# make colorbar
ratio_cbar = mpl.colorbar.ColorbarBase(
    ax_ratio_radius_cb, norm=ratio_radius_norm, cmap=ratio_radius_cmap
)


#

contourlevels = [0.7, 0.8, 0.9, 1.0, 1.1, 1.2]
cs = ax_ratio_radius.contour(
    X,
    Y,
    ratio_hist.T,
    levels=contourlevels,
    linestyles=linestyle_list,
    colors="red",
)
for contour_i, _ in enumerate(contourlevels):
    ratio_cbar.ax.plot(
        [ratio_cbar.ax.get_xlim()[0], ratio_cbar.ax.get_xlim()[1]],
        [contourlevels[contour_i]] * 2,
        "red",
        linestyle=linestyle_list[contour_i],
    )

# def fmt(x):
#     s = f"{x:.1f}"
#     if s.endswith("0"):
#         s = f"{x:.0f}"
#     return rf"{s} \%" if plt.rcParams["text.usetex"] else f"{s} %"

ax_ratio_radius.clabel(cs, cs.levels, inline=True, fontsize=10)


#
ax_radius.set_ylabel(r"Synchronicity $f_{\mathrm{sync}}$")
ax_ratio_radius.set_xlabel(r"Mass ratio $q_{\mathrm{acc}}$")

ax_coverage.set_xscale("log")
ax_radius.set_xscale("log")
ax_ratio_radius.set_xscale("log")

align_axes(fig=fig, axes_list=[ax_coverage, ax_radius, ax_ratio_radius], which_axis="x")
align_axes(fig=fig, axes_list=[ax_coverage, ax_radius, ax_ratio_radius], which_axis="y")


ax_coverage.set_title("coverage and status")
ax_radius.set_title("Roche-lobe radius")
ax_ratio_radius.set_title("RL/RL_egg")

fig.subplots_adjust(hspace=0.2)

#
fig.tight_layout()

# Add info and plot the figure
show_and_save_plot(fig, plot_settings)
