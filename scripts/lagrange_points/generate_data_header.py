"""
Function to create the interpolation table of the sepinsky lagrange points in a format that binary_c requires
"""

import copy
import os

import pandas as pd
from quality_check import quality_check

from ballistic_integration_code.scripts.L1_stream.functions.data_header_functions import (
    data_header_write_data_array,
    data_header_write_input_defines,
    data_header_write_meta_data,
    data_header_write_output_defines,
    get_index_dict,
)
from ballistic_integration_code.scripts.L1_stream.functions.functions import (
    get_git_info_and_time,
)
from ballistic_integration_code.scripts.L1_stream.stage_3.generate_output_datafile_functions import (
    prepare_df,
)


def generate_data_header(
    input_textfile,
    output_textfile,
    settings,
    parameter_dict,
):
    """
    Function to create the interpolation table in a format that binary_c requires
    """

    settings = copy.copy(settings)
    settings["specific_angular_momentum_stream_interpolation_enabled"] = False

    #
    header = "RLOF_HENDRIKS2023_ROCHE_LOBE_INTERPOLATION"
    stage_number = 3

    #
    length_decimals = settings["length_decimals_dataheader"]
    input_parameter_list = copy.copy(
        settings["input_parameter_list_data_header_rochelobe_volume"]
    )
    output_parameter_list = copy.copy(
        settings["output_parameter_list_data_header_rochelobe_volume"]
    )

    # prepare df
    filtered_df, output_parameter_list = prepare_df(
        settings=settings,
        input_interpolation_textfile=input_textfile,
        input_parameter_list=input_parameter_list,
        output_parameter_list=output_parameter_list,
    )

    # Get the number of lines we will output to the table and some others
    num_lines = len(filtered_df.index)
    num_input_parameters = len(input_parameter_list)
    num_output_parameters = len(output_parameter_list)

    # Get indices for input parameters
    input_index_dict = get_index_dict(
        df=filtered_df, columns=input_parameter_list, offset=0
    )
    output_index_dict = get_index_dict(
        df=filtered_df, columns=output_parameter_list, offset=-num_input_parameters
    )

    # #
    # quality_check(df=filtered_df, settings=settings)

    # Get git info
    git_info = get_git_info_and_time()

    # Write to file
    with open(output_textfile, "w") as f:
        # Write top-level explanation
        f.write(
            """
// Roche-lobe volume interpolation dataset.
// This dataset provides donor Roche-lobe radii as a function of the A-factor (sepinsky 2007)
// and the mass ratio (q_acc=m_acc/m_don)\n\n
"""
        )

        # Write git revision information
        f.write(
            """
// Generated on: {} with
// \tgit repository: {}
// \tbranch: {}
// \tcommit: {}\n\n
""".format(
                git_info["datetime_string"],
                git_info["repo_name"],
                git_info["branch_name"],
                git_info["commit_sha"],
            )
        )

        ######
        # Write data header metadata (parameter descriptions, stage number, number of data lines etc)
        data_header_write_meta_data(
            settings=settings,
            input_parameter_list=input_parameter_list,
            output_parameter_list=output_parameter_list,
            parameter_dict=parameter_dict,
            # stage_number=stage_number,
            num_lines=num_lines,
            num_input_parameters=num_input_parameters,
            num_output_parameters=num_output_parameters,
            filehandle=f,
            definition_basename=header,
        )

        ##########
        # Write the input defines
        data_header_write_input_defines(
            parameter_list=input_parameter_list,
            index_dict=input_index_dict,
            filehandle=f,
            definition_basename=header,
        )

        ##########
        # Write the output defines
        data_header_write_output_defines(
            settings=settings,
            parameter_list=output_parameter_list,
            index_dict=output_index_dict,
            filehandle=f,
            definition_basename=header,
        )

        #########
        # Write the data
        data_header_write_data_array(
            df=filtered_df,
            length_decimals=length_decimals,
            num_lines=num_lines,
            filehandle=f,
            definition_basename=header,
        )

        # # Add start for the data array
        # f.write("#define {}_TABLE_DATA \\\n".format(header))

    # # Write data to file
    # with open(output_interpolation_textfile, "a") as f:
    #     for lineno, line in enumerate(df.values.tolist()):
    #         termination = ",\\\n" if not lineno == num_lines - 1 else "\n"
    #         outputline = ",".join([str(el) for el in line]) + termination

    #         f.write(outputline)


if __name__ == "__main__":
    result_dir = "results"

    #
    if os.getenv("BINARY_C"):
        output_dir = os.path.join(os.getenv("BINARY_C"), "src", "RLOF")

    # output_dir = "results"

    #
    input_interpolation_textfile = os.path.join(
        result_dir, "sepinsky_lagrange_points_interpolation_table.txt"
    )
    output_interpolation_textfile = os.path.join(
        output_dir, "RLOF_sepinsky2007_lagrange_interpolation_table.h"
    )

    generate_data_header(input_interpolation_textfile, output_interpolation_textfile)
