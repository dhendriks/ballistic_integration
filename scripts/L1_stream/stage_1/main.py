"""
Main script to run stage 1 of the ballistic integrations:
- grid over f and q
- particle starts exactly at L1
- particle has no initial velocity
"""


import numpy as np

from ballistic_integration_code.scripts.L1_stream.functions.run_all import run_all
from ballistic_integration_code.scripts.L1_stream.stage_1.stage_settings import (
    stage_1_settings,
)

######################
# Run grid           #
######################

settings = stage_1_settings

# ############
# # Test run
# settings = {
#     **settings,
#     "result_dir": os.path.join(
#         os.environ["PROJECT_DATA_ROOT"],
#         "ballistic_data",
#         "L1_stream",
#         "ballistic_stream_integration_results_stage_1_TEST",
#     ),
#     "q_range": 10 ** np.linspace(-2, 2, 2),
#     "q_range": [0.1, 10],
#     # "f_range": np.linspace(0.1, 1, 10),
#     "f_range": [0.2],
#     "verbosity": 0,
# }
# run_all(
#     settings=settings,
#     stage='stage_1',
#     run_trajectories=True,
#     generate_report=True,
#     create_data_header=True,
#     generate_description_table=True
# )
# quit()
# re_run_settings = {"result_dir": settings['result_dir'] + "RE_RUN", "plot_system_at_re_run": True}
# run_all(settings=settings, stage='stage_1', run_trajectories=False, generate_report=False, create_data_header=False, generate_description_table=False,
#     re_run_failed_systems=True, re_run_settings=re_run_settings
# )

############
# Production run
# NOTE: uncomment ALL of the above and configure the stage_settings appropriately
settings = {
    **settings,
    'generate_plot_at_gridpoint': True,
    "num_cores": 4,
    "verbosity": 1,
    "non_synch_velocity_offset_distance_to": 'donor',
    "jacobi_error_tol": 1e-3,
    "q_range": np.concatenate([10**np.linspace(-2,0, 20), 10**np.linspace(0, 2, 10)[1:]]),
    "f_range": np.linspace(0.1, 2, 20),
}
run_all(
    settings=settings,
    stage='stage_1',
    run_trajectories=True,
    generate_report=True,
    create_data_header=True,
    generate_description_table=True
)
# re_run_settings = {"result_dir": settings['result_dir'] + "RE_RUN", "plot_system_at_re_run": True}
# run_all(settings=settings, stage='stage_1', run_trajectories=False, generate_report=False, create_data_header=False, generate_description_table=False
#     re_run_failed_systems=True, re_run_settings=re_run_settings
# )
