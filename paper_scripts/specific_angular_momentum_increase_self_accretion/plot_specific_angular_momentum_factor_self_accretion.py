"""
Function to plot the change in specific angular momentum of the stream for the self accretion situation of the stream
"""

import os

import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
import numpy as np
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib.lines import Line2D

from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    readout_dataset_and_query,
)
from ballistic_integration_code.paper_scripts.radii_plots.settings import (
    xlabel,
    y_label_dict,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

fontsize_lines = 14


def plot_specific_angular_momentum_factor_self_accretion(
    result_file,
    query=None,
    plot_settings={},
):
    """
    Function to plot the change in specific angular momentum of the stream for the self accretion situation of the stream
    """

    y_quantity = "specific_angular_momentum_multiplier_self_accretion"

    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
        exit_code_value=EXIT_CODES.ACCRETION_ONTO_DONOR.value,
    )

    ##################
    # Check results for negative values

    nonzero_values = result_df.query("{} != 0".format(y_quantity))[
        "specific_angular_momentum_multiplier_self_accretion"
    ]
    nonzero_minus_one_values = nonzero_values - 1

    if np.min(nonzero_minus_one_values) < 0:
        negative_value_found = True

        # find value closest to 0 (negative and positive)
        abs_nonzero_minus_one_values = np.abs(nonzero_minus_one_values)
        min_abs_nonzero_minus_one_value = np.min(abs_nonzero_minus_one_values)
    else:
        negative_value_found = False

    ##################
    # Plot our results

    #
    fig, axes = plt.subplots(figsize=(16, 16))

    # Get unique mass synchronicity factors
    unique_nonzero_synchronicity_factors = result_df.query(
        "{} != 0".format(y_quantity)
    )["synchronicity_factor"].unique()

    # Set up colormap
    cmap = plt.cm.get_cmap("viridis", len(unique_nonzero_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    positive_data_lines = []
    negative_data_lines = []

    # Plot results for each synchronicity factor
    for i, synchronicity_factor in enumerate(unique_nonzero_synchronicity_factors):
        # Filter
        filtered_df = result_df[
            result_df.synchronicity_factor == synchronicity_factor
        ].query("{} != 0".format(y_quantity))

        # Mkae sure to only add things when there is data
        count = len(filtered_df.index)
        if count > 0:
            # Plot data
            marker = "o"
            linestyle = ":"
            label = "f = {:1.1f}".format(synchronicity_factor)

            #
            axes.plot(
                filtered_df["massratio_accretor_donor"],
                filtered_df["specific_angular_momentum_multiplier_self_accretion"] - 1,
                marker=marker,
                linestyle=linestyle,
                color=cmaplist[i],
                label=label,
                alpha=0.8,
            )

            all_positive = np.all(
                np.array(
                    [
                        filtered_df[
                            "specific_angular_momentum_multiplier_self_accretion"
                        ]
                        - 1
                    ]
                )
                > 0
            )
            all_negative = np.all(
                np.array(
                    [
                        filtered_df[
                            "specific_angular_momentum_multiplier_self_accretion"
                        ]
                        - 1
                    ]
                )
                < 0
            )

            legend_line_obj = Line2D(
                [0],
                [0],
                marker=marker,
                color=cmaplist[i],
                label=label,
                lw=4,
            )

            #
            if all_positive:
                positive_data_lines.append(legend_line_obj)

            if all_negative:
                negative_data_lines.append(legend_line_obj)

            # Add f-label text
            if plot_settings["add_f_label_text"]:
                rightmost_q_val = filtered_df["massratio_accretor_donor"].tolist()[-1]
                rightmost_y_val = filtered_df[
                    "specific_angular_momentum_multiplier_self_accretion"
                ].tolist()[-1]

                axes.text(
                    rightmost_q_val * 1.1,
                    rightmost_y_val,
                    "f = {:1.1f}".format(synchronicity_factor),
                    fontsize=fontsize_lines,
                )

    ######
    # Make up
    ncol = 1
    frameon = False
    framealpha = 0.5

    # Add legend for the data
    if len(positive_data_lines) > 0:
        positive_legend_data = plt.legend(
            handles=positive_data_lines,
            loc=1,
            ncol=ncol,
            frameon=frameon,
            framealpha=framealpha,
        )
        axes.add_artist(positive_legend_data)
    if len(negative_data_lines) > 0:
        negative_legend_data = plt.legend(
            handles=negative_data_lines,
            loc=4,
            ncol=ncol,
            frameon=frameon,
            framealpha=framealpha,
            bbox_to_anchor=(1.0, 0.075),
        )
        axes.add_artist(negative_legend_data)

    #
    axes.set_xscale("log")

    if negative_value_found:
        current_ylims = axes.get_ylim()
        max_value = np.max(np.abs(np.array(current_ylims)))
        axes.set_ylim([-max_value, max_value])
        axes.set_yscale("symlog", linthresh=10**-2)
    else:
        axes.set_yscale("log")

    axes.set_ylabel(y_label_dict[y_quantity])
    axes.set_xlabel(xlabel)

    # set x-axis
    current_xlim = axes.get_xlim()
    axes.set_xlim([current_xlim[0], current_xlim[1] * 5])
    current_xlim = axes.get_xlim()

    #########
    # add lines and text
    if plot_settings.get("add_line_and_text", True):
        axes.axhline(0, color="grey", alpha=0.5)
        position_text_x = 0.5
        # position_text_x = 10**(np.log10(current_xlim[-1])+np.log10(current_xlim[0]))/2
        trans = mtransforms.ScaledTranslation(10 / 72, -5 / 72, fig.dpi_scale_trans)

        # text
        axes.annotate(
            "Positive torque. Spin up",
            xy=(position_text_x, 0.65),
            xytext=(position_text_x, 0.5 + 0.05),
            arrowprops=dict(facecolor="black", shrink=0.05),
            verticalalignment="center",
            horizontalalignment="center",
            xycoords=axes.transAxes + trans,
            # transform=axes.transAxes+trans,
        )

        # text
        axes.annotate(
            "Negative torque. Spin down",
            xy=(position_text_x, 0.35),
            xytext=(position_text_x, 0.5 - 0.05),
            arrowprops=dict(facecolor="black", shrink=0.05),
            verticalalignment="center",
            horizontalalignment="center",
            xycoords=axes.transAxes + trans,
            # transform=axes.transAxes+trans,
        )

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def generate_specific_angular_momentum_factor_self_accretion_plot(simdir, output_dir):
    """
    Handler function for the plotting
    """

    # Paper ready plots
    plot_settings = {"show_plot": False, "add_f_label_text": False}

    #
    result_file = os.path.join(simdir, "trajectory_summary_data.txt")
    settings_file = os.path.join(simdir, "settings.json")

    # Low velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-3"
    basename = "self_accretion_h_factor_stage_3_low_v_low_A.pdf"
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )

    # # Low velocity large A
    # query_string = "log10normalized_thermal_velocity_dispersion==-3 & log10normalized_stream_area==-1"
    # basename = (
    #     "self_accretion_h_factor_stage_3_low_v_high_A.pdf"
    # )
    # plot_specific_angular_momentum_factor_self_accretion(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # High velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    basename = "self_accretion_h_factor_stage_3_high_v_low_A.pdf"
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(output_dir, basename),
        },
    )

    # # High velocity High A
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5 & log10normalized_stream_area==-1"
    # basename = (
    #     "self_accretion_h_factor_stage_3_high_v_high_A.pdf"
    # )
    # plot_specific_angular_momentum_factor_self_accretion(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )


def handle_paper_plot_specific_angular_momentum_factor_self_accretion(
    input_filename, output_name, show_plot, query_string
):
    """
    General function to handle the paper version of plot for the specific angular momentum of self-accretion material
    """

    #
    plot_settings = {
        "show_plot": show_plot,
        "add_f_label_text": False,
        "add_line_and_text": True,
    }

    plot_specific_angular_momentum_factor_self_accretion(
        input_filename,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": output_name,
        },
    )


def handle_paper_plot_specific_angular_momentum_factor_self_accretion_low_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of plot for the specific angular momentum of self-accretion material for low thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-3.0"

    handle_paper_plot_specific_angular_momentum_factor_self_accretion(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


def handle_paper_plot_specific_angular_momentum_factor_self_accretion_high_v(
    input_filename, output_name, show_plot
):
    """
    Function to handle the paper version of plot for the specific angular momentum of self-accretion material for high thermal velocity
    """

    query_string = "log10normalized_thermal_velocity_dispersion==-0.5"

    handle_paper_plot_specific_angular_momentum_factor_self_accretion(
        input_filename=input_filename,
        output_name=output_name,
        show_plot=show_plot,
        query_string=query_string,
    )


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    plot_settings = {"show_plot": False, "add_f_label_text": False}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    plot_dir = os.path.join(this_file_dir, "plots/")

    #######################
    # Server results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "server_comparison_methods_rmin_stage_1_minimal.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "server_comparison_methods_rmin_stage_2_low_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "server_comparison_methods_rmin_stage_2_high_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }
    # )

    ##############
    # Stage 3 results
    result_dir = os.path.join(
        os.environ["PROJECT_DATA_ROOT"],
        "ballistic_data",
        "L1_stream",
        "server_results",
        "ballistic_stream_integration_results_stage_3_TEST",
    )
    result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # Low velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-3 & log10normalized_stream_area==-4"
    basename = (
        "server_comparison_methods_self_accretion_h_factor_stage_3_low_v_low_A.pdf"
    )
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # Low velocity large A
    query_string = "log10normalized_thermal_velocity_dispersion==-3 & log10normalized_stream_area==-1"
    basename = (
        "server_comparison_methods_self_accretion_h_factor_stage_3_low_v_high_A.pdf"
    )
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # High velocity small A
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5 & log10normalized_stream_area==-4"
    basename = (
        "server_comparison_methods_self_accretion_h_factor_stage_3_high_v_low_A.pdf"
    )
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    # High velocity High A
    query_string = "log10normalized_thermal_velocity_dispersion==-0.5 & log10normalized_stream_area==-1"
    basename = (
        "server_comparison_methods_self_accretion_h_factor_stage_3_high_v_high_A.pdf"
    )
    plot_specific_angular_momentum_factor_self_accretion(
        result_file,
        query=query_string,
        plot_settings={
            **plot_settings,
            "output_name": os.path.join(plot_dir, basename),
        },
    )

    #######################
    # local results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "local_comparison_methods_self_accretion_h_factor_stage_1_minimal.pdf"
    # plot_specific_angular_momentum_factor_self_accretion(
    #     result_file,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "local_comparison_methods_rmin_stage_2_low_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "local_comparison_methods_rmin_stage_2_high_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=False,
    #     add_lubow_plot=False,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    ##############################################################
    # TESTING
    ##############################################################

    # #######################################
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1_TEST",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # #
    # basename = "testing_comparison_methods_rmin_stage_1_TEST.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # #######################################
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2_TEST",
    # )

    # #
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "testing_comparison_methods_rmin_stage_2_low_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "testing_comparison_methods_rmin_stage_2_high_temp.pdf"
    # plot_minimum_radius_grid(
    #     result_file,
    #     query=query_string,
    #     add_fkr_plot=True,
    #     add_lubow_plot=True,
    #     plot_settings={
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )
