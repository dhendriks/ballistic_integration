"""
Function to generate the results dict
"""

import collections

from ballistic_integration_code.scripts.L1_stream.functions.handle_classification_result_dict import (
    handle_classification_result_dict,
)


def generate_result_dict(settings, weight, result_summary, grid_point):
    """
    Function to generate the result dict
    """

    result_dict = {
        ########
        # grid point
        **grid_point,
        ########
        # additional info
        "exit_code": result_summary["exit_code"],
        ########
        # Sub-grid point
        "initial_position": settings["initial_position"],
        "initial_velocity": settings["initial_velocity"],
        "weight": weight,
        ########
        # all trajectories
        "h_init_don": result_summary["h_init_don"],
        ########
        # Result info accretion onto accretor
        "rmin": result_summary["rmin"],
        "rcirc": result_summary["rcirc"],
        "h_min_acc": result_summary["h_min_acc"],
        "stream_orientation": result_summary["stream_orientation"],
        "specific_torque_orbit_onto_accretor": result_summary[
            "specific_torque_orbit_onto_accretor"
        ],
        "magnitude_eccentricity_onto_accretor": result_summary[
            "magnitude_eccentricity_onto_accretor"
        ],
        "true_anomaly_onto_accretor": result_summary["true_anomaly_onto_accretor"],
        ########
        # Result info accretion onto donor
        "specific_torque_orbit_onto_donor": result_summary[
            "specific_torque_orbit_onto_donor"
        ],
        "specific_angular_momentum_multiplier_self_accretion": result_summary[
            "specific_angular_momentum_multiplier_self_accretion"
        ],
    }

    #############
    # add stream interpolation results dict
    if settings["specific_angular_momentum_stream_interpolation_enabled"]:
        # stream interpolation results
        stream_interpolation_result_dict = {
            key: value
            for key, value in result_summary.items()
            if key.startswith("specific_angmom_interpolation_radius_")
        }
        stream_interpolation_result_dict = collections.OrderedDict(
            sorted(
                stream_interpolation_result_dict.items(),
                key=lambda x: int(
                    x[0].replace("specific_angmom_interpolation_radius_", "")
                ),
            )
        )
        result_dict = {**result_dict, **stream_interpolation_result_dict}

    # handle classification trajectory
    result_dict = handle_classification_result_dict(result_dict)

    return result_dict
