"""
David Hendriks 2019

Main script for the L1 stream project:
    Script to integrate ballistic trajectories of particles leaving L1 point

    this file is the main file for the scripts to visualise the orbit and potential of the binary and the accretion disk.

TASKS:
- TODO: fix plotting routine for the trajectory, including the Roche potentials for L1 and L2 equipotentials
- TODO: fix plotting routine with output of energy, angular momentum etc
- TODO: make sure the rotation values are correct (i.e. angular velocity at L1 should match)
"""

import numpy as np
from ballistic_integrator.functions.functions import calc_pos_accretor, calc_pos_donor
from ballistic_integrator.functions.lagrange_points.calculate_lagrange_points import (
    calculate_lagrange_points,
)
from ballistic_integrator.functions.plot_functions import plot_resultfile

from ballistic_integration_code.scripts.L1_stream.functions import main_from_L1


def custom_termination_routine(self):
    """
    Custom termination function to deal with termination when test particle moves away from accretor
    """

    #
    terminate = False

    # Position accretor
    pos_accretor = calc_pos_accretor(
        mass_accretor=self.mass_accretor,
        mass_donor=self.mass_donor,
        separation=self.separation,
    )

    # current position
    pos_particle = self.position

    # get distance between current position and the accretor position
    distance_to_accretor = np.linalg.norm(pos_particle - pos_accretor)

    if self.step > 0:
        if distance_to_accretor > self.previous_distance_to_accretor:
            print(
                "Current distance to accretor: {} previous distance to accretor: {}".format(
                    distance_to_accretor, self.previous_distance_to_accretor
                )
            )
            return True

    self.previous_distance_to_accretor = distance_to_accretor

    return False


#
custom_termination_error_string = (
    "CUSTOM TERMINATION: Particle moving away from accretor"
)
use_custom_termination_routine = True

#################
# Configuration #
#################

extra_settings = {
    "max_time": 1500,
    "dt": 0.001,
    "steps_check_self_intersection": -1,
    "jacobi_error_tol": 1e-5,
    "initial_position_offset": [0.1, 0.0],
    "initial_velocity_offset": [0.0, 0.0],
    "print_info": False,
    "run": True,
    "allow_fallback_to_center": True,
    "result_dir": "results/main",
    "backup_if_data_exists": True,
    # custom termination
    "use_custom_termination_routine": True,
    "custom_termination_error_string": custom_termination_error_string,
    "custom_termination_routine": custom_termination_routine,
}

######################
# Grid of variations #
######################

logq_step = 0.05
# q_range = np.arange(q_step, 1 + q_step, q_step)
q_range = 10 ** np.arange(-1, 1, logq_step)

######################
# Run grid           #
######################

result_dir = extra_settings["result_dir"]
from david_phd_functions.backup_functions.functions import backup_if_exists

##############################################
# Check if the directory of the current simulation exists. If it does, then delete the old dir
if extra_settings.get("backup_if_data_exists", True):
    backup_if_exists(result_dir, remove_old_directory_after_backup=True)


for q in q_range:
    # Set values for a system
    mass_accretor = 1 * q
    mass_donor = 1
    separation = 1

    #######
    # Calculate offset

    # calculate accretor position:
    pos_accretor = calc_pos_accretor(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        separation=separation,
    )

    # Get L1 location
    L_points = calculate_lagrange_points(
        mass_donor, mass_accretor, separation=separation
    )
    l1_pos = L_points["L1"]
    diff_pos_accretor_l1 = l1_pos[:2] - pos_accretor

    #
    extra_settings["initial_position_offset"] = -diff_pos_accretor_l1 / 100

    # Simulate
    resultfile = main_from_L1(
        mass_accretor=mass_accretor,
        mass_donor=mass_donor,
        separation=separation,
        extra_settings=extra_settings,
        custom_termination_routine=custom_termination_routine,
    )

    # plot_resultfile(resultfile, plot_rochelobe_equipotentials=False, plot_settings={'show_plot': True})
