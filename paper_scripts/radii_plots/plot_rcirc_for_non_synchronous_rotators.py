"""
Script containing functions to plot the grid of results of the integration
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import show_and_save_plot
from matplotlib.lines import Line2D

from ballistic_integration_code.paper_scripts.radii_plots.functions import (
    check_correct_gridpoint_queryresult,
    handle_adding_all_comparison_plots,
    handle_query,
    readout_dataset_and_query,
)
from ballistic_integration_code.paper_scripts.radii_plots.settings import (
    xlabel,
    y_label_dict,
)
from ballistic_integration_code.scripts.L1_stream.project_settings import EXIT_CODES

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)

fontsize_lines = 14


def plot_circularisation_radius_grid(
    result_file,
    query=None,
    add_ulrich_plot=True,
    add_fkr_plot=True,
    add_lubow_plot=True,
    plot_settings={},
):
    """
    function to plot the circularisation radius for systems with ballistic non-synch results
    """

    radius_quantity = "rcirc"

    # readout datafile, filter and query
    result_df = readout_dataset_and_query(
        result_file=result_file,
        query=query,
        exit_code_value=EXIT_CODES.ACCRETION_ONTO_ACCRETOR.value,
    )

    # get min and max value of either radii
    radii_df = result_df[["rmin", "rcirc"]]

    max_radius = radii_df[radii_df > 0].max().max()
    min_radius = radii_df[radii_df > 0].min().min()

    # Calculte mass ratio
    q_acc_don = 10 ** np.linspace(-2, 2, 100)

    ###################
    # Plot results fits and equations

    #
    fig, axes = plt.subplots(figsize=(16, 16))

    # Plot the results for different prescriptions in literature
    legend_dict = {
        "add_paper_legend": False,
        "lines_legend_papers": [],
        "labels_legend_papers": [],
    }

    # Handle adding the comparison plots to the paper
    fig, axes, legend_dict = handle_adding_all_comparison_plots(
        fig=fig,
        ax=axes,
        q_acc_don=q_acc_don,
        radius_quantity=radius_quantity,
        legend_dict=legend_dict,
        add_ulrich_plot=add_ulrich_plot,
        add_fkr_plot=add_fkr_plot,
        add_lubow_plot=add_lubow_plot,
    )

    ##################
    # Plot our results

    # Get unique mass synchronicity factors
    unique_nonzero_synchronicity_factors = result_df.query(
        "{} > 0".format(radius_quantity)
    )["synchronicity_factor"].unique()

    # Set up colormap
    cmap = plt.cm.get_cmap("viridis", len(unique_nonzero_synchronicity_factors))
    cmaplist = [cmap(i) for i in range(cmap.N)]
    data_lines = []

    # Plot results for each synchronicity factor
    for i, synchronicity_factor in enumerate(unique_nonzero_synchronicity_factors):
        filtered_df = result_df[result_df.synchronicity_factor == synchronicity_factor]

        # get values and exclude 0
        nonzero_filtered_df = filtered_df.query("{} > 0".format(radius_quantity))

        # Plot data
        marker = "o"
        linestyle = ":"
        label = "f = {:1.1f}".format(synchronicity_factor)

        axes.plot(
            nonzero_filtered_df["massratio_accretor_donor"],
            nonzero_filtered_df[radius_quantity],
            marker=marker,
            linestyle=linestyle,
            color=cmaplist[i],
            label=label,
            alpha=0.8,
        )

        # Plot normal orientation
        normal_orientation_df = nonzero_filtered_df.query("stream_orientation > 0")
        axes.scatter(
            normal_orientation_df["massratio_accretor_donor"],
            normal_orientation_df[radius_quantity],
            marker="^",
            s=100,
            color=cmaplist[i],
            zorder=100,
        )

        # Plot opposite orientation
        opposite_orientation_df = nonzero_filtered_df.query("stream_orientation < 0")
        axes.scatter(
            opposite_orientation_df["massratio_accretor_donor"],
            opposite_orientation_df[radius_quantity],
            marker="v",
            s=100,
            color=cmaplist[i],
            zorder=100,
        )

        if len(nonzero_filtered_df.query("stream_orientation == 0").index) > 0:
            print("FOUND orientation = 0 systems")

        data_lines.append(
            Line2D(
                [0],
                [0],
                marker=marker,
                color=cmaplist[i],
                label=label,
                lw=4,
            )
        )

        # Add f-label text
        if plot_settings["add_f_label_text"]:
            rightmost_q_val = nonzero_filtered_df["massratio_accretor_donor"].tolist()[
                -1
            ]
            rightmost_y_val = nonzero_filtered_df[radius_quantity].tolist()[-1]

            axes.text(
                rightmost_q_val * 1.1,
                rightmost_y_val,
                "f = {:1.1f}".format(synchronicity_factor),
                fontsize=fontsize_lines,
            )

    ######
    # Make up

    # Add legend for the data
    legend_data = plt.legend(handles=data_lines, loc=4, ncol=2)
    axes.add_artist(legend_data)

    # Add legend for the other references
    if legend_dict["add_paper_legend"]:
        legend_papers = plt.legend(handles=legend_dict["lines_legend_papers"], loc=2)
        axes.add_artist(legend_papers)

    # Set ylim
    axes.set_ylim([min_radius * 0.5, max_radius * 2])

    #
    axes.set_xscale("log")
    axes.set_yscale("log")

    axes.set_ylabel(y_label_dict[radius_quantity])
    axes.set_xlabel(xlabel)

    current_xlim = axes.get_xlim()
    axes.set_xlim([current_xlim[0], current_xlim[1] * 2])

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":

    ##############################################################
    # Paper ready plots
    add_ulrich_plot = True
    add_fkr_plot = False
    add_lubow_plot = True

    plot_settings = {"show_plot": False, "add_f_label_text": False}

    #
    plot_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )
    # plot_dir = os.path.join(this_file_dir, "plots/")

    #######################
    # Server results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "server_comparison_methods_rcirc_stage_1_minimal.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "server_results",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "server_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "server_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    #######################
    # local results
    #######################

    # ##############
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Minimal
    # basename = "local_comparison_methods_rcirc_stage_1_minimal.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     add_ulrich_plot=add_ulrich_plot,
    #     add_fkr_plot=add_fkr_plot,
    #     add_lubow_plot=add_lubow_plot,
    #     plot_settings={
    #         **plot_settings,
    #         "output_name": os.path.join(plot_dir, basename),
    #     },
    # )

    # ##############
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "local_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "local_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    ##############################################################
    # TESTING
    ##############################################################

    # #######################################
    # # Stage 1 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_1_TEST",
    # )
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # #
    # basename = "testing_comparison_methods_rcirc_stage_1_TEST.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # #######################################
    # # Stage 2 results
    # result_dir = os.path.join(
    #     os.environ["PROJECT_DATA_ROOT"],
    #     "ballistic_data",
    #     "L1_stream",
    #     "ballistic_stream_integration_results_stage_2_TEST",
    # )

    # #
    # result_file = os.path.join(result_dir, "trajectory_summary_data.txt")

    # # Low temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-3.5"
    # basename = "testing_comparison_methods_rcirc_stage_2_low_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )

    # # High temp result
    # query_string = "log10normalized_thermal_velocity_dispersion==-0.5"
    # basename = "testing_comparison_methods_rcirc_stage_2_high_temp.pdf"
    # plot_circularisation_radius_grid(
    #     result_file,
    #     query=query_string,
    # add_ulrich_plot=add_ulrich_plot,
    # add_fkr_plot=add_fkr_plot,
    # add_lubow_plot=add_lubow_plot,
    # plot_settings={
    #     **plot_settings,
    #     "output_name": os.path.join(plot_dir, basename),
    # }

    # )
