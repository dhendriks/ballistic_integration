"""
Function to plot the lagrange points for ranges of sync factors and q factors in both frames of ref.
"""

import os

import matplotlib.pyplot as plt
import numpy as np
from ballistic_integrator.functions.lagrange_points.lagrange_points_sepinski import (
    get_locations_for_given_f,
)
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    add_label_subplot,
    align_axes,
    show_and_save_plot,
)
from matplotlib.lines import Line2D

custom_mpl_settings.load_mpl_rc()

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)


def plot_lagrange_point_overview_frames_of_reference(
    q_range, f_range, include_center_of_donor, include_center_of_mass, plot_settings
):
    """
    Function to plot the lagrange points for ranges of sync factors and q factors in both frames of ref.
    """

    linewidth = 5
    labelfontsize = 36

    # Plot results for each synchronicity factor
    cmap = plt.cm.get_cmap("viridis", len(f_range))
    cmaplist = [cmap(i) for i in range(cmap.N)]

    ###########
    # Grid to calculate lagrange points
    lagrange_point_dict = {}
    for f_i, f in enumerate(f_range):
        lagrange_point_dict[f] = {
            "center_of_donor": get_locations_for_given_f(
                massratios_accretor_donor=q_range, f=f, original=True
            ),
            "center_of_mass": get_locations_for_given_f(
                massratios_accretor_donor=q_range, f=f, original=False
            ),
            "color": cmaplist[f_i],
        }

    ##########
    # Figure logic
    cols = include_center_of_mass + include_center_of_donor

    # Set up figure
    fig = plt.figure(figsize=(14 * cols, 16))

    # Set up gridspec
    gs = fig.add_gridspec(nrows=3, ncols=cols)

    # center of donor frames
    col_i = 0
    if include_center_of_donor:
        center_of_donor_axes = []
        l2_center_of_donor_axis = fig.add_subplot(gs[0, col_i])
        center_of_donor_axes.append(l2_center_of_donor_axis)
        l1_center_of_donor_axis = fig.add_subplot(gs[1, col_i])
        center_of_donor_axes.append(l1_center_of_donor_axis)
        l3_center_of_donor_axis = fig.add_subplot(gs[2, col_i])
        center_of_donor_axes.append(l3_center_of_donor_axis)
        col_i += 1

    # center of mass frames
    if include_center_of_mass:
        center_of_mass_axes = []
        l2_center_of_mass_axis = fig.add_subplot(gs[0, col_i])
        center_of_mass_axes.append(l2_center_of_mass_axis)
        l1_center_of_mass_axis = fig.add_subplot(gs[1, col_i])
        center_of_mass_axes.append(l1_center_of_mass_axis)
        l3_center_of_mass_axis = fig.add_subplot(gs[2, col_i])
        center_of_mass_axes.append(l3_center_of_mass_axis)
        col_i += 1

    # invisible axis
    axes_invisible = fig.add_subplot(gs[:, :], frame_on=False)
    axes_invisible.set_xticks([])
    axes_invisible.set_yticks([])

    ###########
    # Plot info
    for f in f_range:
        inverted_q_range = 1 / q_range
        # Center of donor
        if include_center_of_donor:
            ## L1
            l1_center_of_donor_axis.plot(
                inverted_q_range,
                lagrange_point_dict[f]["center_of_donor"]["L1"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )

            l2_center_of_donor_axis.plot(
                inverted_q_range,
                lagrange_point_dict[f]["center_of_donor"]["L2"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )
            l3_center_of_donor_axis.plot(
                inverted_q_range,
                lagrange_point_dict[f]["center_of_donor"]["L3"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )

        # Center of mass
        if include_center_of_mass:
            l1_center_of_mass_axis.plot(
                q_range,
                lagrange_point_dict[f]["center_of_mass"]["L1"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )
            l2_center_of_mass_axis.plot(
                q_range,
                lagrange_point_dict[f]["center_of_mass"]["L2"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )
            l3_center_of_mass_axis.plot(
                q_range,
                lagrange_point_dict[f]["center_of_mass"]["L3"][:, 0],
                color=lagrange_point_dict[f]["color"],
                linewidth=linewidth,
            )

    #
    if plot_settings.get("add_panel_label", True):
        panel_i = 0
        fontsize = plot_settings.get("panel_label_fontsize", 36)

        # Center of donor
        if include_center_of_donor:
            # L1
            fig, l1_center_of_donor_axis = add_label_subplot(
                fig=fig,
                ax=l1_center_of_donor_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.5,
                fontsize=fontsize,
                verticalalignment="center",
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1
            # L2
            fig, l2_center_of_donor_axis = add_label_subplot(
                fig=fig,
                ax=l2_center_of_donor_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.90,
                fontsize=fontsize,
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1
            # L3
            fig, l3_center_of_donor_axis = add_label_subplot(
                fig=fig,
                ax=l3_center_of_donor_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.90,
                fontsize=fontsize,
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1

        # Center of mass
        if include_center_of_mass:
            # L1
            fig, l1_center_of_mass_axis = add_label_subplot(
                fig=fig,
                ax=l1_center_of_mass_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.5,
                fontsize=fontsize,
                verticalalignment="center",
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1
            # L2
            fig, l2_center_of_mass_axis = add_label_subplot(
                fig=fig,
                ax=l2_center_of_mass_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.90,
                fontsize=fontsize,
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1
            # L3
            fig, l3_center_of_mass_axis = add_label_subplot(
                fig=fig,
                ax=l3_center_of_mass_axis,
                label_i=panel_i,
                x_loc=0.05,
                y_loc=0.90,
                fontsize=fontsize,
                bbox_dict={"pad": 0.5, "facecolor": "wheat", "boxstyle": "round"},
            )
            panel_i += 1

    ###########
    # Makeup

    # Set scales
    if include_center_of_donor:
        for axis in center_of_donor_axes:
            axis.set_xscale("log")
    if include_center_of_mass:
        for axis in center_of_mass_axes:
            axis.set_xscale("log")

    # Align axes
    if include_center_of_donor and include_center_of_mass:
        align_axes(fig=fig, axes_list=center_of_donor_axes, which_axis="x")
        align_axes(fig=fig, axes_list=center_of_mass_axes, which_axis="x")

        # Align y-axes of each
        for i in range(3):
            align_axes(
                fig=fig,
                axes_list=[center_of_donor_axes[i], center_of_mass_axes[i]],
                which_axis="y",
            )

    # Remove xtick labels
    if include_center_of_donor:
        for axis in center_of_donor_axes[:-1]:
            axis.set_xticklabels([])
    if include_center_of_mass:
        for axis in center_of_mass_axes[:-1]:
            axis.set_xticklabels([])

    # Add titles and labels
    if include_center_of_donor:
        center_of_donor_axes[0].set_title(
            "Centre of donor frame", fontsize=labelfontsize
        )
    if include_center_of_mass:
        center_of_mass_axes[0].set_title("Centre of mass frame", fontsize=labelfontsize)

    if include_center_of_donor:
        left_axes = center_of_donor_axes
    else:
        left_axes = center_of_mass_axes

    left_axes[0].set_ylabel(r"L2$_{x}$", fontsize=labelfontsize)
    left_axes[1].set_ylabel(r"L1$_{x}$", fontsize=labelfontsize)
    left_axes[2].set_ylabel(r"L3$_{x}$", fontsize=labelfontsize)

    # axes_invisible.set_ylabel(
    #     "x-coordinate Lagrange points", labelpad=140, fontsize=labelfontsize
    # )
    axes_invisible.set_xlabel(
        r"$\it{q}_{\mathrm{acc}}$", labelpad=60, fontsize=labelfontsize
    )

    ####
    # Add custom legend
    lines = [
        Line2D(
            [0],
            [0],
            label=r"$\it{{f}}_{{\mathrm{{sync}}}} = {}$".format(f),
            color=cmaplist[f_i],
        )
        for f_i, f in enumerate(f_range)
    ]

    plt.legend(
        handles=lines,
        bbox_to_anchor=(0.5, 1.1),
        loc="center",
        fontsize="x-large",
        frameon=True,
        ncol=3,
    )

    # Align y-labels
    fig.align_ylabels(left_axes)

    #
    fig.tight_layout()

    #
    show_and_save_plot(fig, plot_settings)


def handle_paper_plot_lagrange_point_overview_frames_of_reference(
    output_name, show_plot
):
    """
    Function to plot the lagrange points for different frames of references for differnet synchronicities
    """

    include_center_of_donor = True
    include_center_of_mass = True

    q_range = 10 ** np.arange(-2, 2, 0.1)
    f_range = [0.5, 1, 2]

    plot_lagrange_point_overview_frames_of_reference(
        q_range=q_range,
        f_range=f_range,
        include_center_of_donor=include_center_of_donor,
        include_center_of_mass=include_center_of_mass,
        plot_settings={
            "star_text_color": "white",
            "show_plot": show_plot,
            "output_name": output_name,
        },
    )


if __name__ == "__main__":
    #######
    # Config
    show_plot = False

    include_center_of_donor = True
    include_center_of_mass = True

    q_range = 10 ** np.arange(-2, 2, 0.1)
    f_range = [0.5, 1, 2]

    # Output dir
    output_dir = "plots/"
    output_dir = os.path.join(
        os.getenv("PAPER_ROOT"), "paper_ballistic/paper_tex/figures"
    )

    # basename
    basename = "lagrange_points_FOR.pdf"

    plot_lagrange_point_overview_frames_of_reference(
        q_range=q_range,
        f_range=f_range,
        include_center_of_donor=include_center_of_donor,
        include_center_of_mass=include_center_of_mass,
        plot_settings={
            "star_text_color": "white",
            "show_plot": show_plot,
            "output_name": os.path.join(output_dir, basename),
        },
    )

    # #################
    # #
    # include_center_of_donor = True
    # include_center_of_mass = False

    # q_range = 10 ** np.arange(-9, 9, 0.5)
    # f_range = [np.sqrt(0.64), 1, np.sqrt(5)]

    # # Output dir
    # output_dir = "plots/"

    # # basename
    # basename = "lagrange_points_FOR_testing.pdf"

    # plot_lagrange_point_overview_frames_of_reference(
    #     q_range=q_range,
    #     f_range=f_range,
    #     include_center_of_donor=include_center_of_donor,
    #     include_center_of_mass=include_center_of_mass,
    #     plot_settings={
    #         "star_text_color": "white",
    #         "show_plot": show_plot,
    #         "output_name": os.path.join(output_dir, basename),
    #     },
    # )
