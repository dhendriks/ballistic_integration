"""
Functions to plot the rochelobe radius as a function of q and fsync
"""

import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from calculate_derivative import (
    add_derivative_to_df,
    create_roche_lobe_derivative_interpolator_dict,
    load_df,
)
from comparison_zeta_L import logarithmic_derivative_scale_factor_roche_lobe
from david_phd_functions.plotting import custom_mpl_settings
from david_phd_functions.plotting.utils import (
    delete_out_of_frame_text,
    show_and_save_plot,
)
from utils import call_interpolator

custom_mpl_settings.load_mpl_rc()

matplotlib.rcParams["savefig.dpi"] = 300


##############
#

this_file = os.path.abspath(__file__)
this_file_dir = os.path.dirname(this_file)
plot_output_dir = os.path.join(this_file_dir, "plots/RL_plots/")


#
filename = "results/test/RLOF_Hendriks2023_roche_lobe_interpolation_table.csv"


#
df = load_df(filename=filename)
df = add_derivative_to_df(df=df)

#
interpolator_dict = create_roche_lobe_derivative_interpolator_dict(df=df)


def plot_derivative_relative_to_synchronous(plot_settings):
    """
    Function to calculate the derivative d(RL/a)/dq as function of synchronicity and compare it to the synchronous case
    """

    f_sync_step = 0.1
    f_sync_arr = np.arange(f_sync_step, 2, f_sync_step)

    log10_q_acc_don_step = 0.025
    q_acc_don_arr = 10 ** np.arange(-2, 2 + log10_q_acc_don_step, log10_q_acc_don_step)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    ax_derivative_relative = fig.add_subplot(gs[0, 0])

    cmap = plt.cm.get_cmap("viridis", len(f_sync_arr))

    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    for i, unique_fsync in enumerate(f_sync_arr):
        scale_factor_arr = []
        for q_acc_don in q_acc_don_arr:
            scale_factor = logarithmic_derivative_scale_factor_roche_lobe(
                q_acc_don=q_acc_don, fsync=unique_fsync
            )
            scale_factor_arr.append(scale_factor)
        scale_factor_arr = np.array(scale_factor_arr)
        label = r"$f_{{sync}}$ {:.1f}".format(unique_fsync)

        ax_derivative_relative.plot(
            q_acc_don_arr, scale_factor_arr, label=label, color=cmaplist[i]
        )

    fontsize = 40
    ax_derivative_relative.set_xscale("log")
    ax_derivative_relative.set_title(
        "derivative relative to sync", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.set_xlabel(
        r"Mass ratio $q_{\mathrm{acc}}$", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.legend(ncol=2, loc=4, fontsize=fontsize)
    ax_derivative_relative.set_ylabel(
        "logarithmic derivative relative to sync", fontsize=fontsize * 1.5
    )

    fig.subplots_adjust(hspace=0.2)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_radius_relative_to_synchronous(plot_settings):
    """
    Function to calculate the derivative d(RL/a)/dq as function of synchronicity and compare it to the synchronous case
    """

    roche_lobe_radius_interpolator = interpolator_dict["roche_lobe_radius_interpolator"]

    f_sync_step = 0.1
    f_sync_arr = np.arange(f_sync_step, 2, f_sync_step)

    log10_q_acc_don_step = 0.025
    q_acc_don_arr = 10 ** np.arange(-2, 2 + log10_q_acc_don_step, log10_q_acc_don_step)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    ax_derivative_relative = fig.add_subplot(gs[0, 0])

    cmap = plt.cm.get_cmap("viridis", len(f_sync_arr))

    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    for i, unique_fsync in enumerate(f_sync_arr):
        scale_factor_arr = []
        for q_acc_don in q_acc_don_arr:
            roche_lobe_radius = call_interpolator(
                roche_lobe_radius_interpolator, f_sync=unique_fsync, q_acc_don=q_acc_don
            )
            roche_lobe_radius_sync = call_interpolator(
                roche_lobe_radius_interpolator, f_sync=1, q_acc_don=q_acc_don
            )

            #
            radius_relative_to_sync = roche_lobe_radius / roche_lobe_radius_sync

            #
            scale_factor_arr.append(radius_relative_to_sync)
        scale_factor_arr = np.array(scale_factor_arr)
        label = r"$f_{{sync}}$ {:.1f}".format(unique_fsync)

        ax_derivative_relative.plot(
            q_acc_don_arr, scale_factor_arr, label=label, color=cmaplist[i]
        )

    fontsize = 40
    ax_derivative_relative.set_xscale("log")
    ax_derivative_relative.set_title(
        "Roche-lobe radius relative to synchronous", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.set_xlabel(
        r"Mass ratio $q_{\mathrm{acc}}$", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.legend(ncol=3, loc=4, fontsize=fontsize * 0.75)

    ax_derivative_relative.set_ylabel(
        r"$R_{\mathrm{RL}}(q, f)/R_{\mathrm{RL}}(q, f=1)$", fontsize=fontsize * 1.5
    )

    ax_derivative_relative.set_ylim([0.65, 1.2])

    fig.subplots_adjust(hspace=0.2)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


def plot_radius(plot_settings):
    """
    Function to calculate the derivative d(RL/a)/dq as function of synchronicity and compare it to the synchronous case
    """

    roche_lobe_radius_interpolator = interpolator_dict["roche_lobe_radius_interpolator"]

    f_sync_step = 0.1
    f_sync_arr = np.arange(f_sync_step, 2, f_sync_step)

    log10_q_acc_don_step = 0.025
    q_acc_don_arr = 10 ** np.arange(-2, 2 + log10_q_acc_don_step, log10_q_acc_don_step)

    ##################
    # Set up figure logic
    fig = plt.figure(figsize=(20, 20))
    gs = fig.add_gridspec(nrows=1, ncols=1)
    ax_derivative_relative = fig.add_subplot(gs[0, 0])

    cmap = plt.cm.get_cmap("viridis", len(f_sync_arr))

    cmaplist = [cmap(i) for i in range(cmap.N)]

    #
    for i, unique_fsync in enumerate(f_sync_arr):

        scale_factor_arr = []

        for q_acc_don in q_acc_don_arr:

            roche_lobe_radius = call_interpolator(
                roche_lobe_radius_interpolator, f_sync=unique_fsync, q_acc_don=q_acc_don
            )

            scale_factor_arr.append(roche_lobe_radius)
        scale_factor_arr = np.array(scale_factor_arr)
        label = r"$f_{{sync}}$ {:.1f}".format(unique_fsync)

        ax_derivative_relative.plot(
            q_acc_don_arr, scale_factor_arr, label=label, color=cmaplist[i]
        )

    fontsize = 40
    ax_derivative_relative.set_xscale("log")
    ax_derivative_relative.set_title(r"Roche-lobe radius", fontsize=fontsize * 1.5)
    ax_derivative_relative.set_xlabel(
        r"Mass ratio $q_{\mathrm{acc}}$", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.set_ylabel(
        r"$R_{\mathrm{RL}}(q, f)$", fontsize=fontsize * 1.5
    )
    ax_derivative_relative.legend(ncol=2, loc=1, fontsize=fontsize)

    fig.subplots_adjust(hspace=0.2)

    #
    fig.tight_layout()

    # Add info and plot the figure
    show_and_save_plot(fig, plot_settings)


if __name__ == "__main__":
    ############
    #
    plot_derivative_relative_to_synchronous(
        plot_settings={
            "output_name": os.path.join(
                plot_output_dir, "derivative_radius_relative_to_sync.pdf"
            )
        }
    )

    ############
    #
    plot_radius_relative_to_synchronous(
        plot_settings={
            "output_name": os.path.join(plot_output_dir, "radius_relative_to_sync.pdf")
        }
    )

    ############
    #
    plot_radius(
        plot_settings={"output_name": os.path.join(plot_output_dir, "radius.pdf")}
    )
