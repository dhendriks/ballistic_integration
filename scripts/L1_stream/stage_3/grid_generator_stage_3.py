"""
Script containing the function to generate the grid of trajectory settings
"""

import copy
import itertools

from ballistic_integration_code.settings import standard_system_dict


def grid_generator_stage_3(settings):
    """
    Generator function to generate the trajectory settings for the ballistic stream integrator
    """

    # Set the parameter ranges
    # log10normalized_stream_area_range = settings["log10normalized_stream_area_range"]
    log10normalized_thermal_velocity_dispersion_range = settings[
        "log10normalized_thermal_velocity_dispersion_range"
    ]
    q_range = settings["q_range"]
    f_range = settings["f_range"]

    # Loop over parameters and yield the settings
    for parameters in itertools.product(
        # log10normalized_stream_area_range,
        log10normalized_thermal_velocity_dispersion_range,
        f_range,
        q_range,
    ):
        # Unpack
        # log10normalized_stream_area = parameters[0]
        log10normalized_thermal_velocity_dispersion = parameters[0]
        synchronicity_factor = parameters[1]
        massratio_accretor_donor = parameters[2]

        # Set system dict
        settings["system_dict"] = {
            **standard_system_dict,
            "synchronicity_factor": synchronicity_factor,
            "mass_accretor": massratio_accretor_donor
            * standard_system_dict["mass_donor"],
        }

        # Set grid point
        settings["grid_point"] = {
            "massratio_accretor_donor": massratio_accretor_donor,
            "synchronicity_factor": synchronicity_factor,
            "log10normalized_thermal_velocity_dispersion": log10normalized_thermal_velocity_dispersion,
            # "log10normalized_stream_area": log10normalized_stream_area,
        }

        # Copy the settings
        returned_settings = copy.deepcopy(settings)

        yield returned_settings
